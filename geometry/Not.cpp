//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Not.hpp>

std::ostream& Not::
__put(std::ostream& os) const
{
  os << "not {\n";
  os << (*__object);
  for (size_t i=0; i<numberOfTransformations(); i++)
    os << __trans[i]->povWrite() << '\n';
  os << "}\n";

  return os;
}

ReferenceCounting<Shape> Not::
__getCopy() const
{
  return new Not(*this);
}
