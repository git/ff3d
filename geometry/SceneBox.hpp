//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$



#ifndef _SCENEBOX_HPP_
#define _SCENEBOX_HPP_

#include <TinyVector.hpp>

/**
 * @file   SceneBox.hpp
 * @author St�phane Del Pino
 * @date   Fri Jan  3 10:53:24 2003
 * 
 * @brief  transparent box
 * 
 * This class stored a special transparent box coming from the \p
 * POV-Ray Virtual Reality description. It has the function to help
 * the user to verify that the Structured3DMesh used for computation
 * corresponds to the computationnal domain defined in VR.
 */

class SceneBox
{
private:
  //! Tells if the SceneBox has been initialized.
  bool initialized;

  //! The first corner of the box.
  TinyVector<3, real_t> a;

  //! The second corner of the box.
  TinyVector<3, real_t> b;

public:
  //! Read-only access to \a a.
  const TinyVector<3,real_t>& A() const
  {
    return a;
  }

  //! Read-only access to \a b.
  const TinyVector<3,real_t>& B() const
  {
    return b;
  }

  //! Read-only access to \a initialized.
  bool Initialized() const
  {
    return initialized;
  }

  //! Copies the SceneBox \a SB.
  SceneBox& operator=(const SceneBox& SB)
  {
    initialized = SB.initialized;
    a = SB.A();
    b = SB.B();
    return *this;
  }


  //! Default constructor leads to an empty box not initialized.
  SceneBox()
    : initialized(false),
      a(0),
      b(0)
  {
    ;
  }

  //! Copy constructor.
  SceneBox(const SceneBox& SB)
    : initialized(SB.initialized),
      a(SB.a),
      b(SB.b)
  {
    ;
  }

  //! Constructs the SceneBox whose corners are \a aa ane \a bb.
  SceneBox(const TinyVector<3>& aa, const TinyVector<3>& bb)
  : initialized(true)
  {
    // ordering TinyVector<3> coordinates.
    if (aa[0]>bb[0]) {
      a[0] = bb[0];
      b[0] = aa[0];
    } else {
      a[0] = aa[0];
      b[0] = bb[0];
    }

    if (aa[1]>bb[1]) {
      a[1] = bb[1];
      b[1] = aa[1];
    } else {
      a[1] = aa[1];
      b[1] = bb[1];
    }

    if (aa[2]>bb[2]) {
      a[2] = bb[2];
      b[2] = aa[2];
    } else {
      a[2] = aa[2];
      b[2] = bb[2];
    }

  }
};

#endif  // _SCENEBOX_HPP_

