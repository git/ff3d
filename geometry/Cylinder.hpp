//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef CYLINDER_HPP
#define CYLINDER_HPP

#include <Shape.hpp>

/**
 * @file   Cylinder.hpp
 * @author Stephane Del Pino
 * @date   Sat Feb  7 17:13:00 2004
 * 
 * @brief  This is the class which defines POV-Ray's cylinder.
 */
class Cylinder
  : public Shape
{
private:
  friend class InfiniteCylinder;
  friend class Plane;

  const TinyVector<3, real_t> __c1; /**< center of the upper face */
  const TinyVector<3, real_t> __c2; /**< center of the lower face */

  const TinyVector<3, real_t>
  __center;			/**< usefull vertex to determine the
				   space inside the Cylinder */

  const real_t __radius;	/**< radius of the Cylinder */

  const real_t __radius2;	/**< square of the radius of the
				   Cylinder (since it is usefull very
				   often, it is a good idea to store
				   it). */

  const real_t __half_height;	/**< Half of the height of the
				   cylinder. Usefull to detect the
				   space inside the Cylinder */

  const TinyVector<3, real_t>
  __unaryVector;		/**< unary vector along the @a c1
				   @a c2 axis. */

protected:
  /** 
   * checks if a point is inside the Cylinder
   * 
   * @param x the point to check
   * 
   * @return true if @a x is insde the Cylinder
   */
  inline bool __inShape(const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the Cylinder to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * Get a copy of the Cylinder
   * 
   * @return deep copy of the cylinder
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Constructor
   * 
   * @param a first face center
   * @param b second face center
   * @param r radius
   */
  Cylinder(const TinyVector<3, real_t>& a,
	   const TinyVector<3, real_t>& b,
	   const real_t& r);

  /** 
   * Copy constructor
   * 
   * @param C given Cylinder
   */
  Cylinder(const Cylinder& C);

  /** 
   * Destructor
   * 
   */
  ~Cylinder()
  {
    ;
  }
};

#endif // CYLINDER_HPP
