//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef NOT_HPP
#define NOT_HPP

#include <Shape.hpp>
#include <Object.hpp>

/**
 * @file   Not.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 16:26:14 2006
 * 
 * @brief  not set operation
 */
class Not
  : public Shape
{
private:
  ReferenceCounting<Object>
  __object;			/**< The Shape on which the \p not is applied */

protected:
  /** 
   * Checks if a point is in the shape
   * 
   * @param x the point to check
   * 
   * @return true if @f$ x \in S @f$
   */
  bool __inShape (const TinyVector<3, real_t>& x) const
  {
    return not(__object->inside(x));
  }

  /** 
   * Prints the Union informations to a stream 
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * Gets a copy of the Not
   * 
   * @return deep copy of the Not
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Read-only access to the object defining the Not
   * 
   * @return deep copy of the Not
   */
  ConstReferenceCounting<Object> object() const
  {
    return __object;
  }

  /** 
   * Constructor
   * 
   * @param object given object
   */
  Not(ReferenceCounting<Object> object)
    : Shape(not_),
      __object(object)
  {
    ;
  }

  /** 
   * Copy conctructor
   * 
   * @param N given @p Not
   */
  Not(const Not& N)
    : Shape(N),
      __object(N.__object->getCopy())
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Not()
  {
    ;
  }
};

#endif // _NOT_HPP_

