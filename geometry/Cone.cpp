//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Cone.hpp>

bool Cone::
__inShape(const TinyVector<3, real_t>& x) const
{
  TinyVector<3> c1x = x - __center1;

  const real_t alpha = __axisVector * c1x / __height;

  if (alpha<0)
    return false;
  if (alpha>__height)
    return false;

  const real_t h2 = c1x*c1x - alpha*alpha;

  return (h2<std::pow(alpha/__height*__radius2 + (1-alpha/__height)*__radius1, 2));
}

Cone::
Cone(const Vertex& a,
     const Vertex& b,
     const real_t& r1,
     const real_t& r2)
  : Shape(cone),
    __center1(a),
    __center2(b),
    __axisVector(b-a),
    __height(Norm(__axisVector)),
    __radius1(r1),
    __radius2(r2)
{
  ;
}

Cone::
Cone(const Cone& C)
  : Shape(C),
    __center1(C.__center1),
    __center2(C.__center2),
    __axisVector(C.__axisVector),
    __height(C.__height),
    __radius1(C.__radius1),
    __radius2(C.__radius2)
{
  ;
}

std::ostream& Cone::
__put(std::ostream& s) const
{
  s << "cone {\n" << __center1 << ',' << __radius1
    << ',' << __center2 << ',' << __radius2 << "\n}\n";
  return s;
}

ReferenceCounting<Shape> Cone::
__getCopy() const
{
  return new Cone(*this);
}
