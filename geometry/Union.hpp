//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef UNION_HPP
#define UNION_HPP

#include <Object.hpp>
#include <list>

/**
 * @file   Union.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 16:15:06 2006
 * 
 * @brief This is the class which defines a Virtual Reality set
 * operation: union
 */
class Union
  : public Shape
{
private:
  typedef std::list<ConstReferenceCounting<Object> > ObjectList;

public:
  typedef std::list<ConstReferenceCounting<Object> >::iterator iterator;
  typedef std::list<ConstReferenceCounting<Object> >::const_iterator const_iterator;

private:
  ObjectList __objects;		/**< The list of Shapes contained in the union */

protected:
  /** 
   *  Returns @p true if the point @a x is inside the Union
   * 
   * @param x point to check
   * 
   * @return true if @f$ x \in S @f$
   */
  inline bool __inShape (const TinyVector<3, real_t>& x) const
  {
    for (Union::const_iterator i=__objects.begin();
	 i != __objects.end(); ++i) {
      if((*i)->inside(x)) {
	return true;
      }
    }
    return false;
  }

  /** 
   * prints the union to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * Gets a copy of the Union
   * 
   * @return deep copy of the Union
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  const_iterator begin() const
  {
    return __objects.begin();
  }

  iterator begin()
  {
    return __objects.begin();
  }

  const_iterator end() const
  {
    return __objects.end();
  }

  iterator end()
  {
    return __objects.end();
  }

  /** 
   *  Adds an object to the Union
   * 
   * @param O added object
   */
  void push_back(ConstReferenceCounting<Object> O)
  {
    __objects.push_back(O);
  }

  /** 
   * Constructor
   * 
   */
  Union();

  /** 
   * Copy constructor
   * 
   * @param U given union
   */
  Union(const Union& U);

  /** 
   * Destructor
   * 
   */
  ~Union()
  {
    ;
  }
};

#endif // UNION_HPP
