//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// This class is defined to allow the use of Objects. Objects are
// made of Shapes which have caracteristics like Boundary Conditions, ...

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <Shape.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   Object.hpp
 * @author Stephane Del Pino
 * @date   Sun Aug 31 15:08:43 2003
 * 
 * @brief Describes objects which are made of a shape and maybe a
 * reference
 */
class Object
{
private:
  ConstReferenceCounting<Shape>
  __shape;			/**< the shape */

  bool __hasReference;		/**< true if the object has a reference */

  TinyVector<3,real_t>
  __reference;			/**< the POV-Ray reference of the object */

public:
  /** 
   * Writes the object information in a given stream
   * 
   * @param os the given stream
   * @param o the object to write
   * 
   * @return the modified stream
   */
  friend std::ostream& operator << (std::ostream& os,
				    const Object& o)
  {
    os << (*o.__shape) << '\n';
    return os;
  }

  /** 
   * returns the reference state of the object
   * 
   * @return true if the object has a defined reference
   */
  const bool& hasReference() const
  {
    return __hasReference;
  }

  /** 
   * Access to the reference of the object
   * 
   * @return the reference
   */
  const TinyVector<3>& reference() const
  {
    ASSERT(__hasReference);
    return __reference;
  }

  /** 
   * Read only access to the shape of the object
   * 
   * @return the shape
   */
  ConstReferenceCounting<Shape> shape() const
  {
    ASSERT(__shape != 0);
    return __shape;
  }

  /** 
   * Computes if a vertex is inside the object
   * 
   * @param x the given vertex
   * 
   * @return true if \a x is in the shape
   */
  bool inside(const TinyVector<3, real_t>& x) const
  {
    return __shape->inside(x);
  }

  /** 
   * Sets the reference object
   * 
   * @param aReference the given reference
   */
  void setReference(const TinyVector<3, real_t>& aReference)
  {
    ASSERT(__hasReference == false);
    __hasReference = true;
    __reference = aReference;
  }

  /** 
   * Gets a copy of the Object
   * 
   * @return deep copy of the Object
   */
  ReferenceCounting<Object> getCopy() const
  {
    return new Object(*this);
  }

  /** 
   * Constructs an object using a given shape
   * 
   * @param s the given shape
   */
  Object(ConstReferenceCounting<Shape> s)
    : __shape(s),
      __hasReference(false),
      __reference(0)
  {
    ;
  }

  /** 
   * Constructs an object using another object
   * 
   * @param o a given object
   */
  Object(const Object& o)
    : __shape(o.__shape->getCopy()),
      __hasReference(o.__hasReference),
      __reference(o.__reference)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Object()
  {
    ;
  }
};

#endif // OBJECT_HPP
