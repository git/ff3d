//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef PLANE_HPP
#define PLANE_HPP

#include <Shape.hpp>
#include <Index.hpp>

class Cube;
class Cylinder;
class Cone;

/**
 * @file   Plane.hpp
 * @author Stephane Del Pino
 * @date   Sat Feb  7 21:30:37 2004
 * 
 * @brief This is the class which defines a Virtual Reality Plane.
 */
class Plane
  : public Shape
{
private:
  TinyVector<3, real_t>
  __normal;			/**< normal to the plane */

  real_t __distance;	/**< distance to the origine */

  TinyVector<3, real_t>
  __unitNormal;			/**< unit normal to the plane */

  TinyVector<3, real_t>
  __origine;			/**< normal projection of the
				   origine */

protected:
  /** 
   * Checks if a point is inside the shape
   * 
   * @param x point to check
   * 
   * @return true if @f$ x\in S @f$
   */
  bool __inShape(const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the Plane to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * gets a copy of the Plane
   * 
   * @return deep copy of the Plane
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Constructor
   * 
   * @param normal normal to the plane
   * @param distance distance to the origine
   */
  Plane(const Vertex& normal,
	const real_t& distance);

  /** 
   * Constructs the plane corresponding to a given face number of a
   * cube
   * 
   * @param c the given cube
   * @param faceNumber the face number
   * 
   */
  Plane(const Cube& c, const size_t& faceNumber);

  /** 
   * Constructs the plane corresponding to a given face number of a
   * cylinder
   * 
   * @param c the given cylinder
   * @param faceNumber the face number
   * 
   */
  Plane(const Cylinder& c, const size_t& faceNumber);

  /** 
   * Constructs the plane corresponding to a given face number of a
   * cone
   * 
   * @param c the given cone
   * @param faceNumber the face number
   * 
   */
  Plane(const Cone& c, const size_t& faceNumber);

  /** 
   * Copy constructor
   * 
   * @param P given plane
   */
  Plane(const Plane& P);

  /** 
   * Destructor
   * 
   */
  ~Plane()
  {
    ;
  }
};

#endif // PLANE_HPP
