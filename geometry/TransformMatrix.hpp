//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef TRANSFORM_MATRIX_HPP
#define TRANSFORM_MATRIX_HPP

#include <Transform.hpp>
#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

/**
 * @file   TransformMatrix.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 16:13:20 2006
 * 
 * @brief This class defines a 3x4 matrix using the same description
 * that @p POV-Ray
 */
class TransformMatrix
  : public Transform
{
private:
  TinyVector<3> __translate;	/**< TransformMatrix translation as
				   given in POV-Ray */

  TinyMatrix<3,3> __matrix;	/**< Matrix transformatino */

  TinyMatrix<3,3> __invMatrix;	/**< The inverse of the
				   transformMatrix is stored in the
				   matrix. This way one can know if a
				   vertex is the image of a vertex
				   contained in a base shape; so, if a
				   vertex is in the image of shape! */

public:

  /** 
   *  Applies the TransformMatrix to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ Ax+b @f$
   */
  TinyVector<3,real_t> operator()(const TinyVector<3,real_t>& x) const;

  /** 
   *  Applies the inverse TransformMatrix to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ A^{-1}(x-b) @f$
   */
  TinyVector<3,real_t> inverse(const TinyVector<3,real_t>& x) const;

  /** 
   * Writes the TransformMatrix to a string
   * 
   * @return POVRay string
   */
  std::string povWrite() const;

  /** 
   * Copies the TransformMatrix
   * 
   * @return deep copy of the TransformMatrix
   */
  ReferenceCounting<Transform> getCopy() const;

  //! Constructs a TransformMatrix for a set of given angles passed by \a r.
  /** 
   * Constructor
   * 
   * @param r transformation matrix and vector
   */
  TransformMatrix(const real_t r[12]);

  /** 
   * Copy constructor
   * 
   * @param r given TransformMatrix
   */
  TransformMatrix(const TransformMatrix& r);

  /** 
   * Destructor
   * 
   */
  ~TransformMatrix()
  {
    ;
  }
};

#endif // TRANSFORM_MATRIX_HPP
