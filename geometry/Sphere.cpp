//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Sphere.hpp>

#include <cmath>
#include <fstream>

Sphere::
Sphere(const Vertex& c,
       const real_t& r)
  : Shape(sphere),
    __center(c),
    __radius(r),
    __radius2(r*r)
{
  ;
}

Sphere::
Sphere(const Sphere& S)
  : Shape(S),
    __center (S.__center),
    __radius (S.__radius),
    __radius2(S.__radius2)
{
  ;
}

std::ostream& Sphere::
__put(std::ostream& os) const
{
  os << "sphere {\n" << __center
    << ", " << __radius << '\n';
  for (size_t i=0; i<numberOfTransformations(); i++)
    os << (*__trans[i]).povWrite() << '\n';
  os << "}\n";
  return os;
}

ReferenceCounting<Shape> Sphere::
__getCopy() const
{
  return new Sphere(*this);
}
