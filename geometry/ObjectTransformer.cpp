//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003, 2004 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ObjectTransformer.hpp>
#include <Object.hpp>

#include <Sphere.hpp>
#include <Cylinder.hpp>
#include <Cone.hpp>
#include <Cube.hpp>
#include <Plane.hpp>
#include <Torus.hpp>

#include <Union.hpp>
#include <Difference.hpp>
#include <Intersection.hpp>
#include <Not.hpp>

#include <InfiniteCylinder.hpp>
#include <InfiniteCone.hpp>

ReferenceCounting<Object>
ObjectTransformer::operator()(const Object& o) const
{
  const Shape& s = *o.shape();
  ReferenceCounting<Shape> newShape = o.shape()->getCopy();

  Shape::TransformationsList transformations(__addedTransformations.size()
					     + s.transformationsList().size());

  for (size_t i=0; i<s.transformationsList().size(); ++i) {
    transformations[i+__addedTransformations.size()]
      = s.transformationsList()[i];
  }
  for (size_t i=0; i<__addedTransformations.size(); ++i) {
    transformations[i] = __addedTransformations[i];
  }
  ASSERT(newShape != 0);

  newShape->setTransformationsList(transformations);
  Object* newObject = new Object(newShape);

  // Copies references if needed
  if (o.hasReference()) {
    newObject->setReference(o.reference());
  }

  return newObject;
}
