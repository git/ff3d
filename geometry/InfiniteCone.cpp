//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <InfiniteCone.hpp>
#include <Cone.hpp>

InfiniteCone::
InfiniteCone(const Cone& c)
  : Shape(Shape::infiniteCone),
    __center1(c.__center1),
    __center2(c.__center2),
    __axisVector(c.__axisVector),
    __height(c.__height),
    __radius1(c.__radius1),
    __radius2(c.__radius2)
{
  this->setTransformationsList(c.transformationsList());
}

InfiniteCone::
InfiniteCone(const InfiniteCone& c)
  : Shape(c),
    __center1(c.__center1),
    __center2(c.__center2),
    __axisVector(c.__axisVector),
    __height(c.__height),
    __radius1(c.__radius1),
    __radius2(c.__radius2)
{
  ;
}

bool InfiniteCone::
__inShape(const TinyVector<3, real_t>& x) const
{
  TinyVector<3,real_t> c1x = x - __center1;
  const real_t alpha = __axisVector*c1x / __height;
  const real_t h2 = c1x*c1x - alpha*alpha;

  return (h2<std::pow(alpha/__height*__radius2 + (1-alpha/__height)*__radius1, 2));
}

ReferenceCounting<Shape> InfiniteCone::
__getCopy() const
{
  return new InfiniteCone(*this);
}
