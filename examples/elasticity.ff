// -*- c++ -*-

//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003  Laboratoire J.-L. Lions UPMC Paris

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// Linearized elasticity. A beam under it own weight

vector n = (50,10,10);
vector a = (0,-0.5,-0.5);
vector b = (5, 0.5, 0.5);

mesh M = structured(n,a,b);

double L = 1000; // Lambda
double Mu = 500;

// No domain is needed here, we use standard FEM
solve (u1,u2,u3) in M
  krylov(precond=ichol) // elasticity has bad condition number
			// using incomplete Choleski preconditionning
{
  // Use of variational formula is necessary to impose correct
  // constrainst free boundary conditions.

  test(v1,v2,v3)
   int(Mu*dx(u1)*dx(v1))+int(Mu*dx(u2)*dx(v2))+int(Mu*dx(u3)*dx(v3))
  +int(Mu*dy(u1)*dy(v1))+int(Mu*dy(u2)*dy(v2))+int(Mu*dy(u3)*dy(v3))
  +int(Mu*dz(u1)*dz(v1))+int(Mu*dz(u2)*dz(v2))+int(Mu*dz(u3)*dz(v3))

  +int(Mu*dx(u1)*dx(v1))+int(Mu*dx(u2)*dy(v1))+int(Mu*dx(u3)*dz(v1))
  +int(Mu*dy(u1)*dx(v2))+int(Mu*dy(u2)*dy(v2))+int(Mu*dy(u3)*dz(v2))
  +int(Mu*dz(u1)*dx(v3))+int(Mu*dz(u2)*dy(v3))+int(Mu*dz(u3)*dz(v3))
  
  +int(L*dx(u1)*dx(v1))+int(L*dx(u1)*dy(v2))+int(L*dx(u1)*dz(v3))
  +int(L*dy(u2)*dx(v1))+int(L*dy(u2)*dy(v2))+int(L*dy(u2)*dz(v3))
  +int(L*dz(u3)*dx(v1))+int(L*dz(u3)*dy(v2))+int(L*dz(u3)*dz(v3))

  = int(-v2); // gravity

  u1=0 on M xmin;
  u2=0 on M xmin;
  u3=0 on M xmin;
};

// Saving results component by component

save(medit,"elasticity-u1",M);
save(medit,"elasticity-u1",u1,M);

save(medit,"elasticity-u2",M);
save(medit,"elasticity-u2",u2,M);

save(medit,"elasticity-u3",M);
save(medit,"elasticity-u3",u3,M);
