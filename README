	FreeFEM3D (aka ff3d) is a 3d general solver


0) Contents
   ========

0) Contents
1) Licence issue and Warranty
2) Description
3) Documentation
4) Getting the sources
  a) cvs repository
  b) tarballs
5) Getting binaries
6) Compiling the code
  a) Generalities
  b) Mac OS-X
  c) MS-Windows
7) More informations
  a) Mailing lists
  b) contact us



1) License issue and Warranty
   ==========================

  Copyright (C) 2001-2007 St�phane Del Pino

  ff3d is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

2) Description
   ===========

  FreeFEM3D (aka ff3d) is a 3D solver of partial differential
  equations (PDE).  It is a member of the familly of the freefem
  programs (see http://www.freefem.org).

  ff3d, as well as its cousins, is a PDE solver driven by a
  user-friendly language. It solves many kind of problems such as
  elasticity, fluids (Stokes and Navier-Stokes) and a lot more. The
  user has to enter the equation associated with the problem, giving
  either the PDE in strong formulation or weak (variational)
  formulation.

  ff3d can use either the Finite Elements method (the mesh of the
  geometry being provided by the user) or a Fictitious Domain like
  approach where the geometry is described using Constructive Solid
  Geometry (CSG). This description is done using the POV-Ray language
  but others such as VRML could be added.

  The processing of the results is left to the user.  One can use
  various graphic tools: output in the MEdit mesh format or VTK are
  supported. The implementation of a VTK base visualization module is
  underway.

  The goal of ff3d is to provide a good teaching tool and a research
  toolbox (the code is written in C++ and its design is such that new
  methods can be easily implemented).

3) Documentation
   =============

  A user documentation is available at
	http://www.freefem.org/docs/freefem3dDoc.pdf
  It is written in latex. Solver options are automagically generated
  browsing the code.

  Documentation examples are given in the 'doc/doc-tests' directory.

  Developer documentation does not exist by now, but a doxygen tree is
  browsable at
	http://www.freefem.org/ff3d/doxygen/


4) Getting the sources
   ===================

  Since ff3d is licensed under GNU GPL (see paragraph 1), it sources are
  distributed. FreeFEM3D is a non-GNU project hosted by Savannah
  	http://savannah.nongnu.org
  Sources are archived using a cvs repository. 

  a) cvs repository
     -------------- 
  To download the sources using cvs, one must install cvs (see
  http://www.cvshome.org) and ssh.

  The following two commands will get the sources for you:
        export CVS_RSH=ssh
	cvs -z3 -d:ext:anoncvs@subversions.gnu.org:/cvsroot/ff3d co ff3d

  To get your source tree up to date, in your ff3d directory, just enter
        export CVS_RSH=ssh
	cvs update

  b) tarballs
     --------
  People that only need official releases, may find more convinient to
  only get tarballs. They are provided at
	http://www.freefem.org/ff3d/sources/

5) Getting binaries
   ================

  If one does not want to get into the compiling task, binairies are
  available. At the moment they are not automatically generated, so they
  may not be up to date. Check them at:
	http://www.freefem.org/ff3d/binaries.html
  GNU/Linux, Mac-OS X and MS-Windows binaries are available.


6) Compiling the sources
   =====================

  a) Generalities
     ------------

  Note that g++-4.x is required to compile ff3d!
  [ Previous versions (g++-3.x) compilers are no more supported! ]

  If you got the sources using cvs, to generate the configure script,
  you will also need:
  - autoconf (version higher than 2.50)
  - automake (version 1.9 recommended)
  - libtools

  The following packages are required
  - g++ (version >= 4)
  - bison

  The following are optional (but recommended)
  - autogen
  - vtk (version >= 4)

  The compilation procedure is the following.

  If you got the sources using *cvs* you have to generate the
  'configure' script by yourself. To do so, go to the ff3d directory and
  execute
	autoreconf -i
  This will generate the 'configure' script for you.

=> Note that the configure script is part of the tarball distribution.

  One has now to execute it giving options
	./configure --help
  will list them. Use the '--enable-debug' if you want to develop new
  functionalities.

  b) Mac OS-X
     --------

  To build sources, we recommand the usage of fink
	http://fink.sourceforge.net/
  This will help you in installing the required softwares.

  c) MS-Windows
     ----------

  One has to install cygwin to compile ff3d. Get it at
	http://cygwin.com/

7) More informations
   =================

  a) Mailing lists
     -------------

  Four mailing lists are related to ff3d. They are also hosted by
  Savannah. One can subscribe to them at
	http://savannah.nongnu.org/mail/?group=ff3d

  b) contact us
     ----------

  The best way to contact us, is to write at
	ff3d-users@nongnu.org

  If you want to ask question non specific to ff3d, one can write email
  to
	freefem@ann.jussieu.fr

-- 
  Comments are welcome.

  Project leader:
	- Olivier Pironneau <Olivier.Pironneau@math.jussieu.fr>

  Developers:
	- Jean-Baptiste Apoung Kamga Del Pino <apoung@ann.jussieu.fr>
	- St�phane Del Pino <Stephane.DelPino@math.jussieu.fr>
	- Driss Yakoubi <yakoubi@ann.jussieu.fr>

  Contributor:
	- C�cile Dobrzynski <dobrzyns@ann.jussieu.fr>
	- Pascal Hav� <Pascal.Have@math.jussieu.fr>

  Debian Packager:
	- Christophe Prud'homme <prudhomm@debian.org>
