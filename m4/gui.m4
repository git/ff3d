AC_DEFUN([AC_CHECK_GUI],
[
  AH_TEMPLATE(HAVE_GUI_LIBS,
   [Defined to 1 if GUI libraries are present on the system])

  AC_ARG_ENABLE(
    gui,
  AC_HELP_STRING([--enable-gui],[Builts ff3d with its Qt-Vtk based gui]),
  [ ac_gui=$enableval ],
  [ ac_gui="auto"])

  AC_REQUIRE([AC_CHECK_VTK])
  AC_REQUIRE([AC_CHECK_QT])
  AC_MSG_CHECKING([for ff3d's Vtk/Qt graphic interface contruction])

  if test x$FOUND_QT = xtrue && test x$FOUND_VTK = xtrue
  then
    CAN_BUILT_GUI=true
  fi

  AM_CONDITIONAL(BUILT_GUI,test x$CAN_BUILT_GUI = xtrue -a x$ac_gui != xno)
  if test x$CAN_BUILT_GUI = xtrue
  then
	case $ac_gui in
	("yes"|"auto")
	    AC_DEFINE(HAVE_GUI_LIBS)
	    AC_MSG_RESULT(yes)
	  ;;
	"no")
	    AC_MSG_RESULT(NO *** possible but desactivated ***)
	  ;;
	  *)
	    AC_MSG_ERROR(acinclude error! Should not reach that case.)
          ;;
	esac
  else
	case $ac_gui in
	"yes")
	    AC_MSG_RESULT(no)
	    AC_MSG_ERROR(Building GUI requieres Qt4 and Vtk-5 libraries )
	  ;;
	("auto"|"no")
	    AC_MSG_RESULT(no)
	  ;;
	  *)
	    AC_MSG_ERROR(acinclude error! Should not reach that case.)
          ;;
	esac
  fi

])
