AC_DEFUN([AC_CHECK_VTK],
[
  REQUIRED_VTK_MAJOR=5
  REQUIRED_VTK_MINOR=0

  REQUIRED_VTK=`echo "$REQUIRED_VTK_MAJOR.$REQUIRED_VTK_MINOR"`

  AC_MSG_CHECKING([for VTK library (>= $REQUIRED_VTK)])

  vtkinclude=""
  vtklibpath=""
  FOUND_VTK=false
dnl look for vtk in standard pathes
  for i in /usr /usr/local
  do
    for j in vtk-5.0 vtk-5.4
    do
      if test -e "$i/include/$j/vtkConfigure.h"
      then
        VTK_VERSION=`$GREP VTK_VERSION $i/include/$j/vtkConfigure.h`
        VTK_VERSION=`expr "$VTK_VERSION" : '.*"\(.*\)"' '|' "$VTK_VERSION"`

        VTK_MAJOR=`expr "$VTK_VERSION" : '\(.*\)\..*\..*'`
        VTK_MINOR=`expr "$VTK_VERSION" : '.*\.\(.*\)\..*'`
        if test $VTK_MAJOR -ge $REQUIRED_VTK_MAJOR
        then
          if test $VTK_MINOR -ge $REQUIRED_VTK_MINOR
          then
            vtkinclude=$i/include/$j
            vtklibpath=$i/lib/$j
            FOUND_VTK=true
          fi
        fi
      fi
    done
  done

  if test x$FOUND_VTK != xtrue
  then
     AC_MSG_RESULT(no)
  else
     AC_MSG_RESULT(yes)
     VTK_CXXFLAGS="-I$vtkinclude"
     VTK_LDADD="-L$vtklibpath -lvtkRendering -lvtkGraphics -lvtkHybrid -lvtkImaging -lvtkCommon -lQVTK"
  fi
  AC_SUBST(VTK_CXXFLAGS)
  AC_SUBST(VTK_LDADD)
])
