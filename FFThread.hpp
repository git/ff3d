//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003 Pascal Hav�
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#ifndef FFTHREAD_HPP
#define FFTHREAD_HPP

#include <Thread.hpp>
#include <ThreadStaticCenter.hpp>

#include <config.h>
#include <iostream>

#ifdef HAVE_GUI_LIBS
class QTextEdit;

#include <QtCore/QThread>
#endif // HAVE_GUI_LIBS

/**
 * @file   FFThread.hpp
 * @author Stephane Del Pino
 * @date   Wed Dec  1 20:33:18 2004
 * 
 * @brief  Main ff3d thread class
 * 
 */
class FFThread
#ifdef HAVE_GUI_LIBS
  : public QThread
#else  // HAVE_GUI_LIBS
  : public Thread
#endif // HAVE_GUI_LIBS
{
#ifdef HAVE_GUI_LIBS
private:
  QTextEdit* __console;
public:
  void setConsole(QTextEdit* console);
#endif // HAVE_GUI_LIBS

private:
  std::istream* __in;	/**< the stream to process */

  /** 
   * The threaded member function
   * @note this function is private, so it is not used drectly
   */
  void run();

public:

  /**
   * Forbidden copy constructor
   * 
   */
  FFThread(const FFThread&);  


  void setInput(std::istream& fin);

  /** 
   * Constructor
   * 
   */
  FFThread();

  /** 
   * Destructor
   * 
   */
  ~FFThread();
};

#endif // FFTHREAD_HPP
