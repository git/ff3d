//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef OSTREAM_HPP
#define OSTREAM_HPP

#include <iostream>
#include <fstream>
#include <Console.hpp>
#include <ErrorHandler.hpp>

class OStream
{
public:
  enum Type {
    null,
    standard,
    error,
    file,
    qt_standard,
    qt_error
  };

private:
  Type __type;
  std::ofstream __file;

  template <typename T>
  void _writes(const T& t)
  {
    switch(__type) {
    case standard: {
      std::cout << t;
      break;
    }
    case error: {
      std::cerr << t;
      break;
    }
    case file: {
      __file << t;
      break;
    }
    case qt_standard: {
      Console::instance().writeStd(t);
      break;
    }
    case qt_error: {
      Console::instance().writeError(t);
      break;
    }
    case null: {
      break;
    }
    }
  }

public:
  void setType(const OStream::Type& type)
  {
    __type = type;
  }

  template <typename T>
  OStream& operator<<(const T& t)
  {
    this->_writes(t);
    return *this;
  }

  OStream(const OStream::Type& type)
    : __type(type)
  {
    ;
  }

  OStream(std::string filename)
    : __type(file),
      __file(filename.c_str())
  {
    if (not __file) {
      throw ErrorHandler(__FILE__, __LINE__,
			 "cannot open file "+filename+" for OStream!",
			 ErrorHandler::normal);
    }
  }

  virtual ~OStream()
  {
    ;
  }
};

#endif // OSTREAM_HPP
