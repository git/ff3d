//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef REFERENCE_COUNTING_HPP
#define REFERENCE_COUNTING_HPP

/*!
  \class ReferenceCountingCenter
  
  This class provides a repository to manage reference counting.

  \author Stephane Del Pino
*/

#include <map>
#include <iomanip>

#include <StreamCenter.hpp>

#ifndef   NDEBUG
#include <typeinfo>
#include <string>
//! Used by logs.
#endif // NDEBUG

#include <ThreadStaticBase.hpp>

template <typename T>
class ReferenceCounting;

class ReferenceCountingCenter
  : public ThreadStaticBase<ReferenceCountingCenter>
{
private:
  template <typename T>
  friend class ReferenceCounting;

  template <typename T>
  friend class ConstReferenceCounting;

  //! The counter of references
  std::map <const void*,size_t> __counter;

#ifndef   NDEBUG
  //! associates the name of the associate type.
  std::map <const void*,std::string> __typeName;
#endif // NDEBUG

  //! The number of references for a given \a address
  template <typename ReferenceCounterType>
  const size_t& numberOfReferences(const ReferenceCounterType& rc)
  {
    return __counter[rc.__ref]; 
  }

  //! Adds a reference to a given \a address
  template <typename ReferenceCounterType>
  void addReference(ReferenceCounterType& rc)
  {
    __counter[rc.__ref]++;
#ifndef  NDEBUG
    __typeName[rc.__ref] = rc.typeName();
#endif //NDEBUG
  }

  //! Removes a reference
  template <typename ReferenceCounterType>
  size_t removeReference(ReferenceCounterType& rc)
  {
    const void* address = rc.__ref;
    std::map<const void*,size_t>::iterator i = __counter.find(address);
    if ((i->second == 1)&&(address != 0)) {
      rc.eraseData();
      __counter.erase(i);
#ifndef NDEBUG
      __typeName.erase(address);
#endif // NDEBUG
      return 0;
    }
    return (--(i->second));
  }

  //! The only public part is the constructor and destructor!
public:
  template <typename O>
  void showState(O& os)
  {
#ifndef NDEBUG
    os << "\n###### REFERENCE COUNTING STATE ######\n";
    os << "Number of referenced addresses:  "
	     << std::setw(5) << __counter.size() << '\n';
    size_t n = 0;
    for (std::map<const void*,size_t>::iterator i= __counter.begin();
	 i != __counter.end(); ++i) {
      n += (i->second != 0);
    }
    os << "Number of unallocaded addresses: "
       << std::setw(5) << n << '\n';
    os << "--------------------------------------\n";
    for (std::map<const void*,size_t>::iterator i= __counter.begin();
	 i != __counter.end(); ++i) {
      if (i->second != 0) {
	os << i->first << ": " << __typeName[i->first]
	   << ':' << std::setw(5) << i->second << '\n';
      }
    }
    os << "######################################\n";
#endif // NDEBUG
  }



  //! Builts a ReferenceCountingCenter
  ReferenceCountingCenter()
  {
    ;
  }

  //! Destructs a ReferenceCountingCenter
  ~ReferenceCountingCenter()
  {
#ifndef NDEBUG
    this->showState(fflog(0));
#endif // NDEBUG
  }
};

/*!
  \class ConstReferencedClass
  
  This class provides a repository to manage const reference counting.

  \author Stephane Del Pino
*/

template <typename ReferencedClass>
class ReferenceCounting;

template <typename ReferencedClass>
class ConstReferenceCounting
{
  friend class ReferenceCountingCenter;

  friend class ReferenceCounting<ReferencedClass>;

private:
  //! The ReferenceCountingCenter
  ReferenceCountingCenter& RCC;

  //! The address of the referenced object.
  const ReferencedClass* __ref;

  //! destroys the datas.
  void eraseData()
  {
    delete __ref;
  }

public:

#ifndef NDEBUG
  std::string typeName() const
  {
    return typeid(__ref).name();
  }
#endif // NDEBUG

  bool operator == (const ReferencedClass* ref) const
  {
    return (__ref == ref);
  }

  bool operator != (const ReferencedClass* ref) const
  {
    return (__ref != ref);
  }

  bool operator == (const ReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref == rc.__ref);
  }

  bool operator != (const ReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref != rc.__ref);
  }

  bool operator == (const ConstReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref == rc.__ref);
  }

  bool operator != (const ConstReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref != rc.__ref);
  }

  //! Access to the number of references
  const size_t& numberOfReferences() const
  {
    return RCC.numberOfReferences(*this);
  }

  //! Implicit const cast to ReferencedClass*
  operator const ReferencedClass*() const
  {
    return __ref;
  }

  /** 
   * 
   * read-only access to the referenced value
   * 
   * @return the referenced value
   */
  const ReferencedClass* operator->() const
  {
    ASSERT(__ref != 0);
    return __ref;
  }

  /** 
   * 
   * read-only access to the referenced value
   * 
   * @return the referenced value
   */
  const ReferencedClass& operator *() const
  {
    ASSERT(__ref != 0);
    return *__ref;
  }

  //! operator=
  const ConstReferenceCounting<ReferencedClass>&
  operator = (const ConstReferenceCounting<ReferencedClass>& O)
  {
#ifndef NDEBUG
    fflog(4)
      << __ref << ':' << this->numberOfReferences()
      << ": remove operator=(ConstReferenceCounting<T>&) ConstReferenceCounting on "
      << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.removeReference(*this);

    __ref = O.__ref;

#ifndef NDEBUG
    fflog(4)
      << __ref << ':' << numberOfReferences()
      << ": create operator=(ConstReferenceCounting<T>&) ConstReferenceCounting on "
      << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);

    return *this;
  }

  //! copy constructor
  ConstReferenceCounting(const ConstReferenceCounting<ReferencedClass>& O)
    : RCC(ReferenceCountingCenter::instance()),
      __ref(O.__ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create ConstReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! copy constructor
  ConstReferenceCounting(const ReferenceCounting<ReferencedClass>& O)
    : RCC(ReferenceCountingCenter::instance()),
      __ref(O.__ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create ~copy ConstReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! copy constructor
  ConstReferenceCounting(const ReferencedClass* ref)
    : RCC(ReferenceCountingCenter::instance()),
      __ref(ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create ConstReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! Allows construction 
  ConstReferenceCounting()
    : RCC(ReferenceCountingCenter::instance()),
      __ref(0)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create ConstReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! Destructor
  ~ConstReferenceCounting()
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": remove ConstReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.removeReference(*this);
  }
};

/*!
  \class ReferencedClass
  
  This class provides a repository to manage reference counting.

  \author Stephane Del Pino
*/

template <typename ReferencedClass>
class ReferenceCounting
{
public:
  //! the class ConstReferenceCounting is a friend.
  friend class ConstReferenceCounting<ReferencedClass>;

  friend class ReferenceCountingCenter;
private:
  //! The reference counting center
  ReferenceCountingCenter& RCC;

  //! The referenced address
  ReferencedClass* __ref;
  
  //! destroys the datas.
  void eraseData()
  {
    delete __ref;
  }

public:

  //! Access to the number of references
  const size_t& numberOfReferences() const
  {
    return RCC.numberOfReferences(*this);
  }

  /** 
   * 
   * read-only access to the referenced value
   * 
   * @return the referenced value
   */
  const ReferencedClass* operator ->() const
  {
    ASSERT(__ref != 0);
    return __ref;
  }

  /** 
   * 
   * read-only access to the referenced value
   * 
   * @return the referenced value
   */
  const ReferencedClass& operator *() const
  {
    ASSERT(__ref != 0);
    return *__ref;
  }

  /** 
   * 
   * access to the referenced value
   * 
   * @return the referenced value
   */
  ReferencedClass* operator ->()
  {
    ASSERT(__ref != 0);
    return __ref;
  }

  /** 
   * 
   * access to the referenced value
   * 
   * @return the referenced value
   */
  ReferencedClass& operator *()
  {
    ASSERT(__ref != 0);
    return *__ref;
  }

  //! Implicit cast to ReferencedClass*
  operator ReferencedClass*()
  {
    return __ref;
  }

  //! Implicit cast to ReferencedClass*
  operator const ReferencedClass*() const
  {
    return __ref;
  }

#ifndef NDEBUG
  std::string typeName() const
  {
    return typeid(__ref).name();
  }
#endif // NDEBUG

  bool operator == (const ReferencedClass * ref) const
  {
    return (__ref == ref);
  }

  bool operator != (const ReferencedClass * ref) const
  {
    return (__ref != ref);
  }

  bool operator == (const ReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref == rc.__ref);
  }

  bool operator != (const ReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref != rc.__ref);
  }

  bool operator == (const ConstReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref == rc.__ref);
  }

  bool operator != (const ConstReferenceCounting<ReferencedClass> & rc) const
  {
    return (__ref != rc.__ref);
  }

  //! operator =
  const ReferenceCounting& operator = (ReferencedClass* ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": remove operator=(ReferencedClass*) ReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.removeReference(*this);

    __ref = ref;

#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create operator=(ReferencedClass*) ReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);

    return *this;
  }

  //! operator =
  const ReferenceCounting&
  operator = (const ReferenceCounting<ReferencedClass>& O)
  {
#ifndef NDEBUG
    fflog(4)
      << __ref << ':' << numberOfReferences()
      << ": remove operator=(ReferenceCounting<T>&) ReferenceCounting on "
      << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.removeReference(*this);

    __ref = O.__ref;

#ifndef NDEBUG
    fflog(4)
      << __ref << ':' << numberOfReferences()
      << ": create operator=(ReferenceCounting<T>&) ReferenceCounting on "
      << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);

    return *this;
  }

  //! Copy constructor
  ReferenceCounting(const ReferenceCounting<ReferencedClass>& O)
    : RCC(ReferenceCountingCenter::instance()),
      __ref(O.__ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences()
	     << ": create copy ReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! Address constuctor
  ReferenceCounting(ReferencedClass* ref)
    : RCC(ReferenceCountingCenter::instance()),
      __ref(ref)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences() << ": create ReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! default constructor
  ReferenceCounting()
    : RCC(ReferenceCountingCenter::instance()),
      __ref(0)
  {
#ifndef NDEBUG
    fflog(4) << __ref << ':' << numberOfReferences() << ": create ReferenceCounting on "
	     << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.addReference(*this);
  }

  //! Destructor
  ~ReferenceCounting()
  {
#ifndef NDEBUG
    fflog(4)
      << __ref << ':' << numberOfReferences() << ": remove ReferenceCounting on "
      << typeid(__ref).name() << '\n';
#endif // NDEBUG
    RCC.removeReference(*this);
  }
};

#endif // REFERENCE_COUNTING_HPP
