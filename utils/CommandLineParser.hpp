//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef COMMAND_LINE_PARSER_HPP
#define COMMAND_LINE_PARSER_HPP

#include <string>
#include <vector>
#include <map>

class CommandLineOption;

/**
 * @file   CommandLineParser.hpp
 * @author Stephane Del Pino
 * @date   Mon Dec 27 12:05:38 2004
 * 
 * @brief  command line parser
 * @note this is done by hand for portability reasons
 * 
 */
class CommandLineParser
{
private:
  int __numberOfArguments;	/**< number of given arguments */
  char** __arguments;		/**< argument string */

  enum OptionType {
    help,
    moreHelp,
    version,
    verbosity,
    noPause,
    noWindow
  };

  typedef std::vector<CommandLineOption*> OptionsList;
  OptionsList __options;	/**< available options */

  typedef std::map<std::string, int>
  OptionsLexerType;		/**<  associates name to option number */

  OptionsLexerType __shortOptionsLexer;	/**< Short options lexer */
  OptionsLexerType __longOptionsLexer;  /**< Long options lexer  */

  int __verbosity;		/**< ff3d's verbosity */

  std::string __filename;	/**< name of the file to treat */

  /** 
   * Treats the current option
   * 
   * @param optionNumber the identifier of the option
   * @param argumentNumber the positon of the option in the argument list
   */
  void __treatOption(const int& optionNumber, int& argumentNumber);

  /** 
   * Proceeds to effective parsing of the command line
   * 
   */
  void __parse();

public:
  /** 
   * Prints help message
   * 
   */
  void showHelp() const;

  /** 
   * Prints more-help message
   * 
   */
  void showMoreHelp() const;

  /** 
   * Prints version message
   * 
   */
  void showVersion() const;

  /** 
   * Prints ff3d's usage
   * 
   */
  void showUsage() const;

  /** 
   * Prints ff3d's howto report bug
   * 
   */
  void showBugReport() const;

  /** 
   * Prints ff3d's description
   * 
   */

  void showDescription() const;
  /** 
   * Prints ff3d's licence
   * 
   */
  void showLicence() const;

  /** 
   * Prints ff3d's credits
   * 
   */
  void showCredits() const;

  /** 
   * Checks file name presence
   * 
   * @return true if a filename was specified
   */
  bool hasFilename() const
  {
    return (__filename.size() > 0);
  }

  /** 
   * Returns the filename to treat
   * 
   * @return __filename
   */
  const std::string& filename() const
  {
    return __filename;
  }

  /** 
   * Read-only access to the verbosity
   * 
   * @return __verbosity
   */
  const int& verbosityLevel() const
  {
    return __verbosity;
  }

  /** 
   * Constructor
   * 
   * @param argc number of command line arguments
   * @param argv arguments table
   */
  CommandLineParser(int argc,
		    char** argv);

  /** 
   * Destructor
   * 
   */
  ~CommandLineParser();
};

#endif // COMMAND_LINE_PARSER_HPP
