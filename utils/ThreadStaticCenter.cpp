//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ThreadStaticCenter.hpp>
#include <ThreadStaticBase.hpp>

#include <StreamCenter.hpp>
#include <Console.hpp>

// language utils
#include <VariableLexerRepository.hpp>
#include <VariableRepository.hpp>

// reader utils
#include <XMLFileReader.hpp>

// Quadrarure formulae
#include <QuadratureFormula.hpp>
#include <GaussLobattoManager.hpp>

// Finite elements
#include <Q0HexahedronFiniteElement.hpp>
#include <Q1HexahedronFiniteElement.hpp>
#include <Q2HexahedronFiniteElement.hpp>

#include <P0TetrahedronFiniteElement.hpp>
#include <P1TetrahedronFiniteElement.hpp>
#include <P2TetrahedronFiniteElement.hpp>

#include <Q0Quadrangle3DFiniteElement.hpp>
#include <Q1Quadrangle3DFiniteElement.hpp>
#include <Q2Quadrangle3DFiniteElement.hpp>

#include <P0Triangle3DFiniteElement.hpp>
#include <P1Triangle3DFiniteElement.hpp>
#include <P2Triangle3DFiniteElement.hpp>

#include <SolverInformationCenter.hpp>

#include <config.h>

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif // HAVE_PTHREAD

#include <ReferenceCounting.hpp>

#include <Information.hpp>
#include <ParameterCenter.hpp>
#include <DegreeOfFreedomSetManager.hpp>
#include <NormalManager.hpp>

ThreadStaticCenter::
ThreadStaticCenter()
{
  Console::create();
  StreamCenter::create();

  ReferenceCountingCenter::create();

  /// Quadrature formulae
  QuadratureFormulaP0Tetrahedron::create();
  QuadratureFormulaQ0Hexahedron::create();
  QuadratureFormulaP1Tetrahedron::create();
  QuadratureFormulaQ1Hexahedron::create();
  QuadratureFormulaP2Tetrahedron::create();
  QuadratureFormulaQ2Hexahedron::create();

  QuadratureFormulaP0Triangle3D::create();
  QuadratureFormulaQ0Quadrangle3D::create();
  QuadratureFormulaP1Triangle3D::create();
  QuadratureFormulaQ1Quadrangle3D::create();
  QuadratureFormulaP2Triangle3D::create();
  QuadratureFormulaQ2Quadrangle3D::create();

  GaussLobattoManager::create();

  /// Finite elements
  P0TetrahedronFiniteElement::create();
  Q0HexahedronFiniteElement::create();
  P1TetrahedronFiniteElement::create();
  Q1HexahedronFiniteElement::create();
  P2TetrahedronFiniteElement::create();
  Q2HexahedronFiniteElement::create();

  P0Triangle3DFiniteElement::create();
  Q0Quadrangle3DFiniteElement::create();
  P1Triangle3DFiniteElement::create();
  Q1Quadrangle3DFiniteElement::create();
  P2Triangle3DFiniteElement::create();
  Q2Quadrangle3DFiniteElement::create();

  // file utils
  XMLFileReader::create();

  /// Language utils
  Information::create();
  ParameterCenter::create();
  DegreeOfFreedomSetManager::create();
  NormalManager::create();

  /// Language statics
  VariableLexerRepository::create();
  VariableRepository::create();

  // Solver information manager
  SolverInformationCenter::create();
}

ThreadStaticCenter::
~ThreadStaticCenter()
{
  // Solver information manager
  SolverInformationCenter::destroy();

  /// Language statics
  VariableRepository::destroy();
  VariableLexerRepository::destroy();

  NormalManager::destroy();
  DegreeOfFreedomSetManager::destroy();
  /// Language utils
  ParameterCenter::destroy();
  Information::destroy();

  // file utils
  XMLFileReader::destroy();

  /// Finite elements
  Q2HexahedronFiniteElement::destroy();
  P2TetrahedronFiniteElement::destroy();
  Q1HexahedronFiniteElement::destroy();
  P1TetrahedronFiniteElement::destroy();
  Q0HexahedronFiniteElement::destroy();
  P0TetrahedronFiniteElement::destroy();

  Q2Quadrangle3DFiniteElement::destroy();
  P2Triangle3DFiniteElement::destroy();
  Q1Quadrangle3DFiniteElement::destroy();
  P1Triangle3DFiniteElement::destroy();
  Q0Quadrangle3DFiniteElement::destroy();
  P0Triangle3DFiniteElement::destroy();

  /// Quadrature formulae
  GaussLobattoManager::destroy();

  QuadratureFormulaQ2Hexahedron::destroy();
  QuadratureFormulaP2Tetrahedron::destroy();
  QuadratureFormulaP1Tetrahedron::destroy();
  QuadratureFormulaQ1Hexahedron::destroy();
  QuadratureFormulaP0Tetrahedron::destroy();
  QuadratureFormulaQ0Hexahedron::destroy();

  QuadratureFormulaQ2Quadrangle3D::destroy();
  QuadratureFormulaP2Triangle3D::destroy();
  QuadratureFormulaQ1Quadrangle3D::destroy();
  QuadratureFormulaP1Triangle3D::destroy();
  QuadratureFormulaQ0Quadrangle3D::destroy();
  QuadratureFormulaP0Triangle3D::destroy();

  ReferenceCountingCenter::destroy();

  StreamCenter::destroy();
  Console::destroy();
}
