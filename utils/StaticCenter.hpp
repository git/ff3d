//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef STATIC_CENTER_HPP
#define STATIC_CENTER_HPP

/**
 * @file   StaticCenter.hpp
 * @author St�phane Del Pino
 * @date   Wed Nov  6 00:25:58 2002
 * 
 * @brief  This class is used to allocate some static members
 * 
 * Static construction order is not predictable, using this class it
 *is ensure that allocation will be done always as the same sequence.
 */

class StaticCenter
{
private:
  static bool __instanciated; /**< Ensure that no more than one static
				 center is instanciated */
public:

  /** 
   * Constructes statics
   * 
   */
  StaticCenter();

  /** 
   * Destroyes statics
   * 
   */
  ~StaticCenter();
};

#endif // STATIC_CENTER_HPP
