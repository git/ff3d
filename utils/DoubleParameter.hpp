//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef DOUBLE_PARAMETER_HPP
#define DOUBLE_PARAMETER_HPP

#include <Parameter.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/*!
  class DoubleParameter
  This is the base describes Double Parameters.

  \author St�phane Del Pino.
*/
class DoubleParameter
  : public Parameter
{
private:
  const real_t __defaultDoubleValue;
  real_t __realValue;

  std::ostream& put (std::ostream& os) const
  {
    os << __realValue;
    return os;
  }

public:
  //! Does not add other identifiers.
  void get(IdentifierSet& I)
  {
    ;
  }

  void set(const real_t d)
  {
    __realValue = d;
  }

  void set(const int i)
  {
    __realValue = (real_t)(i);
  }

  void set(const char* c)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the string '"+stringify(c)
		       +"' to a real_t parameter\n",
		       ErrorHandler::normal);
  }

  operator real_t&()
  {
    return __realValue;
  }

  operator real_t() const
  {
    return __realValue;
  }

  void reset()
  {
    __realValue = __defaultDoubleValue;
  }

  const std::string typeName() const
  {
    return "real";
  }

  DoubleParameter(const DoubleParameter& dp)
    : Parameter(dp),
      __defaultDoubleValue(dp.__defaultDoubleValue),
      __realValue(dp.__realValue)
  {
    ;
  }  

  DoubleParameter(const real_t d, const char* label)
    : Parameter(Parameter::Double, label),
      __defaultDoubleValue(d),
      __realValue(d)
  {
    ;
  }

  ~DoubleParameter()
  {
    ;
  }
};

#endif // DOUBLE_PARAMETER_HPP
