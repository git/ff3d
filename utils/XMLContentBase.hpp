//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef XML_CONTENT_BASE_HPP
#define XML_CONTENT_BASE_HPP

/**
 * @file   XMLContentBase.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 10 17:05:40 2007
 * 
 * @brief  base class for content of xml tags
 */
class XMLContentBase
{
public:
  enum Type {
    filePosition
  };

private:
  const Type __type;		/**< type of content */

public:
  /** 
   * Read only access to the type of content
   * 
   * @return __type
   */
  const Type& type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param type type of content
   */
  XMLContentBase(const Type& type)
    : __type(type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~XMLContentBase()
  {
    ;
  }
};

#endif // XML_CONTENT_BASE_HPP
