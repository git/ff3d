//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2004,2005 St�phane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#include <ErrorHandler.hpp>
#include <StreamCenter.hpp>

#include <BaseName.hpp>

void ErrorHandler::writeErrorMessage()
{
  switch(__type) {
  case asked: {
    fferr(3) << "\nremark: exit command explicitly called\n";
  }
  case normal: {
    fferr(3) << '\n' << __filename << ':' << __lineNumber
	     << ":remark: emitted the following message\n";
    fferr(0) << "error: " << __errorMessage << '\n';
    break;
  }
  case compilation: {
    fferr(0) << "\nline " << __lineNumber << ':' << __errorMessage << '\n';
    break;
  }
  case unexpected: {
    fferr(0) << '\n' << __filename << ':' << __lineNumber << ":\n" << __errorMessage << '\n';
    fferr(0) << "\nUNEXPECTED ERROR: this should not occure, please report it\n";
    fferr(0) << "\nBUG REPORT: Please send bug reports to:\n"
	     << "  ff3d-dev@nongnu.org or freefem@ann.jussieu.fr\n"
	     << "or better, use the Bug Tracking System:\n"
	     << "  http://savannah.nongnu.org/bugs/?group=ff3d\n";
    break;
  }
  default: {
    fferr(0) << __filename << ':' << __lineNumber << ": " << __errorMessage << '\n';
    fferr(0) << __FILE__ << ':' << __LINE__ << ":remark:  error type not implemented!\n";
  }
  }
}

ErrorHandler::
ErrorHandler(const std::string& filename,
	     const size_t& lineNumber,
	     const std::string& errorMessage,
	     const Type& type)
  : __filename(baseName(filename)),
    __lineNumber(lineNumber),
    __errorMessage(errorMessage),
    __type(type)
{
  ;
}
