//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef STRINGIFY_HPP
#define STRINGIFY_HPP

#include <string>
#include <sstream>

/**
 * @file   Stringify.hpp
 * @author Pascal Have
 * @date   Sun Nov 28 18:59:11 2004
 * 
 * @brief  converts writable objects to std::string
 * 
 */

/** 
 * stringify function
 * 
 * @param t an object that supports operator<<
 * 
 * @return a string
 */
template<typename T> 
inline std::string stringify(const T & t)
{
  std::ostringstream oss;
  oss << t;
  return oss.str();
}

template<> 
inline std::string stringify<std::string>(const std::string& t)
{
  return t;
}

#endif // STRINGIFY_HPP
