//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2004,2005 St�phane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#ifndef ERROR_HANDLER_HPP
#define ERROR_HANDLER_HPP

#include <cstddef>
#include <string>

/**
 * @file   ErrorHandler.hpp
 * @author Stephane Del Pino
 * @date   Wed Dec 29 16:03:06 2004
 * 
 * @brief  Error Handling class
 * 
 */
class ErrorHandler
{
public:
  enum Type {
    asked,     /**< execution request by the user*/
    compilation, /**< syntax error in a language */
    normal,    /**< normal error due to a bad use of ff3d */
    unexpected /**< Unexpected execution error */
  };

private:
  const std::string __filename;	/**< The source file name where the error occured */
  const size_t __lineNumber;	/**< The line number where exception was raised */
  const std::string __errorMessage; /**< The reporting message */

  const Type __type;		/**< the type of the error */
public:
  /** 
   * Prints the error message
   * 
   */
  virtual void writeErrorMessage();

  /** 
   * The copy constructor
   * 
   * @param e an handled error
   */
  ErrorHandler(const ErrorHandler& e)
    : __filename(e.__filename),
      __lineNumber(e.__lineNumber),
      __errorMessage(e.__errorMessage),
      __type(e.__type)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param filename the source file name
   * @param lineNumber the source line
   * @param errorMessage the reported message
   * @param type the type of the error
   */
  ErrorHandler(const std::string& filename,
	       const size_t& lineNumber,
	       const std::string& errorMessage,
	       const Type& type);

  /** 
   * The destructor
   * 
   */
  virtual ~ErrorHandler()
  {
    ;
  }
};

#endif // ERROR_HANDLER_HPP
