//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef RUNNING_OPTIONS_HPP
#define RUNNING_OPTIONS_HPP

#include <StaticBase.hpp>

/**
 * @file   RunningOptions.hpp
 * @author Stephane Del Pino
 * @date   Wed Dec 29 11:08:33 2004
 * 
 * @brief This class stores running options. This allows to change
 * ff3d's behaviour at running time.
 * 
 */

class RunningOptions
  : public StaticBase<RunningOptions>
{
private:
  bool __haveDisplay;		/**< true if DISPLAY can be opened (unix only) */
  bool __useGUI;		/**< true if GUI is to be used */
  bool __pauseOnError;		/**< ff3d's pause on error for debug */

  size_t __verbosity;		/**< Verbolity */

public:
  /** 
   * Sets the level of verbosity of the code
   * 
   * @param verbosity level of verbosity
   */
  void setVerbosity(const size_t& verbosity)
  {
    __verbosity = verbosity;
  }

  /** 
   * Gets the level of verbosity of the code
   * 
   * @return verbosity level
   */
  const size_t& getVerbosity()
  {
    return __verbosity;
  }

  /** 
   * Readonly access to __haveDisplay
   * 
   * @return __haveDisplay
   */
  const bool& haveDisplay() const
  {
    return __haveDisplay;
  }

  /** 
   * Readonly access to __useGUI
   * 
   * @return __useGUI
   */
  const bool& useGUI() const
  {
    return __useGUI;
  }
  
  /** 
   * Pause on error
   * 
   * @return __pauseOnError
   */
  const bool& pauseOnError() const
  {
    return __pauseOnError;
  }

  /** 
   * Sets Display presence. By default sets use of GUI the same value
   * as Display.
   * 
   * @param b Display value
   */
  void setDisplay(const bool& b)
  {
    __haveDisplay = b;
    __useGUI = b;
  }
 
  /** 
   * Sets GUI usage only when Display is available.
   * 
   * @param b GUI use
   */
  void setGUI(const bool& b)
  {
    if (__haveDisplay) {
      __useGUI = b;
    } else {
      __useGUI = false;
    }
  }

  /** 
   * Pause on error
   * 
   * @return __pauseOnError
   */
  void setNoPauseOnError()
  {
    __pauseOnError = false;
  }
 
  /** 
   * Constructor
   * 
   */
  RunningOptions()
    : __haveDisplay(false),
      __useGUI(false),
      __pauseOnError(true)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~RunningOptions()
  {
    ;
  }
};

#endif // RUNNING_OPTIONS_HPP
