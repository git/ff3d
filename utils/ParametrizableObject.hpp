//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _PARAMETRIZABLE_OBJECT_HPP_
#define _PARAMETRIZABLE_OBJECT_HPP_

/*!
  \class ParametrizableObject

  This class defines objects that can be parametrized and the default values
  of parameters.

  \author St�phane Del Pino.
*/

#include <ReferenceCounting.hpp>
#include <Parameter.hpp>
#include <DoubleParameter.hpp>
#include <IntegerParameter.hpp>
#include <StringParameter.hpp>
#include <EnumParameter.hpp>

#include <IdentifierSet.hpp>
#include <string>

class ParametrizableObject
{
private:
  virtual std::ostream& put(std::ostream& os) const = 0;

  typedef std::map<const char*,
		   ReferenceCounting<Parameter>,
		   StringEquality> ParameterList;

  ParameterList __parameters;

public:
  void get(IdentifierSet& I)
  {
    for (ParameterList::iterator i = __parameters.begin();
	 i != __parameters.end(); ++i)
      {
	(*(*i).second).get(I);
	I.insert((*i).first);
      }
  }

protected:
  void add(ReferenceCounting<Parameter> p)
  {
    __parameters[(*p).label()] = p;
  }

  template <typename EnumType>
  void get(const char* label, EnumType& e) const
  {
    ReferenceCounting<Parameter> p = get(label);
    if((*p).type() == Parameter::Enum) {
      e = dynamic_cast<const EnumParameter<EnumType>&>(*p);
      return;
    }
  }

  void get(const char* label, real_t& d) const
  {
    ReferenceCounting<Parameter> p = get(label);
    if((*p).type() == Parameter::Double) {
      d = dynamic_cast<const DoubleParameter&>(*p);
      return;
    }      
  }

  void get(const char* label, int& i) const
  {
    ReferenceCounting<Parameter> p = get(label);
    if((*p).type() == Parameter::Integer) {
      i = dynamic_cast<const IntegerParameter&>(*p);
      return;
    }      
  }

  void get(const char* label, std::string& s) const
  {
    ReferenceCounting<Parameter> p = get(label);
    if((*p).type() == Parameter::String) {
      s = dynamic_cast<const StringParameter&>(*p);
      return;
    }      
  }

public:

  void reset()
  {
    for (ParameterList::iterator i = __parameters.begin();
	 i != __parameters.end(); ++i) {
      (*(*i).second).reset();
    }
  }

  const ReferenceCounting<Parameter> get(const char* parameterName) const
  {
    ParameterList::const_iterator i = __parameters.find(parameterName);
    if (i != __parameters.end()) {
      return (*i).second;
    } else {
      throw ErrorHandler(__FILE__,__LINE__,
			 stringify(parameterName)+" not found",
			 ErrorHandler::normal);
      return 0;
    }
  }

  /*!
    \brief Constructs a ParametrizableObject
    
    Constructs a ParametrizableObject with a given Parameter p and gives
    the id of the Parametrizable object.
  */
  ParametrizableObject()
  {
    ;
  }

  virtual ~ParametrizableObject()
  {
    ;
  }

  //! Writes the ParametrizableObject.
  friend std::ostream& operator << (std::ostream& os,
				    const ParametrizableObject& P);
};

#endif // _PARAMETRIZABLE_OBJECT_HPP_

