//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef ENDIAN_CONVERTER_HPP
#define ENDIAN_CONVERTER_HPP

#include <Vector.hpp>

template <typename T>
void invertEndianess(T& value)
{
  const size_t n = sizeof(T);
  char* p = reinterpret_cast<char*>(&value);
  for (size_t i=0; i<n/2; ++i) {
    std::swap(p[i],p[n-1-i]);
  }
}

template <typename T>
void invertEndianess(Vector<T>& values)
{
  for (size_t i=0; i<values.size(); ++i) {
    invertEndianess(values[i]);
  }
}

#ifdef WORDS_BIGENDIAN

#include <algorithm>

template <typename T>
void littleEndianize(T& value)
{
  invertEndianess(value);
}

template <typename T>
void littleEndianize(Vector<T>& values)
{
  invertEndianess(values);
}

template <typename T>
void fromLittleEndian(T& value)
{
  invertEndianess(value);
}

template <typename T>
void fromLittleEndian(Vector<T>& values)
{
  invertEndianess(values);
}

template <typename T>
void bigEndianize(T& value)
{
  ;
}

template <typename T>
void bigEndianize(Vector<T>& values)
{
  ;
}

template <typename T>
void fromBigEndian(T& value)
{
  ;
}

template <typename T>
void fromBigEndian(Vector<T>& values)
{
  ;
}

#else // WORDS_BIGENDIAN

template <typename T>
void littleEndianize(T& value)
{
  ;
}

template <typename T>
void littleEndianize(Vector<T>& values)
{
  ;
}

template <typename T>
void fromLittleEndian(T& value)
{
  ;
}

template <typename T>
void fromLittleEndian(Vector<T>& values)
{
  ;
}

template <typename T>
void bigEndianize(T& value)
{
  invertEndianess(value);
}

template <typename T>
void bigEndianize(Vector<T>& values)
{
  invertEndianess(values);
}


template <typename T>
void fromBigEndian(T& value)
{
  invertEndianess(value);
}

template <typename T>
void fromBigEndian(Vector<T>& values)
{
  invertEndianess(values);
}

#endif // WORDS_BIGENDIAN

#endif // ENDIAN_CONVERTER_HPP
