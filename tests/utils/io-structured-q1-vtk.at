# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005, 2006 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_SETUP([Vtk   structured Q1-FEM IO])

AT_KEYWORDS (io)

###########################
# CHECKS STRUCTURED Q1 IO #
###########################

AT_DATA([test.ff],
[[
vector a = (0,0,0);
vector b = (2,3,1);
vector n = (3,2,5);

mesh m = structured(n,a,b);

femfunction f(m) = x*y*z;

// Saves using text mode
save(vtk,"f-text",{x,[f,z+1,-y+2]},m,unix);

// Saves using binary mode
save(vtk,"f-binary",{x,[f,z+1,-y+2]},m,binary);

function zp1 = read(vtk,"f-text.vti":"[f,(z+1),(-y+2)]":1,m);
double error_text = int[m](abs(z+1-zp1));

if (error_text < 1E-8) {
  cout << "read text: ok\n";
} else {
  cout << "read text: failed (" << error_text << ")\n";
}

function _yp2 = read(vtk,"f-binary.vti":"[f,(z+1),(-y+2)]":2,m);
double error_binary = int[m](abs(-y+2-_yp2));

if (error_binary < 1E-8) {
  cout << "read binary: ok\n";
} else {
  cout << "read binary: failed (" << error_binary << ")\n";
}

cat("f-text.vti");
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 test.ff],0,
[[read text: ok
read binary: ok
<?xml version="1.0"?>
<VTKFile type="ImageData">
  <ImageData Origin="0 0 0" Spacing="1 3 0.25" WholeExtent="0 2 0 1 0 4">
    <Piece Extent="0 2 0 1 0 4">
      <PointData>
        <DataArray Name="x" format="ascii" type="Float64">
 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2 0 1 2
        </DataArray>
        <DataArray Name="[f,(z+1),(-y+2)]" NumberOfComponents="3" format="ascii" type="Float64">
 0 1 2 0 1 2 0 1 2 0 1 -1 0 1 -1 0 1 -1 0 1.25 2 0 1.25 2 0 1.25 2 0 1.25 -1 0.75 1.25 -1 1.5 1.25 -1 0 1.5 2 0 1.5 2 0 1.5 2 0 1.5 -1 1.5 1.5 -1 3 1.5 -1 0 1.75 2 0 1.75 2 0 1.75 2 0 1.75 -1 2.25 1.75 -1 4.5 1.75 -1 0 2 2 0 2 2 0 2 2 0 2 -1 3 2 -1 6 2 -1
        </DataArray>
      </PointData>
    </Piece>
  </ImageData>
</VTKFile>
]],
[])

AT_CLEANUP
