# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_BANNER([Utilities])

AT_BANNER([  -> IO Management])

# Medit mesh format
m4_include([utils/io-structured-mesh-medit.at])
m4_include([utils/io-unstructured-tetra-mesh-medit.at])

# Medit functions and fields
m4_include([utils/io-structured-q1-field-medit.at])
m4_include([utils/io-structured-q1-function-medit.at])
m4_include([utils/io-unstructured-p1-field-medit.at])
m4_include([utils/io-unstructured-p1-function-medit.at])

# Raw functions and fields
m4_include([utils/io-structured-q1-field-raw.at])
m4_include([utils/io-structured-q1-function-raw.at])
m4_include([utils/io-unstructured-p1-field-raw.at])
m4_include([utils/io-unstructured-p1-function-raw.at])

# Vtk functions and fields
m4_include([utils/io-structured-q1-vtk.at])
m4_include([utils/io-unstructured-p1-vtk.at])

m4_include([utils/ostream.at])
