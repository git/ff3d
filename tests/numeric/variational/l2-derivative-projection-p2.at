# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_SETUP([L2-projection derivatives (P2-FEM)])

AT_DATA([test.ff],
[[
vertex a = (0,0,0);
vertex b = (1,2,3);
vertex n = (3,3,2);

mesh M = tetrahedrize(structured(n,a,b));

femfunction u(M:P2) = x + 2*y - 6*z + x*y + x*x + z*y + z*z;
function ux =  1 + y + 2*x;
function uy =  2 + x + z;
function uz = -6 + y + 2*z;

solve(dxu:P2) in M
  cg(epsilon=1E-20)
{
  test(v)
   int(dxu*v) = int(dx(u)*v);
}

solve(dyu:P2) in M
  cg(epsilon=1E-20)
{
  test(v)
   int(dyu*v) = int(dy(u)*v);
}

solve(dzu:P2) in M
  cg(epsilon=1E-20)
{
  test(v)
   int(dzu*v) = int(dz(u)*v);
}

double l2errorx = sqrt(int[M:P2]((dxu-ux)^2));
if (l2errorx < 1E-8)
  cout << "dx(u): ok\n";
else
  cout << "oops: dx(u) l2-error is " << l2errorx << "\n";


double l2errory = sqrt(int[M:P2]((dyu-uy)^2));
if (l2errory < 1E-8)
  cout << "dy(u): ok\n";
else
  cout << "oops: dy(u) l2-error is " << l2errory << "\n";

double l2errorz = sqrt(int[M:P2]((dzu-uz)^2));
if (l2errorz < 1E-8)
  cout << "dz(u): ok\n";
else
  cout << "oops: dz(u) l2-error is " << l2errorz << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 test.ff],0,
[[dx(u): ok
dy(u): ok
dz(u): ok
]])

AT_CLEANUP
