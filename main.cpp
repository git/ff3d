//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

/*!
  \mainpage The FreeFEM3D developper Manual.
  \section intro Introduction.
  Write an introdution to the FreeFEM3D project.
  \section code Getting the Code.
  \li How to get the code (download from www.freefem.org, using cvs, ...)
  \li Submitting patches, become a FreeFEM3D developper, ...
  \author St�phane Del Pino
*/

#include <FFThread.hpp>

#include <StaticCenter.hpp>
#include <RunningOptions.hpp>

#include <cstddef>
#include <cstdlib>

#include <string>
#include <fstream>

#ifdef HAVE_GUI_LIBS
#include <GUI.hpp>

GUI* gui = 0;
#endif // HAVE_GUI_LIBS

#include <iostream>

#include <CommandLineParser.hpp>

#ifdef Q_WS_X11
#include <X11/Xlib.h>
#endif // Q_WS_X11

// The static center (everything is there :-)
static StaticCenter staticCenter;

int main (int argc, char *argv[])
{
#ifdef HAVE_GUI_LIBS
#ifdef Q_WS_X11
  Display* display
    = XOpenDisplay(std::getenv( "DISPLAY" ));
  if (display != 0) {
    XCloseDisplay(display); // freeing 1 X11 socket...
    RunningOptions::instance().setDisplay(true);
  }
#else // Q_WS_X11
  // When not running X11, always have Display
  RunningOptions::instance().setDisplay(true);
#endif // Q_WS_X11
#endif // HAVE_GUI_LIBS

  CommandLineParser arguments(argc, argv);
  RunningOptions::instance().setVerbosity(arguments.verbosityLevel());

#ifdef HAVE_GUI_LIBS
  gui = new GUI(argc, argv);
#endif // HAVE_GUI_LIBS

  if (not(RunningOptions::instance().useGUI())) {
    std::ifstream fin(arguments.filename().c_str());

    if (not(fin)) {
      std::cerr << "Error: cannot open file \""
		<< arguments.filename() << "\"\n";
      return 1;
    }

    FFThread mainThread;
    mainThread.setInput(fin);
    mainThread.start();
  }

#ifdef HAVE_GUI_LIBS
  if (gui != 0) delete gui;
#endif // HAVE_GUI_LIBS

  return 0;
}
