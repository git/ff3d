//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <VariableLexerRepository.hpp>

#include <Types.hpp>
class Expression;
class RealExpression;
class BooleanExpression;
class Vector3Expression;
class StringExpression;

class FunctionExpression;

#include <FunctionExpressionMeshReferences.hpp>

class FieldExpression;
class FieldExpressionList;

class MeshExpression;
class MeshExpressionSurface;
class MeshExpressionStructured;

#include <MeshExpression.hpp>

class SceneExpression;
class DomainExpression;
class OFStreamExpression;
class IFStreamExpression;

class UnknownExpression;
class UnknownListExpression;

class TestFunctionVariable;
class TestFunctionExpressionList;

class BoundaryConditionListExpression;
class BoundaryConditionExpression;
class BoundaryExpression;

class PDEOperatorExpression;
class PDEScalarOperatorExpressionOrderZero;
class PDEVectorialOperatorExpressionOrderOne;
class PDEScalarOperatorExpressionOrderOne;
class PDEVectorialOperatorExpressionOrderTwo;

class PDEOperatorSumExpression;

class PDEEquationExpression;

class PDEProblemExpression;
class PDESystemExpression;
class SolverOptionsExpression;

class OptionExpression;

class OStreamExpression ;
class OStreamExpressionList ;

class IStreamExpression ;
class IStreamExpressionList ;

class SolverOptionsExpression;
class SubOptionExpression;
class SubOptionListExpression;

class Variable;
class RealVariable;
class Vector3Variable;
class FunctionVariable;
class OFStreamVariable;
class IFStreamVariable;

class FunctionVariable;

class StringVariable;

class MeshVariable;
class SceneVariable;
class DomainVariable;
class InsideExpression;
class InsideListExpression;

class Instruction;

class IntegratedExpression;
class IntegratedOperatorExpression;

class LinearExpression;

class MultiLinearExpression;
class MultiLinearExpressionSum;
class MultiLinearFormExpression;
class MultiLinearFormSumExpression;

class VariationalDirichletListExpression;
class BoundaryConditionExpressionDirichlet;
class VariationalFormulaExpression;
class VariationalProblemExpression;
class ProblemExpression;

#include <FileDescriptor.hpp>
#include <ScalarDiscretizationTypeBase.hpp>

#include <ScalarFunctionNormal.hpp>

#include <parse.ff.h>

VariableLexerRepository::Type VariableLexerRepository::
find(const std::string& name) const
{
  Container::const_iterator i;
  size_t level=0;
  for (; level<=__blockLevel;++level) {
    i = __repository[level].find(name);
    if (i != __repository[level].end()) break;
  }
  if (level>__blockLevel) return -1;
  if (i == __repository[level].end()) return -1;
  else {
    return i->second;
  } 
}

void VariableLexerRepository::
markAsUnknown(const std::string& name)
{
  Container::iterator i;
  size_t level=0;
  for (; level<=__blockLevel;++level) {
    i = __repository[level].find(name);
    if (i != __repository[level].end()) break;
  }

  if (level>__blockLevel) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot find variable "+name,
		       ErrorHandler::unexpected);
  }

  if (i == __repository[level].end())
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot find variable "+name,
		       ErrorHandler::unexpected);
  } else {
    i->second = VARUNKNOWNID;
  } 
}


void VariableLexerRepository::
add(const std::string& name, const Type& type)
{
  __repository[__blockLevel][name]=type;
}

void VariableLexerRepository::
beginBlock()
{
  __blockLevel++;
  __repository.push_back(Container());
}

void VariableLexerRepository::
endBlock()
{
  ASSERT(__blockLevel>0);
  __repository[__blockLevel].clear();
  __blockLevel--;

  __repository.pop_back();

  // At the end of a block, unknowns are always variables!
  for (size_t level=0; level<=__blockLevel;++level) {
    for (Container::iterator ivariable = __repository[level].begin();
	 ivariable != __repository[level].end(); ++ivariable) {
      if (ivariable->second == VARUNKNOWNID) {
	ivariable->second = VARFNCTID;
      }
    }
  }    
}

VariableLexerRepository::
VariableLexerRepository()
  : __blockLevel(0)
{
  __repository.push_back(Container());
}

VariableLexerRepository::
~VariableLexerRepository()
{
  ;
}

