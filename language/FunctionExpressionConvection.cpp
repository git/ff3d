//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionConvection.hpp>

#include <MeshOfHexahedra.hpp>
#include <Information.hpp>

#include <Convection.hpp>

std::ostream& FunctionExpressionConvection::
put(std::ostream& os) const
{
  if (__scalarFunction != 0) {
    os << *__scalarFunction;
  } else {
    os << "convect(" << *__field << ','
       << *__timeStep << ',' << *__convectedFunction << ')';
  }
  return os;
}

void FunctionExpressionConvection::
__execute()
{
  __convectedFunction->execute();

  __field->execute();
  __timeStep->execute();

  if (Information::instance().usesMesh()) {

    const Mesh& mesh = *Information::instance().getMesh();

    if (__field->numberOfComponents() != 3) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "convection needs a 3 component field:\n"
			 +stringify(*__field)+" has "+stringify(__field->numberOfComponents())+" components",
			 ErrorHandler::normal);
    }

    switch (mesh.type()) {
    case Mesh::cartesianHexahedraMesh: {
      __scalarFunction
	= new Convection<Structured3DMesh>(*__convectedFunction->function(),
					   *__field->field(),
					   __timeStep->realValue(),
					   static_cast<const Structured3DMesh&>(mesh));
      break;
    }
    case Mesh::tetrahedraMesh: {
      __scalarFunction
	= new Convection<MeshOfTetrahedra>(*__convectedFunction->function(),
					   *__field->field(),
					   __timeStep->realValue(),
					   static_cast<const MeshOfTetrahedra&>(mesh));
      break;
    }
    case Mesh::hexahedraMesh: {
      __scalarFunction
	= new Convection<MeshOfHexahedra>(*__convectedFunction->function(),
					  *__field->field(),
					  __timeStep->realValue(),
					  static_cast<const MeshOfHexahedra&>(mesh));
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "convection is not implemented for this kind of mesh",
			 ErrorHandler::unexpected);
    }
    }

    __isToEvaluate = false;
  } else {
    __isToEvaluate = true;
  }
}


FunctionExpressionConvection::
FunctionExpressionConvection(ReferenceCounting<FieldExpression> field,
			     ReferenceCounting<RealExpression> dt,
			     ReferenceCounting<FunctionExpression> phi)
  : FunctionExpression(FunctionExpression::convection),
    __isToEvaluate(true),
    __field(field),
    __timeStep(dt),
    __convectedFunction(phi)
{
  ;
}


FunctionExpressionConvection::
FunctionExpressionConvection(const FunctionExpressionConvection& f)
  : FunctionExpression(f),
    __isToEvaluate(f.__isToEvaluate),
    __field(f.__field),
    __timeStep(f.__timeStep),
    __convectedFunction(f.__convectedFunction)
{
  ;
}

FunctionExpressionConvection::~FunctionExpressionConvection()
{
  ;
}

