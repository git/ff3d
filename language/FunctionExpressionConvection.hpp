//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_CONVECTION_HPP
#define FUNCTION_EXPRESSION_CONVECTION_HPP

#include <ReferenceCounting.hpp>
#include <RealExpression.hpp>

#include <FieldExpression.hpp>
#include <FunctionExpression.hpp>

/**
 * @file   FunctionExpressionConvection.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:30:50 2006
 * 
 * @brief Function expression associated to a convection operator
 * 
 * 
 */
class FunctionExpressionConvection
  : public FunctionExpression
{
private:
  bool __isToEvaluate;		/**< true while function has not been evaluated */

  ReferenceCounting<FieldExpression> __field; /**< advection field */

  ReferenceCounting<RealExpression> __timeStep;	/**< time step expression */

  ReferenceCounting<FunctionExpression> __convectedFunction; /**< convected function */

protected:
  std::ostream& put(std::ostream& os) const;

public:

  /** 
   * Returns @b true if one of the functions has a boundary
   * 
   * @return true if one of the function expression has a boundary
   */
  bool hasBoundaryExpression() const
  {
    return (__field->hasBoundaryExpression() or
	    __convectedFunction->hasBoundaryExpression());
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param field advection field
   * @param dt time step
   * @param phi convected function
   * 
   */
  FunctionExpressionConvection(ReferenceCounting<FieldExpression> field,
			       ReferenceCounting<RealExpression> dt,
			       ReferenceCounting<FunctionExpression> phi);

  /** 
   * Copy constructor
   * 
   * @param e given function expression
   * 
   */
  FunctionExpressionConvection(const FunctionExpressionConvection& e);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionConvection();
};

#endif // FUNCTION_EXPRESSION_CONVECTION_HPP
