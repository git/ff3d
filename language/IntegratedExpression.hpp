//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef INTEGRATED_EXPRESSION_HPP
#define INTEGRATED_EXPRESSION_HPP

#include <Expression.hpp>

class IntegratedExpression
  : public Expression
{
public:
  enum IType {
    unknownFunction,
    testFunction,
    function
  };

private:
  IType __integratedExpressionType;

public:
  const IType& integratedExpressionType() const
  {
    return __integratedExpressionType;
  }

  IntegratedExpression(IntegratedExpression::IType t)
    : Expression(Expression::integrated),
      __integratedExpressionType(t)
  {
    ;
  }

  IntegratedExpression(const IntegratedExpression& I)
    : Expression(I),
      __integratedExpressionType(I.__integratedExpressionType)
  {
    ;
  }

  ~IntegratedExpression()
  {
    ;
  }
};

class IntegratedExpressionUnknown
  : public IntegratedExpression
{
private:
  const std::string __unknownName;

  std::ostream& put(std::ostream& os) const
  {
    os << __unknownName;
    return os;
  }

public:
  void __execute()
  {
    ;
  }

  const std::string& unknownName() const
  {
    return __unknownName;
  }

  IntegratedExpressionUnknown(const std::string& unknownName)
    : IntegratedExpression(IntegratedExpression::unknownFunction),
      __unknownName(unknownName)
  {
    ;
  }

  IntegratedExpressionUnknown(const IntegratedExpressionUnknown& I)
    : IntegratedExpression(I),
      __unknownName(I.__unknownName)
  {
    ;
  }

  ~IntegratedExpressionUnknown()
  {
    ;
  }
};


class IntegratedExpressionTest
  : public IntegratedExpression
{
private:
  const std::string __testFunctionName;

  std::ostream& put(std::ostream& os) const
  {
    os << __testFunctionName;
    return os;
  }

public:
  void __execute()
  {
    ;
  }

  const std::string& testFunctionName() const
  {
    return __testFunctionName;
  }

  IntegratedExpressionTest(const std::string& testFunctionName)
    : IntegratedExpression(IntegratedExpression::testFunction),
      __testFunctionName(testFunctionName)
  {
    ;
  }

  IntegratedExpressionTest(const IntegratedExpressionTest& I)
    : IntegratedExpression(I),
      __testFunctionName(I.__testFunctionName)
  {
    ;
  }

  ~IntegratedExpressionTest()
  {
    ;
  }
};



class IntegratedExpressionFunctionExpression
  : public IntegratedExpression
{
private:
  ReferenceCounting<FunctionExpression> __function;

  std::ostream& put(std::ostream& os) const
  {
    os << (*__function);
    return os;
  }

public:
  void __execute()
  {
    (*__function).execute();
  }

  ReferenceCounting<FunctionExpression> function() const
  {
    return __function;
  }

  IntegratedExpressionFunctionExpression(ReferenceCounting<FunctionExpression> F)
    : IntegratedExpression(IntegratedExpression::function),
      __function(F)
  {
    ;
  }

  IntegratedExpressionFunctionExpression(const IntegratedExpressionFunctionExpression& I)
    : IntegratedExpression(I),
      __function(I.__function)
  {
    ;
  }

  ~IntegratedExpressionFunctionExpression()
  {
    ;
  }
};

#endif // INTEGRATED_EXPRESSION_HPP

