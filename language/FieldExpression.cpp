//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FieldExpression.hpp>

std::ostream&  FieldExpression::
put(std::ostream& os) const
{
  if (__fieldExpression.size()>1) {
    os << '[';
  }

  os << __fieldExpression[0];
  for (size_t i=1; i<__fieldExpression.size(); ++i) {
    os << ',' << __fieldExpression[0];
  }

  if (__fieldExpression.size()>1) {
    os << ']';
  }

  return os;
}

ReferenceCounting<FieldOfScalarFunction>
FieldExpression::
field()
{
  return __field;
}

ConstReferenceCounting<FieldOfScalarFunction>
FieldExpression::
field() const
{
  return __field;
}

void FieldExpression::
__execute()
{
  __field = new FieldOfScalarFunction;
  for (size_t i=0; i<__fieldExpression.size(); ++i) {
    __fieldExpression[i]->execute();
    __field->add(__fieldExpression[i]->function());
  }
}

bool FieldExpression::
hasBoundaryExpression() const
{
  for (size_t i=0; i<__fieldExpression.size(); ++i) {
    if (this->__fieldExpression[i]->hasBoundaryExpression()) {
      return true;
    }
  }
  return false;
}

size_t FieldExpression::
numberOfComponents() const
{
  return __fieldExpression.size();
}

void FieldExpression::
add(ReferenceCounting<FunctionExpression> functionExpression)
{
  __fieldExpression.push_back(functionExpression);
}

FieldExpression::
FieldExpression()
  : Expression(Expression::field)
{
  ;
}

FieldExpression::
~FieldExpression()
{
  ;
}
