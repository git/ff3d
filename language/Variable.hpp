//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include <ReferenceCounting.hpp>
#include <string>

/*!
  \class Variable

  This class is the interface for all variables in the language.

  \author Stephane Del Pino
*/
class Variable
{
public:
  enum Type {
    domain,
    function,
    ifstream,
    mesh,
    ofstream,
    real,
    scene,
    testFuncion,
    unknown,
    unknownList,
    vector3
  };

private:
  //! The type of the variable
  const Variable::Type __type;

  //! the name of the variable
  const std::string __name;

public:
  const Variable::Type& type() const
  {
    return __type;
  }

  const std::string& name() const
  {
    return __name;
  }

  Variable(const std::string& name,
	   const Variable::Type& type)
    : __type(type),
      __name(name)
  {
    ;
  }

  Variable(const Variable& v)
    : __type(v.__type),
      __name(v.__name)
  {
    ;
  }

  virtual ~Variable()
  {
    ;
  }
};


/*!
  \class RealVariable

  This class allow manipulation of real_t variables.

  \author Stephane Del Pino
 */
class RealExpression;
class RealVariable
  : public Variable
{
private:
  ReferenceCounting<RealExpression> __expression;

public:
  ReferenceCounting<RealExpression> expression() const;

  ConstReferenceCounting<RealExpression>
  operator=(ReferenceCounting<RealExpression> e);

  RealVariable(const std::string& name,
	       ReferenceCounting<RealExpression> expression);

  RealVariable(const RealVariable& rv);

  ~RealVariable();
};

/*!
  \class StringVariable

  This class allow manipulation of string variables.

  \author Stephane Del Pino
 */
class StringExpression;
class StringVariable
  : public Variable
{
private:
  ReferenceCounting<StringExpression> __expression;

public:
  ReferenceCounting<StringExpression> expression() const;

  ConstReferenceCounting<StringExpression>
  operator=(ReferenceCounting<StringExpression> e);

  StringVariable(const std::string& name);

  StringVariable(const std::string& name,
		ReferenceCounting<StringExpression> expression);

  StringVariable(const StringVariable& rv);

  ~StringVariable();
};

/*!
  \class Vector3Variable

  This class allow manipulation of Vector3 variables.

  \author Stephane Del Pino
 */
class Vector3Expression;
class Vector3Variable
  : public Variable
{
private:
  ReferenceCounting<Vector3Expression> __expression;

public:
  ReferenceCounting<Vector3Expression> expression() const;

  ConstReferenceCounting<Vector3Expression>
  operator=(ReferenceCounting<Vector3Expression> e);

  Vector3Variable(const std::string& name,
		  ReferenceCounting<Vector3Expression> expression);

  Vector3Variable(const Vector3Variable& rv);

  ~Vector3Variable();
};


/*!
  \class FunctionVariable

  This class allow manipulation of Function variables.

  \author Stephane Del Pino
 */
class FunctionExpression;
class FunctionVariable
  : public Variable
{
private:
  ReferenceCounting<FunctionExpression> __expression;

  // This is used to check if function variable is an Unknown
  bool __subscribed;

public:

  void subscribe()
  {
    __subscribed = true;
  }

  void unsubscribe()
  {
    __subscribed = false;
  }

  const bool& isSubscribed() const
  {
    return __subscribed;
  }

  ReferenceCounting<FunctionExpression> expression() const;

  ConstReferenceCounting<FunctionExpression>
  operator=(ReferenceCounting<FunctionExpression> e);

  FunctionVariable(const std::string& name,
		   ReferenceCounting<FunctionExpression> expression);

  FunctionVariable(const FunctionVariable& rv);

  ~FunctionVariable();
};

/*!
  \class TestFunctionVariable

  This class allow manipulation of Test Functions.

  \author Stephane Del Pino
 */
class TestFunctionVariable
  : public Variable
{
public:
  TestFunctionVariable(const std::string& name);

  TestFunctionVariable(const TestFunctionVariable& rv);

  ~TestFunctionVariable();
};



/*!
  \class MeshVariable

  This class allow manipulation of mesh variables.

  \author Stephane Del Pino
 */
class MeshExpression;
class MeshVariable
  : public Variable
{
private:
  ReferenceCounting<MeshExpression> __expression;

public:
  ReferenceCounting<MeshExpression> expression() const;

  ConstReferenceCounting<MeshExpression>
  operator=(ReferenceCounting<MeshExpression> e);

  MeshVariable(const std::string& name);

  MeshVariable(const std::string& name,
	       ReferenceCounting<MeshExpression> expression);

  MeshVariable(const MeshVariable& rv);

  ~MeshVariable();
};

/*!
  \class SceneVariable

  This class allow manipulation of scene variables.

  \author Stephane Del Pino
 */
class SceneExpression;
class SceneVariable
  : public Variable
{
private:
  ReferenceCounting<SceneExpression> __expression;

public:
  ReferenceCounting<SceneExpression> expression() const;

  ConstReferenceCounting<SceneExpression>
  operator=(ReferenceCounting<SceneExpression> e);

  SceneVariable(const std::string& name);

  SceneVariable(const std::string& name,
		ReferenceCounting<SceneExpression> expression);

  SceneVariable(const SceneVariable& rv);

  ~SceneVariable();
};


/*!
  \class DomainVariable

  This class allow manipulation of domain variables.

  \author Stephane Del Pino
 */
class DomainExpression;
class DomainVariable
  : public Variable
{
private:
  ReferenceCounting<DomainExpression> __expression;

public:
  ReferenceCounting<DomainExpression> expression() const;

  ConstReferenceCounting<DomainExpression>
  operator=(ReferenceCounting<DomainExpression> e);

  DomainVariable(const std::string& name);

  DomainVariable(const std::string& name,
		 ReferenceCounting<DomainExpression> expression);

  DomainVariable(const DomainVariable& rv);

  ~DomainVariable();
};


/*!
  \class OFStreamVariable

  This class allow manipulation of domain variables.

  \author Stephane Del Pino
 */
class OFStreamExpression;
class OFStreamVariable
  : public Variable
{
private:
  ReferenceCounting<OFStreamExpression> __expression;

public:
  ReferenceCounting<OFStreamExpression> expression() const;

  ConstReferenceCounting<OFStreamExpression>
  operator=(ReferenceCounting<OFStreamExpression> e);

  OFStreamVariable(const std::string& name);

  OFStreamVariable(const std::string& name,
		   ReferenceCounting<OFStreamExpression> expression);

  OFStreamVariable(const OFStreamVariable& rv);

  ~OFStreamVariable();
};


/*!
  \class IFStreamVariable

  This class allow manipulation of ifstream variables.

  \author Stephane Del Pino
 */
class IFStreamExpression;
class IFStreamVariable
  : public Variable
{
private:
  ReferenceCounting<IFStreamExpression> __expression;

public:
  ReferenceCounting<IFStreamExpression> expression() const;

  ConstReferenceCounting<IFStreamExpression>
  operator=(ReferenceCounting<IFStreamExpression> e);

  IFStreamVariable(const std::string& name);

  IFStreamVariable(const std::string& name,
		   ReferenceCounting<IFStreamExpression> expression);

  IFStreamVariable(const IFStreamVariable& rv);

  ~IFStreamVariable();
};

#endif // VARIABLE_HPP
