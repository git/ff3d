//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_EXPRESSION_HPP
#define MESH_EXPRESSION_HPP

#include <Expression.hpp>
#include <Vector3Expression.hpp>
#include <RealExpression.hpp>

#include <cmath>
#include <Variable.hpp>

#include <FileDescriptor.hpp>

#include <list>

/**
 * @file   MeshExpression.hpp
 * @author Stephane Del Pino
 * @date   Sat Sep  4 20:02:23 2004
 * 
 * @brief  Mesh expression management
 * 
 */

class Mesh;
class MeshExpression
  : public Expression
{
protected:
  ConstReferenceCounting<Mesh> __mesh;

public:
  enum TypeOfMesh {
    extract,
    octree,
    periodic,
    read,
    simplify,
    surface,
    spectral,
    structured,
    tetrahedrize,
    tetrahedrizeDomain,
    transform,
    undefined,
    unstructured,
    variable
  };

private:
  MeshExpression::TypeOfMesh __typeOfMesh;

public:
  ConstReferenceCounting<Mesh> mesh() const;

  const MeshExpression::TypeOfMesh& typeOfMesh() const
  {
    return __typeOfMesh;
  }

  MeshExpression(const MeshExpression& e);

  MeshExpression(ReferenceCounting<Mesh> m,
		 const MeshExpression::TypeOfMesh& t);

  virtual ~MeshExpression();
};

class DomainExpression;

/*!
  \class MeshExpressionStructured

  This class defines the class of structured Mesh expressions.

  \author Stephane Del Pino
 */
class MeshExpressionStructured
  : public MeshExpression
{
private:
  ReferenceCounting<Vector3Expression> __meshSize;
  ReferenceCounting<Vector3Expression> __corner1;
  ReferenceCounting<Vector3Expression> __corner2;

  std::ostream& put(std::ostream& os) const;

public:
  void __execute();

  MeshExpressionStructured(ReferenceCounting<Vector3Expression> size,
			   ReferenceCounting<Vector3Expression> corner1,
			   ReferenceCounting<Vector3Expression> corner2);

  MeshExpressionStructured(const MeshExpressionStructured& m);

  ~MeshExpressionStructured();
};

/*!
  \class MeshExpressionSpectral

  This class defines the class of structured Mesh expressions.

  \author Stephane Del Pino
 */
class MeshExpressionSpectral
  : public MeshExpression
{
private:
  ReferenceCounting<Vector3Expression> __degree;
  ReferenceCounting<Vector3Expression> __corner1;
  ReferenceCounting<Vector3Expression> __corner2;

  std::ostream& put(std::ostream& os) const;

public:
  void __execute();

  MeshExpressionSpectral(ReferenceCounting<Vector3Expression> degrees,
			 ReferenceCounting<Vector3Expression> corner1,
			 ReferenceCounting<Vector3Expression> corner2);

  MeshExpressionSpectral(const MeshExpressionSpectral& m);

  ~MeshExpressionSpectral();
};

/*!
  \class MeshExpressionSurface

  This class defines the class of Surface Mesh expressions.

  \author Stephane Del Pino
 */
class MeshExpressionSurface
  : public MeshExpression
{
private:
  ReferenceCounting<DomainExpression> __domain;
  ReferenceCounting<MeshExpression> __volumeMesh;

  std::ostream& put(std::ostream& os) const;

  template <typename MeshType>
  void __getSurfaceMesh(MeshType& mesh);

public:
  void __execute();

  MeshExpressionSurface(ReferenceCounting<DomainExpression> domain,
			ReferenceCounting<MeshExpression> volumeMesh);

  MeshExpressionSurface(ReferenceCounting<MeshExpression> volumeMesh);

  MeshExpressionSurface(const MeshExpressionSurface& m);

  ~MeshExpressionSurface();
};

class MeshExpressionOctree
  : public MeshExpression
{
private:
  ReferenceCounting<DomainExpression> __domainExpression;
  ReferenceCounting<MeshExpression> __meshExpression;
  ReferenceCounting<RealExpression> __levelExpression;

  std::ostream& put(std::ostream& os) const;

public:
  void __execute();

  MeshExpressionOctree(ReferenceCounting<DomainExpression> domainExpression,
		       ReferenceCounting<MeshExpression> meshExpression,
		       ReferenceCounting<RealExpression> levelExpression);

  MeshExpressionOctree(const MeshExpressionOctree& m);

  ~MeshExpressionOctree();
};

class MeshExpressionVariable
  : public MeshExpression
{
private:
  const std::string __meshName;
  ReferenceCounting<MeshVariable> __meshVariable;

  std::ostream& put(std::ostream& os) const
  {
    os << __meshVariable->name();
    if (StreamCenter::instance().getDebugLevel() > 3) {
      os << '{' << (*__meshVariable->expression()) << '}';
    }
    return os;
  }

public:
  void __execute();

  MeshExpressionVariable(const std::string& meshName);

  MeshExpressionVariable(const MeshExpressionVariable& e);

  ~MeshExpressionVariable();
};


class MeshExpressionRead
  : public MeshExpression
{
private:
  ReferenceCounting<MeshExpression> __meshVariable;

  ReferenceCounting<FileDescriptor> __fileDescriptor;

  ReferenceCounting<StringExpression> __filename;

  std::ostream& put(std::ostream& os) const;

public:
  void __execute();

  MeshExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		     ReferenceCounting<StringExpression> filename);

  MeshExpressionRead(const MeshExpressionRead& e);

  ~MeshExpressionRead();
};


class MeshExpressionSimplify
  : public MeshExpression
{
private:
  ReferenceCounting<MeshExpression> __originalMesh;

  std::ostream& put(std::ostream& os) const;

public:
  void __execute();

  MeshExpressionSimplify(ReferenceCounting<MeshExpression>);

  MeshExpressionSimplify(const MeshExpressionSimplify& e);

  ~MeshExpressionSimplify();
};

class MeshExpressionExtract
  : public MeshExpression
{
private:
  ReferenceCounting<MeshExpression> __originalMesh;
  ReferenceCounting<RealExpression> __referenceToExtract;

  std::ostream& put(std::ostream& os) const;

  template <typename MeshType>
  void __extract();

public:
  void __execute();

  MeshExpressionExtract(ReferenceCounting<MeshExpression>,
			ReferenceCounting<RealExpression>);

  MeshExpressionExtract(const MeshExpressionExtract& e);

  ~MeshExpressionExtract();
};


class MeshExpressionTetrahedrize
  : public MeshExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "tetrahedrize mesh";
    return os;
  }

  ReferenceCounting<MeshExpression> __inputMesh;

public:
  void __execute();

  MeshExpressionTetrahedrize(ReferenceCounting<MeshExpression> m);

  MeshExpressionTetrahedrize(const MeshExpressionTetrahedrize& m);

  ~MeshExpressionTetrahedrize();
};

/**
 * @class  MeshExpressionTetrahedrizeDomain
 * @author Stephane Del Pino
 * @date   Sat Sep  4 20:00:57 2004
 * 
 * @brief performs a bad quality mesh generation of the domain to
 * remove visualization artifacts. Not to be used for computation.
 * 
 */

class MeshExpressionTetrahedrizeDomain
  : public MeshExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "tetrahedrize domain mesh";
    return os;
  }

  ReferenceCounting<MeshExpression> __inputMesh;
  ReferenceCounting<DomainExpression> __domain;

public:
  void __execute();

  MeshExpressionTetrahedrizeDomain(ReferenceCounting<MeshExpression> m,
				   ReferenceCounting<DomainExpression> d);

  MeshExpressionTetrahedrizeDomain(const MeshExpressionTetrahedrizeDomain& m);

  ~MeshExpressionTetrahedrizeDomain();
};


class MeshExpressionUndefined
  : public MeshExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "undefined mesh";
    return os;
  }

public:

  void __execute()
  {
    ;
  }

  MeshExpressionUndefined();

  MeshExpressionUndefined(const MeshExpressionUndefined& m);

  ~MeshExpressionUndefined();
};

class VerticesSet;
class FieldExpression;
class MeshExpressionTransform
  : public MeshExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "transformed mesh";
    return os;
  }

  ReferenceCounting<MeshExpression> __inputMesh;

  ReferenceCounting<FieldExpression> __transformationField;

public:

  void __execute();

  MeshExpressionTransform(ReferenceCounting<MeshExpression> m,
			  ReferenceCounting<FieldExpression> f);

  MeshExpressionTransform(const MeshExpressionTransform& m);

  ~MeshExpressionTransform();
};

class MeshExpressionPeriodic
  : public MeshExpression
{
public:
  typedef std::list<std::pair<ReferenceCounting<RealExpression>,
			      ReferenceCounting<RealExpression> > >
  MappedReferencesList;

private:
  std::ostream& put(std::ostream& os) const
  {
    os << "periodic mesh";
    return os;
  }

  ReferenceCounting<MeshExpression> __inputMesh;

  ReferenceCounting<MappedReferencesList> __mappedReferences;
public:

  void __execute();

  MeshExpressionPeriodic(ReferenceCounting<MeshExpression> m,
			 ReferenceCounting<MappedReferencesList> references);

  MeshExpressionPeriodic(const MeshExpressionPeriodic& m);

  ~MeshExpressionPeriodic();
};

#endif // _MESH_EXPRESSION_HPP_

