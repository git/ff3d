//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_NORMAL_COMPONENT_HPP
#define FUNCTION_EXPRESSION_NORMAL_COMPONENT_HPP

#include <FunctionExpression.hpp>
#include <ScalarFunctionNormal.hpp>
/**
 * @file   FunctionExpressionNormalComponent.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 00:42:34 2006
 * 
 * @brief  Manages normal-component expressions
 * 
 */
class FunctionExpressionNormalComponent
  : public FunctionExpression
{
private:
  ScalarFunctionNormal::ComponentType __normalType; /**< type of normal component \f$(n_x, n_y \mbox{ or } n_z)\f$ */

public:
  /** 
   * This function requires a boundary
   * 
   * @return @b true
   */
  bool hasBoundaryExpression() const
  {
    return true;
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Copy constructor
   * 
   * @param f the given function
   * 
   */
  FunctionExpressionNormalComponent(const FunctionExpressionNormalComponent& f)
    : FunctionExpression(f),
      __normalType(f.__normalType)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param componentType type of the normal component
   * 
   */
  FunctionExpressionNormalComponent(ScalarFunctionNormal::ComponentType componentType)
    : FunctionExpression(FunctionExpression::normalComponent),
      __normalType(componentType)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionNormalComponent()
  {
    ;
  }
};

#endif // FUNCTION_EXPRESSION_NORMAL_COMPONENT_HPP
