//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _BOOLEAN_EXPRESSION_HPP_
#define _BOOLEAN_EXPRESSION_HPP_

#include <Expression.hpp>
#include <RealExpression.hpp>

class BooleanExpression
  : public Expression
{
protected:
  bool __boolValue;

private:
  std::ostream& put(std::ostream& os) const
  {
    os << __boolValue;
    return os;
  }

public:
  /*!
    Returns the value of the expression.
    \todo Return type should use traits.
   */
  bool boolValue() const
  {
    return __boolValue;
  }

  BooleanExpression(const BooleanExpression& e)
    : Expression(e),
      __boolValue(e.__boolValue)
  {
    ;
  }

  BooleanExpression()
    : Expression(Expression::boolean)
  {
    ;
  }

  virtual ~BooleanExpression()
  {
    ;
  }
};

class BooleanExpressionValue
  : public BooleanExpression
{
public:
  void __execute()
  {
    ;
  }

  BooleanExpressionValue(bool b)
  {
    __boolValue = b;
  }

  BooleanExpressionValue(const BooleanExpressionValue& re)
    : BooleanExpression(re)
  {
    ;
  }

  ~BooleanExpressionValue()
  {
    ;
  }
};

template <bool (*F)(const bool x)>
class BooleanExpressionUnaryOperator
  : public BooleanExpression
{
private:
  ReferenceCounting<BooleanExpression> __r;

public:
  void __execute()
  {
    __r->execute();
    __boolValue = (*F)(__r->boolValue());
  }

  BooleanExpressionUnaryOperator(ReferenceCounting<BooleanExpression> r)
    : __r(r)
  {
    ;
  }

  BooleanExpressionUnaryOperator(const BooleanExpressionUnaryOperator<F>& e)
    : __r(e.__r)
  {
    ;
  }

  ~BooleanExpressionUnaryOperator()
  {
    ;
  }
};

template <bool(*B)(const real_t x, const real_t y)>
class BooleanExpressionCompareOperator
  : public BooleanExpression
{
private:
  ReferenceCounting<RealExpression> __r1;
  ReferenceCounting<RealExpression> __r2;

  real_t __r1Value;
  real_t __r2Value;

public:
  void __execute()
  {
    __r1->execute();
    __r1Value = __r1->realValue();
    __r2->execute();
    __r2Value = __r2->realValue();

    __boolValue = B(__r1Value, __r2Value);
  }

  BooleanExpressionCompareOperator(ReferenceCounting<RealExpression> r1,
				   ReferenceCounting<RealExpression> r2)
    : __r1(r1),
      __r2(r2),
      __r1Value(0),
      __r2Value(0)
  {
    ;
  }

  BooleanExpressionCompareOperator(const BooleanExpressionCompareOperator<B>& e)
    : __r1(e.__r1),
      __r2(e.__r2),
      __r1Value(e.__r1Value),
      __r2Value(e.__r2Value)
  {
    ;
  }

  ~BooleanExpressionCompareOperator()
  {
    ;
  }
};

template <bool(*B)(const bool a, const bool b)>
class BooleanExpressionBinaryOperator
  : public BooleanExpression
{
private:
  ReferenceCounting<BooleanExpression> __r1;
  ReferenceCounting<BooleanExpression> __r2;

public:
  void __execute()
  {
    __r1->execute();
    __r2->execute();
    __boolValue = B(__r1->boolValue(), __r2->boolValue());
  }

  BooleanExpressionBinaryOperator(ReferenceCounting<BooleanExpression> r1,
				  ReferenceCounting<BooleanExpression> r2)
    : __r1(r1),
      __r2(r2)
  {
    ;
  }

  BooleanExpressionBinaryOperator(const BooleanExpressionBinaryOperator<B>& e)
    : __r1(e.__r1),
      __r2(e.__r2)
  {
    ;
  }

  ~BooleanExpressionBinaryOperator()
  {
    ;
  }
};

/*
class BooleanVariable;
class BooleanExpressionVariable
  : public BooleanExpression
{
private:
  ReferenceCounting<BooleanVariable> __booleanVariable;
  ReferenceCounting<BooleanExpression> __expression;

public:
  const real_t value() const;

  void __execute();

  BooleanExpressionVariable(ReferenceCounting<BooleanVariable> r);

  BooleanExpressionVariable(const BooleanExpressionVariable& e);

  ~BooleanExpressionVariable();
};

*/
#endif // _BOOLEAN_EXPRESSION_HPP_

