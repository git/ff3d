//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionLinearBasis.hpp>
#include <ScalarFunctionLinearBasis.hpp>

#include <ErrorHandler.hpp>

void FunctionExpressionLinearBasis::
__execute()
{
  switch (__basisType) {
  case FunctionExpressionLinearBasis::x: {
    __scalarFunction = new ScalarFunctionLinearBasis(ScalarFunctionLinearBasis::x);
    break;
  }
  case FunctionExpressionLinearBasis::y: {
    __scalarFunction = new ScalarFunctionLinearBasis(ScalarFunctionLinearBasis::y);
    break;
  }
  case FunctionExpressionLinearBasis::z: {
    __scalarFunction = new ScalarFunctionLinearBasis(ScalarFunctionLinearBasis::z);
    break;
  }
  }
}

FunctionExpressionLinearBasis::
FunctionExpressionLinearBasis(const std::string& name)
  : FunctionExpression(FunctionExpression::linearBase)
{
  if (name.size() != 1) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Unknown linear expression basis: \""
		       +name+"\"",
		       ErrorHandler::unexpected);
  }
  switch (name[0]) {
  case 'x': {
    __basisType = FunctionExpressionLinearBasis::x;
    break;
  }
  case 'y': {
    __basisType = FunctionExpressionLinearBasis::y;
    break;
  }
  case 'z': {
    __basisType = FunctionExpressionLinearBasis::z;
    break;
  }
  }
}

FunctionExpressionLinearBasis::
FunctionExpressionLinearBasis(const FunctionExpressionLinearBasis& f)
  : FunctionExpression(f),
    __basisType(f.__basisType)
{
  ;
}

FunctionExpressionLinearBasis::
~FunctionExpressionLinearBasis()
{
  ;
}
