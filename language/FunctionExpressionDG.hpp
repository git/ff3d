//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_DG_HPP
#define FUNCTION_EXPRESSION_DG_HPP

#include <FunctionExpression.hpp>

#include <ScalarDiscretizationTypeBase.hpp>

class MeshExpression;

/**
 * @file   FunctionExpressionDG.hpp
 * @author Stephane Del Pino
 * @date   Sun Jan 27 13:17:27 2008
 * 
 * @brief Manipulates discontinuous finite element function
 * expressions
 */
class FunctionExpressionDG
  : public FunctionExpression
{
private:
  const ScalarDiscretizationTypeBase::Type
  __discretizationType;		/**< type of discretization */

  ReferenceCounting<MeshExpression>
  __mesh;			/**< mesh of discretization */

  ReferenceCounting<FunctionExpression>
  __functionExpression;		/**< expression defining the function */

  /** 
   * Converts FEM type to DG type
   * 
   * @param d given fem type
   * 
   * @return dg type
   * 
   * @bug this function should not exist:
   * ScalarDiscretizationTypeBase::Type should no be used within the
   * language
   */
  static ScalarDiscretizationTypeBase::Type
  __DGTypeFromFEMType(const ScalarDiscretizationTypeBase::Type& d);
public:
  /** 
   * Access to the mesh
   * 
   * @bug This function should not exist
   * @return __mesh
   */
  ReferenceCounting<MeshExpression> mesh() const
  {
#warning SHOULD NOT USE THIS FUNCTION
    return __mesh;
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param mesh mesh of discretization
   * @param e expression of the function
   * @param femType discretization type
   * 
   */
  FunctionExpressionDG(ReferenceCounting<MeshExpression> mesh,
		       ReferenceCounting<FunctionExpression> e,
		       const ScalarDiscretizationTypeBase::Type& femType);

  /** 
   * Copy constructor
   * 
   * @param f given finite element function expression
   * 
   */
  FunctionExpressionDG(const FunctionExpressionDG& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionDG();
};

#endif // FUNCTION_EXPRESSION_DG_HPP
