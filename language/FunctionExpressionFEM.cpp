//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionFEM.hpp>

#include <MeshExpression.hpp>
#include <Information.hpp>

#include <FEMFunctionBuilder.hpp>
#include <FEMFunctionBase.hpp>

#include <ScalarFunctionBase.hpp>

#include <Mesh.hpp>

#include <ScalarDiscretizationTypeFEM.hpp>

void FunctionExpressionFEM::__execute()
{
  __mesh->execute();

  Information::instance().setMesh(__mesh->mesh());

  FEMFunctionBuilder builder;

  if (__functionExpression != 0) {
    __functionExpression->execute();

    builder.build(ScalarDiscretizationTypeFEM(__discretizationType),
		  __mesh->mesh(),
		  *(__functionExpression->function()));
  } else {
    builder.build(ScalarDiscretizationTypeFEM(__discretizationType),
		  __mesh->mesh());
  }

  __scalarFunction = builder.getBuiltScalarFunction();

  // function has now been evaluated.
  __functionExpression = 0;
  Information::instance().unsetMesh();

}

FunctionExpressionFEM::
FunctionExpressionFEM(ReferenceCounting<MeshExpression> mesh,
		      ReferenceCounting<FunctionExpression> function,
		      const ScalarDiscretizationTypeBase::Type& discretizationType)
  : FunctionExpression(FunctionExpression::fem),
    __discretizationType(discretizationType),
    __mesh(mesh),
    __functionExpression(function)
{
  ;
}

FunctionExpressionFEM::
FunctionExpressionFEM(const FunctionExpressionFEM& femFunction)
  : FunctionExpression(femFunction),
    __discretizationType(femFunction.__discretizationType),
    __mesh(femFunction.__mesh),
    __functionExpression(femFunction.__functionExpression)
{
  ;
}

FunctionExpressionFEM::~FunctionExpressionFEM()
{
  ;
}
