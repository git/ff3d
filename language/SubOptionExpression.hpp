//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _SUB_OPTION_EXPRESSION_HPP_
#define _SUB_OPTION_EXPRESSION_HPP_

#include <Expression.hpp>
#include <StringExpression.hpp>
#include <RealExpression.hpp>

class SubOptionExpression
  : public Expression
{
public:
  enum SOEType {
    stringValue,
    realValue,
    integerValue
  };

protected:
  ReferenceCounting<StringExpression> __name;

  virtual void setOption(std::string& name) = 0;

private:
  SubOptionExpression::SOEType __type;

public:
  void evaluate(const std::string& basename)
  {
    std::string name = basename;
    name += "::";
    name += (*__name).value();

    setOption(name);
  }

  SubOptionExpression(const SubOptionExpression& soe)
    : Expression(soe),
      __name(soe.__name),
      __type(soe.__type)
  {
    ;
  }

  SubOptionExpression(ReferenceCounting<StringExpression> name,
		      SubOptionExpression::SOEType type)
    : Expression(Expression::subOption),
      __name(name),
      __type(type)
  {
    ;
  }

  virtual ~SubOptionExpression()
  {
    ;
  }
};

class SubOptionExpressionString
  : public SubOptionExpression
{
private:
  ReferenceCounting<StringExpression> __value;

  std::ostream& put(std::ostream& os) const
  {
    os << *__name << "::" << *__value;
    return os;
  }

  void setOption(std::string& name);

public:
  void __execute()
  {
    (*__name).execute();
    (*__value).execute();
  }

  SubOptionExpressionString(ReferenceCounting<StringExpression> name,
			    ReferenceCounting<StringExpression> value)
    : SubOptionExpression(name, SubOptionExpression::stringValue),
      __value(value)
  {
    ;
  }
};


class SubOptionExpressionReal
  : public SubOptionExpression
{
private:
  ReferenceCounting<RealExpression> __value;

  std::ostream& put(std::ostream& os) const
  {
    os << *__name << "::" << *__value;
    return os;
  }

  void setOption(std::string& name);

public:
  void __execute()
  {
    (*__name).execute();
    (*__value).execute();
  }

  SubOptionExpressionReal(ReferenceCounting<StringExpression> name,
			  ReferenceCounting<RealExpression> value)
    : SubOptionExpression(name, SubOptionExpression::realValue),
      __value(value)
  {
    ;
  }
};

#endif // _SUB_OPTION_EXPRESSION_HPP_

