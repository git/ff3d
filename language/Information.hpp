//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef INFORMATION_HPP
#define INFORMATION_HPP

#include <ReferenceCounting.hpp>
class Mesh;
class Scene;
class UnknownListExpression;

#include <stack>
#include <StaticBase.hpp>

/**
 * @file   Information.hpp
 * @author Stephane Del Pino
 * @date   Sun Nov 28 15:07:51 2004
 * 
 * @brief Contains informations related to ff3d's behaviour
 * 
 * 
 */
class Information
  : public StaticBase<Information>
{
private:
  /// if not NULL contains the Mesh which is used in the instruction
  std::stack<ConstReferenceCounting<Mesh> > __mesh;

  /// the last defined sceneExpression.
  ConstReferenceCounting<Scene> __scene;

  /// The current Unknown list.
  ReferenceCounting<UnknownListExpression> __unknownListExpression;

  /// Choose which algorithm is used for surface mesh generation. If
  /// true, does not use mesh intersection algorithm.
  bool __coarseMesh;

public:
  /** 
   * Read only access to the mesh on the top of the stack
   * 
   * @return latest mesh
   */
  ConstReferenceCounting<Mesh> getMesh();

  /** 
   * Sets a new mesh
   * 
   * @param mesh a mesh
   */
  void setMesh(ConstReferenceCounting<Mesh> mesh);

  /** 
   * pops the mesh on the top of the stack
   * 
   */
  void unsetMesh();

  /** 
   * Checks if a mesh is in use
   * 
   * @return true if the stack is not empty
   */
  bool usesMesh() const;

  /** 
   * Access to the unknown list expression
   * 
   * @return __unknownListExpression
   */
  ReferenceCounting<UnknownListExpression> getUnknownList();

  /** 
   * Set the unknown list expression
   * 
   * @param ul the unknown list
   */
  void setUnknownList(ReferenceCounting<UnknownListExpression> ul);

  /** 
   * Unset the unknown list expression
   * 
   */
  void unsetUnknownList();

  /** 
   * Checks if an unknown list expression is in use
   * 
   * @return @b true if an unknown list is in use
   */
  bool usesUnknownList() const;

  /** 
   * Access to the scene
   * 
   * @return __scene
   */
  ConstReferenceCounting<Scene> getScene();

  /** 
   * Set a scene
   * 
   * @param scene a given Scene
   */
  void setScene(ConstReferenceCounting<Scene> scene);

  /** 
   * Unset the scene
   * 
   */
  void unsetScene();

  /** 
   * Checks if a Scene is in use
   * 
   * @return @b true if __scene in non null
   */
  bool usesScene() const;

  /** 
   * Sets the coarse mesh parameter
   * 
   * @param b 
   */
  void setCoarseMesh(const bool& b);

  /** 
   * read-only access to __coarseMesh
   * 
   * @return __coarseMesh
   */
  const bool& coarseMesh() const;

  /** 
   * Constructor
   * 
   */
  Information();

  /** 
   * Copy constructor
   * 
   * @param I an Information
   * 
   */
  Information(const Information& I);

  /** 
   * Destructor
   * 
   */
  ~Information();
};

#endif // INFORMATION_HPP
