//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MULTI_LINEAR_FORM_EXPRESSION_HPP
#define MULTI_LINEAR_FORM_EXPRESSION_HPP

#include <Expression.hpp>
#include <MultiLinearExpressionSum.hpp>

#include <BoundaryExpression.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>


class MultiLinearFormExpression
  : public Expression
{
public:
  enum FormType {
    oneD,
    twoD,
    threeD
  };

private:
  ReferenceCounting<BoundaryExpression> __boundary;
  ReferenceCounting<MultiLinearExpressionSum> __multiLinearExpression;
  const FormType __formType;

  std::ostream& put(std::ostream& os) const
  {
    switch(__formType) {
    case oneD: {
      os << "int1d(";
      break;
    }
    case twoD: {
      os << "int2d(" << *__boundary << ")(";
      break;
    }
    case threeD: {
      os << "int3d(";
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }

    os << *__multiLinearExpression << ')';

    return os;
  }

public:
  bool hasPOVBoundary() const
  {
    if (__boundary != 0)
      return (*__boundary).hasPOVBoundary();
    else
      return false;
  }

  ReferenceCounting<BoundaryExpression> boundaryExpression()
  {
    return __boundary;
  }

  ReferenceCounting<MultiLinearExpressionSum> multiLinearExpression()
  {
    return __multiLinearExpression;
  }

  void __execute()
  {
    (*__multiLinearExpression).execute();
    if(__boundary != 0) {
      (*__boundary).execute();
    }
  }

  const FormType& formType() const
  {
    return __formType;
  }

  MultiLinearFormExpression(ReferenceCounting<MultiLinearExpressionSum> me,
			    const FormType t,
			    ReferenceCounting<BoundaryExpression> b = 0)
    : Expression(Expression::multiLinearForm),
      __boundary(b),
      __multiLinearExpression(me),
      __formType(t)
  {
    //! checks the validity of the expression
    (*me).check();
  }

  MultiLinearFormExpression(const MultiLinearFormExpression& m)
    : Expression(m),
      __boundary(m.__boundary),
      __multiLinearExpression(m.__multiLinearExpression),
      __formType(m.__formType)
  {
    ;
  }

  ~MultiLinearFormExpression()
  {
    ;
  }
};


class MultiLinearFormSumExpression
  : public Expression
{
private:
  typedef std::list<ReferenceCounting<MultiLinearFormExpression> > ListType;

public:
  typedef ListType::iterator iterator;

private:
  ListType __listPlus;
  ListType __listMinus;

  std::ostream& put(std::ostream& os) const
  {
    bool first = true;

    for (ListType::const_iterator i = __listPlus.begin();
	 i != __listPlus.end(); ++i) {
      if (!first) {
	os << '+';
      } else {
	first = false;
      }
      os << *(*i);
    }

    for (ListType::const_iterator i = __listMinus.begin();
	 i != __listMinus.end(); ++i) {
      os << '-' << *(*i);
    }
    
    return os;
  }

public:
  bool hasPOVBoundary() const
  {
    for (ListType::const_iterator i = __listMinus.begin();
	 i != __listMinus.end(); ++i) {
      if ((**i).hasPOVBoundary()) return true;
    }

    for (ListType::const_iterator i = __listPlus.begin();
	 i != __listPlus.end(); ++i) {
      if ((**i).hasPOVBoundary()) return true;
    }

    return false;
  }

  void clear()
  {
    __listPlus.clear();
    __listMinus.clear();
  }

  const MultiLinearFormSumExpression&
  operator-=(MultiLinearFormSumExpression& M)
  {
    for (MultiLinearFormSumExpression::iterator i = M.plusBegin();
	 i != M.plusEnd(); ++i) {
      __listMinus.push_back(*i);
    }

    for (MultiLinearFormSumExpression::iterator i = M.minusBegin();
	 i != M.minusEnd(); ++i) {
      __listPlus.push_back(*i);
    }

    return *this;
  }

  MultiLinearFormSumExpression::iterator
  plusBegin()
  {
    return __listPlus.begin();
  }

  MultiLinearFormSumExpression::iterator
  plusEnd()
  {
    return __listPlus.end();
  }


  MultiLinearFormSumExpression::iterator
  minusBegin()
  {
    return __listMinus.begin();
  }

  MultiLinearFormSumExpression::iterator
  minusEnd()
  {
    return __listMinus.end();
  }

  void __execute()
  {
    for (ListType::iterator i = __listPlus.begin();
	 i != __listPlus.end(); ++i) {
      (*(*i)).execute();
    }

    for (ListType::iterator i = __listMinus.begin();
	 i != __listMinus.end(); ++i) {
      (*(*i)).execute();
    }
  }

  void plus(ReferenceCounting<MultiLinearFormExpression> ml)
  {
    __listPlus.push_back(ml);
  }

  void minus(ReferenceCounting<MultiLinearFormExpression> ml)
  {
    __listMinus.push_back(ml);
  }

  MultiLinearFormSumExpression()
    : Expression(Expression::multiLinearFormSum)
  {
    ;
  }

  MultiLinearFormSumExpression(const MultiLinearFormSumExpression& m)
    : Expression(m),
      __listPlus(m.__listPlus),
      __listMinus(m.__listMinus)
  {
    ;
  }

  ~MultiLinearFormSumExpression()
  {
    ;
  }

};

#endif // MULTI_LINEAR_FORM_EXPRESSION_HPP

