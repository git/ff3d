/* -*- c++ -*- */

/* $Id$ */

 // $Log$
 // Revision 1.16  2000/11/29 15:12:36  delpino
 // Fix.
 //
 // Revision 1.15  2000/11/28 12:40:31  delpino
 // Small fixes and make code closer to C++ norm in order to compile using GCC 3.0
 // and CodeWarrior 6.
 //
 // Revision 1.14  2000/11/20 16:24:29  delpino
 // Use now 'derive(u,x)' instead of 'dx(u)' for computation of derivative.
 //
 // Revision 1.13  2000/11/13 16:00:28  delpino
 // Added Domain and began Multiple methode implementation.
 //
 // Revision 1.12  2000/10/29 23:53:51  delpino
 // Added the 'in' keyword.
 //
 // Revision 1.11  2000/10/03 15:56:20  delpino
 // Added dxx dyy and dzz, but they are not already used.
 //
 // Revision 1.10  2000/09/20 08:57:31  delpino
 // Added dx(.), dy(.), dz(.) functions and boolean functions.  Now user can
 // compute partial derivatives of Q1 functions and define function and define
 // "boolean" functions the following way: one((x>0)&&(y*x<1)).
 //
 // Revision 1.9  2000/08/25 15:15:11  delpino
 // Add token '^' which was missing.
 //
 // Revision 1.8  2000/08/22 15:30:00  delpino
 // Language changes:
 // - 'd u/dn' is obsolete, one should use 'dnu(u)' to define
 //   co-normal for Neumann boundary conditions.
 // - faces of 3D Mesh are no longer called by numbers but using
 //   the more friendly 'xmin', 'xmax', 'ymin',... keywords
 // - starting point of gradient could be defined the following
 //   way: solve(u),u=f { ... }
 //
 // Revision 1.7  2000/08/17 19:40:03  delpino
 // add 'exp()', better string management (concatenation and conversion
 // from exp using 'str()' are now possible) and system calls are now possible.
 //
 // Revision 1.6  2000/08/08 10:16:48  delpino
 // Add Mass Operator into freefem language.
 //
 // Revision 1.5  2000/08/05 09:44:14  delpino
 // Add dxread keyword to read variable in a file.
 //
 // Revision 1.4  2000/08/04 22:43:25  delpino
 // Add abs function.
 //
 // Revision 1.3  2000/07/11 13:30:29  delpino
 // Add 'do {...} while (...)' loops.

%x blocskip
%{ 
  #include <PDEProblem.hpp>
  #include <PDEOperator.hpp>
  #include <UserFunction.hpp>
  #include <parse.ff.hpp>

  #include <UserDefine.hpp>

  #include "parse.h"
  #include <stdio.h>

  #include <stack>

  int mylineno = 0;
  bool declaration = false;
  int declaration_type = -1;

  using namespace std;
%}

%option outfile="lex.yy.c"
%option yylineno
%option prefix="ff"
%option noyywrap

/* Using this option is slower than default but IS *NEEDED* to deal
   with loops. If not buffers are scan from disk one for all and then
   getting back to WHILE position is not possible. A trick may exist,
   but as files are not so big this proceeding this way is not a problem */
%option always-interactive
     
string  \"[^\n"]+\"
comment "//".*"\n"|"/*".*"*/"
     
ws      [ \t]+
     
alpha   [A-Za-z]
dig     [0-9]
name    ({alpha}|{dig}|\$)({alpha}|{dig}|[_.$])*
num1    {dig}+\.?([eE][-+]?{dig}+)?
num2    {dig}*\.{dig}+([eE][-+]?{dig}+)?
oper    [\+\-\/\*\=]
separ   [","";""("")"]
param   ["x""y""z"]
number  {num1}|{num2}

%%

{ws}    /* skip blanks and tabs */

{comment}


"do"  {
         // store position for DO {...} WHILE (...) loops
         fflval.isd = new IStreamDescriptor;
	 fflval.isd->stream_pos = yyin->tellg() - yy_n_chars;
	 fflval.isd->input = &yyin;
         return DO;
       }

"while"  {
           // store position for WHILE (...) DO {...} loops
           fflval.isd = new IStreamDescriptor;
           fflval.isd->stream_pos = yyin->tellg() - yy_n_chars;
	   fflval.isd->input = &yyin;
	   return WHILE;
         }

<*>"\n"     { /* skip new lines */
              mylineno++;
            }

<*>"{"   {
	    return '{';
         }

<blocskip>[^{^}^\n]* {
                       cerr << mylineno << ": not executing\n";
                     }

<*>"}"   {
            return '}';
         }


 /*********************/
 /* Boolean operators */
 /*********************/

"!"  { return NOT; }

"<=" {
       return LEQ;
     }

">=" {
       return GEQ;
     }

"<" {
       return LT;
     }

">" {
       return GT;
     }

"==" {
       return EQ;
     }


"!=" {
       return NE;
     }

"&&" {
       return AND;
     }

"||" {
       return OR;
     }

"true" {
         return TRUE;
       }

"false" {
         return FALSE;
       }

"^" {
         return '^';
    }

"++" { return INCR; }

"--" { return DECR; }

"double" {
             declaration = true;
             declaration_type = VAR;
             return DOUBLE;
           }

"function" {
             declaration = true;
             declaration_type = FNCTID;
             return FUNCTION;
           }

"array" {
             declaration = true;
             declaration_type = FNCTID;
             return ARRAY;
           }

"structmesh" {
               declaration = true;
               declaration_type = MESHID;
               return STRUCTMESH;
             }

"scene" {
           declaration = true;
           declaration_type = SCENEID;
           return SCENE;
        }

"domain" {
           declaration = true;
           declaration_type = DOMID;
           return DOM;
         }


"solve" {
	  declaration = true;
          declaration_type = UNKNOWN;
	  return SOLVE;
	}

"method" {
           return METHOD;
         }


"penalized" {
              return PENALIZED;
            }

"immersed" {
             return IMMERSED;
           }

"one" {
        return ONE;
      }

"abs" {
        return ABS;
      }

"sin" {
        return SIN;
      }

"exp" {
        return EXP;
      }

"cos" {
        return COS;
      }


"tan" {
        return TAN;
      }

"asin" {
        return ASIN;
      }

"acos" {
        return ACOS;
      }


"atan" {
        return ATAN;
      }


"sqrt" {
        return SQRT;
      }

"Linf"  {
          return LINFNORM;
        }

"L2"    {
          return L2NORM;
        }

"dxplot" {
            return DXPLOT;
         }

"dxread" {
            return DXREAD;
         }

"povplot" {
            return POVPLOT;
          }

"print" {
          return PRINT;
        }

"str" {
        return STR;
      }

"system" {
          return SYSTEM;
        }

"on"    {
	  return ON;
	}

"in"    {
          return IN;
        }

"by"    {
          return BY;
        }

"inside" {
           return INSIDE;
         }

"outside" {
            return OUTSIDE;
          }

"convect" {
            return CONVECT;
          }

"div" 	{
	  return DIV;
	}

"d"     {
          return DROND;
        }

"dnu"    {
          return DNU;
        }


 /* first order partial differential operators. */

"dx"    {
          return DX;
        }

"dy"    {
          return DY;
        }

"dz"    {
          return DZ;
        }

 /* second order partial differential operators. */

"dxx"   {
          return DXX;
        }

"dxy"   {
          return DXY;
        }

"dxz"   {
          return DXZ;
        }

"dyx"   {
          return DYX;
        }

"dyy"   {
          return DYY;
        }

"dyz"   {
          return DYZ;
        }

"dzx"   {
          return DZX;
        }

"dzy"   {
          return DZY;
        }

"dzz"   {
          return DZZ;
        }

"grad" 	{
	  return GRAD;
	}

"derive" {
           return DERIVE;
         }

"vertex"|"vector" {
           declaration = true;
           declaration_type = VECTID;
           return VECTOR;
         }

"xmin" { return XMIN; }

"ymin" { return YMIN; }

"zmin" { return ZMIN; }

"xmax" { return XMAX; }

"ymax" { return YMAX; }

"zmax" { return ZMAX; }

{number} {
           sscanf(yytext,"%lf",&fflval.val);
           return NUM;
         }

{separ}  {
           return yytext[0];
         }

{oper}  { return yytext[0]; }

{param} {
          switch (yytext[0]) {
	  case 'x': {
	    return XX;
	  }
	  case 'y': {
	    return YY;
	  }
	  case 'z': {
	    return ZZ;
	  }
	  }
        }

{name}   {
           UserDefine* ud = first->Get(yytext);
           if (ud == NULL) {
             if (declaration)
               ud = first->Add(yytext, declaration_type);
             else {
               cerr << mylineno << ':' << yytext << " is not defined\n";
               exit(1);
             }
             declaration = false;
             declaration_type = -1;
           }
           fflval.tptr = ud;
	   return ud->Type();
         }

{string} {
           fflval.str = new char[strlen(yytext)-1];
           for (size_t i=0; i<strlen(yytext)-2; i++) {
             fflval.str[i]=yytext[i+1];
           }
	   fflval.str[strlen(yytext)-2] = '\0';
           return STRING;
         }

%%
