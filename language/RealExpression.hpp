//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef REAL_EXPRESSION_HPP
#define REAL_EXPRESSION_HPP

#include <Types.hpp>
#include <ErrorHandler.hpp>

#include <Expression.hpp>

#include <cmath>

#include <Variable.hpp>

#include <ScalarDiscretizationTypeBase.hpp>

class RealExpression
  : public Expression
{
protected:
  //! The value.
  real_t __realValue;

private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->realValue();
    return os;
  }

public:
  virtual ReferenceCounting<RealExpression> value() = 0;

  /** 
   * Read only access to the real value
   * 
   * @todo Return type should use traits.
   * @return the value of the expression.
   */
  inline const real_t& realValue() const
  {
    return __realValue;
  }

  /** 
   * Access to the real value
   * 
   * @todo Return type should use traits.
   * @return the value of the expression.
   */
  inline real_t& realValue()
  {
    return __realValue;
  }

  RealExpression(const RealExpression& e)
    : Expression(e),
      __realValue(e.__realValue)
  {
    ;
  }

  RealExpression()
    : Expression(Expression::real),
      __realValue(0)
  {
    ;
  }

  virtual ~RealExpression()
  {
    ;
  }
};

class RealExpressionValue
  : public RealExpression
{
public:
  ReferenceCounting<RealExpression> value()
  {
    return this;
  }

  void __execute()
  {
    ;
  }

  RealExpressionValue(const real_t d)
  {
    __realValue = d;
  }

  RealExpressionValue(const RealExpressionValue& re)
  {
    ;
  }

  ~RealExpressionValue()
  {
    ;
  }
};

class BooleanExpression;
class RealExpressionBoolean
  : public RealExpression
{
private:
  ReferenceCounting<BooleanExpression> __booleanExpression;

public:
  ReferenceCounting<RealExpression> value();

  void __execute();

  RealExpressionBoolean(ReferenceCounting<BooleanExpression> be);

  RealExpressionBoolean(const RealExpressionBoolean& re);

  ~RealExpressionBoolean();
};

class RealExpressionVariable
  : public RealExpression
{
private:
  std::string __variableName;
  ReferenceCounting<RealVariable> __realVariable;
  ReferenceCounting<RealExpression> __expression;

  //! Read *this to the input is.
  std::istream& _get(std::istream& is);

public:
  friend class RealExpressionPreIncrement;
  friend class RealExpressionPostIncrement;
  friend class RealExpressionPreDecrement;
  friend class RealExpressionPostDecrement;

  ReferenceCounting<RealExpression> value();

  void __execute();

  RealExpressionVariable(const std::string& variableName);

  RealExpressionVariable(const RealExpressionVariable& e);

  ~RealExpressionVariable();
};

class FunctionExpression;
class Vector3Expression;

/**
 * @note This class implementation is ugly
 * 
 */
class RealExpressionFunctionEvaluate
  : public RealExpression
{
private:
  ReferenceCounting<FunctionExpression> __realFunction;
  ReferenceCounting<Vector3Expression> __v;
  ReferenceCounting<RealExpression> __x;
  ReferenceCounting<RealExpression> __y;
  ReferenceCounting<RealExpression> __z;

public:
  ReferenceCounting<RealExpression> value();

  void __execute();

  RealExpressionFunctionEvaluate(ReferenceCounting<FunctionExpression> f,
				 ReferenceCounting<RealExpression> x,
				 ReferenceCounting<RealExpression> y,
				 ReferenceCounting<RealExpression> z);

  RealExpressionFunctionEvaluate(ReferenceCounting<FunctionExpression> f,
				 ReferenceCounting<Vector3Expression> v);

  RealExpressionFunctionEvaluate(const RealExpressionFunctionEvaluate& e);

  ~RealExpressionFunctionEvaluate();
};

class RealExpressionIntegrate
  : public RealExpression
{
private:
  ReferenceCounting<FunctionExpression> __realFunction;
  ReferenceCounting<MeshExpression> __mesh;
  const ScalarDiscretizationTypeBase::Type __discretizationType;
  ReferenceCounting<Vector3Expression> __integrationDegrees;

  template <typename MeshType, typename QuadratureType>
  real_t __integrate(const MeshType& M, const QuadratureType& Q, FunctionExpression& f);

  template <typename MeshType>
  real_t __integrate(const MeshType& M, FunctionExpression& f);

public:
  ReferenceCounting<RealExpression> value();

  void __execute();

  RealExpressionIntegrate(ReferenceCounting<FunctionExpression> f,
			  ReferenceCounting<MeshExpression> m,
			  const ScalarDiscretizationTypeBase::Type& discretizationType = ScalarDiscretizationTypeBase::lagrangianFEM1,
			  ReferenceCounting<Vector3Expression> integrationDegrees = 0);

  RealExpressionIntegrate(const RealExpressionIntegrate& e);

  ~RealExpressionIntegrate();
};

class RealExpressionMinMax
  : public RealExpression
{
private:
  const std::string __operatorName;

  ReferenceCounting<FunctionExpression> __realFunction;
  ReferenceCounting<MeshExpression> __mesh;

public:
  ReferenceCounting<RealExpression> value();

  void __execute();

  RealExpressionMinMax(const std::string& operatorName,
		       ReferenceCounting<FunctionExpression> f,
		       ReferenceCounting<MeshExpression> m);

  RealExpressionMinMax(const RealExpressionMinMax& e);

  ~RealExpressionMinMax();
};

class RealExpressionPreIncrement
  : public RealExpression
{
private:
  ReferenceCounting<RealExpressionVariable> __variable;

public:
  ReferenceCounting<RealExpression> value()
  {
    return new RealExpressionValue(__realValue);
  }

  void __execute()
  {
    (*__variable).execute();
    __realValue = (*__variable).realValue()+1;
    (*(*__variable).__realVariable) = new RealExpressionValue(__realValue);
  }

  RealExpressionPreIncrement(ReferenceCounting<RealExpressionVariable> v)
    : __variable(v)
  {
    ;
  }

  RealExpressionPreIncrement(const RealExpressionPreIncrement& r)
    : RealExpression(r),
      __variable(r.__variable)
  {
    ;
  }

  ~RealExpressionPreIncrement()
  {
    ;
  }
};

class RealExpressionPostIncrement
  : public RealExpression
{
private:
  ReferenceCounting<RealExpressionVariable> __variable;

public:
  ReferenceCounting<RealExpression> value()
  {
    return new RealExpressionValue(__realValue);
  }

  void __execute()
  {
    (*__variable).execute();
    __realValue = (*__variable).realValue();
    (*(*__variable).__realVariable) = new RealExpressionValue(__realValue+1);
  }

  RealExpressionPostIncrement(ReferenceCounting<RealExpressionVariable> v)
    : __variable(v)
  {
    ;
  }

  RealExpressionPostIncrement(const RealExpressionPostIncrement& r)
    : RealExpression(r),
      __variable(r.__variable)
  {
    ;
  }

  ~RealExpressionPostIncrement()
  {
    ;
  }
};

class RealExpressionPreDecrement
  : public RealExpression
{
private:
  ReferenceCounting<RealExpressionVariable> __variable;

public:
  ReferenceCounting<RealExpression> value()
  {
    return this;
  }

  void __execute()
  {
    (*__variable).execute();
    __realValue = (*__variable).realValue()-1;
    (*(*__variable).__realVariable) = new RealExpressionValue(__realValue);
  }

  RealExpressionPreDecrement(ReferenceCounting<RealExpressionVariable> v)
    : __variable(v)
  {
    ;
  }

  RealExpressionPreDecrement(const RealExpressionPreDecrement& r)
    : RealExpression(r),
      __variable(r.__variable)
  {
    ;
  }

  ~RealExpressionPreDecrement()
  {
    ;
  }
};

class RealExpressionPostDecrement
  : public RealExpression
{
private:
  ReferenceCounting<RealExpressionVariable> __variable;

public:
  ReferenceCounting<RealExpression> value()
  {
    return this;
  }

  void __execute()
  {
    (*__variable).execute();
    __realValue = (*__variable).realValue();
    (*(*__variable).__realVariable) = new RealExpressionValue(__realValue-1);
  }

  RealExpressionPostDecrement(ReferenceCounting<RealExpressionVariable> v)
    : __variable(v)
  {
    ;
  }

  RealExpressionPostDecrement(const RealExpressionPostDecrement& r)
    : RealExpression(r),
      __variable(r.__variable)
  {
    ;
  }

  ~RealExpressionPostDecrement()
  {
    ;
  }
};

class RealExpressionCFunction
  : public RealExpression
{
private:
  const std::string __cfunction;
  ReferenceCounting<RealExpression> __r;

public:
  ReferenceCounting<RealExpression> value()
  {
    return new RealExpressionValue(__realValue);
  }

  void __execute()
  {
    (*__r).execute();
    if (__cfunction == "abs") {
      __realValue = std::abs(__r->realValue());
      return;
    }
    if (__cfunction == "sin") {
      __realValue = std::sin(__r->realValue());
      return;
    }
    if (__cfunction == "cos") {
      __realValue = std::cos(__r->realValue());
      return;
    }
    if (__cfunction == "tan") {
      __realValue = std::tan(__r->realValue());
      return;
    }
    if (__cfunction == "asin") {
      __realValue = std::asin(__r->realValue());
      return;
    }
    if (__cfunction == "acos") {
      __realValue = std::acos(__r->realValue());
      return;
    }
    if (__cfunction == "atan") {
      __realValue = std::atan(__r->realValue());
      return;
    }
    if (__cfunction == "sqrt") {
      __realValue = std::sqrt(__r->realValue());
      return;
    }
    if (__cfunction == "exp") {
      __realValue = std::exp(__r->realValue());
      return;
    }
    if (__cfunction == "log") {
      __realValue = std::log(__r->realValue());
      return;
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }

  RealExpressionCFunction(const std::string& cfuntion,
			  ReferenceCounting<RealExpression> r)
    : __cfunction(cfuntion),
      __r(r)
  {
    ;
  }

  RealExpressionCFunction(const RealExpressionCFunction& e)
    : __cfunction(e.__cfunction),
      __r(e.__r)
  {
    ;
  }

  ~RealExpressionCFunction()
  {
    ;
  }
};

template <real_t (*F)(const real_t x)>
class RealExpressionUnaryOperator
  : public RealExpression
{
private:
  ReferenceCounting<RealExpression> __r;

public:
  ReferenceCounting<RealExpression> value()
  {
    return new RealExpressionValue(__realValue);
  }

  void __execute()
  {
    (*__r).execute();
    __realValue = (*F)((*__r).realValue());
  }

  RealExpressionUnaryOperator(ReferenceCounting<RealExpression> r)
    : __r(r)
  {
    ;
  }

  RealExpressionUnaryOperator(const RealExpressionUnaryOperator<F>& e)
    : __r(e.__r)
  {
    ;
  }

  ~RealExpressionUnaryOperator()
  {
    ;
  }
};

template <real_t(*B)(const real_t x, const real_t y)>
class RealExpressionBinaryOperator
  : public RealExpression
{
private:
  ReferenceCounting<RealExpression> __r1;
  ReferenceCounting<RealExpression> __r2;

public:
  ReferenceCounting<RealExpression> value()
  {
    return new RealExpressionValue(__realValue);
  }

  void __execute()
  {
    (*__r1).execute();
    (*__r2).execute();
    __realValue = B((*__r1).realValue(), (*__r2).realValue());
  }

  RealExpressionBinaryOperator(ReferenceCounting<RealExpression> r1,
			       ReferenceCounting<RealExpression> r2)
    : __r1(r1),
      __r2(r2)
  {
    ;
  }

  RealExpressionBinaryOperator(const RealExpressionBinaryOperator<B>& e)
    : __r1(e.__r1),
      __r2(e.__r2)
  {
    ;
  }

  ~RealExpressionBinaryOperator()
  {
    ;
  }
};

#endif // REAL_EXPRESSION_HPP
