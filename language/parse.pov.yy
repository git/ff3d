%{ /* -*- c++ -*- */

   /*
    *  This file is part of ff3d - http://www.freefem.org/ff3d
    *  Copyright (C) 2001, 2002, 2003 St�phane Del Pino
    * 
    *  This program is free software; you can redistribute it and/or modify
    *  it under the terms of the GNU General Public License as published by
    *  the Free Software Foundation; either version 2, or (at your option)
    *  any later version.
    * 
    *  This program is distributed in the hope that it will be useful,
    *  but WITHOUT ANY WARRANTY; without even the implied warranty of
    *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *  GNU General Public License for more details.
    * 
    *  You should have received a copy of the GNU General Public License
    *  along with this program; if not, write to the Free Software Foundation,
    *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
    * 
    *  $Id$
    * 
    */

#define YYDEBUG 1

#include <cstdlib>
#include <sstream>

#include <iostream>
#include <stack>
#include <cctype>
#include <string>

#include <parse.pov.hpp>
#include <SceneBox.hpp>

#include <Scene.hpp>

#include <Sphere.hpp>
#include <Cube.hpp>
#include <Cylinder.hpp>
#include <Cone.hpp>
#include <Plane.hpp>
#include <Torus.hpp>

#include <Not.hpp>
#include <Union.hpp>
#include <Intersection.hpp>
#include <Difference.hpp>

#include <stdlib.h>
#include <stdio.h>

#include <POVLexer.hpp>

#include <ErrorHandler.hpp>
#include <Stringify.hpp>

 extern std::vector<std::string> path;       // include files paths
 bool execute = true;

 variable * variables; 
 int nbvariables;

 extern POVLexer* povlexer;

 void yyerror (char* s);
 void declarevar (char* name, char* val);
 void expend(std::string& expendval);

 extern Scene* pScene;
 SceneBox SB;

 enum statement {
   condition,
   loop
 };

 std::stack<bool> ExecStack;
 std::stack<statement> StatementStack;
 std::stack<long int> StreamPosStack;

 bool BoolOp = false; // used if the actual object is a
                      // construction object that belongs
                      // to a boolean operation
 std::stack<bool> BoolOpStack;
 std::stack<std::vector<ReferenceCounting<Object> > > OpStack; // Used to store temporarly objects

#define povlex povlexer->povlex

%}

%union {
  real_t val;
  real_t vect[3];
  real_t vect4[4];
  real_t mat[12];
  char* str;
  int integer;
  long int stream_pos;
  parsetrans trans;
}

%token <val> NUM
%token STRING KEYWORD
%token SPHERE BOX CYLINDER CONE PLANE TORUS
%token UNION INTERSECTION DIFFERENCE OBJECT INVERSE
%token <str> NAME
%token DECLARE
%token <str> TRANSF MATRIXTRANSF
%token PIGMENT COLOR RGB RGBF

%token IF IFDEF IFNDEF ELSE END
%token <stream_pos> WHILE
%token AND OR NOT
%token LE GE EQ NE

%type <vect> vector
%type <mat> matrix
%type <vect4> vector4
%type <val> exp
%type <vect4> objref
%type <trans> modifier
%type <str> variable
%type <integer> boolexp /* must use int for bool because lexer is in C :-( */
/* precedence definitions */
%left AND OR NOT
%left EQ NE GE LE '<' '>'
%left '-' '+'
%left '*' '/'
%left NEG     /* negation--unary minus */
%left '{' '}'


/* Don't care about following shift/reduce problems: */
%expect 1

%% /* Grammar rules and actions follow */
  
input:    /* empty */
| input variable { }
| input object { }
| input iftok { }
;

iftok: IFDEF '(' NAME ')'  {
       ExecStack.push(execute);
       StatementStack.push(condition);
       if (execute) {
	 bool defined_var = false;
	 for (int i=0; i<nbvariables; i++) {
	   if ( strcmp(variables[i].name,$<str>3) ) {
	     defined_var = true;
	     break;
	   }
	 }
	 if (defined_var) {
	   execute = true;
	 } else {
	   execute = false;
	 }
       }
      }
      | IFNDEF '(' NAME ')'  {
	ExecStack.push(execute);
	StatementStack.push(condition);
	if (execute) {
	  bool defined_var = false;
	  for (int i=0; i<nbvariables; i++) {
	    if ( strcmp(variables[i].name,$<str>3) ) {
	      defined_var = true;
	      break;
	    }
	  }
	  if (!defined_var) {
	    execute = true;
	  } else {
	    execute = false;
	  }
	}
      }
      | IF '(' boolexp ')' {
	ExecStack.push(execute);
	StatementStack.push(condition);
	if (execute) {
	  execute = $3;
	}
      }
      | ELSE {
	if (ExecStack.top()) { // This means that the IF THEN ELSE statment is not to completly ignore
          execute = ! execute;
	}
      }
      | END {
	if (StatementStack.top()==loop) {
	  fferr(1) << __FILE__ << ':' << __LINE__
		   << ": Not implemented\n";
	  std::exit(1);
// 	  if (execute)
// 	    fseek(yyin, StreamPosStack.top(),SEEK_SET);
// 	  StreamPosStack.pop();
	}
	execute = ExecStack.top();
	StatementStack.pop();
	ExecStack.pop();
      }
      | WHILE '(' boolexp ')' {
	ExecStack.push(execute);
	StatementStack.push(loop);
	if (execute) {
	  StreamPosStack.push($1); // To go back to position in file.
	  execute = $3;
	}
      }
    ;
     

    variable: DECLARE NAME '=' exp ';' {
                if (execute) {
		  std::string varname = $2;
		  int varnum = 0;

		  bool found = false;
		  for (int i=0; i<nbvariables; i++)
		    if (varname == variables[i].name) {
		      found = true;
		      varnum = i;
		      if (variables[i].type == 1) { // var was a string
			free(variables[i].val.s);
		      }
		      break;
		    }
		  if (!found) {
		    varnum = nbvariables;
		    nbvariables++;
		    variables = (variable*)realloc(variables, nbvariables);
		  }
		  variables[varnum].name = new char[strlen($2)+1];
		  strcpy(variables[varnum].name, $2);
		  variables[varnum].val.d = $4;
                }
              }
            | DECLARE NAME '=' SPHERE '{' NAME '}'{
	      if (execute) {
		declarevar($2, $6); // not complete
		std::cerr << __FILE__ << ':' << __LINE__
		     << ':' << "Incorrect implementation\n";
		std::exit(1);
	      }
	    }
     ;


     objref: PIGMENT '{' COLOR RGB vector '}'
             {
	       for (size_t i=0; i<3; ++i)
		 $$[i] = $5[i];
	       $$[3] = 0;
	     }
           | PIGMENT '{' COLOR RGBF vector4 '}'
             {
	       for (size_t i=0; i<4; ++i)
		 $$[i] = $5[i];
	     }
     ;


     modifier:   /* empty: could be no modifiers, otherwise, initialization is done */ 
                {
		  if (execute) {
		    $<trans.number>$ = 0;
		    $<trans.hasRef>$ = false;
		    $<trans.inverse>$ = false;
		    for (size_t i=0; i<4; ++i)
		      $<trans.ref>$[i] = 0;
		  }
		}
              | modifier objref
                {
		  if (execute) {
		    $<trans.number>$ = $<trans.number>1;
		    $<trans.type>$ = new TransType[($<trans.number>$)];
		    $<trans.vect>$[0] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[1] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[2] = new real_t[($<trans.number>$)];

		    for (size_t i=0; i<12; ++i)
		      $<trans.mat>$[i] = $<trans.mat>1[i];

		    /* copy old modifier values */
		    for (int i=0; i<$<trans.number>1; i++) {
		      $<trans.type>$[i] = $<trans.type>1[i];
		      $$.vect[0][i] = $1.vect[0][i];
		      $$.vect[1][i] = $1.vect[1][i];
		      $$.vect[2][i] = $1.vect[2][i];
		    }
		    if ($<trans.number>1 > 0) {
		      delete[] $<trans.type>1; /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] $<trans.vect>1[i];
		    }
		    $<trans.inverse>$ = $<trans.inverse>1;

		    $<trans.hasRef>$ = true;

		    for (size_t i=0; i<4; ++i)
		      $<trans.ref>$[i] = $<vect4>2[i];
		  }
		}
              | modifier TRANSF vector
                {
		  if (execute) {

		    TransType* savetype;
		    real_t* savevect[3];
		  
		    savetype = new TransType[$<trans.number>$+1];
		    for (int i=0; i<3; i++)
		      savevect[i] = new real_t[$<trans.number>$+1];

		    for (int i=0; i<$<trans.number>$; i++) {
		      savetype[i] = $<trans.type>$[i]; /* keeps old values */
		      savevect[0][i] = $<trans.vect>$[0][i];
		      savevect[1][i] = $<trans.vect>$[1][i];
		      savevect[2][i] = $<trans.vect>$[2][i];
		    }

		    if ($<trans.number>$ > 0) {
		      delete[] $<trans.type>$; /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] $<trans.vect>$[i];
		    }

		    $<trans.type>$ = new TransType[($<trans.number>$+1)];
		    $<trans.vect>$[0] = new real_t[($<trans.number>$+1)];
		    $<trans.vect>$[1] = new real_t[($<trans.number>$+1)];
		    $<trans.vect>$[2] = new real_t[($<trans.number>$+1)];

		    /* restore old values */
		    for (int i=0; i<$<trans.number>$; i++) {
		      $<trans.type>$[i] = savetype[i];
		      $<trans.vect>$[0][i] = savevect[0][i];
		      $<trans.vect>$[1][i] = savevect[1][i];
		      $<trans.vect>$[2][i] = savevect[2][i];
		    }

		    $<trans.hasRef>$ = $<trans.hasRef>1;
		    $<trans.inverse>$ = $<trans.inverse>1;
		    for (size_t i=0; i<4; ++i)
		      $<trans.ref>$[i] = $<trans.ref>1[i];

		    delete[] savetype;
		    for (int i=0; i<3; i++)
		      delete [] savevect[i];
		    if (!strcmp ($<str>2,"rotate")) {
		      $<trans.type>$[$<trans.number>$] = rotation;
		    } else if (!strcmp ($<str>2,"translate")) {
		      $<trans.type>$[$<trans.number>$] = translation;
		    } else if (!strcmp ($<str>2,"scale")) {
		      $<trans.type>$[$<trans.number>$] = scale;
		    } else {
		      std::cerr << "parse error unknown "
				<< $<str>2 << " check parsing in combo.y\n";
		    }
		    for (int i=0; i<3; i++)
		      $<trans.vect>$[i][$<trans.number>$] = $<vect>3[i];
		    
		    $<trans.number>$++;
		  }
		}
              | modifier MATRIXTRANSF matrix
                {
		  if (execute) {
		    $<trans.number>$ = $<trans.number>1;
		    $<trans.type>$ = new TransType[($<trans.number>$)];
		    $<trans.vect>$[0] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[1] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[2] = new real_t[($<trans.number>$)];

		    /* copy old modifier values */
		    for (int i=0; i<$<trans.number>1; i++) {
		      $<trans.type>$[i] = $<trans.type>1[i];
		      $$.vect[0][i] = $1.vect[0][i];
		      $$.vect[1][i] = $1.vect[1][i];
		      $$.vect[2][i] = $1.vect[2][i];
		    }
		    if ($<trans.number>1 > 0) {
		      delete[] $<trans.type>1; /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] $<trans.vect>1[i];
		    }
		    $<trans.inverse>$ = $<trans.inverse>1;
		    $<trans.hasRef>$ = $<trans.hasRef>1;

		    for (size_t i=0; i<12; ++i)
		      $<trans.mat>$[i] = $<mat>3[i];

		    $<trans.type>$ = new TransType[1];
		    $<trans.type>$[0] = matrix;

 		    $<trans.number>$=0;
 		  }
		}
              | modifier INVERSE
                {
		  if (execute) {
		    $<trans.number>$ = $<trans.number>1;
		    $<trans.type>$ = new TransType[($<trans.number>$)];
		    $<trans.vect>$[0] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[1] = new real_t[($<trans.number>$)];
		    $<trans.vect>$[2] = new real_t[($<trans.number>$)];

		    /* copy old modifier values */
		    for (int i=0; i<$<trans.number>1; i++) {
		      $<trans.type>$[i] = $<trans.type>1[i];
		      $$.vect[0][i] = $1.vect[0][i];
		      $$.vect[1][i] = $1.vect[1][i];
		      $$.vect[2][i] = $1.vect[2][i];
		    }
		    if ($<trans.number>1 > 0) {
		      delete[] $<trans.type>1; /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] $<trans.vect>1[i];
		    }
		    $<trans.hasRef>$ = $<trans.hasRef>1;

		    $<trans.inverse>$ = not($<trans.inverse>1);
 		  }		  
		}
     ;

     object: SPHERE '{' vector ',' exp modifier '}'
                {
		  if (execute) {

		    if ($<trans.ref>6[3] == 0) { // means that the object is not transparent!
		      Vertex c($<vect>3[0], $<vect>3[1], $<vect>3[2]);
		      real_t& r = $5;
		      parsetrans& transformations = $6;

		      Shape* s = new Sphere(c,r);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>6) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ($<trans.hasRef>6) {
			O->setReference(TinyVector<3>($<trans.ref>6[0],
						      $<trans.ref>6[1],
						      $<trans.ref>6[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
              | TORUS '{' exp ',' exp modifier '}'
                {
		  if (execute) {

		    if ($<trans.ref>6[3] == 0) { // means that the object is not transparent!
		      real_t& r1 = $3;
		      real_t& r2 = $5;
		      parsetrans& transformations = $6;

		      Shape* s = new Torus(r1,r2);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>6) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
 		      if ($<trans.hasRef>6) {
			O->setReference(TinyVector<3>($<trans.ref>6[0],
						      $<trans.ref>6[1],
						      $<trans.ref>6[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
              | BOX '{' vector ',' vector modifier '}'
                {
		  if (execute) {
		    Vertex a($<vect>3[0], $<vect>3[1], $<vect>3[2]);
		    Vertex b($<vect>5[0], $<vect>5[1], $<vect>5[2]);
		    if ($<trans.ref>6[3] == 0) { // means the object is not transparent
		      parsetrans& transformations = $6;

		      Shape* s = new Cube(a,b);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>6) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ($<trans.hasRef>6) {
			O->setReference(TinyVector<3>($<trans.ref>6[0],
						      $<trans.ref>6[1],
						      $<trans.ref>6[2]));
		      }

		      /* put the cube in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    } else {
		      SB = SceneBox(a, b);
		    }
		  }
		}
              | CYLINDER '{' vector ',' vector ',' exp modifier '}'
                {
		  if (execute) {
		    if ($<trans.ref>8[3] == 0) { // means the object is not transparent
		      Vertex a($<vect>3[0], $<vect>3[1], $<vect>3[2]);
		      Vertex b($<vect>5[0], $<vect>5[1], $<vect>5[2]);
		      real_t radius = $7;
		      parsetrans& transformations = $8;

		      Shape* s = new Cylinder(a,b,radius);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>8) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ($<trans.hasRef>8) {
			O->setReference( TinyVector<3>($<trans.ref>8[0],
						       $<trans.ref>8[1],
						       $<trans.ref>8[2]));
		      }

		      /* put the cylinder in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  } 
		}
              | CONE '{' vector ',' exp ',' vector ',' exp modifier '}'
                {
		  if (execute) {
		    if ($<trans.ref>8[3] == 0) { // means the object is not transparent
		      Vertex a($<vect>3[0], $<vect>3[1], $<vect>3[2]);
		      Vertex b($<vect>7[0], $<vect>7[1], $<vect>7[2]);
		      real_t radius1 = $5;
		      real_t radius2 = $9;
		      parsetrans& transformations = $10;

		      Shape* s = new Cone(a,b,radius1, radius2);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>8) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ($<trans.hasRef>10) {
			O->setReference( TinyVector<3>($<trans.ref>10[0],
						       $<trans.ref>10[1],
						       $<trans.ref>10[2]));
		      }

		      /* put the cylinder in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
          | PLANE '{' vector ',' exp modifier '}'
                {
		  if (execute) {

		    if ($<trans.ref>6[3] == 0) { // means that the object is not transparent!
		      Vertex c($<vect>3[0], $<vect>3[1], $<vect>3[2]);
		      real_t& r = $5;
		      parsetrans& transformations = $6;

		      Shape* s = new Plane(c,r);
		      (*s).parseTransform(transformations);

		      if ($<trans.inverse>6) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ($<trans.hasRef>6) {
			O->setReference( TinyVector<3>($<trans.ref>6[0],
						       $<trans.ref>6[1],
						       $<trans.ref>6[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
              | UNION '{' { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
            objects modifier '}'
              { 
		 if (execute) {
		   if ($<trans.ref>5[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Union* u = new Union();
		     for (size_t i=0; i<OpStack.top().size(); i++)
		       u->push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = $5;
		     u->parseTransform(transformations);

		     Object* O = new Object(u);
		     if ($<trans.hasRef>5) {
		       O->setReference(TinyVector<3>($<trans.ref>5[0],
						    $<trans.ref>5[1],
						    $<trans.ref>5[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
          | INTERSECTION '{' { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
            objects modifier '}'
              { 
		 if (execute) {
		   if ($<trans.ref>5[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Intersection* I = new Intersection();

		     for (size_t i=0; i<OpStack.top().size(); i++)
		       I->push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = $5;
		     (*I).parseTransform(transformations);

		     Object* O = new Object(I);
		      if ($<trans.hasRef>5) {
			O->setReference( TinyVector<3>($<trans.ref>5[0],
						       $<trans.ref>5[1],
						       $<trans.ref>5[2]));
		      }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
          | DIFFERENCE '{' { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
            objects modifier '}'
              {
		 if (execute) {
		   if ($<trans.ref>5[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Difference* d = new Difference();
	
		     for (size_t i=0; i<OpStack.top().size(); i++) {
		       (*d).push_back(OpStack.top()[i]);
		     }
		     OpStack.pop();

		     parsetrans& transformations = $5;
		     (*d).parseTransform(transformations);

		     Object* O = new Object(d);
		     if ($<trans.hasRef>5) {
		       O->setReference(TinyVector<3>($<trans.ref>5[0],
						     $<trans.ref>5[1],
						     $<trans.ref>5[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
           | OBJECT '{' { // object is treated like an union limited to one object
#ifdef    __GNUC__
#warning REWORK OBJECT TREATMENT
#endif // __GNUC__
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
            object modifier '}'
              { 
		 if (execute) {
		   if ($<trans.ref>5[3] == 0) { // means the object is not transparent

		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Union* u = new Union(); 
		     for (size_t i=0; i<OpStack.top().size(); i++)
		       (*u).push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = $5;
		     (*u).parseTransform(transformations);

		     Object* O = new Object(u);
		     if ($<trans.hasRef>5) {
		       O->setReference(TinyVector<3>($<trans.ref>5[0],
						     $<trans.ref>5[1],
						     $<trans.ref>5[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
     ;
     

     objects: object
            | object objects
     ;

      exp:  NUM                     { $$ = $1;         }
  	    | exp '+' exp           { $$ = $1 + $3;    }
	    | exp '-' exp           { $$ = $1 - $3;    }
	    | exp '*' exp           { $$ = $1 * $3;    }
	    | exp '/' exp           { $$ = $1 / $3;    }
	    | '(' exp ')'           { $$ = $2 ;        }
	    | '-' exp     %prec NEG { $$ = -$2;        }
            ;

      boolexp: exp                  { $$ = (fabs($1)>1E-6); }
            | exp '>' exp           { $$ = ($1 > $3);       }
            | exp '<' exp           { $$ = ($1 < $3);       }
            | exp GE  exp           { $$ = ($1 >= $3);      }
            | exp LE  exp           { $$ = ($1 <= $3);      }
            | exp EQ  exp           { $$ = ($1 == $3);      }
            | exp NE  exp           { $$ = ($1 != $3);      }
            | boolexp AND boolexp   { $$ = $1 && $3;        }
            | boolexp OR boolexp    { $$ = $1 || $3;        }
            | NOT boolexp           { $$ = !($2);           }
            | '(' boolexp ')'       { $$ = $2;              }
  	    ;


      matrix:  '<' exp ',' exp ',' exp ','
		   exp ',' exp ',' exp ','
		   exp ',' exp ',' exp ','
		   exp ',' exp ',' exp '>' { 
                                             $<mat>$[ 0] = $2;
					     $<mat>$[ 1] = $4;
					     $<mat>$[ 2] = $6;
                                             $<mat>$[ 3] = $8;
					     $<mat>$[ 4] = $10;
					     $<mat>$[ 5] = $12;
                                             $<mat>$[ 6] = $14;
					     $<mat>$[ 7] = $16;
					     $<mat>$[ 8] = $18;
                                             $<mat>$[ 9] = $20;
					     $<mat>$[10] = $22;
					     $<mat>$[11] = $24;
              }
	    ;

      vector:  '<' exp ',' exp ',' exp '>' { 
                                             $<vect>$[0] = $2;
					     $<vect>$[1] = $4;
					     $<vect>$[2] = $6;
              }
            | '-' vector { // opposit vector
                                             $<vect>$[0] = - $<vect>2[0];
					     $<vect>$[1] = - $<vect>2[1];
					     $<vect>$[2] = - $<vect>2[2];	        
              }
            | vector '-' vector { // difference between 2 vectors
                                             $<vect>$[0] = $<vect>1[0] - $<vect>3[0];
					     $<vect>$[1] = $<vect>1[1] - $<vect>3[1];
					     $<vect>$[2] = $<vect>1[2] - $<vect>3[2];
              }
            | vector '+' vector { // sum of 2 vectors
                                             $<vect>$[0] = $<vect>1[0] + $<vect>3[0];
					     $<vect>$[1] = $<vect>1[1] + $<vect>3[1];
					     $<vect>$[2] = $<vect>1[2] + $<vect>3[2];
              }
            | vector '*' exp { // product by a scalar on right
                                             $<vect>$[0] = $<vect>1[0] * $3;
					     $<vect>$[1] = $<vect>1[1] * $3;
					     $<vect>$[2] = $<vect>1[2] * $3;
              }
            | vector '/' exp { // division by a scalar on right
                                             $<vect>$[0] = $<vect>1[0] / $3;
					     $<vect>$[1] = $<vect>1[1] / $3;
					     $<vect>$[2] = $<vect>1[2] / $3;
              }
            | exp '*' vector { // product by a scalar on left
                                             $<vect>$[0] = $<vect>3[0] * $1;
					     $<vect>$[1] = $<vect>3[1] * $1;
					     $<vect>$[2] = $<vect>3[2] * $1;
              }
            ;

      vector4:  '<' exp ',' exp ',' exp ',' exp '>' { 
                                             $<vect4>$[0] = $2;
					     $<vect4>$[1] = $4;
					     $<vect4>$[2] = $6;
					     $<vect4>$[3] = $8;
              }
            | '-' vector4 { // opposit vector4
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = - $<vect4>2[i];
              }
            | vector4 '-' vector4 { // difference between 2 vector4s
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = $<vect4>1[i] - $<vect4>3[i];
              }
            | vector4 '+' vector4 { // sum of 2 vector4s
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = $<vect4>1[i] + $<vect4>3[i];
              }
            | vector4 '*' exp { // product by a scalar on right
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = $<vect4>1[i] * $3;
              }
            | vector4 '/' exp { // division by a scalar on right
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = $<vect4>1[i] / $3;
              }
            | exp '*' vector4 { // product by a scalar on left
  	                                   for (size_t i=0; i<4; ++i)
                                             $<vect4>$[i] = $<vect4>3[i] * $1;
              }
            ;
     %%

     
void yyerror(char * s) {
   throw ErrorHandler("PARSED FILE",povlexer->lineno(),
		      stringify(s)+"'"+povlexer->YYText()+"' unexpected",
		      ErrorHandler::compilation);
}
void declarevar (char* name, char* val) {
  bool found = false;
  int varnum = 0;

  std::string expendval = val;
  // Substitute variables value in varname.
  expend(expendval);

  for (int i=0; i<nbvariables; ++i)
    if (strcmp(variables[i].name, name) == 0) {
      // The Variable's value is going to change
      found = true;
      delete [] variables[i].val.s;
      varnum = i;
    }
  if (!found) {
    // storing new variable
    varnum = nbvariables;
    nbvariables++;
    variables = (variable*)realloc(variables, nbvariables);
    variables[varnum].name = new char[strlen(name)+1];
    strcpy(variables[varnum].name, name);
  }
  // Affecting value
  variables[varnum].val.s = new char[expendval.size() + 1];
  strcpy(variables[varnum].val.s, expendval.c_str());
}

void expend(std::string& expendval) {
  int i=0;
  while (i<nbvariables) {
    std::string var = variables[i].name;
    std::string val = variables[i].val.s;
    int begining = expendval.find_first_of(variables[i].name);
    if (begining >= 0) {
      bool replace = true;
      if (begining > 0) {
	replace = !isalnum(expendval[begining-1]);
      }
      replace = (replace) && isalnum(expendval[begining+var.size()]);
      if (replace) {

	std::stringstream tempstr;
	for (int i=0; i<begining; i++)
	  tempstr << expendval[i];
	tempstr << val;
	for (size_t i=begining+var.size()-1; i < expendval.size(); i++)
	  tempstr << expendval[i];
	tempstr << std::ends;

	expendval = tempstr.str();
      }

    } else {
      i++;
    }
  }
}


