//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <IStreamExpression.hpp>

#include <IFStreamExpression.hpp>
#include <Variable.hpp>

#include <VariableRepository.hpp>

std::ostream&
IStreamExpression::
put(std::ostream & os) const
{
  return os;
}

void
IStreamExpression::
__execute()
{
  if (__istream == 0) {
    __ifstreamVariable
      = VariableRepository::instance().findVariable<IFStreamVariable>(__ifstreamName);
  }
}

void
IStreamExpression::
operator>>(Expression& E)
{
  if (__istream != 0) {
    (*__istream) >> E;
  } else {
    ASSERT(__ifstreamVariable->expression()->ifstream() != 0);
    (*__ifstreamVariable->expression()->ifstream()) >> E;
  }
}

IStreamExpression::
IStreamExpression(std::istream* is)
  : Expression(Expression::istreamexpression),
    __ifstreamName("none"),
    __istream(is),
    __ifstreamVariable(0)
{
  ;
}

IStreamExpression::
IStreamExpression(const std::string& ifstreamName)
  : Expression(Expression::istreamexpression),
    __ifstreamName(ifstreamName),
    __istream(0),
    __ifstreamVariable(0)
{
  ;
}

IStreamExpression::
IStreamExpression(const IStreamExpression& is)
  : Expression(*this),
    __ifstreamName(is.__ifstreamName),
    __istream(is.__istream),
    __ifstreamVariable(is.__ifstreamVariable)
{
  ;
}

IStreamExpression::
~IStreamExpression()
{
  ;
}

