//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef PDEPROBLEM_EXPRESSION_HPP
#define PDEPROBLEM_EXPRESSION_HPP

#include <Expression.hpp>
#include <Variable.hpp>

#include <PDEEquationExpression.hpp>
#include <BoundaryConditionListExpression.hpp>
/*!
  \class PdeproblemExpression

  This class defines the base class of Pdeproblem expressions.

  \author Stephane Del Pino
 */
class PDEProblem;
class PDEProblemExpression
  : public Expression
{
public:
  enum PDEProblemType {
    description,
    undefined,
    variable
  };

private:
  PDEProblemExpression::PDEProblemType __pdeProblemType;

protected:
  ReferenceCounting<PDEProblem> __pdeProblem;

public:
  /** 
   * Checks if the PDEProblemExpression contains POVRay references. If
   * This verification is required by standard FEM.
   * 
   * @return true if the problem has boundary conditions on a POV boundary
   */
  virtual bool hasPOVBoundary() const = 0;

  /** 
   * Checks if the PDEProblemExpression expression contains predefined
   * surface meshes. This verification is required by standard FEM.
   * 
   * @return true if one of the boundaries is defined using an
   * external mesh
   */
  virtual bool hasPredefinedBoundary() const = 0;


  ReferenceCounting<PDEProblem> pdeProblem();

  const PDEProblemExpression::PDEProblemType& pdeProblemType() const
  {
    return __pdeProblemType;
  }

  PDEProblemExpression(const PDEProblemExpression& e);

  PDEProblemExpression(const PDEProblemExpression::PDEProblemType& t);

  virtual ~PDEProblemExpression();
};

class PDEProblemExpressionDescription
  : public PDEProblemExpression
{
private:
  std::ostream& put(std::ostream& os) const;

  const std::string __unknownName;
  ReferenceCounting<PDEEquationExpression> __pdeEquation;
  ReferenceCounting<BoundaryConditionListExpression> __bcList;

public:
  /** 
   * Checks if the PDEProblemExpressionDescription contains POVRay
   * references. If it does the execution is stopped. This
   * verification is required by standard FEM.
   * 
   * @return true if the problem has boundary conditions on a POV boundary
   */
  bool hasPOVBoundary() const;

  /** 
   * Checks if the PDEProblemExpressionDescription contains uses
   * external surface mesh for boundary condition. This verification
   * is required by standard FEM.
   * 
   * @return true if the problem has boundary conditions contains
   * external surface mesh
   */
  bool hasPredefinedBoundary() const;

  void __execute();

  ReferenceCounting<PDEEquationExpression> pdeEquation();

  ReferenceCounting<BoundaryConditionListExpression> bcList();

  PDEProblemExpressionDescription(const std::string& unknownName,
				  ReferenceCounting<PDEEquationExpression> pdeEquation,
				  ReferenceCounting<BoundaryConditionListExpression> bcList);

  PDEProblemExpressionDescription(const PDEProblemExpressionDescription& e);

  ~PDEProblemExpressionDescription();
};

#endif // PDEPROBLEM_EXPRESSION_HPP
