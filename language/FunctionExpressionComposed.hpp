//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_COMPOSED_HPP
#define FUNCTION_EXPRESSION_COMPOSED_HPP

#include <FunctionExpression.hpp>

/**
 * @file   FunctionExpressionComposed.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:22:36 2006
 * 
 * @brief  Function composition expression
 * 
 * 
 */
class FunctionExpressionComposed
  : public FunctionExpression
{
private:
  ReferenceCounting<FunctionExpression> __function; /**< The function */

  ReferenceCounting<FunctionExpression> __functionExpressionX; /**< function returning \f$ x \f$ */
  ReferenceCounting<FunctionExpression> __functionExpressionY; /**< function returning \f$ y \f$ */
  ReferenceCounting<FunctionExpression> __functionExpressionZ; /**< function returning \f$ z \f$ */

public:
  /** 
   * Return true if one of the functions has a boundary
   * 
   * @return if one of the functions has a boundary
   */
  bool hasBoundaryExpression() const
  {
    return (__function->hasBoundaryExpression() or
	    __functionExpressionX->hasBoundaryExpression() or
	    __functionExpressionY->hasBoundaryExpression() or
	    __functionExpressionZ->hasBoundaryExpression());
  }

  /** 
   * execute the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param f the function
   * @param X the @e x function
   * @param Y the @e y function
   * @param Z the @e z function
   * 
   */
  FunctionExpressionComposed(ReferenceCounting<FunctionExpression> f,
			     ReferenceCounting<FunctionExpression> X,
			     ReferenceCounting<FunctionExpression> Y,
			     ReferenceCounting<FunctionExpression> Z);

  /** 
   * Copy constructor
   * 
   * @param f the composed function
   * 
   */
  FunctionExpressionComposed(const FunctionExpressionComposed& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionComposed();
};

#endif // FUNCTION_EXPRESSION_COMPOSED_HPP
