//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <MultiLinearExpression.hpp>

#include <EmbededFunctions.hpp>
#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <FunctionExpressionConstant.hpp>
#include <FunctionExpressionBinaryOperation.hpp>

std::ostream&
MultiLinearExpression::
put(std::ostream& os) const
{
  bool notFirst = false;
  for (LinearListType::const_iterator i = __linearList.begin();
       i != __linearList.end(); ++i) {
    if (notFirst) {
      os << '*';
    } else {
      notFirst = true;
    }
    os << (*(*i));
  }

  for (FunctionListType::const_iterator i = __functionList.begin();
       i != __functionList.end(); ++i) {
    if (notFirst) {
      os << '*';
    } else {
      notFirst = true;
    }
    os << (*(*i));
  }

  for (RealListType::const_iterator i = __realList.begin();
       i != __realList.end(); ++i) {
    if (notFirst) {
      os << '*';
    } else {
      notFirst = true;
    }
    os << (*(*i));
  }

  return os;
}


ReferenceCounting<FunctionExpression>
MultiLinearExpression::
getFunction()
{
  ReferenceCounting<FunctionExpression> f = 0;
  ReferenceCounting<RealExpression> r = 0;

  if (__realList.size() > 0) {
    RealListType::iterator i = __realList.begin();
    r = *i;
    i++;
    for ( ; i != __realList.end(); ++i) {
      r = new RealExpressionBinaryOperator<product>(r,*i);
    }
  }
  if (__functionList.size() > 0) {
    FunctionListType::iterator i = __functionList.begin();
    f = *i;
    ++i;
    for ( ; i != __functionList.end(); ++i) {
      f = new FunctionExpressionBinaryOperation(BinaryOperation::product, f, *i);
    }
  }

  for (LinearListType::iterator i = __linearList.begin();
       i != __linearList.end(); ++i) {
    switch ((*(*i)).formType()) {
    case (LinearExpression::elementary): {
      break;			// nothing to do
    }
    case (LinearExpression::elementaryTimesFunction): {
      LinearExpressionElementaryTimesFunction& L
	= dynamic_cast<LinearExpressionElementaryTimesFunction&>(*(*i));
      if (f == 0) {
	f = L.function();
      } else {
	f = new FunctionExpressionBinaryOperation(BinaryOperation::product, f, L.function());
      }
      break;
    }
    case (LinearExpression::elementaryTimesReal): {
      LinearExpressionElementaryTimesReal& L
	= dynamic_cast<LinearExpressionElementaryTimesReal&>(*(*i));
      if (r == 0) {
	r = L.real_t();
      } else {
	r = new RealExpressionBinaryOperator<product>(r,L.real_t());
      }
      break;
    }
    case (LinearExpression::elementaryTimesFunctionOperator): {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }
  }

  if ((r == 0)&&(f == 0)) {
    r = new RealExpressionValue(1);
    f = new FunctionExpressionConstant(r);
  } else if (r == 0) {
    ; // everything is ok !
  } else if (f == 0){
    f = new FunctionExpressionConstant(r);
  } else {
    f = new FunctionExpressionBinaryOperation(BinaryOperation::product, f, new FunctionExpressionConstant(r));
  }

  return f;
}

const std::string&
MultiLinearExpression::
getUnknownName() const
{
  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedExpression& IE =
      *(*i)->integrated()->integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::unknownFunction) {
      const IntegratedExpressionUnknown& I
	= dynamic_cast<const IntegratedExpressionUnknown&>(IE);

      return I.unknownName();
    }
    ++i;
  }
    
  throw ErrorHandler(__FILE__,__LINE__,
		     "Could not find unknown name",
		     ErrorHandler::unexpected);
  static std::string badName("unknown function");
  return badName;
}


const std::string&
MultiLinearExpression::
getTestFunctionName() const
{
  ConstReferenceCounting<TestFunctionVariable> t = 0;
  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedExpression& IE =
      *(*i)->integrated()->integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::testFunction) {
      const IntegratedExpressionTest& I
	= dynamic_cast<const IntegratedExpressionTest&>(IE);
      return I.testFunctionName();
    }
    ++i;
  }
  throw ErrorHandler(__FILE__,__LINE__,
		     "Could not find testfunction name",
		     ErrorHandler::unexpected);
  static std::string badName("unknown function");
  return badName;
}


IntegratedOperatorExpression::OperatorType
MultiLinearExpression::
getUnknownOperator() const
{
  IntegratedOperatorExpression::OperatorType t
    = IntegratedOperatorExpression::undefined;

  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();

    const IntegratedExpression& IE = *I.integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::unknownFunction) {
      t = I.operatorType();
    }
    ++i;
  }

  return t;
}

IntegratedOperatorExpression::OperatorType
MultiLinearExpression::
getTestFunctionOperator() const
{
  IntegratedOperatorExpression::OperatorType t
    = IntegratedOperatorExpression::undefined;

  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();

    const IntegratedExpression& IE = *I.integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::testFunction) {
      t = I.operatorType();
    }
    ++i;
  }

  return t;
}

IntegratedOperatorExpression::OperatorType
MultiLinearExpression::
getFunctionOperator() const
{
  IntegratedOperatorExpression::OperatorType t
    = IntegratedOperatorExpression::undefined;

  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();

    const IntegratedExpression& IE = *I.integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::function) {
      t = I.operatorType();
    }
    ++i;
  }

  return t;
}

VariationalOperator::Property
MultiLinearExpression::
getUnknownProperty() const
{
  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();

    const IntegratedExpression& IE = *I.integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::unknownFunction) {
      if (I.isJump()) {
	return VariationalOperator::jump;
      }
      if (I.isMean()) {
	return VariationalOperator::mean;
      }
    }
    ++i;
  }

  return VariationalOperator::normal;
}

VariationalOperator::Property
MultiLinearExpression::
getTestFunctionProperty() const
{
  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();

    const IntegratedExpression& IE = *I.integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::testFunction) {
      if (I.isJump()) {
	return VariationalOperator::jump;
      }
      if (I.isMean()) {
	return VariationalOperator::mean;
      }
    }
    ++i;
  }

  return VariationalOperator::normal;
}

ReferenceCounting<FunctionExpression>
MultiLinearExpression::
getFunctionExpression() const
{
  ReferenceCounting<FunctionExpression> f = 0;
  LinearListType::const_iterator i = __linearList.begin();
  while (i != __linearList.end()) {
    const IntegratedExpression& IE =
      *(*i)->integrated()->integratedExpression();

    if (IE.integratedExpressionType()
	== IntegratedExpression::function) {
      const IntegratedExpressionFunctionExpression& I
	= dynamic_cast<const IntegratedExpressionFunctionExpression&>(IE);

      f = I.function();
    }
    ++i;
  }
  ASSERT(f != 0);
  return f;
}

MultiLinearExpression::OperatorType
MultiLinearExpression::
operatorType() const
{
  IntegratedOperatorExpression::OperatorType unknown
    = IntegratedOperatorExpression::undefined;

  IntegratedOperatorExpression::OperatorType test
    = IntegratedOperatorExpression::undefined;

  IntegratedOperatorExpression::OperatorType function
    = IntegratedOperatorExpression::undefined;

  for(LinearListType::const_iterator i = __linearList.begin();
      i != __linearList.end(); ++i) {
    const IntegratedOperatorExpression& I = *(*i)->integrated();
    switch(I.integratedExpression()->integratedExpressionType()) {
    case IntegratedExpression::testFunction: {
      test = I.operatorType();
      break;
    }
    case IntegratedExpression::unknownFunction: {
      unknown = I.operatorType();
      break;
    }
    case IntegratedExpression::function: {
      function = I.operatorType();
      break;
    }
    }
  }

  OperatorType t = undefined;

  switch (__linearList.size()) {
  case 1: {
    switch (test) {
    case IntegratedOperatorExpression::orderZero: {
      t=V;
      break;
    }
    case IntegratedOperatorExpression::gradient: {
      t=gradV;
      break;
    }
    case IntegratedOperatorExpression::dx:
    case IntegratedOperatorExpression::dy:
    case IntegratedOperatorExpression::dz: {
      return dxV;
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }
    break;
  }
  case 2: {
    if (test != IntegratedOperatorExpression::undefined) {
      if (unknown != IntegratedOperatorExpression::undefined) {
	switch (unknown) {
	case IntegratedOperatorExpression::orderZero: {
	  switch (test) {
	  case IntegratedOperatorExpression::orderZero: {
	    t=UV;
	    break;
	  }
	  case IntegratedOperatorExpression::gradient:
	  case IntegratedOperatorExpression::dx:
	  case IntegratedOperatorExpression::dy:
	  case IntegratedOperatorExpression::dz: {
	    t=UdxV;
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	  }
	  break;
	}
	case IntegratedOperatorExpression::gradient: {
	  switch (test) {
	  case IntegratedOperatorExpression::orderZero: {
	    t=dxUV;
	    break;
	  }
	  case IntegratedOperatorExpression::gradient: {
	    t=gradUgradV;
	    break;
	  }
	  case IntegratedOperatorExpression::dx:
	  case IntegratedOperatorExpression::dy:
	  case IntegratedOperatorExpression::dz: {
	    return dxUdxV;
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	  }
	  break;
	}
	case IntegratedOperatorExpression::dx:
	case IntegratedOperatorExpression::dy:
	case IntegratedOperatorExpression::dz: {
	  switch (test) {
	  case IntegratedOperatorExpression::orderZero: {
	    t=dxUV;
	    break;
	  }
	  case IntegratedOperatorExpression::gradient:
	  case IntegratedOperatorExpression::dx:
	  case IntegratedOperatorExpression::dy:
	  case IntegratedOperatorExpression::dz: {
	    t=dxUdxV;
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);
	}
	}
      } else {
	switch (function) {
	case IntegratedOperatorExpression::gradient: {
	  switch (test) {
	  case IntegratedOperatorExpression::gradient: {
	    t = gradFgradV;
	    break;
	  }
	  case IntegratedOperatorExpression::orderZero:
	  case IntegratedOperatorExpression::dx:
	  case IntegratedOperatorExpression::dy:
	  case IntegratedOperatorExpression::dz:
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	  }
	  break;
	}
	case IntegratedOperatorExpression::dx:
	case IntegratedOperatorExpression::dy:
	case IntegratedOperatorExpression::dz: {
	  switch (test) {
	  case IntegratedOperatorExpression::orderZero: {
	    t = dxFV;
	    break;
	  }
	  case IntegratedOperatorExpression::gradient:
	  case IntegratedOperatorExpression::dx:
	  case IntegratedOperatorExpression::dy:
	  case IntegratedOperatorExpression::dz: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);
	}
	}
      }
    }
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
  return t;
}


MultiLinearExpression::LinearFormType
MultiLinearExpression::
linearFormType() const
{
  LinearFormType t;
  LinearListType::const_iterator i = __linearList.begin();
  switch (__linearList.size()) {
  case 1: {
    const IntegratedExpression& I=*(*i)->integrated()->integratedExpression();
    if (I.integratedExpressionType() != IntegratedExpression::testFunction) {
      const std::string errorMsg
	= "error defining "+stringify(*this)+"\n"
	"1-linear operator should be defined using test functions";

      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg,
			 ErrorHandler::normal);
    } else {
      return linear;
    }
    break;
  }
  case 2: {
    return biLinear;
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }

  return t;
}


void
MultiLinearExpression::
check()
{
  if (__linearList.size() > 2) {
    this->execute();
    const std::string errorMsg
      = stringify(*this)+" is "+stringify(__linearList.size())
      +"-linear.\ncan discretize at most 2-linear expressions\n";

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg,
		       ErrorHandler::normal);
  } else if (__linearList.size() == 2) {
    LinearListType::iterator i = __linearList.begin();
    IntegratedExpression::IType itype
      = (*(*(*(*i)).integrated()).integratedExpression()).integratedExpressionType();
    i++;
    IntegratedExpression::IType jtype
      = (*(*(*(*i)).integrated()).integratedExpression()).integratedExpressionType();
    if (itype == jtype) {
      this->execute();
      const std::string errorMsg 
	= stringify(*this)+" is non linear\n"
	+"Cannot discretize non linear operators\n";

      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg,
			 ErrorHandler::normal);
    }
  }
}
