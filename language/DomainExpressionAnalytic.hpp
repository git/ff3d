//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DOMAIN_EXPRESSION_ANALYTIC_HPP
#define DOMAIN_EXPRESSION_ANALYTIC_HPP

#include <DomainExpression.hpp>
class FunctionExpression;

/**
 * @file   DomainExpressionAnalytic.hpp
 * @author Stephane Del Pino
 * @date   Thu Sep 28 21:07:11 2006
 * 
 * @brief domain defined by function expression for instance
 * @f$ \Omega=\{x/ f(x)<g(x)\} @f$
 * 
 */
class DomainExpressionAnalytic
  : public DomainExpression
{
private:
  ReferenceCounting<FunctionExpression>
  __function;			/**< the domain definition */

  /** 
   * Overload of the put function
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param f domain definition
   */
  DomainExpressionAnalytic(ReferenceCounting<FunctionExpression> f);

  /** 
   * Copy constuctor
   * 
   * @param d given domain
   */
  DomainExpressionAnalytic(const DomainExpressionAnalytic& d);

  /** 
   * Destructor
   * 
   */
  ~DomainExpressionAnalytic();
};

#endif // DOMAIN_EXPRESSION_ANALYTIC_HPP
