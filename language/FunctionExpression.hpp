//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_HPP
#define FUNCTION_EXPRESSION_HPP

#include <Expression.hpp>
#include <ReferenceCounting.hpp>

#include <iostream>

class ScalarFunctionBase;
/**
 * @file   FunctionExpression.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:05:53 2006
 * 
 * @brief  Function expression base class
 * 
 */
class FunctionExpression
  : public Expression
{
public:
  /// functions types ids
  enum FunctionType {
    binaryOperation,
    cfunction,
    composed,
    constant,
    convection,
    derivative,
    dg,
    fem,
    lagrange,
    legendre,
    linearBase,
    meshReferences,
    normalComponent,
    unaryMinus,
    variable,

    domainCharacteristic,
    meshCharacteristic,
    objectCharacteristic,

    integrate,
    functionValue,
    not_,
    read
  };
private:
  const FunctionType __type;	/**< Type of function */

  bool __unknown;		/**< is true if the function is unknown */

protected:
  ConstReferenceCounting<ScalarFunctionBase> __scalarFunction; /**< The function associated to the expression */

  /** 
   * Specialization of the put function
   * 
   * @param os the stream
   * 
   * @return the stream @a os
   */
  virtual std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Returns @b true if the functions has a boundary
   * 
   * @return false as a default value
   */
  virtual bool hasBoundaryExpression() const;

  /** 
   * Read only access to the type of the function
   * 
   * @return __type
   */
  const FunctionExpression::FunctionType& type() const;

  /** 
   * Access to the scalar function
   * 
   * @return __scalarFunction
   */
  ConstReferenceCounting<ScalarFunctionBase> function() const;

  /** 
   * Access the value of the function expression
   * 
   * @return a FunctionExpressionValue
   */
  ReferenceCounting<FunctionExpression> value();

  /** 
   * Copy constructor
   * 
   * @param e the original function expression
   * 
   */
  FunctionExpression(const FunctionExpression& e);

  /** 
   * Builds a function expression of type @a type
   * 
   * @param type the type of the function expression
   * 
   */
  FunctionExpression(const FunctionExpression::FunctionType& type);

  /** 
   * Destructor
   * 
   */
  virtual ~FunctionExpression();
};

#endif // FUNCTION_EXPRESSION_HPP
