//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_MESH_CHARACTERISTIC_HPP
#define FUNCTION_EXPRESSION_MESH_CHARACTERISTIC_HPP

#include <FunctionExpression.hpp>
#include <MeshExpression.hpp>

#include <ReferenceCounting.hpp>

/**
 * @file   FunctionExpressionMeshCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul 18 00:08:11 2006
 * 
 * @brief Describes characteristic function of a mesh
 * 
 * 
 */
class FunctionExpressionMeshCharacteristic
  : public FunctionExpression
{
private:
  ReferenceCounting<MeshExpression> __meshExpression; /**< Expression of the mesh */

  std::ostream& put(std::ostream& os) const; /**< overload of the mesh */

public:
  /** 
   * execute the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param meshExpression mesh expression
   * 
   */
  FunctionExpressionMeshCharacteristic(ReferenceCounting<MeshExpression> meshExpression);

  /** 
   * Copy constructor
   * 
   * @param f Original expression
   * 
   */
  FunctionExpressionMeshCharacteristic(const FunctionExpressionMeshCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionMeshCharacteristic();
};

#endif // FUNCTION_EXPRESSION_MESH_CHARACTERISTIC_HPP
