//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef OSTREAM_EXPRESSION_LIST_HPP
#define OSTREAM_EXPRESSION_LIST_HPP

#include <Expression.hpp>
#include <list>

class OStreamExpressionList
  : public Expression
{
private:
  typedef std::list<ReferenceCounting<Expression> > ListType;
  ListType __expressions;

  std::ostream & put(std::ostream & os) const
  {
    for (ListType::const_iterator i=__expressions.begin();
	 i != __expressions.end(); ++i) {
      os << (*(*i));
    }
    return os;
  }

public:
  void operator << (ReferenceCounting<Expression> e)
  {
    __expressions.push_back(e);
  }

  void __execute()
  {
    for(ListType::iterator i=__expressions.begin();
	i != __expressions.end(); ++i) {
      (*(*i)).execute();
    }
  }

  OStreamExpressionList()
    : Expression(Expression::ostreamExpressionList)
  {
    ;
  }

  ~OStreamExpressionList()
  {
    ;
  }
};

#endif // OSTREAM_EXPRESSION_LIST_HPP

