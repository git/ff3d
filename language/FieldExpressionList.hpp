//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FIELD_EXPRESSION_LIST_HPP
#define FIELD_EXPRESSION_LIST_HPP

#include <Expression.hpp>

#include <ReferenceCounting.hpp>
#include <FieldExpression.hpp>

#include <vector>

/**
 * @file   FieldExpressionList.hpp
 * @author St�phane Del Pino
 * @date   Thu Feb 22 12:12:47 2007
 * 
 * @brief  manages a list of fields of functions
 */
class FieldExpressionList
  : public Expression
{
private:
  std::vector <ReferenceCounting<FieldExpression> > __list;

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

  /** 
   * Copy constructor is forbiden
   * 
   */
  FieldExpressionList(const FieldExpressionList&);

public:
  /** 
   * Access to the ith field
   * 
   * @return the ith field
   */
  ReferenceCounting<FieldExpression> field(const size_t& i);

  /** 
   * Read-only access to the ith field
   * 
   * @return the ith field
   */
  ConstReferenceCounting<FieldExpression> field(const size_t& i) const;

  /** 
   * Executes the field expression
   * 
   */
  void __execute();

  /** 
   * Access to the number of fields in the list
   * 
   * @return the number of fields in the list
   */
  size_t numberOfFields() const;

  /** 
   * Adds a field to the list
   * 
   * @param field next field of the list
   */
  void add(ReferenceCounting<FieldExpression> field);

  /** 
   * Constructor
   */
  FieldExpressionList();

  /** 
   * Destructor
   * 
   */
  ~FieldExpressionList();
};

#endif // FIELD_EXPRESSION_LIST_HPP
