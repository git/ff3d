//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_FORMULA_EXPRESSION_HPP
#define VARIATIONAL_FORMULA_EXPRESSION_HPP

#include <Expression.hpp>

#include <ReferenceCounting.hpp>
#include <MultiLinearFormExpression.hpp>

#include <list>

#include <BoundaryExpression.hpp>
#include <VariationalOperatorExpression.hpp>

/**
 * @file   VariationalFormulaExpression.hpp
 * @author Stephane Del Pino
 * @date   Mon May 27 18:16:55 2002
 * 
 * @brief  reads the bilinear form and interprets it
 * 
 * This class jobs is to understand the bilinear and linear forms in
 *order to discretize them.
 */
class VariationalFormulaExpression
  : public Expression
{
private:
  friend class VariationalProblemExpression;

  ReferenceCounting<MultiLinearFormSumExpression>
  __givenMultiLinearExpression; /**< the fiven multi-linear expression */

  typedef std::list<ReferenceCounting<VariationalBilinearOperatorExpression> >
  BilinearOperatorList;

  BilinearOperatorList __bilinearPlus; /**< added bilinear expressions */
  BilinearOperatorList __bilinearMinus;	/**< removed bilinear expressions */

  BilinearOperatorList __bilinearBorderPlus; /**< added bilinear expressions living on a border */
  BilinearOperatorList __bilinearBorderMinus;	/**< removed bilinear expressions living on a border */

  typedef std::list<ReferenceCounting<VariationalLinearOperatorExpression> >
  LinearOperatorList;

  LinearOperatorList __linearPlus; /**< added linear expressions */
  LinearOperatorList __linearMinus; /**< removed linear expressions */

  LinearOperatorList __linearBorderPlus; /**< added linear expressions living on a border */
  LinearOperatorList __linearBorderMinus; /**< removed linear expressions living on a border */

  /** 
   * printes operators lists
   * 
   * @param listType a list
   * @param delimiter typically '+' or '-'
   * @param first true if the variational term is the first to write
   * @param os given stream
   * 
   * @return modified stream
   */
  template <typename ListType>
  std::ostream& __putList(const ListType& listType,
			  char delimiter,
			  bool& first,
			  std::ostream& os) const;

  /** 
   * Overloading of Expression::put()
   * 
   * @param os incoming stream
   * 
   * @return os modified stream
   */
  std::ostream& put(std::ostream& os) const;

  /** 
   * Internal function used to rewrite the variational formula in term of 
   * linear and bilinear form
   */
  void __getBandLForms2D(MultiLinearExpressionSum::iterator begin,
			 MultiLinearExpressionSum::iterator end,
			 BilinearOperatorList& bilinearPlus,
			 BilinearOperatorList& bilinearMinus,
			 LinearOperatorList& linearPlus,
			 LinearOperatorList& linearMinus,
			 ReferenceCounting<BoundaryExpression> b);

  /** 
   * Internal function used to rewrite the variational formula in term of 
   * linear and bilinear form
   */
  void __getBandLForms3D(MultiLinearExpressionSum::iterator begin,
			 MultiLinearExpressionSum::iterator end,
			 BilinearOperatorList& bilinearPlus,
			 BilinearOperatorList& bilinearMinus,
			 LinearOperatorList& linearPlus,
			 LinearOperatorList& linearMinus);

  /** 
   * Internal function used to rewrite the variational formula in term of 
   * linear and bilinear form
   */
  void __getBilinearAndLinearForms(MultiLinearFormSumExpression::iterator begin,
				   MultiLinearFormSumExpression::iterator end,
				   BilinearOperatorList& bilinearPlus,
				   BilinearOperatorList& bilinearMinus,
				   BilinearOperatorList& bilinearBorderPlus,
				   BilinearOperatorList& bilinearBorderMinus,
				   LinearOperatorList& linearPlus,
				   LinearOperatorList& linearMinus,
				   LinearOperatorList& linearBorderPlus,
				   LinearOperatorList& linearBorderMinus);

public:

  /** 
   * Returns true if one of the boundaries is defined by a POVRef
   * 
   * @return true if one of the boundaries is defined by a POVRef
   */
  bool hasPOVBoundary() const;

  /** 
   * Returns true if one of the boundaries is defined by an external mesh
   * 
   * @return true if one of the boundaries is defined by an external mesh
   */
  bool hasPredefinedBoundary() const;

  /** 
   * Expression::execute() overloading
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param m multi-linear form
   * 
   */
  VariationalFormulaExpression(ReferenceCounting<MultiLinearFormSumExpression> m);

  /** 
   * Copy constructor
   * 
   * @param v a Variational formula
   * 
   */
  VariationalFormulaExpression(const VariationalFormulaExpression& v);

  /** 
   * The destructor
   * 
   */
  ~VariationalFormulaExpression();
};

#endif // VARIATIONAL_FORMULA_EXPRESSION_HPP

