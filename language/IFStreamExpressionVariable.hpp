//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef IFSTREAM_EXPRESSION_VARIABLE_HPP
#define IFSTREAM_EXPRESSION_VARIABLE_HPP

#include <IFStreamExpression.hpp>
#include <Variable.hpp>

/**
 * @file   OFStreamExpressionVariable.hpp
 * @author Stephane Del Pino
 * @date   Mon Sep 29 00:12:23 2008
 * 
 * @brief  ifstream expression variable
 */
class IFStreamExpressionVariable
  : public IFStreamExpression
{
private:
  const std::string __ifstreamName;

  ReferenceCounting<IFStreamVariable>
  __ifstreamVariable;		/**< the variable */

  /** 
   * Overload of the put function
   * 
   * @param is given stream
   * 
   * @return is
   */
  std::ostream& put(std::ostream& os) const
  {
    os << __ifstreamVariable->name();
    return os;
  }

public:
  /** 
   * Executes the variable expression
   * 
   */
  void __execute();

  /** 
   * Access to the ofstream expression related to this variable
   * 
   * @return __ifstreamVariable
   */
  ConstReferenceCounting<IFStreamExpression>
  ifstreamExpression() const;

  /** 
   * Constructor
   * 
   * @param ifstreamName given variable name
   */
  IFStreamExpressionVariable(const std::string& ifstreamName);

  /** 
   * Copy constructor
   * 
   * @param e given expression
   */
  IFStreamExpressionVariable(const IFStreamExpressionVariable& e);

  /** 
   * Destructor
   * 
   */
  ~IFStreamExpressionVariable();
};

#endif // IFSTREAM_EXPRESSION_VARIABLE_HPP
