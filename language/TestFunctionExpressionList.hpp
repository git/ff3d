//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef TEST_FUNCTION_EXPRESSION_LIST_HPP
#define TEST_FUNCTION_EXPRESSION_LIST_HPP

#include <Expression.hpp>
#include <Variable.hpp>

#include <vector>

/**
 * @file   TestFunctionExpressionList.hpp
 * @author St�phane Del Pino
 * @date   Sat Feb 10 13:07:57 2007
 * 
 * @brief  managed test function list
 * 
 */
class TestFunctionExpressionList
  : public Expression
{
private:
  typedef std::vector<ReferenceCounting<TestFunctionVariable> > ListType;
  ListType __list;		/**< List of test functions */

  /** 
   * Expression::put overload
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Returns the number of test functions
   * 
   * @return size
   */
  size_t size() const;

  /** 
   * Gets the number of a test function in the list
   * 
   * @param name test function name
   * 
   * @return its position in the list
   */
  size_t number(const std::string& name) const;

  /** 
   * Adds a test functino to the list
   * 
   * @param t 
   */
  void add(ReferenceCounting<TestFunctionVariable> t);

  /** 
   * Execute the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   */
  TestFunctionExpressionList();

  /** 
   * Destructor
   * 
   */
  ~TestFunctionExpressionList();
};

#endif // TEST_FUNCTION_EXPRESSION_LIST_HPP
