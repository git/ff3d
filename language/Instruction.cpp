//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <config.h>

#include <cstdlib>
#include <fstream>

#include <Mesh.hpp>

#include <Structured3DMesh.hpp>

#include <Instruction.hpp>
#include <FunctionExpression.hpp>

#include <FunctionExpressionFEM.hpp>
#include <FunctionExpressionValue.hpp>

#include <FEMFunctionBuilder.hpp>

#include <StringExpression.hpp>

#include <Scene.hpp>

#include <MeshExpression.hpp>
#include <Variable.hpp>

#include <Information.hpp>

#include <VTKDriver.hpp>

#include <FEMFunction.hpp>
#include <LagrangeFunction.hpp>
#include <LegendreFunction.hpp>

#include <ErrorHandler.hpp>

#include <WriterMedit.hpp>
#include <WriterRaw.hpp>
#include <WriterVTK.hpp>

void InstructionExec::__execute()
{
  __command->execute();
  std::string command = __command->value();
  ffout(2) << "executing: " << command << '\n';
#ifdef ALLOW_EXEC
  int result = system(command.c_str());
  if (result == -1) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "could not execute: "+command,
		       ErrorHandler::normal);
  }
#else // ALLOW_EXEC
  fferr(2) << "warning: this version was not compiled with 'exec' support.\n";
#endif // ALLOW_EXEC
}

InstructionExec::InstructionExec(ReferenceCounting<StringExpression> command)
  : Instruction(Instruction::exec),
    __command(command)
{
  ;
}

InstructionExec::InstructionExec(const InstructionExec& I)
  : Instruction(I),
    __command(I.__command)
{
  ;
}

InstructionExec:: ~InstructionExec()
{
  ;
}


void InstructionCat::__execute()
{
  __filename->execute();

  const std::string filename = __filename->value();
  std::ifstream fin(filename.c_str());

  if (not fin) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "could not open file \""+filename+"\"",
		       ErrorHandler::normal);
  }
  char c = fin.get();
  while (not(fin.eof())) {
    ffout(0) << c;
    c = fin.get();
  }
}

InstructionCat::
InstructionCat(ReferenceCounting<StringExpression> filename)
  : Instruction(Instruction::cat),
    __filename(filename)
{
  ;
}

InstructionCat::
InstructionCat(const InstructionCat& I)
  : Instruction(I),
    __filename(I.__filename)
{
  ;
}

InstructionCat::
~InstructionCat()
{
  ;
}


InstructionSave::
InstructionSave(ReferenceCounting<FileDescriptor> descriptor,
		ReferenceCounting<StringExpression> fileName,
		ReferenceCounting<MeshExpression> mesh)
  : Instruction(Instruction::save),
    __fileDescriptor(descriptor),
    __fileName(fileName),
    __mesh(mesh)
{
  ;
}

InstructionSave::InstructionSave(const InstructionSave& I)
  : Instruction(I),
    __fileDescriptor(I.__fileDescriptor),
    __fileName(I.__fileName),
    __mesh(I.__mesh)
{
  ;
}

InstructionSave::~InstructionSave()
{
  ;
}

InstructionSaveMesh::
InstructionSaveMesh(ReferenceCounting<FileDescriptor> descriptor,
		    ReferenceCounting<StringExpression> fileName,
		    ReferenceCounting<MeshExpression> mesh)
  : InstructionSave(descriptor, fileName, mesh)
{
  ;
}

InstructionSaveMesh::
InstructionSaveMesh(const InstructionSaveMesh& I)
  : InstructionSave(I)
{
  ;
}

InstructionSaveMesh::
~InstructionSaveMesh()
{
  ;
}

void InstructionSaveMesh::__execute()
{
  __mesh->execute();
  __fileName->execute();

  Information::instance().setMesh(__mesh->mesh());

  ReferenceCounting<WriterBase> writer;

  switch(__fileDescriptor->format()) {
  case FileDescriptor::medit: {
    writer = new WriterMedit(__mesh->mesh(),
			     __fileName->value(),
			     *__fileDescriptor);
    break;
  }
  case FileDescriptor::raw: {
    writer = new WriterRaw(__mesh->mesh(),
			   __fileName->value(),
			   *__fileDescriptor);
    break;
  }
  case FileDescriptor::vtk: {
    writer = new WriterVTK(__mesh->mesh(),
			   __fileName->value(),
			   *__fileDescriptor);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "file type \""+__fileDescriptor->formatName()+'\"',
		       ErrorHandler::unexpected);
  }
  }

  writer->proceed();

  //! The mesh is nomore used in this region.
  Information::instance().unsetMesh();
}

void
InstructionAffectation<FunctionExpression, FunctionVariable>::
__execute()
{
  FunctionVariable* functionVariable
    = VariableRepository::instance().findVariable<FunctionVariable>(__variableName);

  ConstReferenceCounting<FunctionExpression> f = functionVariable->expression();

  const ScalarFunctionBase& scalarFunction = *f->function();

  switch(scalarFunction.type()) {
  case ScalarFunctionBase::femfunction: {
    const FEMFunctionBase& femFunction = dynamic_cast<const FEMFunctionBase&>(scalarFunction);

    Information::instance().setMesh(femFunction.baseMesh());

    __expression->execute();

    FEMFunctionBuilder builder;
    builder.build(femFunction.discretizationType(),
		  femFunction.baseMesh(),
		  *__expression->function());

    (*functionVariable)
      = new FunctionExpressionValue(builder.getBuiltScalarFunction(), false);

    Information::instance().unsetMesh();
    break;
  }
  case ScalarFunctionBase::legendre: {
    const LegendreFunction& legendreFunction
      = dynamic_cast<const LegendreFunction&>(scalarFunction);
    const Mesh* mesh = legendreFunction.mesh();
    Information::instance().setMesh(mesh);

    __expression->execute();

    (*functionVariable)
      = new FunctionExpressionValue(new LegendreFunction(legendreFunction.mesh(),
							 *__expression->function()),
				    false);
    Information::instance().unsetMesh();
    break;
  }
  case ScalarFunctionBase::lagrange: {
    const LagrangeFunction& lagrangeFunction
      = dynamic_cast<const LagrangeFunction&>(scalarFunction);
    const Mesh* mesh = lagrangeFunction.mesh();
    Information::instance().setMesh(mesh);

    __expression->execute();

    (*functionVariable)
      = new FunctionExpressionValue(new LagrangeFunction(lagrangeFunction.mesh(),
							 *__expression->function()),
				    false);
    Information::instance().unsetMesh();
    break;
  }
  default: {
    __expression->execute();
    (*functionVariable) = __expression->value();
  }
  }
}


InstructionSaveFieldList::
InstructionSaveFieldList(ReferenceCounting<FileDescriptor> descriptor,
			 ReferenceCounting<StringExpression> fileName,
			 ReferenceCounting<FieldExpressionList> fieldList,
			 ReferenceCounting<MeshExpression> mesh)
  : InstructionSave(descriptor,fileName,mesh),
    __fieldList(fieldList)
{
  ;
}

InstructionSaveFieldList::
InstructionSaveFieldList(const InstructionSaveFieldList& I)
  : InstructionSave(I),
    __fieldList(I.__fieldList)
{
  ;
}

InstructionSaveFieldList::
~InstructionSaveFieldList()
{
  ;
}

void InstructionSaveFieldList::
__execute()
{
  __mesh->execute();
  __fieldList->execute();
  __fileName->execute();

  Information::instance().setMesh(__mesh->mesh());

  const std::string CR = __fileDescriptor->cr();

  ReferenceCounting<WriterBase> writer;

  switch(__fileDescriptor->format()) {
  case FileDescriptor::medit: {
    writer = new WriterMedit(__mesh->mesh(),
			     __fileName->value(),
			     *__fileDescriptor);
    break;
  }
  case FileDescriptor::raw: {
    writer = new WriterRaw(__mesh->mesh(),
			   __fileName->value(),
			   *__fileDescriptor);
    break;
  }
  case FileDescriptor::vtk: {
    writer = new WriterVTK(__mesh->mesh(),
			   __fileName->value(),
			   *__fileDescriptor);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "file type \""+__fileDescriptor->formatName()+'\"',
		       ErrorHandler::unexpected);
  }
  }

  for (size_t iList = 0; iList<__fieldList->numberOfFields(); ++iList) {
    ReferenceCounting<FieldExpression> field = __fieldList->field(iList);

    switch(field->numberOfComponents()) {
    case 0: {
      break;
    }
    case 1: {
      writer->add(field->field()->function(0));
      break;
    }
    default: {
      writer->add(field->field());
    }
    }
  }

  writer->proceed();

  //! The mesh is nomore used in this region.
  Information::instance().unsetMesh();
}

void InstructionPlot::__execute()
{
  __mesh->execute();

  const Mesh& M = *(__mesh->mesh());
  VTKDriver d;

  if (__f == 0) {
    d.plot(M);
  } else {
    __f->execute();
    ConstReferenceCounting<ScalarFunctionBase> u
      = __f->value()->function();
    d.plot(M, u);
  }
}

InstructionPlot::InstructionPlot(ReferenceCounting<MeshExpression> m,
				 ReferenceCounting<FunctionExpression> f)
  : Instruction(Instruction::plot),
    __mesh(m), 
    __f(f)
{
  ;
}

InstructionPlot::InstructionPlot(const InstructionPlot& I)
  : Instruction(I),
    __mesh(I.__mesh)
{
  ;
}

InstructionPlot::~InstructionPlot()
{
  ;
}

void InstructionUsingScene::__execute()
{
  __sceneExpression->execute();
  Information::instance().setScene(__sceneExpression->scene());
}

InstructionUsingScene::InstructionUsingScene(ReferenceCounting<SceneExpression> e)
  : Instruction(Instruction::Using),
    __sceneExpression(e)
{
  ;
}

InstructionUsingScene::InstructionUsingScene(const InstructionUsingScene& I)
  : Instruction(I),
    __sceneExpression(I.__sceneExpression)
{
  ;
}

InstructionUsingScene::~InstructionUsingScene()
{
  ;
}

void InstructionCoarseMesh::__execute()
{
  Information::instance().setCoarseMesh(__coarseMesh);
}

void InstructionBlockBegin::__execute()
{
  VariableRepository::instance().beginBlock();
}

void InstructionBlockEnd::__execute()
{
  VariableRepository::instance().endBlock();
}

