//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_OPERATOR_EXPRESSION_HPP
#define VARIATIONAL_OPERATOR_EXPRESSION_HPP

#include <Expression.hpp>

#include <Variable.hpp>

#include <ReferenceCounting.hpp>
#include <FunctionExpression.hpp>

#include <BoundaryExpression.hpp>

#include <VariationalOperator.hpp>

/**
 * @file   VariationalOperatorExpression.hpp
 * @author Stephane Del Pino
 * @date   Mon May 27 17:51:28 2002
 * 
 * @brief  Variational operator description
 * 
 * This file contain "Variational operators" description. It means
 *that all PDE operator's associated weak forms can be found here. 
 */
class VariationalBilinearOperatorExpression
  : public Expression
{
public:
  enum OperatorType {
    mugradUgradV,		/**< \f$ \int \mu \nabla u \nabla v \f$ */
    alphaDxUDxV,		/**< \f$ \int \nabla u A \nabla v \f$ */
    nuDxUV,			/**< \f$ \int \nu \partial_{x_i} u v \f$ */
    nuUdxV,			/**< \f$ \int \nu u \partial_{x_i} v \f$ */
    alphaUV,			/**< \f$ \int \alpha u v \f$ */
  };

private:
  OperatorType __operatorType;	/**< operator type */

  ReferenceCounting<BoundaryExpression> __border; /**< border */

  /** 
   * Checks that no boundary expression is used in volumic expression
   * (normal to the boundary for instance)
   */
  void __checkNoBoundaryExpression() const {
    if (this->__hasBoundaryExpression()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot evaluate the expression \""
			 +stringify(*this)+
			 "\" in the volume",
			 ErrorHandler::normal);
    }
  }

protected:
  /** 
   * proceed execution
   * 
   */
  virtual void __executeRemaining() = 0;
  virtual bool __hasBoundaryExpression() const = 0;

  std::string __propertyBeginWrite(const VariationalOperator::Property& property) const
  {
    switch(property) {
    case VariationalOperator::mean: {
      return "{";
      break;
    }
    case VariationalOperator::jump: {
      return "[";
      break;
    }
    default: {
      return "";
    }
    }
  }

  std::string __propertyEndWrite(const VariationalOperator::Property& property) const
  {
    switch(property) {
    case VariationalOperator::mean: {
      return "}";
      break;
    }
    case VariationalOperator::jump: {
      return "]";
      break;
    }
    default: {
      return "";
    }
    }
  }

  std::string __unknownTypeBeginWrite() const
  {
    return __propertyBeginWrite(__unknownProperty);
  }

  std::string __unknownTypeEndWrite() const
  {
    return __propertyEndWrite(__unknownProperty);
  }

  std::string __testTypeBeginWrite() const
  {
    return __propertyBeginWrite(__testFunctionProperty);
  }

  std::string __testTypeEndWrite() const
  {
    return __propertyEndWrite(__testFunctionProperty);
  }

  char __directionName(const size_t& i) const
  {
    switch(i) {
    case 0: return 'x';
    case 1: return 'y';
    case 2: return 'z';
    default: {
      throw ErrorHandler(__FILE__,__LINE__,": unknown direction",
			 ErrorHandler::unexpected);
      return '0';
    }
    }
  }


  const std::string
  __unknownName;		/**< unknown variable */
  
  const VariationalOperator::Property
  __unknownProperty;		/**< unknown property */

  const std::string
  __testFunctionName;		/**< test function */

  const VariationalOperator::Property
  __testFunctionProperty;	/**< test function property */

public:

  /** 
   * Returns the border Expression
   * 
   * @return __border
   */
  ReferenceCounting<BoundaryExpression> border() const
  {
    return __border;
  }

  /** 
   * Expression::execute() overloading
   * 
   */
  void __execute()
  {
    if(__border != 0) {
      (*__border).execute();
    }
    __executeRemaining();
    if(__border == 0) {
      __checkNoBoundaryExpression();
    }
  }

  /** 
   * 
   * Access to the operator type
   * 
   * @return __operatorType
   */  OperatorType operatorType()
  {
    return __operatorType;
  }

  /** 
   * 
   * Access to the unknown variable
   * 
   * @return __unknownName
   */
  const std::string& unknownName() const
  {
    return __unknownName;
  }

  /** 
   * 
   * Access to the test function
   * 
   * @return __testFunctionName
   */
  const std::string& testFunctionName() const
  {
    return __testFunctionName;
  }

  /** 
   * Gets unknown property
   * 
   * @return __unknownProperty
   */
  const VariationalOperator::Property& unknownProperty() const
  {
    return __unknownProperty;
  }

  /** 
   * Gets test function property
   * 
   * @return __testFunctionProperty
   */
  const VariationalOperator::Property& testFunctionProperty() const
  {
    return __testFunctionProperty;
  }

  /** 
   * Copy Constructor
   * 
   * @param V Variational Bilinear Operator
   */
  VariationalBilinearOperatorExpression(const VariationalBilinearOperatorExpression& V)
    : Expression(V),
      __operatorType(V.__operatorType),
      __border(V.__border),
      __unknownName(V.__unknownName),
      __unknownProperty(V.__unknownProperty),
      __testFunctionName(V.__testFunctionName),
      __testFunctionProperty(V.__testFunctionProperty)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param type type of operator 
   * @param border the border where to compute this term
   * @param unknownName unknown function name
   * @param unknownProperty properties of the unknown function
   * @param testFunctionName test function name
   * @param testFunctionProperty properties of the test function
   */
  VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::OperatorType type,
					ReferenceCounting<BoundaryExpression> border,
					const std::string& unknownName,
					const VariationalOperator::Property& unknownProperty,
					const std::string& testFunctionName,
					const VariationalOperator::Property& testFunctionProperty)
    : Expression(Expression::variationalBilinearOperator),
      __operatorType(type),
      __border(border),
      __unknownName(unknownName),
      __unknownProperty(unknownProperty),
      __testFunctionName(testFunctionName),
      __testFunctionProperty(testFunctionProperty)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalBilinearOperatorExpression()
  {
    ;
  }
};

/**
 * @class  VariationalLinearOperatorExpression
 * @author Stephane Del Pino
 * @date   Wed May 29 11:13:20 2002
 * 
 * @brief  Describes linear operators
 * 
 * This is used to describe linear operators going to the right hand
 * side of a variational formulation.
 */

class VariationalLinearOperatorExpression
  : public Expression
{
public:
  enum OperatorType {
    FV,				/**< \f$\int fv\f$ */
    FdxGV,			/**< \f$ \int f\partial_{x_i} g v \f$ */
    FdxV,			/**< \f$ \int f\partial_{x_i} v \f$ */
    FgradGgradV			/**< \f$ \int f\\nabla g\cdot\nabla v \f$ */
  };

private:
  const OperatorType
  __operatorType;		/**< operator type */

  ReferenceCounting<BoundaryExpression> __border; /**< border */

  virtual void __executeRemaining() = 0;
  void __checkNoBoundaryExpression() const {
    if (this->__hasBoundaryExpression()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot evaluate the expression \""
			 +stringify(*this)+
			 "\" in the volume",
			 ErrorHandler::normal);
    }
  }

protected:

  std::string __propertyBeginWrite(const VariationalOperator::Property& property) const
  {
    switch(property) {
    case VariationalOperator::mean: {
      return "{";
      break;
    }
    case VariationalOperator::jump: {
      return "[";
      break;
    }
    default: {
      return "";
    }
    }
  }

  std::string __propertyEndWrite(const VariationalOperator::Property& property) const
  {
    switch(property) {
    case VariationalOperator::mean: {
      return "}";
      break;
    }
    case VariationalOperator::jump: {
      return "]";
      break;
    }
    default: {
      return "";
    }
    }
  }

  std::string __testTypeBeginWrite() const
  {
    return __propertyBeginWrite(__testFunctionProperty);
  }

  std::string __testTypeEndWrite() const
  {
    return __propertyEndWrite(__testFunctionProperty);
  }

  virtual bool __hasBoundaryExpression() const = 0;

  char __directionName(const size_t& i) const
  {
    switch(i) {
    case 0: return 'x';
    case 1: return 'y';
    case 2: return 'z';
    default: {
      throw ErrorHandler(__FILE__,__LINE__,": unknown direction",
			 ErrorHandler::unexpected);
      return '0';
    }
    }
  }

  const std::string
  __testFunctionName;		/**< test function name */

  const VariationalOperator::Property
  __testFunctionProperty;	/**< test function property */

public:
  /** 
   * Returns the border Expression
   * 
   * @return __border
   */
  ReferenceCounting<BoundaryExpression> border() const
  {
    return __border;
  }

  /** 
   * Expression::execute() overloading
   * 
   */
  void __execute()
  {
    if(__border != 0) {
      (*__border).execute();
    }
    __executeRemaining();
    if(__border == 0) {
      __checkNoBoundaryExpression();
    }

  }

  /** 
   * 
   * Access to the operator type
   * 
   * @return __operatorType
   */
  OperatorType operatorType()
  {
    return __operatorType;
  }

  /** 
   * 
   * Access to the test function
   * 
   * @return __testFunctionName
   */
  const std::string& testFunctionName() const
  {
    return __testFunctionName;
  }

  /** 
   * Copy Constructor
   * 
   * @param V Variational Linear Operator
   */
  VariationalLinearOperatorExpression(const VariationalLinearOperatorExpression& V)
    : Expression(V),
      __operatorType(V.__operatorType),
      __border(V.__border),
      __testFunctionName(V.__testFunctionName),
      __testFunctionProperty(V.__testFunctionProperty)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param type type of operator 
   * @param border the border where the variational linear operator is defined
   * @param testFunctionName test function
   * @param testFunctionProperty test function property
   */
  VariationalLinearOperatorExpression(VariationalLinearOperatorExpression::OperatorType type,
				      ReferenceCounting<BoundaryExpression> border,
				      const std::string& testFunctionName,
				      const VariationalOperator::Property& testFunctionProperty)
    : Expression(Expression::variationalBilinearOperator),
      __operatorType(type),
      __border(border),
      __testFunctionName(testFunctionName),
      __testFunctionProperty(testFunctionProperty)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalLinearOperatorExpression()
  {
    ;
  }
};

/**********************
 * BILINEAR OPERATORS *
 **********************/

/**
 * @class  VariationalMuGradUGradVExpression
 * @author Stephane Del Pino
 * @date   Tue May 28 15:43:26 2002
 * 
 * @brief  \f$\mu\nabla u\nabla v\f$ operator
 * 
 * \f$\mu\nabla u\nabla v\f$ operator
 */
class VariationalMuGradUGradVExpression
  : public VariationalBilinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __mu; /**< viscosity \f$\mu\f$*/

  bool __hasBoundaryExpression() const
  {
    return (*__mu).hasBoundaryExpression();
  }

public:

  /** 
   * Access to the viscosity \f$\mu\f$
   * 
   * 
   * @return __mu
   */
  ReferenceCounting<FunctionExpression> mu()
  {
    return __mu;
  }

  /** 
   * VariationalBilinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__mu).execute();
  }

  /** 
   * put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__mu
       << '*'
       << this->__unknownTypeBeginWrite()
       << "grad(" << __unknownName << ')'
       << this->__unknownTypeEndWrite()
       << '*'
       << this->__testTypeBeginWrite()
       << "grad(" << __testFunctionName << ')'
       << this->__testTypeEndWrite();
    return os;
  }

  /** 
   * Constructor
   * 
   * @param mu viscosity @f$ \mu @f$
   * @param unknownName unknown function name
   * @param unknownProperty unknown function properties
   * @param testFunctionName test function name
   * @param testFunctionProperty test function properties
   * @param border boundary
   */
  VariationalMuGradUGradVExpression(ReferenceCounting<FunctionExpression> mu,
				    const std::string& unknownName,
				    const VariationalOperator::Property& unknownProperty,
				    const std::string& testFunctionName,
				    const VariationalOperator::Property& testFunctionProperty,
				    ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::
					    mugradUgradV,
					    border,
					    unknownName,
					    unknownProperty,
					    testFunctionName,
					    testFunctionProperty),
      __mu(mu)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalMuGradUGradVExpression(const VariationalMuGradUGradVExpression& V)
    : VariationalBilinearOperatorExpression(V),
      __mu(V.__mu)
  {
    ;
  }


  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalMuGradUGradVExpression()
  {
    ;
  }
};


/**
 * @class  VariationalAlphaDxUDxVExpression
 * @author Stephane Del Pino
 * @date   Tue May 28 15:43:26 2002
 * 
 * @brief  \f$\alpha \partial{x_j} u\partial_{x_i} v\f$ operator
 * 
 * \f$\alpha \partial_{x_j} u\partial{x_i} v\f$
 */
class VariationalAlphaDxUDxVExpression
  : public VariationalBilinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __Alpha; /**< \f$\alpha\f$*/
  size_t __i;			/**< the \f$ i\f$ in \f$\partial_{x_i}\f$ */
  size_t __j;			/**< the \f$ j\f$ in \f$\partial_{x_j}\f$  */

  /** 
   * put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__Alpha
       << '*'
       << this->__unknownTypeBeginWrite()
       << 'd' << __directionName(__j) << '(' << __unknownName << ')'
       << this->__unknownTypeEndWrite()
       << '*'
       << this->__testTypeBeginWrite()
       << 'd' << __directionName(__i) << '(' << __testFunctionName << ')'
       << this->__testTypeEndWrite();
    return os;
  }

  bool __hasBoundaryExpression() const
  {
    return (*__Alpha).hasBoundaryExpression();
  }

public:
  /** 
   * Access to \f$\alpha\f$
   * 
   * 
   * @return __Alpha
   */
  ReferenceCounting<FunctionExpression> alpha()
  {
    return __Alpha;
  }

  /** 
   * Access to __i
   * 
   * 
   * @return __i
   */
  const size_t& i() const
  {
    return __i;
  }

  /** 
   * Access to __j
   * 
   * 
   * @return __j
   */
  const size_t& j() const
  {
    return __j;
  }

  /** 
   * VariationalBilinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__Alpha).execute();
  }

  /** 
   * Constructor of the type \f$ \alpha \partial_{x_i} u \partial_{x_j} v\f$
   * 
   * @param alpha \f$ \alpha \f$
   * @param i \f$ \partial_{x_i} \f$
   * @param j \f$ \partial_{x_j} \f$
   * @param unknownName unknown function name
   * @param unknownProperty properties of unknown function
   * @param testFunctionName test function name
   * @param testFunctionProperty properties of the test function
   * @param border the border where to compute this term if required.
   */
  VariationalAlphaDxUDxVExpression(ReferenceCounting<FunctionExpression> alpha,
				   const size_t i,
				   const size_t j,
				   const std::string& unknownName,
				   const VariationalOperator::Property& unknownProperty,
				   const std::string& testFunctionName,
				   const VariationalOperator::Property& testFunctionProperty,
				   ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::
					    alphaDxUDxV,
					    border,
					    unknownName,
					    unknownProperty,
					    testFunctionName,
					    testFunctionProperty),
      __Alpha(alpha),
      __i(i),
      __j(j)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalAlphaDxUDxVExpression(const VariationalAlphaDxUDxVExpression& V)
    : VariationalBilinearOperatorExpression(V),
      __Alpha(V.__Alpha),
      __i(V.__i),
      __j(V.__j)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalAlphaDxUDxVExpression()
  {
    ;
  }
};



/**
 * @class  VariationalNuUdxVExpression
 * @author Stephane Del Pino
 * @date   Tue May 28 15:43:26 2002
 * 
 * @brief  \f$\nu u\partial_{x_i} v\f$ operator
 * 
 * \f$\nu u\partial_{x_i} v\f$
 */
class VariationalNuUdxVExpression
  : public VariationalBilinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __nu; /**< \f$\nu\f$*/
  size_t __number;		/**< the \f$ i\f$ in \f$\partial_{x_i}\f$ */

  /** 
   * put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__nu
       << '*'
       << this->__unknownTypeBeginWrite()
       << __unknownName
       << this->__unknownTypeEndWrite()
       << '*'
       << this->__testTypeBeginWrite()
       << 'd' << __directionName(__number) << '(' << __testFunctionName << ')'
       << this->__testTypeEndWrite();

    return os;
  }

  bool __hasBoundaryExpression() const
  {
    return (*__nu).hasBoundaryExpression();
  }

public:
  /** 
   * Access to \f$\nu\f$
   * 
   * 
   * @return __nu
   */
  ReferenceCounting<FunctionExpression> nu()
  {
    return __nu;
  }

  /** 
   * Access to __number
   * 
   * 
   * @return __number
   */
  const size_t& number() const
  {
    return __number;
  }

  /** 
   * VariationalBilinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__nu).execute();
  }

  /** 
   * Constructor
   * 
   * @param nu @f$ \nu @f$
   * @param number the number of the derivative variable (@f$ i @f$)
   * @param unknownName unknown name
   * @param unknownProperty unknown properties
   * @param testFunctionName test function name
   * @param testFunctionProperty test function properties
   * @param border boundary
   */
  VariationalNuUdxVExpression(ReferenceCounting<FunctionExpression> nu,
			      const size_t& number,
			      const std::string& unknownName,
			      const VariationalOperator::Property& unknownProperty,
			      const std::string& testFunctionName,
			      const VariationalOperator::Property& testFunctionProperty,
			      ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::
					    nuUdxV,
					    border,
					    unknownName,
					    unknownProperty,
					    testFunctionName,
					    testFunctionProperty),
      __nu(nu),
      __number(number)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V given VariationalNuUdxVExpression
   *
   */
  VariationalNuUdxVExpression(const VariationalNuUdxVExpression& V)
    : VariationalBilinearOperatorExpression(V),
      __nu(V.__nu),
      __number(V.__number)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalNuUdxVExpression()
  {
    ;
  }
};

/**
 * @class  VariationalNuDxUVExpression
 * @author Stephane Del Pino
 * @date   Tue May 28 15:43:26 2002
 * 
 * @brief  \f$\nu u\partial_{x_i} v\f$ operator
 * 
 * \f$\nu u\partial_{x_i} v\f$
 */
class VariationalNuDxUVExpression
  : public VariationalBilinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __nu; /**< \f$\nu\f$*/
  size_t __number;		/**< the \f$ i\f$ in \f$\partial_{x_i}\f$ */

  /** 
   * put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__nu
       << '*'
       << this->__unknownTypeBeginWrite()
       << 'd' << __directionName(__number) << '(' << __unknownName << ')'
       << this->__unknownTypeEndWrite()
       << '*'
       << this->__testTypeBeginWrite()
       << __testFunctionName
       << this->__testTypeEndWrite();
    return os;
  }

  bool __hasBoundaryExpression() const
  {
    return (*__nu).hasBoundaryExpression();
  }

public:
  /** 
   * Access to \f$\nu\f$
   * 
   * 
   * @return __nu
   */
  ReferenceCounting<FunctionExpression> nu()
  {
    return __nu;
  }

  /** 
   * Access to __number
   * 
   * 
   * @return __number
   */
  const size_t& number() const
  {
    return __number;
  }

  /** 
   * VariationalBilinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__nu).execute();
  }

  /** 
   * 
   * 
   * @param nu @f$ \nu @f$
   * @param number the derivative variable number (\f$ i\f$)
   * @param unknownName unknown name
   * @param unknownProperty unknown properties
   * @param testFunctionName test function name
   * @param testFunctionProperty test function properties
   * @param border boundary
   */
  VariationalNuDxUVExpression(ReferenceCounting<FunctionExpression> nu,
			      const size_t& number,
			      const std::string& unknownName,
			      const VariationalOperator::Property& unknownProperty,
			      const std::string& testFunctionName,
			      const VariationalOperator::Property& testFunctionProperty,
			      ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::
					    nuDxUV,
					    border,
					    unknownName,
					    unknownProperty,
					    testFunctionName,
					    testFunctionProperty),
      __nu(nu),
      __number(number)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalNuDxUVExpression(const VariationalNuDxUVExpression& V)
    : VariationalBilinearOperatorExpression(V),
      __nu(V.__nu),
      __number(V.__number)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalNuDxUVExpression()
  {
    ;
  }
};


/**
 * @class  VariationalAlphaUVExpression
 * @author Stephane Del Pino
 * @date   Tue May 28 16:36:45 2002
 * 
 * @brief   \f$\alpha u v\f$ operator
 * 
 * \f$\alpha u v\f$ operator
 */
class VariationalAlphaUVExpression
  : public VariationalBilinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __Alpha; /**< mass term \f$\alpha\f$*/

  bool __hasBoundaryExpression() const
  {
    return (*__Alpha).hasBoundaryExpression();
  }

  /** 
   * VariationalBilinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__Alpha).execute();
  }

public:
  ReferenceCounting<FunctionExpression> alpha()
  {
    return __Alpha;
  }

  /** 
   * put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__Alpha
       << '*'
       << this->__unknownTypeBeginWrite()
       << __unknownName
       << this->__unknownTypeEndWrite()
       << '*'
       << this->__testTypeBeginWrite()
       << __testFunctionName
       << this->__testTypeEndWrite();
    return os;
  }

  /** 
   * Constructor
   * 
   * @param alpha \f$\alpha\f$, the mass coefficient
   * @param unknownName unknown function name
   * @param unknownProperty properties of the unknown function
   * @param testFunctionName test function name
   * @param testFunctionProperty properties of the test function
   * @param border the border where to compute the integral if required.
   */
  VariationalAlphaUVExpression(ReferenceCounting<FunctionExpression> alpha,
			       const std::string& unknownName,
			       const VariationalOperator::Property& unknownProperty,
			       const std::string& testFunctionName,
			       const VariationalOperator::Property& testFunctionProperty,
			       ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalBilinearOperatorExpression(VariationalBilinearOperatorExpression::alphaUV,
					    border,
					    unknownName,
					    unknownProperty,
					    testFunctionName,
					    testFunctionProperty),
      __Alpha(alpha)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalAlphaUVExpression(const VariationalAlphaUVExpression& V)
    : VariationalBilinearOperatorExpression(V),
      __Alpha(V.__Alpha)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalAlphaUVExpression()
  {
    ;
  }
};


/********************
 * LINEAR OPERATORS *
 ********************/

/**
 * @class  VariationalFVExpression
 * @author Stephane Del Pino
 * @date   Wed May 29 11:19:01 2002
 * 
 * @brief  describes \f$\int fv\f$
 * 
 * used to describe \f$\int fv\f$
 */
class VariationalFVExpression
  : public VariationalLinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __f; /**< \f$ f\f$*/

  /** 
   * VariationalLinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__f).execute();
  }


  bool __hasBoundaryExpression() const
  {
    return (*__f).hasBoundaryExpression();
  }

  /** 
   * put function overloading
   * 
   * @param os the input (modified) stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__f
       << '*'
       << this->__testTypeBeginWrite()
       << __testFunctionName
       << this->__testTypeEndWrite();
    return os;
  }

public:

  /** 
   * 
   * Returns the \f$ f\f$ function
   * 
   * @return __f
   */
  ReferenceCounting<FunctionExpression> f()
  {
    return __f;
  }

  /** 
   * Constructor
   * 
   * @param f \f$ f\f$, the second member
   * @param testFunctionName a test function
   * @param testFunctionProperty a test function property
   * @param border the border where it is computed
   * 
   */
  VariationalFVExpression(ReferenceCounting<FunctionExpression> f,
			  const std::string& testFunctionName,
			  const VariationalOperator::Property& testFunctionProperty,
			  ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalLinearOperatorExpression(VariationalLinearOperatorExpression::FV,
					  border, testFunctionName, testFunctionProperty),
      __f(f)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalFVExpression(const VariationalFVExpression& V)
    : VariationalLinearOperatorExpression(V),
      __f(V.__f)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalFVExpression()
  {
    ;
  }
};

/**
 * @class  VariationalFdxGVExpression
 * @author Stephane Del Pino
 * @date   Sun Jun 23 19:51:48 2002
 * 
 * @brief  \f$ b(v) = \int f\partial_{x_i} g v \f$
 */
class VariationalFdxGVExpression
  : public VariationalLinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __f; /**< \f$ f\f$*/
  ReferenceCounting<FunctionExpression> __g; /**< \f$ g\f$*/
  const size_t __number;	/**< \f$ i \f$ in \f$\partial_{x_i}\f$ */

  bool __hasBoundaryExpression() const
  {
    return ((*__f).hasBoundaryExpression() or
	    (*__g).hasBoundaryExpression());
  }

  /** 
   * VariationalLinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__f).execute();
    (*__g).execute();
  }

  /** 
   * put function overloading
   * 
   * @param os the input (modified) stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__f << '*'
       << 'd'<< __directionName(__number) << '(' << *__g << ')'
       << this->__testTypeBeginWrite()
       << '*' << __testFunctionName
       << this->__testTypeEndWrite();
    return os;
  }

public:
  /** 
   * Access to __number
   * 
   * @return __number
   */
  const size_t& number() const
  {
    return __number;
  }

  /** 
   * 
   * Returns the \f$ f\f$ function
   * 
   * @return __f
   */
  ReferenceCounting<FunctionExpression> f()
  {
    return __f;
  }

  /** 
   * 
   * Returns the \f$ g\f$ function
   * 
   * @return __g
   */
  ReferenceCounting<FunctionExpression> g()
  {
    return __g;
  }

  /** 
   * Constructor
   * 
   * @param f the function \f$ f\f$
   * @param g the function \f$ g \f$
   * @param testFunctionName a test function
   * @param testFunctionProperty a test function property
   * @param n the number of the derivative variable (\f$ i\f$)
   * @param border the border where it is computed
   * 
   */
  VariationalFdxGVExpression(ReferenceCounting<FunctionExpression> f,
			     ReferenceCounting<FunctionExpression> g,
			     const std::string& testFunctionName,
			     const VariationalOperator::Property& testFunctionProperty,
			     const size_t& n,
			     ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalLinearOperatorExpression(VariationalLinearOperatorExpression::FdxGV,
					  border, testFunctionName, testFunctionProperty),
      __f(f),
      __g(g),
      __number(n)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalFdxGVExpression(const VariationalFdxGVExpression& V)
    : VariationalLinearOperatorExpression(V),
      __f(V.__f),
      __g(V.__g),
      __number(V.__number)
  {
    ;
  }


  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalFdxGVExpression()
  {
    ;
  }
};

/**
 * @class  VariationalFdxGVExpression
 * @author Stephane Del Pino
 * @date   Sun Jun 23 19:51:48 2007
 * 
 * @brief  \f$ b(v) = \int f\partial_{x_i} v \f$
 */
class VariationalFdxVExpression
  : public VariationalLinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __f; /**< \f$ f\f$*/
  const size_t __number;	/**< \f$ i \f$ in \f$\partial_{x_i}\f$ */

  bool __hasBoundaryExpression() const
  {
    return __f->hasBoundaryExpression();
  }

  /** 
   * VariationalLinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    __f->execute();
  }

  /** 
   * put function overloading
   * 
   * @param os the input (modified) stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__f << '*'
       << this->__testTypeBeginWrite()
       <<'d'<< __directionName(__number) << '(' <<  __testFunctionName << ')'
       << this->__testTypeEndWrite();
    return os;
  }

public:
  /** 
   * Access to __number
   * 
   * @return __number
   */
  const size_t& number() const
  {
    return __number;
  }

  /** 
   * 
   * Returns the \f$ f\f$ function
   * 
   * @return __f
   */
  ReferenceCounting<FunctionExpression> f()
  {
    return __f;
  }

  /** 
   * Constructor
   * 
   * @param f the function \f$ f\f$
   * @param testFunctionName a test function
   * @param testFunctionProperty a test function property
   * @param n the number of the derivative variable (\f$ i\f$)
   * @param border the border where it is computed
   */
  VariationalFdxVExpression(ReferenceCounting<FunctionExpression> f,
			    const std::string& testFunctionName,
			    const VariationalOperator::Property& testFunctionProperty,
			    const size_t& n,
			    ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalLinearOperatorExpression(VariationalLinearOperatorExpression::FdxV,
					  border, testFunctionName, testFunctionProperty),
      __f(f),
      __number(n)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V 
   *
   */
  VariationalFdxVExpression(const VariationalFdxVExpression& V)
    : VariationalLinearOperatorExpression(V),
      __f(V.__f),
      __number(V.__number)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalFdxVExpression()
  {
    ;
  }
};

/**
 * @class  VariationalOperatorExpression
 * @author Stephane Del Pino
 * @date   Sun Nov 21 18:20:15 2004
 * 
 * @brief  \f$ b(v) = \int f\nabla g\cdot\nabla v \f$
 * 
 */
class VariationalFgradGgradVExpression
  : public VariationalLinearOperatorExpression
{
private:
  ReferenceCounting<FunctionExpression> __f; /**< \f$ f\f$*/
  ReferenceCounting<FunctionExpression> __g; /**< \f$ g\f$*/

  /** 
   * VariationalLinearOperatorExpression::__executeRemaining() overloading
   * 
   */
  void __executeRemaining()
  {
    (*__f).execute();
    (*__g).execute();
  }

  bool __hasBoundaryExpression() const
  {
    return ((*__f).hasBoundaryExpression() or
	    (*__g).hasBoundaryExpression());
  }

  /** 
   * put function overloading
   * 
   * @param os the input (modified) stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__f << "*grad("<< *__g << ')'
       <<'*'
       << this->__testTypeBeginWrite()
       << "grad(" << __testFunctionName << ')'
       << this->__testTypeEndWrite();
    return os;
  }

public:
  /** 
   * 
   * Returns the \f$ f\f$ function
   * 
   * @return __f
   */
  ReferenceCounting<FunctionExpression> f()
  {
    return __f;
  }

  /** 
   * 
   * Returns the \f$ g\f$ function
   * 
   * @return __g
   */
  ReferenceCounting<FunctionExpression> g()
  {
    return __g;
  }


  /** 
   * Constructor
   * 
   * @param f the function \f$ f\f$
   * @param g the function \f$ g \f$
   * @param testFunctionName a test function
   * @param testFunctionProperty a test function property
   * @param border the border where it is computed
   */
  VariationalFgradGgradVExpression(ReferenceCounting<FunctionExpression> f,
				   ReferenceCounting<FunctionExpression> g,
				   const std::string& testFunctionName,
				   const VariationalOperator::Property& testFunctionProperty,
				   ReferenceCounting<BoundaryExpression> border = 0)
    : VariationalLinearOperatorExpression(VariationalLinearOperatorExpression::FgradGgradV,
					  border, testFunctionName, testFunctionProperty),
      __f(f),
      __g(g)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V a VariationalFgradGgradVExpression
   *
   */
  VariationalFgradGgradVExpression(const VariationalFgradGgradVExpression& V)
    : VariationalLinearOperatorExpression(V),
      __f(V.__f),
      __g(V.__g)
  {
    ;
  }


  /** 
   * 
   * Destructor
   * 
   */
  ~VariationalFgradGgradVExpression()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_EXPRESSION_HPP

