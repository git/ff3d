//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef ISTREAM_EXPRESSION_HPP
#define ISTREAM_EXPRESSION_HPP

#include <Expression.hpp>
#include <istream>

class IFStreamVariable;

class IStreamExpression
  : public Expression
{
private:
  const std::string
  __ifstreamName;		/**< name of the ofstream variable */

  std::istream* __istream;	/**< standard stream */

  IFStreamVariable*
  __ifstreamVariable;		/**< ifstream variable */

  /** 
   * Overload of the put function
   * 
   * @param os givem stream
   * 
   * @return os
   */
  std::ostream & put(std::ostream & os) const;

public:
  /** 
   * execute the expression
   * 
   */
  void __execute();

  /** 
   * Reads an expression from the stream
   * 
   * @param e given expression
   */
  void operator>>(Expression& e);

  /** 
   * Constructor for a standard istream
   * 
   * @param os given standard istream
   */
  IStreamExpression(std::istream* is);

  /** 
   * Constructor that uses an ofstream
   * 
   * @param ifstreamName given ifstream
   */
  IStreamExpression(const std::string& ifstreamName);

  /** 
   * Copy constructor
   * 
   * @param is given IStreamExpression
   */
  IStreamExpression(const IStreamExpression& is);

  /** 
   * Destructor
   * 
   */
  ~IStreamExpression();
};

#endif // ISTREAM_EXPRESSION_HPP
