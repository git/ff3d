//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <IFStreamExpressionVariable.hpp>

#include <VariableRepository.hpp>

ConstReferenceCounting<IFStreamExpression>
IFStreamExpressionVariable::ifstreamExpression() const
{
  return __ifstreamVariable->expression();
}

IFStreamExpressionVariable::
IFStreamExpressionVariable(const std::string& ifstreamName)
  : IFStreamExpression(IFStreamExpression::variable),
    __ifstreamName(ifstreamName),
    __ifstreamVariable(0)
{
  ;
}

IFStreamExpressionVariable::
IFStreamExpressionVariable(const IFStreamExpressionVariable& e)
  : IFStreamExpression(e),
    __ifstreamName(e.__ifstreamName),
    __ifstreamVariable(e.__ifstreamVariable)
{
  ;
}

IFStreamExpressionVariable::
~IFStreamExpressionVariable()
{
  ;
}

void IFStreamExpressionVariable::
__execute()
{
  __ifstreamVariable = VariableRepository::instance().findVariable<IFStreamVariable>(__ifstreamName);
  __fin = __ifstreamVariable->expression()->ifstream();
}
