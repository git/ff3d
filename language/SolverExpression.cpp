//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <SolverExpression.hpp>

#include <Solver.hpp>
#include <PDEProblem.hpp>
#include <PDESystem.hpp>
#include <SolverDriver.hpp>
#include <BoundaryConditionSet.hpp>

#include <FunctionExpressionValue.hpp>

#include <PDESystemExpression.hpp>
#include <VariationalProblemExpression.hpp>

#include <ScalarDiscretizationTypeBase.hpp>
#include <ScalarDiscretizationTypeLagrange.hpp>
#include <ScalarDiscretizationTypeLegendre.hpp>

#include <DiscretizationType.hpp>

#include <ScalarFunctionBase.hpp>

#include <FEMSolution.hpp>
#include <LegendreSolution.hpp>
#include <LagrangeSolution.hpp>

#include <VariationalProblem.hpp>

#include <SceneExpression.hpp>
#include <Scene.hpp>

#include <Structured3DMesh.hpp>
#include <Information.hpp>

#include <DegreeOfFreedomSetBuilder.hpp>

#include <ParameterCenter.hpp>

#include <LagrangeFunction.hpp>
#include <LegendreFunction.hpp>
#include <FEMFunction.hpp>
#include <FEMFunctionBuilder.hpp>

#include <DomainExpression.hpp>
#include <DomainExpressionSet.hpp>
#include <DomainExpressionVariable.hpp>
#include <DomainExpressionAnalytic.hpp>

#include <VariableRepository.hpp>


std::ostream& SolverExpression::
put(std::ostream& os) const
{
  os << __FILE__ << ':' << __LINE__ << ": NOT IMPLEMENTED\n";
  return os;
}


SolverExpression::
SolverExpression(ReferenceCounting<UnknownListExpression> unknownList,
		 ReferenceCounting<MeshExpression> mesh,
		 ReferenceCounting<SolverOptionsExpression> solverOptions,
		 ReferenceCounting<ProblemExpression> problemExpression,
		 ReferenceCounting<DomainExpression> domainExpression)
  : Expression(Expression::solver),
    __unknownList(unknownList),
    __mesh(mesh),
    __solverOptions(solverOptions),
    __problemExpression(problemExpression),
    __domain(domainExpression)
{
  ;
}

SolverExpression::
~SolverExpression()
{
  ;
}

void SolverExpression::
__setScene(ReferenceCounting<DomainExpression> domainExp) const
{
  switch(domainExp->domainType()) {
  case DomainExpression::set: {
    DomainExpressionSet& D
      = dynamic_cast<DomainExpressionSet&>(*domainExp);

    const SceneExpression& SE
      = dynamic_cast<const SceneExpression&>(*(D.scene()));
    Information::instance().setScene(SE.scene());
    break;
  }
  case DomainExpression::analytic: {
    DomainExpressionAnalytic& D
      = dynamic_cast<DomainExpressionAnalytic&>(*domainExp);

    Information::instance().setScene(D.domain()->scene());
    break;
  }
  case DomainExpression::variable: {
    DomainExpressionVariable& D
      = dynamic_cast<DomainExpressionVariable&>(*domainExp);
    __setScene(D.domainExpression());
    break;
  }
  case DomainExpression::undefined: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined domain",
		       ErrorHandler::normal);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected domain type",
		       ErrorHandler::unexpected);
  }
  }
}

void SolverExpression::
__execute()
{
  //! The unknowns
  Information::instance().setUnknownList(__unknownList);

  //! Reset solver options.
  ParameterCenter::instance().reset();

  __mesh->execute();

  //! A mesh is used there. We inform others about that ...
  Information::instance().setMesh(__mesh->mesh());

  __unknownList->execute();

  if (__domain != 0) {
    __domain->execute();
    __setScene(__domain);
  }

  __solverOptions->execute();
  __problemExpression->execute();

  if (__domain != 0) {
    switch (__problemExpression->problemType()) {
    case ProblemExpression::pdeSystem: {
      (*dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem()).setDomain(__domain->domain());
      break;
    }
    case ProblemExpression::variationalProblem: {
      (*dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem()).setDomain(__domain->domain());
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected problem type",
			 ErrorHandler::unexpected);
    }
    }
  } else {
    if (__problemExpression->hasPOVBoundary()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot use POVRay references in standard FEM discretization.\n"
			 "You probably forgot to specify the domain in the 'solve' bloc.\n"
			 "Refere to the Fictitious Domain part of the documentation",
			 ErrorHandler::normal);
    }
    if (__problemExpression->hasPredefinedBoundary()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot external surface mesh in standard FEM discretization.\n"
			 "You probably forgot to specify the domain in the 'solve' bloc.\n"
			 "Refere to the Fictitious Domain part of the documentation",
			 ErrorHandler::normal);
    }
  }

  DiscretizationType discretization;

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;
    unknown.execute();

    ReferenceCounting<ScalarDiscretizationTypeBase> unknownDiscretization = 0;
    
    ScalarDiscretizationTypeBase::Type unknownDiscretizationType
      = ScalarDiscretizationTypeBase::getDefault(unknown.discretizationType());

    switch(unknownDiscretizationType) {
    case ScalarDiscretizationTypeBase::functionLike: {
      ReferenceCounting<FunctionVariable> unknownVariable
	= VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

      ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();
      switch (f->type()) {
      case ScalarFunctionBase::femfunction: {
 	unknownDiscretization
 	  = new ScalarDiscretizationTypeFEM(dynamic_cast<const FEMFunctionBase&>(*f).discretizationType());
	break;
      }
      case ScalarFunctionBase::legendre: {
	const LegendreFunction& legendreFunction
	  = dynamic_cast<const LegendreFunction&>(*f);
	unknownDiscretization
	  = new ScalarDiscretizationTypeLegendre(legendreFunction.mesh()->degrees(),
						 legendreFunction.mesh()->shape().a(),
						 legendreFunction.mesh()->shape().b());
	break;
      }
      case ScalarFunctionBase::lagrange: {
	const LagrangeFunction& lagrangeFunction
	  = dynamic_cast<const LagrangeFunction&>(*f);
	unknownDiscretization
	  = new ScalarDiscretizationTypeLagrange(lagrangeFunction.mesh()->degrees(),
						 lagrangeFunction.mesh()->shape().a(),
						 lagrangeFunction.mesh()->shape().b());
	break;
      }
      default: {
	std::cerr << (*unknownVariable->expression()) << '\n';
	throw ErrorHandler(__FILE__,__LINE__,
			   "cannot determine discretization type from unknown type",
			   ErrorHandler::unexpected);
      }
      }
      unknown.setDiscretizationType(unknownDiscretization->type());
      break;
    }
    case ScalarDiscretizationTypeBase::spectralLegendre: {
      ReferenceCounting<FunctionVariable> unknownVariable
	= VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

      ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();
      switch (f->type()) {
      case ScalarFunctionBase::legendre: {
	const LegendreFunction& legendreFunction
	  = dynamic_cast<const LegendreFunction&>(*f);
	unknownDiscretization
	  = new ScalarDiscretizationTypeLegendre(legendreFunction.mesh()->degrees(),
						 legendreFunction.mesh()->shape().a(),
						 legendreFunction.mesh()->shape().b());
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "unknown is not a spectral function",
			   ErrorHandler::unexpected);
      }
      }
      unknown.setDiscretizationType(unknownDiscretization->type());
      break;      
    }
    case ScalarDiscretizationTypeBase::spectralLagrange: {
      ReferenceCounting<FunctionVariable> unknownVariable
	= VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

      ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();
      switch (f->type()) {
      case ScalarFunctionBase::lagrange: {
	const LagrangeFunction& lagrangeFunction
	  = dynamic_cast<const LagrangeFunction&>(*f);
	unknownDiscretization
	  = new ScalarDiscretizationTypeLagrange(lagrangeFunction.mesh()->degrees(),
						 lagrangeFunction.mesh()->shape().a(),
						 lagrangeFunction.mesh()->shape().b());
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "unknown is not a spectral function",
			   ErrorHandler::unexpected);
      }
      }
      unknown.setDiscretizationType(unknownDiscretization->type());
      break;      
    }
    case ScalarDiscretizationTypeBase::lagrangianFEM0:
    case ScalarDiscretizationTypeBase::lagrangianFEM1:
    case ScalarDiscretizationTypeBase::lagrangianFEM2: {
      unknownDiscretization
	= new ScalarDiscretizationTypeFEM(unknownDiscretizationType);
      break;
    }
    case ScalarDiscretizationTypeBase::undefined: {
      // Go to P1 finite element
      unknownDiscretization
	= new ScalarDiscretizationTypeFEM(ScalarDiscretizationTypeBase::lagrangianFEM1);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Unknown discretization",
			 ErrorHandler::unexpected);
    }
    }

    ASSERT(unknownDiscretization != 0);

    discretization.add(unknownDiscretization);
  }

  ASSERT(discretization.number()>0);

  switch (discretization.getFamily()) {
  case DiscretizationType::fem: {
    this->__solveFEM(discretization);
    break;
  }
  case DiscretizationType::spectralLegendre: {
    this->__solveLegendre(discretization);
    break;
  }
  case DiscretizationType::spectralLagrange: {
    this->__solveLagrange(discretization);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Discretization '("+stringify(discretization)+")' is not possible",
		       ErrorHandler::unexpected);
  }
  }

  //! The unknown are no more there.
  Information::instance().unsetUnknownList();

  //! The mesh is no more used in this region.
  Information::instance().unsetMesh();
}

void SolverExpression::
__solveFEM(const DiscretizationType& discretization)
{
  ReferenceCounting<DegreeOfFreedomSetBuilder> dofBuilder;

  if (__domain != 0) {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh(),
					       *__domain->domain());
  } else {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh());
  }

  const DegreeOfFreedomSet& degreeOfFreedomSet
    = dofBuilder->degreeOfFreedomSet();

  FEMSolution u(degreeOfFreedomSet);

  typedef
    std::vector<ReferenceCounting<FEMFunctionBase> >
    UnknownList;
  UnknownList UL;
  ConstReferenceCounting<Mesh> mesh = __mesh->mesh();

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;

    const size_t unknownNumber = __unknownList->number(unknown.name());

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();

    FEMFunctionBuilder femFunctionBuilder;
    femFunctionBuilder.build(dynamic_cast<const ScalarDiscretizationTypeFEM&>(discretization[unknownNumber]), mesh, *f);
    UL.push_back(femFunctionBuilder.getBuiltFEMFunction());
  }

  u.setUserFunction(UL);

  ConstReferenceCounting<Problem> P;
  switch (__problemExpression->problemType()) {
  case ProblemExpression::pdeSystem: {
    P = (PDESystem*)(dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem());
    break;
  }
  case ProblemExpression::variationalProblem: {
    P = (VariationalProblem*)(dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected problem type",
		       ErrorHandler::unexpected);
  }
  }

  ffout(2) << "Solving Problem:\n" << '\n';
  ffout(2) << "\tUnknowns: " << *__unknownList << '\n';
  ffout(2) << "\tProblem:  " << *__problemExpression << '\n';

  if (__domain != 0) {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fictitiousFEM);
    sd.run();
  } else {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fem);
    sd.run();
  }

  u.getUserFunction(UL);

  UnknownListExpressionSet& UList
    = dynamic_cast<UnknownListExpressionSet&>(*__unknownList);
  size_t i=0;
  for(UnknownListExpressionSet::listType::iterator iunknown = UList.giveList().begin();
      iunknown != UList.giveList().end(); ++iunknown) {
    UnknownExpression& unknown = **iunknown;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    const FEMFunctionBase* value = UL[i];
    (*unknownVariable)
      = new FunctionExpressionValue(value, false);

    ++i;
  }
}

void SolverExpression::
__solveLegendre(const DiscretizationType& discretization)
{
  ReferenceCounting<DegreeOfFreedomSetBuilder> dofBuilder;
  if (__domain != 0) {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh(),
					       *__domain->domain());
  } else {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh());
  }

  switch (__mesh->mesh()->type()) {
  case Mesh::spectralMesh:
  case Mesh::octreeMesh: {
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot use spectral method on '"+__mesh->mesh()->typeName()+"'",
		       ErrorHandler::normal);
  }
  }

  const DegreeOfFreedomSet& degreeOfFreedomSet
    = dofBuilder->degreeOfFreedomSet();

  LegendreSolution u(degreeOfFreedomSet);

  typedef
    std::vector<ReferenceCounting<LegendreFunction> >
    UnknownList;
  UnknownList UL;

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();

    if (f->type() != ScalarFunctionBase::legendre) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Cannot use non legendre functions as input for spectral legendre method",
			 ErrorHandler::unexpected);
    }
    const LegendreFunction* originalSFunction
      = dynamic_cast<const LegendreFunction*>(static_cast<const ScalarFunctionBase*>(f));

    ReferenceCounting<LegendreFunction> sFunction = new LegendreFunction(*originalSFunction);

    UL.push_back(sFunction);
  }

  u.setUserFunction(UL);

  ConstReferenceCounting<Problem> P;
  switch (__problemExpression->problemType()) {
  case ProblemExpression::pdeSystem: {
    P = (PDESystem*)(dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem());
    break;
  }
  case ProblemExpression::variationalProblem: {
    P = (VariationalProblem*)(dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected problem type",
		       ErrorHandler::unexpected);
  }
  }

  ffout(2) << "Solving Problem:\n" << '\n';
  ffout(2) << "\tUnknowns: " << *__unknownList << '\n';
  ffout(2) << "\tProblem:  " << *__problemExpression << '\n';

  if (__domain != 0) {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fictitiousSpectralLegendre);
    sd.run();
  } else {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::spectralLegendre);
    sd.run();
  }

  u.getUserFunction(UL);

  UnknownListExpressionSet& UList
    = dynamic_cast<UnknownListExpressionSet&>(*__unknownList);
  size_t i=0;
  for(UnknownListExpressionSet::listType::iterator iunknown = UList.giveList().begin();
      iunknown != UList.giveList().end(); ++iunknown) {
    UnknownExpression& unknown = **iunknown;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    const LegendreFunction* value = UL[i];
    (*unknownVariable)
      = new FunctionExpressionValue(value, false);

    ++i;
  }
}

void SolverExpression::
__solveLagrange(const DiscretizationType& discretization)
{
  ReferenceCounting<DegreeOfFreedomSetBuilder> dofBuilder;
  if (__domain != 0) {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh(),
					       *__domain->domain());
  } else {
    dofBuilder = new DegreeOfFreedomSetBuilder(discretization,
					       *__mesh->mesh());
  }

  switch (__mesh->mesh()->type()) {
  case Mesh::spectralMesh:
  case Mesh::octreeMesh: {
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot use spectral method on '"+__mesh->mesh()->typeName()+"'",
		       ErrorHandler::normal);
  }
  }

  const DegreeOfFreedomSet& degreeOfFreedomSet
    = dofBuilder->degreeOfFreedomSet();

  LagrangeSolution u(degreeOfFreedomSet);

  typedef
    std::vector<ReferenceCounting<LagrangeFunction> >
    UnknownList;
  UnknownList UL;

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();

    if (f->type() != ScalarFunctionBase::lagrange) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Cannot use non lagrange functions as input for spectral lagrange method",
			 ErrorHandler::unexpected);
    }
    const LagrangeFunction* originalSFunction
      = dynamic_cast<const LagrangeFunction*>(static_cast<const ScalarFunctionBase*>(f));

    ReferenceCounting<LagrangeFunction> sFunction = new LagrangeFunction(*originalSFunction);

    UL.push_back(sFunction);
  }

  u.setUserFunction(UL);

  ConstReferenceCounting<Problem> P;
  switch (__problemExpression->problemType()) {
  case ProblemExpression::pdeSystem: {
    P = (PDESystem*)(dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem());
    break;
  }
  case ProblemExpression::variationalProblem: {
    P = (VariationalProblem*)(dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected problem type",
		       ErrorHandler::unexpected);
  }
  }

  ffout(2) << "Solving Problem:\n" << '\n';
  ffout(2) << "\tUnknowns: " << *__unknownList << '\n';
  ffout(2) << "\tProblem:  " << *__problemExpression << '\n';

  if (__domain != 0) {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fictitiousSpectralLagrange);
    sd.run();
  } else {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::spectralLagrange);
    sd.run();
  }

  u.getUserFunction(UL);

  UnknownListExpressionSet& UList
    = dynamic_cast<UnknownListExpressionSet&>(*__unknownList);
  size_t i=0;
  for(UnknownListExpressionSet::listType::iterator iunknown = UList.giveList().begin();
      iunknown != UList.giveList().end(); ++iunknown) {
    UnknownExpression& unknown = **iunknown;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    const LagrangeFunction* value = UL[i];
    (*unknownVariable)
      = new FunctionExpressionValue(value, false);

    ++i;
  }
}
