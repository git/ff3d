//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_EXPRESSION_LIST_HPP
#define BOUNDARY_EXPRESSION_LIST_HPP

#include <BoundaryExpression.hpp>

#include <list>

/**
 * @file   BoundaryExpressionList.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 15:56:30 2006
 * 
 * @brief Defines list of boundary expression
 */
class BoundaryExpressionList
  : public BoundaryExpression
{
public:
  typedef std::list<ReferenceCounting<BoundaryExpression> > List;

private:
  List __boundaries;		/**< list of boundary expressions */

  /** 
   * Writes the list of boundary expressions to a stream
   * 
   * @param os the given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Checks if one of the boundaries is a POVRay boundary
   * 
   * @return true if one of the boundaries contains a POVRay boundary
   */
  bool hasPOVBoundary() const
  {
    for (List::const_iterator i = __boundaries.begin();
	 i != __boundaries.end(); ++i) {
      if ((*i)->hasPOVBoundary()) {
	return true;
      }
    }
    return false;
  }

  /** 
   * Checks if one of the boundaries is defined on a predefined
   * external mesh
   * 
   * @return true if one of the boundaries contains a predefined
   * external mesh
   */
  bool hasPredefinedBoundary() const
  {
    for (List::const_iterator i = __boundaries.begin();
	 i != __boundaries.end(); ++i) {
      if ((*i)->hasPredefinedBoundary()) {
	return true;
      }
    }
    return false;
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Adds a boundary to the list
   * 
   * @param boundary the bondary expression to add
   */
  void add(ReferenceCounting<BoundaryExpression> boundary)
  {
    __boundaries.push_back(boundary);
  }

  /** 
   * Constructor
   * 
   */
  BoundaryExpressionList();

  /** 
   * Copy constructor
   * 
   * @param l given list
   */
  BoundaryExpressionList(const BoundaryExpressionList& l);

  /** 
   * Destructor
   * 
   */
  ~BoundaryExpressionList();
};

#endif // BOUNDARY_EXPRESSION_LIST_HPP
