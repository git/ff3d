//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_LEGENDRE_HPP
#define FUNCTION_EXPRESSION_LEGENDRE_HPP

#include <FunctionExpression.hpp>

class MeshExpression;

/**
 * @file   FunctionExpressionLegendre.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul  5 14:31:21 2006
 * 
 * @brief Manipulates finite element function expressions
 * 
 */
class FunctionExpressionLegendre
  : public FunctionExpression
{
private:

  ReferenceCounting<MeshExpression>
  __mesh;			/**< mesh of discretization */

  ReferenceCounting<FunctionExpression>
  __functionExpression;		/**< expression defining the function */

public:

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param mesh mesh of discretization
   * @param e expression of the function
   */
  FunctionExpressionLegendre(ReferenceCounting<MeshExpression> mesh,
			     ReferenceCounting<FunctionExpression> e);

  /** 
   * Copy constructor
   * 
   * @param f given finite element function expression
   * 
   */
  FunctionExpressionLegendre(const FunctionExpressionLegendre& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionLegendre();
};

#endif // FUNCTION_EXPRESSION_LEGENDRE_HPP
