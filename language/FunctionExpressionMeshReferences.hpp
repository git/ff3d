//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_MESH_REFERENCES_HPP
#define FUNCTION_EXPRESSION_MESH_REFERENCES_HPP

#include <FunctionExpression.hpp>
#include <RealExpression.hpp>

#include <vector>
/**
 * @file   FunctionExpressionMeshReferences.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 00:09:36 2006
 * 
 * @brief This class describes function defined by references
 * expression
 * 
 */
class FunctionExpressionMeshReferences
  : public FunctionExpression
{
public:
  /**
   * @class  ReferencesSet
   * @author Stephane Del Pino
   * @date   Wed Jul 19 00:10:28 2006
   * 
   * @brief This class is manipulates associations of references and
   * function expressions
   * 
   */
  class ReferencesSet
  {
    friend class FunctionExpressionMeshReferences;

  public:
    typedef std::vector<std::pair<ReferenceCounting<RealExpression>,
				  ReferenceCounting<FunctionExpression> > >
    FunctionReferences;		/**< @typedef std::vector<std::pair<ReferenceCounting<RealExpression>,ReferenceCounting<FunctionExpression> > > FunctionReferences
				   Container of references and functions association */

  private:
    FunctionReferences __functionReferences; /**< the container */

  public:
    /** 
     * Access to the set of references-function expressions
     * association
     * 
     * @return __functionReferences
     */
    FunctionReferences& functionReferences()
    {
      return __functionReferences;
    }

    /** 
     * Checks if a boundary is required to evaluate this function
     * 
     * @return @b true if one of the functions contained in
     * __functionReferences requires a boundary
     */
    bool hasBoundaryExpression() const
    {
      bool found = false;
      for (FunctionReferences::const_iterator i = __functionReferences.begin();
	   i != __functionReferences.end(); ++i) {
	found = (*(*i).second).hasBoundaryExpression();
      }
      return found;
    }

    /** 
     * Writes the reference set @a r to a given stream
     * 
     * @param os given stream
     * @param r the reference set
     * 
     * @return os
     */
    friend std::ostream& operator << (std::ostream& os, const ReferencesSet& r)
    {
      FunctionReferences::const_iterator i = r.__functionReferences.begin();
      if (i != r.__functionReferences.end())
	os << *i->first << ':' << *i->second;
      i++;
      for (; i != r.__functionReferences.end(); ++i) {
	os << ',' << *i->first << ':' << *i->second;
      }
      return os;
    }

    /** 
     * Executes the set of references and function expressions
     * 
     */
    void execute()
    {
      for (FunctionReferences::iterator i = __functionReferences.begin();
	   i != __functionReferences.end(); ++i) {
	RealExpression* r = i->first;
	r->execute();
	i->second->execute();
      }
    }

    /** 
     * Adds a reference @a ref and its associated function expression @a function
     * 
     * @param ref the reference
     * @param function the function expression
     */
    void add(ReferenceCounting<RealExpression> ref,
	     ReferenceCounting<FunctionExpression> function)
    {
      __functionReferences.push_back(std::make_pair(ref, function));
    }

    /** 
     * Constructor
     * 
     * @param ref a given referencre
     * @param function its associated function expression
     */
    ReferencesSet(ReferenceCounting<RealExpression> ref,
		  ReferenceCounting<FunctionExpression> function)
    {
      __functionReferences.push_back(std::make_pair(ref, function));
    }

    /** 
     * Copy constructor
     * 
     * @param r a given reference set
     */
    ReferencesSet(const ReferencesSet& r)
      : __functionReferences(r.__functionReferences)
    {
      ;
    }

    /** 
     * Destructor
     * 
     */
    ~ReferencesSet()
    {
      ;
    }
  };

  enum ItemType {
    element,
    vertex,
    undefined
  };

private:
  std::string __itemName;	/**< The name of the used items */

  ReferenceCounting<MeshExpression> __mesh; /**< the mesh defining the function */
  ReferenceCounting<ReferencesSet> __referenceSet; /**< the set of references */

  /** 
   * Checks the ItemType of the current FunctionExpressionMeshReferences
   * 
   * @return the ItemType associated to the __itemName
   */
  ItemType __getItemType();

  std::ostream& put(std::ostream& os) const;
public:
  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param itemName the given item name to use check references
   * @param mesh the mesh used to define the function
   * @param ref the references set
   */
  FunctionExpressionMeshReferences(const std::string itemName,
				   ReferenceCounting<MeshExpression> mesh,
				   ReferenceCounting<ReferencesSet> ref);

  /** 
   * Copy constructor
   * 
   * @param f a given FunctionExpressionMeshReferences
   */
  FunctionExpressionMeshReferences(const FunctionExpressionMeshReferences& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionMeshReferences();
};


#endif // FUNCTION_EXPRESSION_MESH_REFERENCES_HPP
