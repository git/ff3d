//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _PDEOPERATORSUM_EXPRESSION_HPP_
#define _PDEOPERATORSUM_EXPRESSION_HPP_

#include <list>

#include <Expression.hpp>
#include <PDEOperatorExpression.hpp>

class VectorialPDEOperator;
class PDEOperatorSumExpression
  : public Expression
{
protected:
  ReferenceCounting<VectorialPDEOperator> __vectorPDEOperator;

  typedef std::list<ReferenceCounting<PDEOperatorExpression> >
  PDEOperatorExpressionList;

  ReferenceCounting<PDEOperatorExpressionList> __sumList;

  ReferenceCounting<PDEOperatorExpressionList> __differenceList;

private:
  std::ostream& put(std::ostream& os) const
  {
    for(PDEOperatorExpressionList::const_iterator i = (*__sumList).begin();
	i != (*__sumList).end(); ++i) {
      if (i != (*__sumList).begin())
	os << " + ";
      os << (*(*i));
    }
    for(PDEOperatorExpressionList::const_iterator i = (*__differenceList).begin();
	i != (*__differenceList).end(); ++i) {
      os << " - " << (*(*i));
    }
    return os;
  }
public:
  ReferenceCounting<VectorialPDEOperator> vectorPDEOperator();

  void __execute();

  void add(ReferenceCounting<PDEOperatorExpression> p)
  {
    (*__sumList).push_back(p);
  }

  void minus(ReferenceCounting<PDEOperatorExpression> p)
  {
    (*__differenceList).push_back(p);
  }

  void unaryMinus()
  {
    std::swap(__differenceList,__sumList);
  }

  void add(ReferenceCounting<PDEOperatorSumExpression> p);

  void minus(ReferenceCounting<PDEOperatorSumExpression> p);

  PDEOperatorSumExpression(const PDEOperatorSumExpression& e);

  PDEOperatorSumExpression();

  virtual ~PDEOperatorSumExpression();
};

#endif // _PDEOPERATORSUM_EXPRESSION_HPP_

