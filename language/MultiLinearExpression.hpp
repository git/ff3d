//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MULTI_LINEAR_EXPRESSION_HPP
#define MULTI_LINEAR_EXPRESSION_HPP

#include <Expression.hpp>
#include <LinearExpression.hpp>

#include <VariationalOperator.hpp>

#include <list>

class MultiLinearExpression
  : public Expression
{
public:
  enum LinearFormType {
    linear,
    biLinear
  };

  enum OperatorType {
    gradUgradV,
    dxUdxV,
    dxUV,
    UdxV,
    UV,
    gradV,
    gradFgradV,
    dxV,
    V,
    dxFV,
    undefined
  };

  typedef std::list<ReferenceCounting<LinearExpression> > LinearListType;
  typedef std::list<ReferenceCounting<FunctionExpression> > FunctionListType;
  typedef std::list<ReferenceCounting<RealExpression> > RealListType;

private:
  LinearListType __linearList;
  FunctionListType __functionList;
  RealListType __realList;

  std::ostream& put(std::ostream& os) const;

public:

  ReferenceCounting<FunctionExpression>
  getFunction();

  const std::string& getUnknownName() const;

  const std::string& getTestFunctionName() const;

  IntegratedOperatorExpression::OperatorType
  getUnknownOperator() const;

  IntegratedOperatorExpression::OperatorType
  getTestFunctionOperator() const;

  IntegratedOperatorExpression::OperatorType
  getFunctionOperator() const;

  VariationalOperator::Property
  getUnknownProperty() const;

  VariationalOperator::Property
  getTestFunctionProperty() const;

  ReferenceCounting<FunctionExpression> getFunctionExpression() const;

  LinearListType::iterator beginLinear()
  {
    return __linearList.begin();
  }

  LinearListType::iterator endLinear()
  {
    return __linearList.end();
  }

  FunctionListType::iterator beginFunction()
  {
    return __functionList.begin();
  }

  FunctionListType::iterator endFunction()
  {
    return __functionList.end();
  }

  RealListType::iterator beginReal()
  {
    return __realList.begin();
  }

  RealListType::iterator endReal()
  {
    return __realList.end();
  }

  OperatorType operatorType() const;

  LinearFormType linearFormType() const;

  void check();

  void __execute()
  {
    for (LinearListType::iterator i = __linearList.begin();
	 i != __linearList.end(); ++i) {
      (*(*i)).execute();
    }

    for (FunctionListType::iterator i = __functionList.begin();
	 i != __functionList.end(); ++i) {
      (*(*i)).execute();
    }

    for (RealListType::iterator i = __realList.begin();
	 i != __realList.end(); ++i) {
      (*(*i)).execute();
    }
  }

  void times(ReferenceCounting<LinearExpression> LF)
  {
    __linearList.push_back(LF);
  }

  void times(ReferenceCounting<FunctionExpression> f)
  {
    __functionList.push_back(f);
  }

  void times(ReferenceCounting<RealExpression> r)
  {
    __realList.push_back(r);
  }

  MultiLinearExpression(ReferenceCounting<LinearExpression> LF)
    : Expression(Expression::multiLinearExp)
  {
    __linearList.push_back(LF);
  }

  MultiLinearExpression(const MultiLinearExpression& MLF)
    : Expression(MLF),
      __linearList(MLF.__linearList),
      __functionList(MLF.__functionList),
      __realList(MLF.__realList)
  {
    ;
  }

  virtual ~MultiLinearExpression()
  {
    ;
  }
};

#endif // MULTI_LINEAR_EXPRESSION_HPP

