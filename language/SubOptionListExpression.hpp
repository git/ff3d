//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _SUB_OPTION_LIST_EXPRESSION_HPP_
#define _SUB_OPTION_LIST_EXPRESSION_HPP_

#include <ReferenceCounting.hpp>

#include <Expression.hpp>
#include <SubOptionExpression.hpp>

#include <list>

class SubOptionListExpression
  : public Expression
{
public:
  typedef std::list<ReferenceCounting<SubOptionExpression> > SubOptionList;

private:
  SubOptionList __list;

  std::ostream& put(std::ostream& os) const
  {
    os << '\n' <<__FILE__ << ':' << __LINE__ << ": Not implemented\n";
    return os;
  }

public:
  void __execute()
  {
    for (SubOptionList::iterator i = __list.begin();
	 i != __list.end(); ++i) {
      (*(*i)).execute();
    }
  }

  void evaluate(const std::string& basename)
  {
    for (SubOptionList::iterator i = __list.begin();
	 i != __list.end(); ++i) {
      (*(*i)).evaluate(basename);
    }
  }

  void add(ReferenceCounting<SubOptionExpression> soe)
  {
    __list.push_back(soe);
  }

  SubOptionListExpression()
    : Expression(Expression::subOptionList)
  {
    ;
  }

  ~SubOptionListExpression()
  {
    ;
  }
};

#endif // _SUB_OPTION_LIST_EXPRESSION_HPP_

