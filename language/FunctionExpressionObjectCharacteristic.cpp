//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionObjectCharacteristic.hpp>
#include <Scene.hpp>
#include <Information.hpp>

#include <ScalarFunctionObjectCharacteristic.hpp>
#include <Vector3Expression.hpp>

// std::ostream& FunctionExpressionCharacteristic::put(std::ostream& os) const
// {
//   os << "one(<" << (*__reference).value(0)
//      << ',' << (*__reference).value(1)
//      << ',' << (*__reference).value(2) << ">)";
//   return os;
// }

// real_t FunctionExpressionCharacteristic::value(const real_t& x,
// 					       const real_t& y,
// 					       const real_t& z) const
// {
//   TinyVector<3> V(x,y,z);
//   for (std::list<Object* >::const_iterator i = __objects.begin();
//        i != __objects.end(); ++i)
//     if ((*(*i)).inside(V))
//       return 1;
//   return 0;
// }

void FunctionExpressionObjectCharacteristic::__execute()
{
  ConstReferenceCounting<Scene> scene
    = Information::instance().getScene();

  __reference->execute();

  TinyVector<3> r;
  for (size_t i=0; i<3; ++i)
    r[i] = (*__reference).value(i);

  __scalarFunction = new ScalarFunctionObjectCharacteristic(scene, r);
}

FunctionExpressionObjectCharacteristic
::FunctionExpressionObjectCharacteristic(ReferenceCounting<Vector3Expression> ref)
  : FunctionExpression(FunctionExpression::objectCharacteristic),
    __reference(ref)
{
  ;
}

FunctionExpressionObjectCharacteristic::
FunctionExpressionObjectCharacteristic(const FunctionExpressionObjectCharacteristic& f)
  : FunctionExpression(f),
    __reference(f.__reference),
    __scene(f.__scene)
{
  ;
}

FunctionExpressionObjectCharacteristic::
~FunctionExpressionObjectCharacteristic()
{
  ;
}

