//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Vector3Expression.hpp>
#include <Variable.hpp>

#include <VariableRepository.hpp>

real_t Vector3ExpressionVariable::value(const size_t& i) const
{
  ASSERT(i<3);
  return __value[i];
}

ReferenceCounting<RealExpression>
Vector3ExpressionVariable::component(const size_t& i) const
{
  ASSERT (i<3);
  return (*(*__vector3Variable).expression()).component(i);
}

void Vector3ExpressionVariable::__execute()
{
  __vector3Variable = VariableRepository::instance().findVariable<Vector3Variable>(__vector3Name);
  for (size_t i=0; i<3; ++i)
    __value[i] = __vector3Variable->expression()->value(i);
}

Vector3ExpressionVariable::
Vector3ExpressionVariable(const std::string& vector3Name)
  : __vector3Name(vector3Name),
    __vector3Variable(0)
{
  for (size_t i=0; i<3; ++i)
    __value[i]=0;
}

Vector3ExpressionVariable::
Vector3ExpressionVariable(const Vector3ExpressionVariable& e)
  : __vector3Name(e.__vector3Name),
    __vector3Variable(e.__vector3Variable)
{
  for (size_t i=0; i<3; ++i)
    __value[i]=e.__value[i];
}

Vector3ExpressionVariable::
~Vector3ExpressionVariable()
{
  ;
}
