//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 Stéphane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <StreamCenter.hpp>

#include <QRealSpinBox.hpp>

#include <cmath>

QRealSpinBox::QRealSpinBox(QWidget* parent)
  : QSpinBox(parent),
  __validator(new QDoubleValidator( -2e10, 2e10, 2, this))
{
  this->setRange(int(-2e10), int(2e10));
  fferr(1) << __FILE__ << ':' << __LINE__ << ":warning: Validator not inactivated\n";
//   this->setValidator( __validator ); 
  this->setValue( 0.0 );
}

QRealSpinBox::QRealSpinBox(const real_t& minValue,
			   const real_t& maxValue,
			   const int& decimals, 
			   const real_t& step,
			   QWidget* parent)
  : QSpinBox(parent ),
    __validator(new QDoubleValidator( minValue, maxValue, 2, this)),
    __decimals( decimals )
{
  this->setRange(int( minValue * std::pow( 10., decimals )), int( maxValue * std::pow( 10., decimals ) ));
  this->setSingleStep(int(rint(step * std::pow(10., decimals))));
  fferr(1) << __FILE__ << ':' << __LINE__ << ":warning: Validator not inactivated\n";
//   setValidator( __validator ); 
  this->setValue( 0.5*(minValue+maxValue) );
}

QRealSpinBox::~QRealSpinBox()
{
  ;
}  

QString QRealSpinBox::mapValueToText ( int givenValue )
{
  QString s;
  s.setNum(givenValue/std::pow( 10., __decimals ), 'f', __decimals );
  return s;
}

int QRealSpinBox::mapTextToValue ( bool * ok )
{
  return int( cleanText().toFloat( ok ) * std::pow( 10., __decimals ) );
}

real_t QRealSpinBox::value() const
{
  return QSpinBox::value()/std::pow( 10., __decimals );
}

int QRealSpinBox::percentage() const
{
  if (maximum() != minimum()) {
    return 100 * (QSpinBox::value() - minimum())
      / (maximum() - minimum());
  } else {
    return 100;
  }
}
void QRealSpinBox::setPercentage(int i)
{
//   this->QSpinBox::setPercentage(i);
  this->QSpinBox::setValue(int((QSpinBox::minimum()*(99.-i)
				+ QSpinBox::maximum()*i)/99.));
}


real_t QRealSpinBox::minValue() const
{
  return minimum()/std::pow( 10., __decimals );
}

real_t QRealSpinBox::maxValue() const
{
  return maximum()/std::pow( 10., __decimals );
}

void QRealSpinBox::setValue( real_t givenValue )
{
  this->QSpinBox::setValue(int(givenValue * std::pow( 10., __decimals )));
}

void QRealSpinBox::setRange( real_t minValue, real_t maxValue )
{
  QSpinBox::setRange(int(minValue *  std::pow(10., __decimals )), 
		     int(maxValue *  std::pow(10., __decimals )));
  __validator->setRange(minValue, maxValue, 2);
}

void QRealSpinBox::valueChange()
{
  emit valueChanged(value());
}

QSize QRealSpinBox::sizeHint() const
{
  QFontMetrics fm = fontMetrics();
  int h = fm.height();
  if ( h < 12 )       // ensure enough space for the button pixmaps
    h = 12;
  int w = 35;         // minimum width for the value
  int wx = fm.width( "  " );
  QString s;
  s.setNum( minValue(), 'f', __decimals );
  s.prepend( prefix() );
  s.append( suffix() );
  w = std::max( w, fm.width( s ) + wx );
  s.setNum( maxValue(), 'f', __decimals );
  s.prepend( prefix() );
  s.append( suffix() );
  w = std::max( w, fm.width( s ) + wx );
  s = specialValueText();
  w = std::max( w, fm.width( s ) + wx );

  QSize r ( h // buttons AND frame both sides
	  + 6 // right/left margins
	  + w, // widest value
//        frameWidth() * 2 // top/bottom frame
//        w * 2
	  + 4 // top/bottom margins
	  + h // font height
	  );
    return r;
}
