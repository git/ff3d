//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 Stéphane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef Q_REAL_SPIN_BOX_HPP
#define Q_REAL_SPIN_BOX_HPP

#include <QtGui/QSpinBox>
#include <QtGui/QValidator>

#include <Types.hpp>

/**
 * @file   QRealSpinBox.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct 17 23:48:13 2004
 * 
 * @brief Fills the blank in Qt: QSpinBox only manages integers values
 * @note this is highly inspired from Joerg Ott example found on
 * qt-interest mailing list.
 * 
 */

class QRealSpinBox
  : public QSpinBox
{
  Q_OBJECT
 private:
  QDoubleValidator* __validator;
  int __decimals;

public:
  QRealSpinBox(QWidget* parent = 0);

  QRealSpinBox(const real_t& minValue,
	       const real_t& maxValue,
	       const int& decimals = 2,
	       const real_t& step = 1.0, 
               QWidget* parent = 0);

  ~QRealSpinBox();

  virtual QSize sizeHint () const;

  real_t value() const;
  int percentage() const;

  real_t minValue() const;
  real_t maxValue() const;

  void setRange( real_t minValue, real_t maxValue );

 public slots:
  void setValue( real_t value );
  void setPercentage( int value );

 signals:
  void valueChanged( real_t value );

 protected:
  virtual QString mapValueToText ( int value );
  virtual int mapTextToValue ( bool * ok = 0 );
  virtual void valueChange ();
};

#endif // Q_REAL_SPIN_BOX_HPP
