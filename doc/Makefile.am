# -*- makefile -*-
#
# Author: Christophe Prud'homme
#
# Copyright (C) 2004 Christophe Prud'homme <prudhomm@debian.org>
#
# Distributed under the GPL(GNU Public License):
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

SUFFIXES	= .cpp .hpp .idl .c .h .f .F .o .moc .tex .eps .pdf .ps .dvi

EXTRA_DIST	=				\
	cygwin.eps				\
	cygwin.pdf				\
	eclbkbox.sty				\
	example1-u-dx.pdf			\
	example1-u-dx.ps			\
	example1-u-medit.pdf			\
	example1-u-medit.ps			\
	example1-v-dx.pdf			\
	example1-v-dx.ps			\
	example1-v-medit.pdf			\
	example1-v-medit.ps			\
	folder.eps				\
	folder.pdf				\
	freefem3dDoc.tex			\
	onesphere.eps				\
	onesphere.pdf				\
	penal1.ps				\
	penal1.pdf				\
	penal2.ps				\
	penal2.pdf				\
	penal3.ps				\
	penal3.pdf				\
	penal4.ps				\
	penal4.pdf				\
	penal5.ps				\
	penal5.pdf				\
	references.bib				\
	resultmedit.eps				\
	resultmedit.pdf				\
	scene1.eps				\
	scene1.pdf				\
	t3D.eps					\
	t3D.pdf					\
	u.eps					\
	u.pdf					\
	uu.eps					\
	uu.pdf					\
						\
	ff3d.1

if GENERATE_DOCUMENTATION

DOCTESTS =					\
	doc-tests/example1.ff			\
	doc-tests/example1.general		\
	doc-tests/example1.pov			\
	doc-tests/example2.ff			\
	doc-tests/example3.ff			\
	doc-tests/example4.ff			\
	doc-tests/example5.ff			\
	doc-tests/penal1.ff			\
	doc-tests/penal2.ff			\
	doc-tests/penal3.ff			\
	doc-tests/penal4.ff			\
	doc-tests/penal5.ff			\
	doc-tests/sphere.pov			\
	doc-tests/statements.ff			\
	doc-tests/scalar.net			\
	doc-tests/void.pov


dist_doc_DATA =					\
	freefem3dDoc.pdf			\
	freefem3dDoc.dvi			\
	freefem3dDoc.ps

all: freefem3dDoc.pdf freefem3dDoc.dvi freefem3dDoc.ps keywords
	make mostlyclean

freefem3dDoc.pdf:				\
	example1-u-medit.pdf			\
	example1-u-dx.pdf			\
	example1-v-medit.pdf			\
	example1-v-dx.pdf			\
	freefem3dDoc.tex			\
	keywords.tex				\
	parameters.tex				\
	references.bib				\
	scene1.pdf				\
	$(DOCTESTS)

freefem3dDoc.dvi:				\
	example1-u-medit.ps			\
	example1-u-dx.ps			\
	example1-v-medit.ps			\
	example1-v-dx.ps			\
	freefem3dDoc.tex			\
	keywords.tex				\
	parameters.tex				\
	references.bib				\
	scene1.eps				\
	$(DOCTESTS)

freefem3dDoc.ps: freefem3dDoc.dvi

# to be used for debugging purpose
INTERACTION=-interaction batchmode

.tex.pdf:
	make mostlyclean
	(export TEXINPUTS=$(srcdir)/: && pdflatex -jobname pdf-`basename $< .tex` -interaction batchmode `basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && makeindex pdf-`basename $< .tex`)
	(export BIBINPUTS=$(srcdir)/: && bibtex pdf-`basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && pdflatex -jobname pdf-`basename $< .tex` -interaction batchmode `basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && pdflatex -jobname pdf-`basename $< .tex` $(INTERACTION) `basename $< .tex`)
	(mv pdf-`basename $< .tex`.pdf freefem3dDoc.pdf)

.tex.dvi:
	make mostlyclean
	(export TEXINPUTS=$(srcdir)/: && latex -interaction batchmode `basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && makeindex `basename $< .tex`)
	(export BIBINPUTS=$(srcdir)/: && bibtex `basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && latex -interaction batchmode `basename $< .tex`)
	(export TEXINPUTS=$(srcdir)/: && latex $(INTERACTION) `basename $< .tex`)

.tex.ps:
	(export TEXINPUTS=$(srcdir)/: && dvips `basename $< .tex`.dvi -o `basename $< .tex`.ps)

parameters.tex: $(top_srcdir)/*/*Options*pp $(top_srcdir)/scripts/getOptions.pl $(top_srcdir)/scripts/listOptions
	(cd $(top_srcdir) && scripts/listOptions) > $(top_builddir)/doc/parameters.tex

keywords.tex: $(top_srcdir)/language/FFLexer.cpp $(top_srcdir)/scripts/listKeywordsFormatted keywords
	(echo "% automatically generated: *do not edit*" > $(top_builddir)/doc/keywords.tex)
	($(top_srcdir)/scripts/listKeywordsFormatted $(top_builddir)/doc/keywords) >> $(top_builddir)/doc/keywords.tex

keywords: $(top_srcdir)/language/FFLexer.cpp $(top_srcdir)/scripts/listKeywords
	(cd $(top_srcdir) && scripts/listKeywords $(top_srcdir)/language/FFLexer.cpp) > $(top_builddir)/doc/keywords

BUILT_SOURCES=					\
	keywords				\
	keywords.tex				\
	parameters.tex

else ! GENERATE_DOCUMENTATION

BUILT_SOURCES=

endif ! GENERATE_DOCUMENTATION

MOSTLYCLEANFILES=					\
	freefem3dDoc.aux				\
	freefem3dDoc.blg				\
	freefem3dDoc.bbl				\
	freefem3dDoc.idx				\
	freefem3dDoc.ilg				\
	freefem3dDoc.ind				\
	freefem3dDoc.log				\
	freefem3dDoc.out				\
	freefem3dDoc.toc				\
	pdf-freefem3dDoc.aux				\
	pdf-freefem3dDoc.blg				\
	pdf-freefem3dDoc.bbl				\
	pdf-freefem3dDoc.idx				\
	pdf-freefem3dDoc.ilg				\
	pdf-freefem3dDoc.ind				\
	pdf-freefem3dDoc.log				\
	pdf-freefem3dDoc.out				\
	pdf-freefem3dDoc.toc			

CLEANFILES=					\
	freefem3dDoc.pdf			\
	freefem3dDoc.ps				\
	freefem3dDoc.dvi			\
	keywords				\
	keywords.tex				\
	parameters.tex

SUBDIRS	=					\
	 doc-tests

#  install the man page
man_MANS=ff3d.1
