//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2002, 2003 Pascal Have 

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

/**
 * @file   trapFPE.c
 * @author Pascal Have
 * @date   Tue Feb 25 20:03:22 2003
 * 
 * @brief  this piece of code helps you in finding floating operations errors
 * 
 */

#include <config.h>

#if !defined(NDEBUG) && defined (__i386__) && !defined(HAVE_GUI_LIBS) && !defined(__CYGWIN__)

#define MY_FLAGS FE_INVALID
//( FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW)

/* definition of glibc version */
#include <features.h>

/* file containing constants */
#if __GLIBC_MINOR__ > 0
#include <fenv.h>
int feenableexcept (int excepts);
int fedisableexcept (int excepts);
int fegetexcept (void);
#else
#include <fpu_control.h>
#endif

static void __attribute__ ((constructor)) trapfpe ()
{
#if __GLIBC_MINOR__ > 0
  /* glibc version > 2.0 */
  /* On intel
   *  unsigned short cw;
   *  asm volatile ("fnstcw %0":"=m" (cw)); // Get flags
   *  cw &= ~traps;                         // Enable new flags
   *  asm volatile ("fldcw %0"::"m" (cw));  // Set flags
   */
  feenableexcept(MY_FLAGS);
#else
  /* glibc 2.0 Version */
  __setfpucw (_FPU_DEFAULT & ~(_FPU_MASK_IM | _FPU_MASK_ZM | _FPU_MASK_OM | _FPU_MASK_UM));
#endif
}

#endif // not defined( NDEBUG) and defined (__i386__) and not defined (HAVE_GUI_LIBS)
