//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BASEMATRIX_HPP
#define BASEMATRIX_HPP

#include <BaseVector.hpp>
#include <cstddef>
#include <config.h>

class BaseMatrix
{
public:
  enum Type {
    doubleHashedMatrix,
    sparseMatrix,
#ifdef    HAVE_PETSC
    petscMatrix,
#endif // HAVE_PETSC
    unAssembled
  };

protected:
  BaseMatrix::Type __type;

  size_t __size;

public:
  /** 
   * Sets the matrix to zero
   * 
   */
  virtual void reset() = 0;

  //! used to get the diagonal of the Matrix and stores it in the vector X.
  virtual void getDiagonal(BaseVector& X) const = 0;

  //! Computes z = A*x+y
  virtual void transposedTimesX(const BaseVector& X,
				BaseVector& Z) const = 0;

  //! Computes z = A*x
  virtual void timesX(const BaseVector& X,
		      BaseVector& Z) const = 0;

  const size_t& size() const
  {
    return __size; 
  }

  const BaseMatrix::Type& type() const
  {
    return __type; 
  }

  BaseMatrix(const BaseMatrix::Type t,
	     const size_t& size = 0)
    : __type(t),
      __size(size)
  {
    ;
  }

  BaseMatrix(const BaseMatrix& B)
    : __type(B.__type),
      __size(B.__size)
  {
    ;
  }

  virtual ~BaseMatrix()
  {
    ;
  }
};

#endif // BASEMATRIX_HPP

