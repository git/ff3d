//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef UN_ASSEMBLED_MATRIX_HPP
#define UN_ASSEMBLED_MATRIX_HPP

#include <Types.hpp>

#include <BaseMatrix.hpp>
#include <ReferenceCounting.hpp>

class Discretization;
class BoundaryConditionDiscretization;

class UnAssembledMatrix
  : public BaseMatrix
{
private:
  ConstReferenceCounting<Discretization> __discretization;
  ConstReferenceCounting<BoundaryConditionDiscretization> __BCDiscretization;

public:
  void reset()
  {
    ;
  }

  void getDiagonal(BaseVector& X) const;

  void transposedTimesX(const BaseVector& X, BaseVector& Z) const;

  //! Computes z = A*x
  void timesX(const BaseVector& X,
	      BaseVector& Z) const;

  void setDiscretization(ConstReferenceCounting<Discretization> discretization);
  void setBoundaryConditions(ConstReferenceCounting<BoundaryConditionDiscretization> bcDiscret);

  UnAssembledMatrix(size_t size);

  ~UnAssembledMatrix();
};

#endif //UN_ASSEMBLED_MATRIX_HPP
