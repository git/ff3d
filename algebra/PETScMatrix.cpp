//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <PETScMatrix.hpp>

void PETScMatrix::
timesX(const BaseVector& X,
       BaseVector& Z) const
{
  throw ErrorHandler(__FILE__, __LINE__,
		     "not implemented",
		     ErrorHandler::unexpected);
}

PETScMatrix::
PETScMatrix(const DoubleHashedMatrix& A)
  : BaseMatrix(BaseMatrix::petscMatrix, A.numberOfLines()),
    __petscMatrix(new Mat)
{
  Mat& petscMatrix = *__petscMatrix;
  int argc = 1;
  char* myArgv[] = { "ff3d" };
  char** argv = myArgv;
  PetscInitialize(&argc, &argv, 0, 0);

  size_t nz = 0;
  for (int i=0; i<int(__size); ++i) {
    nz = std::max(A.numberOfLineNonNull(i),nz);
  }

  MatCreateSeqAIJ(PETSC_COMM_WORLD,
		  A.numberOfLines(),
		  A.numberOfColumns(),
		  nz,
		  0,
		  &petscMatrix);

  MatAssemblyBegin(petscMatrix, MAT_FINAL_ASSEMBLY);

  std::vector<int> columns;
  std::vector<real_t> values;
  for (int i=0; i<int(__size); ++i) {
    for (DoubleHashedMatrix::const_iterator browsLine = A.beginOfLine(i);
	 browsLine != A.endOfLine(i); ++browsLine) {
      const int j = browsLine.first();
      const real_t value = browsLine.second();
      columns.push_back(j);
      values.push_back(value);
    }
    MatSetValues(petscMatrix,
		 1, &i, columns.size(), &columns[0],
		 &values[0], INSERT_VALUES);
    columns.clear();
    values.clear();
  }

  MatAssemblyEnd(petscMatrix, MAT_FINAL_ASSEMBLY);
}

PETScMatrix::
~PETScMatrix()
{
  MatDestroy(*__petscMatrix);
  delete __petscMatrix;
}
