//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


// This class is a generic interface to Preconditioners
// It might be improved a lot. PDEOperator/Preconditioner
// implementation may change a lot in the futur.

#ifndef PRECONDITIONER_HPP
#define PRECONDITIONER_HPP
#include <Vector.hpp>
#include <Problem.hpp>

#include <ErrorHandler.hpp>

#include <string>

/*!  \class Preconditioner

  This class decribes a generic linear system preconditioner, it is the base
  class for all preconditionners.

  \todo
  \li Make TypeName pure virtual.

  \author St�phane Del Pino
 */
class Preconditioner
{
public:

  enum Type {
    none,
    diagonale,
    incompleteCholeskiFactorization,
    multigrid,
    spectralFEM
  };

protected:
  //! The type of preconditioner.
  Preconditioner::Type __type;

  // The preconditioner should know the PDE Problem it is working on.
  const Problem& __problem;

public:
  //! Initialization of the Preconditioner.
  virtual void initializes() = 0;

  //! Computes \f$ z = P^{-1} r \f$.
  virtual void computes(const Vector<real_t>& r ,
			Vector<real_t>& z) const = 0;

  //! Computes \f$ z = P^{-1} r \f$.
  virtual void computesTransposed(const Vector<real_t>& r ,
				  Vector<real_t>& z) const
  {
    this->computes(r,z);
  }

  virtual std::string name() const = 0;

  /*! Constructs a preconditioner for the PDEProblem \a Pb using the
    Preconditioner::Type \a type.
   */
  Preconditioner(const Problem& problem,
		 const Preconditioner::Type& t)
    : __type(t),
      __problem(problem)
  {
    ;
  }

  //! Copy constructor.
  Preconditioner(const Preconditioner& P)
    : __type(P.__type),
      __problem(P.__problem)
  {
    ;
  }

  // Read-only access to the type of the Preconditioner.
  const Preconditioner::Type& Type() const
  {
    return __type;
  }

  //! virtual Destructor.
  virtual ~Preconditioner()
  {
    ;
  }
};

#endif // PRECONDITIONER_HPP

