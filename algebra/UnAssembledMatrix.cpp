//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#include <TinyMatrix.hpp>

#include <UnAssembledMatrix.hpp>
#include <Discretization.hpp>
#include <BoundaryConditionDiscretization.hpp>

void UnAssembledMatrix::getDiagonal(BaseVector& X) const
{
  static_cast<Vector<real_t>&>(X) = 0;
  (*__discretization).getDiagonal(X);
  (*__BCDiscretization).getDiagonal(X);
}

void UnAssembledMatrix::transposedTimesX(const BaseVector& X,
					 BaseVector& Z) const
{
  static_cast<Vector<real_t>&>(Z) = 0;
  if(__discretization != 0) {
   (*__discretization).transposedTimesX(X,Z);
  }
  if(__BCDiscretization != 0) {
    (*__BCDiscretization).transposedTimesX(X,Z);
  }
}

void UnAssembledMatrix::timesX(const BaseVector& X,
			       BaseVector& Z) const
{
  static_cast<Vector<real_t>&>(Z) = 0;
  if(__discretization != 0) {
   (*__discretization).timesX(X,Z);
  }
  if(__BCDiscretization != 0) {
    (*__BCDiscretization).timesX(X,Z);
  }
}

void UnAssembledMatrix::
setDiscretization(ConstReferenceCounting<Discretization> discretization)
{
  __discretization = discretization;
}

void UnAssembledMatrix::
setBoundaryConditions(ConstReferenceCounting<BoundaryConditionDiscretization> bcDiscretization)
{
  __BCDiscretization = bcDiscretization;
}

UnAssembledMatrix::UnAssembledMatrix(size_t size)
  : BaseMatrix(BaseMatrix::unAssembled, size),
    __discretization(0),
    __BCDiscretization(0)
{
  ;
}

UnAssembledMatrix::~UnAssembledMatrix()
{
  ;
}

