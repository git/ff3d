//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef STRUCTURED_3D_VECTOR_HPP
#define STRUCTURED_3D_VECTOR_HPP

#include <Vector.hpp>
#include <Array3DShape.hpp>

/**
 * @file   Structured3DVector.hpp
 * @author St�phane Del Pino
 * @date   Wed Feb 13 21:48:48 2008
 * 
 * @brief This class defines a template Vector class to store huge
 *  quantity of datas defined on a 3d structure (ie: defined on a 3d
 *  mesh). The stored elements type is the template argument.
 */
template <typename T>
class Structured3DVector
  : public Vector<T>
{
private:
  //! allows access to the data using the structured organization.
  T*** __structured3dView;

  //! The vector shape.
  Array3DShape __shape;

  /** 
   * Constructs the structured view
   * 
   */
  void __buildView()
  {
    __structured3dView = new T**[__shape.nx()];
    for (size_t i=0; i < __shape.nx(); ++i) {
      __structured3dView[i] = new T* [__shape.ny()];
      for (size_t j=0; j < __shape.ny(); ++j) {
	__structured3dView[i][j]
	  = &Vector<T>::__values[i*__shape.ny()*__shape.nz() + j*__shape.nz()];
      }
    }
  }

  /** 
   * Cleans structured view
   * 
   */
  void __clean()
  {
    for (size_t i=0; i<__shape.nx(); ++i)
      delete[] __structured3dView[i];
    delete [] __structured3dView;
  }
public:

  /** 
   * Number of values in direction x
   * 
   * @return number of x values
   */
  size_t nx() const
  {
    return __shape.nx();
  }

  /** 
   * Number of values in direction y
   * 
   * @return number of y values
   */
  size_t ny() const
  {
    return __shape.ny();
  }

  /** 
   * Number of values in direction z
   * 
   * @return number of z values
   */
  size_t nz() const
  {
    return __shape.nz();
  }

  /** 
   * Read only access to the shape of the array
   * 
   * @return 
   */
  const Array3DShape& shape() const
  {
    return __shape;
  }

  /** 
   * Read-only access to the @f$ (i,j,k) @f$ element of the vector
   * 
   * @return element @f$ (i,j,k) @f$
   */
  const T& operator()(const size_t& i, const size_t& j, const size_t& k) const
  {
    ASSERT ((i<__shape.nx()) and (j<__shape.ny()) and (k<__shape.nz()));
    return __structured3dView[i][j][k];
  }

  /** 
   * Access to the @f$ (i,j,k) @f$ element of the vector
   * 
   * @return element @f$ (i,j,k) @f$
   */
  T& operator()(const size_t& i, const size_t& j, const size_t& k)
  {
    ASSERT ((i<__shape.nx()) and (j<__shape.ny()) and (k<__shape.nz()));
    return __structured3dView[i][j][k];
  }

  /** 
   * Sets all values of the array to the value @a t
   * 
   * @param t given value
   * 
   * @return new vector
   */
  Structured3DVector<T>& operator=(const T& t)
  {
    this->Vector<T>::operator=(t);
    return *this;
  }

  /** 
   * Copies a structured 3d vector
   * 
   * @param v given vector
   * 
   * @return new vector
   */
  Structured3DVector<T>& operator=(const Structured3DVector<T>& v)
  {
    ASSERT (__shape==v.__shape);
    this->Vector<T>::operator=(v);
    return *this;
  }

  /** 
   * Copies a vector
   * 
   * @param v given vector
   * 
   * @return new vector
   */
  const Structured3DVector<T>& operator=(const Vector<T>& v)
  {
    ASSERT (this->size() == v.size());
    this->Vector<T>::operator=(v);
    return *this;
  }

  /** 
   * Resizes the vector to a new shape
   * 
   * @param shape given new shape
   */
  void resize(const Array3DShape& shape)
  {
    this->__clean();
    __shape = shape;
    this->Vector<T>::resize(shape.size());
    this->__buildView();    
  }

  /** 
   * Constructor
   * 
   * @param nx number of elements in x direction
   * @param ny number of elements in y direction
   * @param nz number of elements in z direction
   */
  Structured3DVector(const size_t& nx, const size_t& ny, const size_t& nz)
    : Vector<T>(nx*ny*nz),
      __shape(nx,ny,nz)
  {
    this->__buildView();
  }

  /** 
   * Constructor
   * 
   * @param shape given shape of the Structured3DVector
   */
  Structured3DVector(const Array3DShape& shape)
    : Vector<T>(shape.size()),
      __shape(shape)
  {
    this->__buildView();
  }

  /** 
   * Copy constructor
   * 
   * @param v given Structured3DVector
   */
  Structured3DVector(const Structured3DVector& v)
    : Vector<T>(v),
      __shape(v.__shape)
  {
    this->__buildView();
  }

  /** 
   * Destructor
   * 
   */
  ~Structured3DVector()
  {
    this->__clean();
  }
};

#endif // STRUCTURED_3D_VECTOR_HPP
