//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _BI_CONJUGATE_GRADIENT_STABILIZED_OPTIONS_HPP_
#define _BI_CONJUGATE_GRADIENT_STABILIZED_OPTIONS_HPP_

//     : ParametrizableObject("bicg",
// 			   (Parameter("epsilon", 1E-5),
// 			    Parameter("max_iter", 500)))

#include <ParametrizableObject.hpp>

class BiConjugateGradientStabilizedOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  static const char* identifier()
  {
    // autodoc: "to change bi-conjugate gradient stabilized options"
    return "bicgstab";
  }

  real_t epsilon()
  {
    real_t eps = 0;
    get("epsilon",eps);
    return eps;
  }

  int maxiter()
  {
    int maxiter = 0;
    get("maxiter",maxiter);
    return maxiter;
  }

  explicit BiConjugateGradientStabilizedOptions()
  {
    // autodoc: "the maximum number of iterations"
    add(new IntegerParameter(500, "maxiter"));
    // autodoc: "the factor of reduction of the residu"
    add(new DoubleParameter (1E-5,"epsilon"));
  }

  ~BiConjugateGradientStabilizedOptions()
  {
    ;
  }
};
#endif // _BICONJUGATE_GRADIENT_STABILIZED_OPTIONS_HPP_

