//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_NOT_HPP
#define SCALAR_FUNCTION_NOT_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionNot.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 10:58:42 2006
 * 
 * @brief function that returns 1 if the argument function is 0 and
 * returns 0 else
 * 
 */
class ScalarFunctionNot
  : public ScalarFunctionBase
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __function;			/**< input function @f$ f @f$ */

  /** 
   * Writes the function in a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "not(" << *__function << ')';
    return os;
  }

public:
  /** 
   * Evaluates the function at position @f$ X @f$
   * 
   * @param X position of evaluation
   * 
   * @return @f$ \mbox{not}(f(X))@f$
   */
  real_t operator()(const TinyVector<3, real_t>& X) const
  {
    const bool value = ((*__function)(X) != 0);
    return not(value);
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param function the input function @f$ f @f$
   */
  ScalarFunctionNot(ConstReferenceCounting<ScalarFunctionBase> function)
    : ScalarFunctionBase(ScalarFunctionBase::not_),
      __function(function)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionNot()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_NOT_HPP
