//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARYSURFACEMESH_HPP
#define BOUNDARYSURFACEMESH_HPP

/**
 * @file   BoundarySurfaceMesh.hpp
 * @author Stephane Del Pino
 * @date   Fri Aug 27 23:49:25 2004
 * 
 * @brief  describes boundaries defined by Surface Meshes.
 * 
 * 
 */
#include <Boundary.hpp>
#include <ReferenceCounting.hpp>

class SurfaceMesh;

class BoundarySurfaceMesh
  : public Boundary
{
private:
  ConstReferenceCounting<SurfaceMesh> __surfaceMesh;

  void put(std::ostream& os) const;

public:
  ConstReferenceCounting<SurfaceMesh> surfaceMesh() const;

  BoundarySurfaceMesh(const BoundarySurfaceMesh& b);

  BoundarySurfaceMesh(ConstReferenceCounting<SurfaceMesh> s);

  ~BoundarySurfaceMesh();
};

#endif // BOUNDARYSURFACEMESH_HPP

