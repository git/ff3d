//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FIELD_OF_SCALAR_FUNCTION_HPP
#define FIELD_OF_SCALAR_FUNCTION_HPP

#include <ScalarFunctionBase.hpp>
#include <ReferenceCounting.hpp>

#include <vector>

class FieldOfScalarFunction
{
private:
  std::vector<ConstReferenceCounting<ScalarFunctionBase> > __field;

  FieldOfScalarFunction(const FieldOfScalarFunction&);
public:
  friend std::ostream& operator << (std::ostream& os,
				    const FieldOfScalarFunction& field);

  size_t numberOfComponents() const
  {
    return __field.size();
  }

  ConstReferenceCounting<ScalarFunctionBase>
  function(const size_t& i) const;

  void add(ConstReferenceCounting<ScalarFunctionBase> function);

  FieldOfScalarFunction();

  ~FieldOfScalarFunction();
};

#endif // FIELD_OF_SCALAR_FUNCTION_HPP
