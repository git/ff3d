//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_LINEAR_BORDER_OPERATOR_HPP
#define VARIATIONAL_LINEAR_BORDER_OPERATOR_HPP

#include <VariationalBorderOperator.hpp>

/**
 * @file   VariationalLinearBorderOperator.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  2 22:04:06 2002
 * 
 * @brief  describes linear border operators
 * 
 * Linear Border Operators, ie: right hand side of the equation
 */
class VariationalLinearBorderOperator
  : public VariationalBorderOperator
{
public:
  enum Type {
    FV
  };

private:
  const VariationalLinearBorderOperator::Type
  __type;			/**< linearBorder oerator type */

public:
  /** 
   * Returns the type of the operator
   * 
   * @return __type
   */
  const VariationalLinearBorderOperator::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param t the type of linear variational operator
   * @param testFunctionNumber the test function number
   * @param boundary the boundary where the linear operator is defined
   */
  VariationalLinearBorderOperator(VariationalLinearBorderOperator::Type t,
				  const size_t& testFunctionNumber,
				  ConstReferenceCounting<Boundary> boundary)
    : VariationalBorderOperator(testFunctionNumber, boundary),
      __type(t)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V another VariationalLinearBorderOperator
   */
  VariationalLinearBorderOperator(const VariationalLinearBorderOperator& V)
    : VariationalBorderOperator(V),
      __type(V.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalLinearBorderOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_LINEAR_BORDER_OPERATOR_HPP

