//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FieldOfScalarFunction.hpp>

std::ostream& operator << (std::ostream& os,
			   const FieldOfScalarFunction& field)
{
  ASSERT(field.numberOfComponents()>0);
  if (field.numberOfComponents()>1) os << '[';

  os << *field.function(0);
  for (size_t i=1; i<field.numberOfComponents(); ++i) {
    os << ',' << *field.function(i);
  }
  if (field.numberOfComponents()>1) os << ']';

  return os;
}


ConstReferenceCounting<ScalarFunctionBase>
FieldOfScalarFunction::
function(const size_t& i) const
{
  ASSERT(i<__field.size());
  return __field[i];
}

void
FieldOfScalarFunction::
add(ConstReferenceCounting<ScalarFunctionBase> function)
{
  __field.push_back(function);
}

FieldOfScalarFunction::
FieldOfScalarFunction()
{
  ;
}

FieldOfScalarFunction::
~FieldOfScalarFunction()
{
  ;
}
