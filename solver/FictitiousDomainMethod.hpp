//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FICTITIOUS_DOMAIN_METHOD_HPP
#define FICTITIOUS_DOMAIN_METHOD_HPP

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <Method.hpp>

#include <Problem.hpp>
#include <PDESystem.hpp>

#include <Solution.hpp>

#include <DegreeOfFreedomSet.hpp>

#include <Mesh.hpp>

#include <DiscretizationType.hpp>

class BoundaryConditionDiscretization;

/**
 * @file   FictitiousDomainMethod.hpp
 * @author Stephane Del Pino
 * @date   Mon Nov  3 18:29:42 2003
 * 
 * @brief  discretizes standard fictitious domain like methods
 * 
 */
class FictitiousDomainMethod
  : public Method
{
protected:
  ReferenceCounting<BaseMatrix> __A; /**< The matrix */
  ReferenceCounting<BaseVector> __b; /**< The second member */

  ConstReferenceCounting<Mesh> __mesh; /**< The discretization mesh */

  ConstReferenceCounting<Problem> __problem; /**< The problem to discretze */

  const DegreeOfFreedomSet& __degreeOfFreedomSet; /**< The degrees of freedom set */

private:
  /** 
   * Computes the degrees of freedom set and modifies the problem.
   * The problem is modified to take into account the domain geometry.
   * This template version use a specific \a MeshType.
   * 
   * @param givenProblem will be modified and stored in \a __problem
   */
  template <typename MeshType>
  void __computesDegreesOfFreedom(ConstReferenceCounting<Problem> givenProblem);

  template <typename CellType>
  real_t __integrateCharacteristicFunction(const CellType& e, const Domain& D);

  /** 
   * Effectively discretizes the problem
   * 
   */
  template <typename MeshType,
	    ScalarDiscretizationTypeBase::Type TypeOfDiscretization>
  void __discretizeOnMesh();

  /** 
   * Effectively discretizes the problem
   * 
   */
  template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization>
  void __discretize();

protected:
  /** 
   * Specific call to the boundary condition discretization
   * 
   * @return the boundary condition discretization
   */
  virtual ReferenceCounting<BoundaryConditionDiscretization> discretizeBoundaryConditions() = 0;

  /** 
   * Computes the degrees of freedom set and modifies the problem.
   * The problem is modified to take into account the domain geometry
   * 
   * @param givenProblem will be modified and stored in \a __problem
   */
  void computesDegreesOfFreedom(ConstReferenceCounting<Problem> givenProblem);
public:
  /** 
   * Discretizes the problem P
   * 
   * @param P the problem to discretize
   */
  void Discretize (ConstReferenceCounting<Problem> P);

  /** 
   * Computes the solution u of the problem
   * 
   * @param u the solution
   */
  void Compute (Solution& u);

  /** 
   * Read only access to the problem
   * 
   * @return the problem
   */
  const Problem& problem() const
  {
    return *__problem;
  }

  /** 
   * Access to the mesh
   * 
   * @return the mesh
   */
  const Mesh& mesh() const
  {
    return *__mesh;
  }

  /** 
   * Constructs a FictitiousDomainMethod using a mesh and a given
   * degree of freedom set.
   * 
   * @param discretization a discretization type
   * @param mesh a given mesh
   * @param dof a given degree of freedom set
   * 
   */
  FictitiousDomainMethod(const DiscretizationType& discretization,
			 ConstReferenceCounting<Mesh> mesh,
			 const DegreeOfFreedomSet& dof)
    : Method(discretization),
      __mesh(mesh),
      __degreeOfFreedomSet(dof)
  {
    ;
  }

  /** 
   * Virtual Destructor
   * 
   */
  virtual ~FictitiousDomainMethod()
  {
    ;
  }
};

#endif // FICTITIOUS_DOMAIN_METHOD_HPP
