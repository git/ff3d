//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Q0HexahedronFiniteElement.hpp>

TinyVector<3, real_t> Q0HexahedronFiniteElement::__massCenter(0.5, 0.5, 0.5);

const size_t
Q0HexahedronFiniteElement::
facesDOF[Hexahedron::NumberOfFaces][1]
= {{std::numeric_limits<size_t>::max()},
   {std::numeric_limits<size_t>::max()},
   {std::numeric_limits<size_t>::max()},
   {std::numeric_limits<size_t>::max()},
   {std::numeric_limits<size_t>::max()},
   {std::numeric_limits<size_t>::max()}};

real_t
Q0HexahedronFiniteElement::W(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 1;
}

real_t
Q0HexahedronFiniteElement::dxW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}

real_t
Q0HexahedronFiniteElement::dyW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}

real_t 
Q0HexahedronFiniteElement::dzW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}
