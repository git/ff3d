//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <LagrangeSolution.hpp>
#include <LagrangeFunction.hpp>

void
LagrangeSolution::
setUserFunction (const std::vector<ReferenceCounting<LagrangeFunction> >& U)
{
  Vector<real_t>& v = *__values;
  for (size_t i=0; i<__degreeOfFreedomSet.numberOfVariables(); ++i) {
    const ScalarDegreeOfFreedomPositionsSet& dofPositionSet = __degreeOfFreedomSet.positionsSet(i);
    const LagrangeFunction& u = *U[i];
    for(size_t j=0; j<dofPositionSet.number(); ++j) {
      v[__degreeOfFreedomSet(i,j)]=u[j];
    }
  }
}

void
LagrangeSolution::
getUserFunction(std::vector<ReferenceCounting<LagrangeFunction> >& U) const
{
  const Vector<real_t>& v = *__values;
  for (size_t i=0; i<__degreeOfFreedomSet.numberOfVariables(); ++i) {
    const ScalarDegreeOfFreedomPositionsSet& dofPositionSet = __degreeOfFreedomSet.positionsSet(i);
    LagrangeFunction& u = *U[i];
    for(size_t j=0; j<dofPositionSet.number(); ++j) {
      u[j] =  v[__degreeOfFreedomSet(i,j)];
    }
  }
}
