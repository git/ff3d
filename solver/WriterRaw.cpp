//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <WriterRaw.hpp>
#include <Mesh.hpp>

#include <fstream>

#include <ScalarFunctionBase.hpp>
#include <FieldOfScalarFunction.hpp>

#include <FEMFunction.hpp>

#include <config.h>
#include <EndianConverter.hpp>

void WriterRaw::
__saveScalarFunction(std::ostream& file,
		     const ScalarFunctionBase& f) const
{
  Vector<double> values(__mesh->numberOfVertices());

  switch (f.type()) {
  case ScalarFunctionBase::femfunction: {
    const FEMFunctionBase& fem
      = static_cast<const FEMFunctionBase&>(f);
    if (fem.baseMesh() == __mesh) {
      for (size_t i=0; i<values.size(); ++i) {
	values[i] = static_cast<double>(fem[i]);
      }
      break;
    } // if not continues the standard method
  }
  default: {
    for (size_t i=0; i<values.size(); ++i) {
      const TinyVector<3,real_t>& X = __mesh->vertex(i);
      values[i] = f(X);
    }
  }
  }

  switch (__fileDescriptor.type()) {
  case FileDescriptor::binary: {

#ifdef WORDS_BIGENDIAN
    littleEndianize(values);
#endif // WORDS_BIGENDIAN

    file.write(reinterpret_cast<char*>(&values[0]),
	       values.size()*sizeof(double)/sizeof(char));
    break;
  }
  case FileDescriptor::formatDefault:
  case FileDescriptor::dos:
  case FileDescriptor::mac:
  case FileDescriptor::unices: {
    for (size_t i=0; i<values.size(); ++i) {
      file << values[i] << __CR;
    }
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected file type",
		       ErrorHandler::unexpected);
  }
  }
}


void WriterRaw::
proceed() const
{
  std::ofstream file(__filename.c_str());
  if (file.bad()) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot open file '"
		       +stringify(__filename)+"'",
		       ErrorHandler::normal);
  }

  if (__fieldList.size() + __scalarFunctionList.size() == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot save mesh in raw format: '"
		       +stringify(__filename)+"'",
		       ErrorHandler::normal);
  }

  if (__fieldList.size() + __scalarFunctionList.size() > 1) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot save more than one field or function in raw format: '"
		       +stringify(__filename)+"'",
		       ErrorHandler::normal);
  }

  if (__fieldList.size() > 0) {
    const FieldOfScalarFunction& field = *__fieldList[0];
    for (size_t i = 0; i<field.numberOfComponents(); ++i) {
      const ScalarFunctionBase& f = *field.function(i);
      __saveScalarFunction(file,f);
    }
  }

  // Save scalar function
  if (__scalarFunctionList.size() > 0) {
    const ScalarFunctionBase& f = *__scalarFunctionList[0];
    __saveScalarFunction(file,f);
  }
}

WriterRaw::
WriterRaw(ConstReferenceCounting<Mesh> mesh,
	  const std::string& filename,
	  const FileDescriptor& fileDescriptor)
  : WriterBase(mesh,
	       filename,
	       fileDescriptor)
{
  ;
}

WriterRaw::
~WriterRaw()
{
  ;
}
