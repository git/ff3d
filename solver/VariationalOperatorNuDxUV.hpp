//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_OPERATOR_NU_DX_U_V_HPP
#define VARIATIONAL_OPERATOR_NU_DX_U_V_HPP

#include <VariationalBilinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorNuDxUV.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 23 13:48:04 2002
 * 
 * @brief  @f$ a(u,v) = \int\nu\partial_{x_i} u v@f$
 * 
 */
class VariationalNuDxUVOperator
  : public VariationalBilinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __nu;				/**< @f$\nu@f$ */

  const size_t __i;		/**< @f$ i@f$ in @f$\partial_{x_i}@f$ */
public:
  /** 
   * Access to @f$\nu@f$
   * 
   * @return __nu
   */
  ConstReferenceCounting<ScalarFunctionBase> nu() const
  {
    return __nu;
  }

  /** 
   * Access to @f$ i @f$
   * 
   * @return __i
   */
  const size_t& i() const
  {
    return __i;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int c\nu\partial_{x_i} u\, v @f$
   */
  ReferenceCounting<VariationalBilinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalNuDxUVOperator* newOperator
      = new VariationalNuDxUVOperator(*this);

    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__nu);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    newOperator->__nu = functionBuilder.getBuiltFunction();

    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param unknownNumber unknown number
   * @param unknownProperty unknown property
   * @param testFunctionNumber  test function number
   * @param testFunctionProperty  test function property
   * @param nu @f$\nu@f$
   * @param i the @f$ i@f$ in @f$\partial_{x_i}@f$
   */
  VariationalNuDxUVOperator(const size_t& unknownNumber,
			    const VariationalOperator::Property& unknownProperty,
			    const size_t& testFunctionNumber,
			    const VariationalOperator::Property& testFunctionProperty,
			    ConstReferenceCounting<ScalarFunctionBase> nu,
			    const size_t& i)
    : VariationalBilinearOperator(VariationalBilinearOperator::nuDxUV,
				  unknownNumber, unknownProperty,
				  testFunctionNumber, testFunctionProperty),
      __nu(nu),
      __i(i)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V given VariationalNuDxUVOperator
   */
  VariationalNuDxUVOperator(const VariationalNuDxUVOperator& V)
    : VariationalBilinearOperator(V),
      __nu(V.__nu),
      __i(V.__i)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalNuDxUVOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_NU_DX_U_V_HPP
