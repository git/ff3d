//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VERTICES_SET_HPP
#define VERTICES_SET_HPP

#include <Vertex.hpp>
#include <Vector.hpp>

/**
 * @file   VerticesSet.hpp
 * @author Stephane Del Pino
 * @date   Wed Jun  9 15:20:54 2004
 * 
 * @brief  Manages vertices set
 * 
 * This class main purpose is to provide a uniq interface to all
 * vertices sets, particularly useful for meshes management.
 */

class VerticesSet
{
private:
    Vector<Vertex> __vertices;
public:
  /** 
   * Returns the number of the vertex V in the list
   * 
   * @param v a vertex
   * 
   * @return the vertex number
   */
  inline size_t number(const Vertex& v) const
  {
    return __vertices.number(v);
  }

  /**
   * Read-only access to the number of vertices.
   * 
   * @return the number of vertices of the set
   */
  inline const size_t& numberOfVertices() const
  {
    return __vertices.size();
  }

  /** 
   * Change the size of the vertices container.
   * 
   * @param size the new size of the vertex set
   *
   * @note all data are lost!
   */
  inline void setNumberOfVertices(const size_t& size)
  {
    __vertices.resize(size);
  }

  /** 
   * Access to a vertex according to its number
   * 
   * @param i the vertex number
   * 
   * @return the ith vertex
   */
  inline const Vertex& operator[](const size_t& i) const
  {
    return __vertices[i];/// bounds are checked by the Vector class
  }

  /** 
   * Read-only access to a vertex according to its number
   * 
   * @param i the vertex number
   * 
   * @return the ith vertex
   */
  inline Vertex& operator[](const size_t& i)
  {
    return __vertices[i];/// bounds are checked by the Vector class
  }

  /** 
   * Constructs a VerticesSet to a given size s
   * 
   * @param s the given size
   */
  VerticesSet(const size_t& s)
    : __vertices(s)
  {
    ;
  }

  /** 
   * Copies a VerticesSet
   * 
   * @param v the given VerticesSet
   */
  VerticesSet(const VerticesSet& v)
    : __vertices(v.__vertices)
  {
    ;
  }

  /** 
   * Destructs a VerticesSet
   * 
   */
  ~VerticesSet()
  {
    ;
  }
};

#endif // VERTICES_SET_HPP
