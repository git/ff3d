//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_FORMAT_READER_HPP
#define MESH_FORMAT_READER_HPP

#include <MeshReader.hpp>

#include <set>
#include <cstdio>

/**
 * @file   MeshFormatReader.hpp
 * @author St�phane Del Pino
 * @date   Sun Dec 15 17:45:02 2002
 * 
 * @brief  Reads mesh from file in the 'mesh' format.
 * 
 */

class MeshFormatReader
  : public MeshReader
{
private:
  struct VertexError {};

  /**
   * List of allowed keyword in mesh file
   * 
   */
  enum KeywordType {
    EndOfFile,
    Unknown,
    MeshVersionFormatted,
    MeshDimension,
    Vertices,
    Triangles,
    Quadrilaterals,
    Tetrahedra,
    Hexahedra,
    Normals,
    Tangents,
    Corners,
    Edges,
    Ridges
  };

  /**
   * List of known keywords
   * 
   */
  typedef std::map<std::string, int> KeywordList;

  KeywordList __keywordList;	/**< The keyword list */

  /**
   * Type for keyword
   * 
   */
  typedef std::pair<std::string, int> Keyword;

  /** 
   * Skips next comments if exists
   * 
   */
  void __skipComments();

  /** 
   * Reads the next keyword and returns its token
   * 
   * @return KeywordToken
   */
  MeshFormatReader::Keyword __nextKeyword();

  /** 
   * Checks if a number is a valid vertex number
   * 
   * @param i a vertex number
   */
  inline void __checkVertex(const size_t& i);

  /** 
   * Gets a vertex number
   * 
   * @return next integer
   */
  inline size_t __getVertexNumber();

  /** 
   * get list of vertices
   * 
   */
  void __getVertices();

  /**
   * Read list of vertices
   * 
   */
  void __readVertices();

  /**
   * Read list of hexahedra
   * 
   */
  void __readHexahedra();

  /**
   * Read list of quadrilaterals
   * 
   */
  void __readQuadrilaterals();

  /**
   * Read list of tetrahedra
   * 
   */
  void __readTetrahedra();

  /**
   * Read list of triangles
   * 
   */
  void __readTriangles();

  /**
   * Read list of vertices
   * 
   */
  void __readElements();

  /** 
   * Copy constructor is forbidden
   * 
   * @param M a given MeshFormatReader
   */
  MeshFormatReader(const MeshFormatReader& M);

  /** 
   * Common interface for writing references
   * 
   * @param references the set of computed references
   * @param objectName the type of refernces
   */
  void __writeReferences(const std::set<size_t>& references,
			 std::string objectName);
public:

  /** 
   * Constructor
   * 
   * @param s the filename
   */
  MeshFormatReader(const std::string & s);

  /** 
   * Destructor
   * 
   */
  ~MeshFormatReader()
  {
    ;
  }
};

#endif // MESH_FORMAT_READER_HPP
