//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SURFACE_MESH_OF_QUADRANGLES_HPP
#define SURFACE_MESH_OF_QUADRANGLES_HPP

#include <Vector.hpp>
#include <Quadrangle.hpp>

#include <SurfaceMesh.hpp>
#include <NormalManager.hpp>

#include <ErrorHandler.hpp>
#include <EdgesBuilder.hpp>

class SurfaceMeshOfQuadrangles
  : public SurfaceMesh
{
public:
  typedef Quadrangle CellType;
  typedef Edge FaceType;

  typedef SurfaceMeshOfQuadrangles Transformed;

  typedef struct {} BorderMeshType; /**< BorderMeshType is not defined  */

  class iterator
    : public Mesh::T_iterator<SurfaceMeshOfQuadrangles, Quadrangle>
  {
  private:
    typedef Mesh::T_iterator<SurfaceMeshOfQuadrangles, Quadrangle> iterator_base;

  public:
    iterator&
    operator=(const iterator& i)
    {
      iterator_base::operator=(i);
      NormalManager::instance().update(__iterator);

      return *this;
    }

    iterator&
    operator=(CellType* cell)
    {
      iterator_base::operator=(cell);
      NormalManager::instance().update(__iterator);

      return *this;
    }

    iterator(SurfaceMeshOfQuadrangles& m,
	     iterator_base::Position position = iterator_base::Begin)
      : iterator_base(m, position)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    iterator(SurfaceMeshOfQuadrangles& m,
	     const size_t& cellNumber)
      : iterator_base(m,cellNumber)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    iterator(const iterator& i)
      : iterator_base(i)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    ~iterator()
    {
      NormalManager::instance().unsubscribe();
    }
  };

  class const_iterator
    : public Mesh::T_iterator<const SurfaceMeshOfQuadrangles, const Quadrangle>
  {
  private:
    typedef Mesh::T_iterator<const SurfaceMeshOfQuadrangles, const Quadrangle> const_iterator_base;

  public:
    const_iterator&
    operator=(const const_iterator& i)
    {
      const_iterator_base::operator=(i);
      NormalManager::instance().update(__iterator);

      return *this;
    }

    const_iterator&
    operator=(CellType* iterator)
    {
      const_iterator_base::operator=(iterator);
      NormalManager::instance().update(__iterator);

      return *this;
    }

    const_iterator(const SurfaceMeshOfQuadrangles& m,
		   const_iterator_base::Position position = const_iterator_base::Begin)
      : const_iterator_base(m, position)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    const_iterator(SurfaceMeshOfQuadrangles& m,
		   const size_t& cellNumber)
      : const_iterator_base(m,cellNumber)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    const_iterator(const const_iterator& i)
      : const_iterator_base(i)
    {
      NormalManager::instance().subscribe(__iterator);
    }

    ~const_iterator()
    {
      NormalManager::instance().unsubscribe();
    }
  };

private:
  ReferenceCounting<Vector<Quadrangle> > __cells;

public:
  bool hasBorderMesh() const
  {
    return false;
  }

  ConstReferenceCounting<Mesh> borderBaseMesh() const
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented yet",
		       ErrorHandler::unexpected);

    return 0;
  }

  std::string typeName() const
  {
    return "surface mesh of quadrilaterals";
  }


  void buildEdges()
  {
    EdgesBuilder<SurfaceMeshOfQuadrangles> edgesBuilder(*this);
    __edgesSet = edgesBuilder.edgesSet();
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const real_t& x, const real_t& y, const real_t& z) const
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Trying to find 3d point on a surface mesh",
		       ErrorHandler::normal);
    return false;
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const TinyVector<3>& p) const
  {
    return this->inside(p[0], p[1], p[2]);
  }  

  void computesFictitiousCells() const
  {
    this->__computesFictitiousCells<Quadrangle>(*__cells);
  }

  Quadrangle& cell(size_t i)
  {
    return (*__cells)[i];
  }

  const Quadrangle& cell(size_t i) const
  {
    return (*__cells)[i];
  }

  size_t cellNumber(const Quadrangle& c) const
  {
    return (*__cells).number(c);
  }

  //! Reserves storage for \a n SurfElem.
  inline void setNumberOfCells(const int n)
  {
    (*__cells).resize(n);
  }

  //! Read-only access to the number of cells.
  inline const size_t& numberOfCells() const
  {
    return (*__cells).size();
  }

  /** 
   * Read-only access to a face
   * 
   * @param i the face number
   * 
   * @return the \a i th face
   */
  const FaceType& face(const size_t& i) const
  {
    return (*__edgesSet)[i];
  }

  SurfaceMeshOfQuadrangles(const size_t theNumberOfCells)
    : SurfaceMesh(Mesh::surfaceMeshQuadrangles),
      __cells(new Vector<Quadrangle>(theNumberOfCells))
  {
    ;
  }

  SurfaceMeshOfQuadrangles(ReferenceCounting<VerticesSet> vertices,
			   ReferenceCounting<VerticesCorrespondance> correspondances,
			   ReferenceCounting<Vector<Quadrangle> > quadrangles)
    : SurfaceMesh(Mesh::surfaceMeshQuadrangles,
		  vertices,
		  correspondances),
      __cells(quadrangles)
  {
    ;
  }

  SurfaceMeshOfQuadrangles()
    : SurfaceMesh(Mesh::surfaceMeshQuadrangles)
  {
    ;
  }

  ~SurfaceMeshOfQuadrangles()
  {
    ;
  }
};

#endif // SURFACE_MESH_OF_QUADRANGLES_HPP

