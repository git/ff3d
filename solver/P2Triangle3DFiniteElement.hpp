//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef P2_TRIANGLE3D_FINITE_ELEMENT_HPP
#define P2_TRIANGLE3D_FINITE_ELEMENT_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <QuadratureFormula.hpp>
#include <ConformTransformation.hpp>

#include <LagrangianFiniteElement.hpp>
#include <ThreadStaticBase.hpp>

class P2Triangle3DFiniteElement
  : public ThreadStaticBase<P2Triangle3DFiniteElement>,
    public LagrangianFiniteElement<6, P2Triangle3DFiniteElement,
				   QuadratureFormulaP2Triangle3D>
{
private:
  static TinyVector<3, real_t> __massCenter; /**< mass center of the reference element */

public:
  enum {
    numberOfDegreesOfFreedom = 6,
    numberOfVertexDegreesOfFreedom = 1,
    numberOfEdgeDegreesOfFreedom = 1,
    numberOfFaceDegreesOfFreedom = 0,
    numberOfVolumeDegreesOfFreedom = 0
  };

  /** 
   * returns the mass center of the reference element
   * 
   * @return __massCenter
   */
  static const TinyVector<3, real_t>& massCenter()
  {
    return __massCenter;
  }


  real_t W  (const size_t& i, const TinyVector<3>& X) const;
  real_t dxW(const size_t& i, const TinyVector<3>& X) const;
  real_t dyW(const size_t& i, const TinyVector<3>& X) const;
  real_t dzW(const size_t& i, const TinyVector<3>& X) const;

  inline const
  TinyVector<QuadratureType::numberOfQuadraturePoints,
	     TinyVector<3> >&
  integrationVertices() const
  {
    return QuadratureType::instance().vertices();
  }

  P2Triangle3DFiniteElement()
  {
    ;
  }

  ~P2Triangle3DFiniteElement()
  {
    ;
  }
};

#endif // P2_TRIANGLE_FINITE_ELEMENT_HPP
