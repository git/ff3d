//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef LAGRANGE_SOLUTION_HPP
#define LAGRANGE_SOLUTION_HPP

#include <PDESolution.hpp>
class LagrangeFunction;

class LagrangeSolution
  : public PDESolution
{
private:
  //! No copy constructor
  LagrangeSolution(const LagrangeSolution&);

public:
  void setUserFunction (const std::vector<ReferenceCounting<LagrangeFunction> >& U);

  void getUserFunction(std::vector<ReferenceCounting<LagrangeFunction> >& U) const;

  LagrangeSolution(const DegreeOfFreedomSet& d)
    : PDESolution(d)
  {
    ;
  }

  ~LagrangeSolution()
  {
    ;
  }
};

#endif // LAGRANGE_SOLUTION_HPP
