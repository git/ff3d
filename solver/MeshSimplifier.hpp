//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESHSIMPLIFIER_HPP
#define MESHSIMPLIFIER_HPP

#include <MeshGenerator.hpp>
#include <set>
#include <list>
class MeshSimplifier
  : public MeshGenerator
{
private:

  template <typename CellType>
  struct EdgeDelete {
    Edge::Pair  edel;
    CellType* Tdel;
    bool exist;
  }; 

  
  template <typename MeshType>
  void __proceed(const MeshType& m);
  
  template <typename CellType>
  struct Internals;
    
public:
  
  /** 
   * Constructor
   * 
   * @param originalMesh the mesh to simplify
   * 
   */
  MeshSimplifier(ConstReferenceCounting<Mesh> originalMesh);

  /**
   * Copy constructor
   * 
   */
  MeshSimplifier(const MeshSimplifier& m);

  /** 
   * Destructor
   * 
   * 
   * @return 
   */
  ~MeshSimplifier()
  {
    ;
  }
};

#endif // MESHSIMPLIFIER_HPP
