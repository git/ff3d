//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DISCRETIZATION_TYPE_HPP
#define DISCRETIZATION_TYPE_HPP

#include <ScalarDiscretizationTypeBase.hpp>

#include <ReferenceCounting.hpp>
#include <vector>
#include <iostream>

/**
 * @file   DiscretizationType.hpp
 * @author Stephane Del Pino
 * @date   Mon May 30 23:28:37 2005
 * 
 * @brief This class describes vectorial types of Discretization
 * 
 */
class DiscretizationType
{
public:
  enum Family {
    fem,
    spectralLagrange,
    spectralLegendre,
    unknown
  };

  typedef std::vector<ConstReferenceCounting<ScalarDiscretizationTypeBase> >
  ScalarDiscretizationTypeList;

private:
  ScalarDiscretizationTypeList __types;  /**< Types of discretization */

  friend std::ostream& operator<<(std::ostream& os,
				  const DiscretizationType& discretization)
  {
    os << ScalarDiscretizationTypeBase::name(discretization.__types[0]->type());
    for (size_t i=1; i<discretization.__types.size(); ++i) {
      os << ',' << ScalarDiscretizationTypeBase::name(discretization.__types[i]->type());
    }
    return os;
  }

  Family __componentFamily(const size_t& i) const
  {
    ASSERT(i<__types.size());

    switch(__types[i]->type()) {
    case ScalarDiscretizationTypeBase::lagrangianFEM0:
    case ScalarDiscretizationTypeBase::lagrangianFEM1:
    case ScalarDiscretizationTypeBase::lagrangianFEM2: {
      return fem;
    }
    case ScalarDiscretizationTypeBase::spectralLagrange: {
      return spectralLagrange;
    }
    case ScalarDiscretizationTypeBase::spectralLegendre: {
      return spectralLegendre;
    }
    case ScalarDiscretizationTypeBase::undefined:
    case ScalarDiscretizationTypeBase::functionLike:
    default: {
      return unknown;
    }
    }
  }

public:

  /** 
   * Returns the family of the discretization type
   * 
   * @return the family
   */
  Family getFamily() const
  {
    Family family = this->__componentFamily(0);

    for (size_t i=1; i<__types.size(); ++i) {
      if (family != this->__componentFamily(i)) {
	return unknown;
      }
    }

    return family;
  }

  /** 
   * Gets the number of discretizations in the list
   * 
   * @return __types.size()
   */
  size_t number() const
  {
    return __types.size();
  }

  /** 
   * Read-only access to the type of the @a i th type of discretization
   * 
   * @param i number of the discretized variable
   * 
   * @return __types[i]
   */
  const ScalarDiscretizationTypeBase& operator[](const size_t& i) const
  {
    ASSERT(i<__types.size());
    return *(__types[i]);
  }

  /** 
   * Adds a type to the list of types
   * 
   * @param type type to add
   */
  void add(ConstReferenceCounting<ScalarDiscretizationTypeBase> type)
  {
    __types.push_back(type);
  }

  /** 
   * Constructor
   * 
   */
  DiscretizationType()
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d originale discretization type
   */
  DiscretizationType(const DiscretizationType& d)
    : __types(d.__types)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~DiscretizationType()
  {
    ;
  }
};

#endif // DISCRETIZATION_TYPE_HPP
