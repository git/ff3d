
//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 Stephane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#ifndef NORMAL_MANAGER_HPP
#define NORMAL_MANAGER_HPP

#include <ThreadStaticBase.hpp>

#include <SurfElem.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   NormalManager.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct 30 23:48:45 2005
 * 
 * @brief  Manages normal
 * 
 * This class manages the current normal of when iterating over
 * surface elements. This implementation is very simple and consideres
 * that the normal is evaluated over one surface element at once,
 * which seems quite natural.
 */
class NormalManager
  : public ThreadStaticBase<NormalManager>
{
private:
  const SurfElem* __surfElem;	/**< The current surface element */

  size_t __counter;		/**< counts the current number of subscriptions */

  mutable ReferenceCounting<TinyVector<3, real_t> >
  __normal;			/**< This vector contains the normal vector */

  /** 
   * Builds the normal vector if it is not built
   * 
   */
  void __buildNormal() const
  {
    if (__surfElem == 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Cannot evaluate normal, no surface is defined",
			 ErrorHandler::unexpected);
    }
    if (__normal == 0) {
      __normal = new TinyVector<3, real_t>(__surfElem->normal());
    }
  }

public:
  /** 
   * Subscribes the element @a s for whom the normal may be computed.
   * 
   * @param s the element to subscribe
   */
  void subscribe(const SurfElem* s)
  {
    __counter++;
    __surfElem = s;
  }

  /** 
   * Uses a new surface element
   * 
   * @param s the new surface element
   */
  void update(const SurfElem* s)
  {
    ASSERT(__surfElem != 0);
    __surfElem = s;
    __normal = 0; // normal is to recompute
  }

  /** 
   * This is called to inform the NormalManager that the surface have
   * been iterated, this avoid the use of wrong normal value
   */
  void unsubscribe()
  {
    ASSERT(__counter != 0);
    __counter--;
    if (__counter == 0) {
      __surfElem = 0;
      __normal = 0;
    }
  }
  
  /** 
   * Access to the first component of the normal
   * 
   * @return \f$ n_x \f$
   */
  real_t nx() const
  {
    __buildNormal();
    return (*__normal)[0];
  }

  /** 
   * Access to the second component of the normal
   * 
   * @return \f$ n_y \f$
   */
  real_t ny() const
  {
    __buildNormal();
    return (*__normal)[1];
  }

  /** 
   * Access to the third component of the normal
   * 
   * @return \f$ n_z \f$
   */
  real_t nz() const
  {
    __buildNormal();
    return (*__normal)[2];
  }

  /** 
   * Default constructor
   * 
   */
  explicit NormalManager()
    : __surfElem(0),
      __counter(0),
      __normal(0)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~NormalManager()
  {
    ASSERT(__counter  == 0);
    ASSERT(__surfElem == 0);
    ASSERT(__normal   == 0);
  }
};

#endif // NORMAL_MANAGER_HPP
