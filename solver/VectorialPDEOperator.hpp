//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// This class describes a vectorial PDE operator.
// It means a PDE operator that can be applied to a vectorial function.


#ifndef VECTORIAL_PDE_OPERATOR_HPP
#define VECTORIAL_PDE_OPERATOR_HPP

#include <vector>
#include <PDEOperatorSum.hpp>

/**
 * @file   VectorialPDEOperator.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 13:01:22 2006
 * 
 * @brief  descibes a vectorial PDE operator
 * 
 */
class VectorialPDEOperator
{
private:
  std::vector<ReferenceCounting<PDEOperatorSum> >
  __vectorPDEOperator;		/**< vector of PDEOperatorSum
				   associated to each unknown */

public:
  /** 
   * Read-only access to the size of the vector @em ie: the number of
   * unknowns
   * 
   * @return the number of equations
   */
  size_t size() const
  {
    return __vectorPDEOperator.size();
  }

  /** 
   * access to the ith Component of the vectorial PDE operator
   * 
   * @param i the desired componenent
   * 
   * @return __vectorPDEOperator[i]
   */
  ReferenceCounting<PDEOperatorSum>
  operator[](const size_t& i)
  {
    ASSERT(i<__vectorPDEOperator.size());
    return __vectorPDEOperator[i];
  }

  /** 
   * read-only access to the ith Component of the vectorial PDE operator
   * 
   * @param i the desired componenent
   * 
   * @return __vectorPDEOperator[i]
   */
  ConstReferenceCounting<PDEOperatorSum>
  operator[](const size_t& i) const
  {
    ASSERT(i<__vectorPDEOperator.size());
    return __vectorPDEOperator[i];
  }

  /** 
   * Writes the VectorialPDEOperator to a stream
   * 
   * @param os output sream
   * @param vectorialPDEOperator the VectorialPDEOperator to write
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const VectorialPDEOperator& vectorialPDEOperator)
  {
    if (vectorialPDEOperator.__vectorPDEOperator.size() > 0) {
      for(size_t i=0; i<vectorialPDEOperator.__vectorPDEOperator.size(); ++i) {
	if (i!=0)
	  os << " + ";
	os << '(' << *(vectorialPDEOperator.__vectorPDEOperator[i]) << ")(u_" << i << ')';
      }
    } else {
      os << 0;
    }
    return os;
  }

  /** 
   * "multiplies" the VectorialPDEOperator by a coefficient
   * 
   * @param c the coefficient
   * 
   * @return a new VectorialPDEOperator whose all PDEOperatorSum have
   * been "multiplied" by the coefficient
   */
  ConstReferenceCounting<VectorialPDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VectorialPDEOperator* newVectorialPDEOperator = new VectorialPDEOperator(this->size());

    for (size_t i=0; i<__vectorPDEOperator.size(); ++i)
      (*newVectorialPDEOperator).__vectorPDEOperator[i] = (*__vectorPDEOperator[i]) * c;

    return newVectorialPDEOperator;
  }

  /** 
   * addes a VectorialPDEOperator to the current
   * 
   * @param vectorialPDEOperator given vectorial PDE operator
   * 
   * @return modified VectorialPDEOperator
   */
  const VectorialPDEOperator&
  operator+=(const VectorialPDEOperator& vectorialPDEOperator)
  {
    ASSERT(__vectorPDEOperator.size() == vectorialPDEOperator.__vectorPDEOperator.size());

    for (size_t i=0; i<__vectorPDEOperator.size(); ++i)
      for (size_t j=0; j< vectorialPDEOperator.__vectorPDEOperator[i]->numberOfOperators(); ++j)
	(*__vectorPDEOperator[i]).add((*(vectorialPDEOperator.__vectorPDEOperator[i]))[j]);

    return *this;
  }

  /** 
   * substracts a VectorialPDEOperator to the current
   * 
   * @param vectorialPDEOperator given vectorial PDE operator
   * 
   * @return modified VectorialPDEOperator
   */
  const VectorialPDEOperator&
  operator-=(const VectorialPDEOperator& vectorialPDEOperator)
  {
    ASSERT(__vectorPDEOperator.size() == vectorialPDEOperator.__vectorPDEOperator.size());

    for (size_t i=0; i<__vectorPDEOperator.size(); ++i)
      for (size_t j=0; j<vectorialPDEOperator.__vectorPDEOperator[i]->numberOfOperators(); ++j)
	(*__vectorPDEOperator[i]).add((*vectorialPDEOperator.__vectorPDEOperator[i])[j]);

    return *this;
  }

  /** 
   * Constructor
   * 
   * @param dimension number of equations
   */
  VectorialPDEOperator(const size_t& dimension)
    : __vectorPDEOperator(dimension)
  {
    for(size_t i=0; i<dimension; ++i)
      __vectorPDEOperator[i] = new PDEOperatorSum();
  }
  
  /** 
   * Constructor
   * 
   */
  VectorialPDEOperator()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VectorialPDEOperator()
  {
    ;
  }
};

#endif // VECTORIAL_PDE_OPERATOR_HPP
