//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_LINEAR_BASIS_HPP
#define SCALAR_FUNCTION_LINEAR_BASIS_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionLinearBasis.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:42:18 2006
 * 
 * @brief  linear function canonical basis
 * 
 */
class ScalarFunctionLinearBasis
  : public ScalarFunctionBase
{
public:
  enum BasisType {
    x,
    y,
    z
  };

private:
  const BasisType __basisType;	/**< type of basis function */

  /** 
   * Write the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    switch (__basisType) {
    case ScalarFunctionLinearBasis::x: {
      os << 'x';
      break;
    }
    case ScalarFunctionLinearBasis::y: {
      os << 'y';
      break;
    }
    case ScalarFunctionLinearBasis::z: {
      os << 'z';
      break;
    }
    }
    return os;
  }

public:
  /** 
   * Evaluates the function at point @f$ X @f$
   * 
   * @param X evaluation point
   * 
   * @return @f$ X_i @f$
   */
  real_t operator()(const TinyVector<3, real_t>& X) const
  {
    switch (__basisType) {
    case ScalarFunctionLinearBasis::x: {
      return X[0];
    }
    case ScalarFunctionLinearBasis::y: {
      return X[1];
    }
    case ScalarFunctionLinearBasis::z: {
      return X[2];
    }
    }
    // never reached
    return 0;
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param basisType basis function type
   */
  ScalarFunctionLinearBasis(const BasisType& basisType)
    : ScalarFunctionBase(ScalarFunctionBase::linearBasis),
      __basisType(basisType)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f original function
   */
  ScalarFunctionLinearBasis(const ScalarFunctionLinearBasis& f)
    : ScalarFunctionBase(f),
      __basisType(f.__basisType)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionLinearBasis()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_LINEAR_BASIS_HPP
