//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_OBJECT_CHARACTERISTIC_HPP
#define SCALAR_FUNCTION_OBJECT_CHARACTERISTIC_HPP

#include <ScalarFunctionBase.hpp>
#include <TinyVector.hpp>
#include <ReferenceCounting.hpp>

#include <list>

class Scene;
class Object;

/**
 * @file   ScalarFunctionObjectCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 11:08:48 2006
 * 
 * @brief  Charateristic function of an object
 * 
 */
class ScalarFunctionObjectCharacteristic
  : public ScalarFunctionBase
{
private:
  const TinyVector<3,real_t>
  __reference;			/**< reference of the object */
  std::list<ConstReferenceCounting<Object> >
  __objects;			/**< list of object of that reference */

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "(<" << __reference[0]
       <<  ',' << __reference[1]
       <<  ',' << __reference[2]
       << ">)";
    return os;
  }

public:
  /** 
   * Evaluates the function at point @f$ X @f$
   * 
   * @param X position
   * 
   * @return 1 if @a X is inside one of the objects of the list, 0
   * else
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param scene the scene
   * @param ref the reference of objects to select in the scene
   */
  ScalarFunctionObjectCharacteristic(ConstReferenceCounting<Scene> scene,
				     const TinyVector<3, real_t>& ref);

  /** 
   * Copy constructor
   * 
   * @param f original function
   */
  ScalarFunctionObjectCharacteristic(const ScalarFunctionObjectCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionObjectCharacteristic();
};

#endif // SCALAR_FUNCTION_OBJECT_CHARACTERISTIC_HPP
