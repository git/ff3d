//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FACES_BUILDER_HPP
#define FACES_BUILDER_HPP

#include <map>
#include <TinyVector.hpp>

/**
 * @file   FacesBuilder.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct 16 16:34:36 2005
 * 
 * @brief  Builds faces of given meshes.
 * @todo   Should use elements sets and not meshes to build faces sets
 */

template <typename MeshType>
class FacesBuilder
{
private:
  typedef typename MeshType::FaceType FaceType;
  typedef typename MeshType::CellType CellType;
  typedef TinyVector<FaceType::NumberOfVertices, size_t> FaceVertices;

  std::map<FaceVertices, FaceVertices> __facesIdSet;

  ReferenceCounting<FacesSet<FaceType> > __facesSet;
public:

  ReferenceCounting<FacesSet<FaceType> >
  facesSet()
  {
    return __facesSet;
  }

  FacesBuilder(MeshType& mesh)
  {
    ffout(3) << "- FacesBuilder: building...\n";
    for (typename MeshType::const_iterator i(mesh); not(i.end()); ++i) {
      const CellType& cell = *i;
      for (size_t j=0; j<CellType::NumberOfFaces; ++j) {
	std::set<size_t> vertices;
	FaceVertices face;
	for (size_t k=0; k<FaceType::NumberOfVertices ;++k) {
	  face[k] = mesh.vertexNumber(cell(CellType::faces[j][k]));
	  vertices.insert(face[k]);
	}
	ASSERT(vertices.size() == FaceType::NumberOfVertices);
	FaceVertices faceId;
	size_t k=0;
	for (std::set<size_t>::const_iterator iv = vertices.begin();
	     iv != vertices.end(); ++iv,++k) {
	  faceId[k] = *iv;
	}

	if (__facesIdSet.find(faceId) == __facesIdSet.end()) {
	  __facesIdSet.insert(__facesIdSet.end(),
			      std::make_pair(faceId, face));
	}
      }
    }

    __facesSet = new FacesSet<FaceType>(__facesIdSet.size());
    FacesSet<FaceType>& facesSet = *__facesSet;
    size_t i=0;
    for (typename std::map<FaceVertices, FaceVertices>::const_iterator
	   f = __facesIdSet.begin(); f != __facesIdSet.end(); ++f, ++i) {
      TinyVector<FaceType::NumberOfVertices, Vertex*> newFace;

      for (size_t k=0; k<FaceType::NumberOfVertices; ++k) {
	const size_t vertexNumber = f->second[k];
	newFace[k] = &mesh.vertex(vertexNumber);
      }

      facesSet[i] = FaceType(newFace);
    }
    ffout(3) << "- FacesBuilder: done\n";
  }

  ~FacesBuilder()
  {
    ;
  }
};

#endif // FACES_BUILDER_HPP
