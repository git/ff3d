//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SECOND_ORDER_OPERATOR_HPP
#define SECOND_ORDER_OPERATOR_HPP

#include <TinyMatrix.hpp>
#include <PDEOperator.hpp>

/**
 * @file   SecondOrderOperator.hpp
 * @author St�phane Del Pino
 * @date   Mon Jun  9 23:27:51 2003
 * 
 * @brief  second order partial differencial operators
 * 
 * The class SecondOrderOperator describes second order partial
 * differencial operators: \f$ \nabla\cdot A \nabla \f$ where \f$ A
 * \f$ is a \f$ 3\times 3 \f$ matrix of functions.
 *
 * \par example: if \f$ A = I \f$, the operator is the Laplacian.
 */
class SecondOrderOperator
  : public PDEOperator
{
public:
  typedef
  TinyMatrix<3,3, ConstReferenceCounting<ScalarFunctionBase> >
  Matrix;			/**< @typedef TinyMatrix<3,3, ConstReferenceCounting<ScalarFunctionBase> > Matrix
				   matrix of functions */

private:
  ConstReferenceCounting<SecondOrderOperator::Matrix>
  __A;				/**< \f$ A \f$: \f$ A_{ij} \f$ */

public:
  /** 
   * Checks if a coefficient of @f$ A @f$ is set
   * 
   * @param i line
   * @param j column
   * 
   * @return true if @f$ A_{ij} @f$ has been set
   */
  bool isSet(const int&i,
	     const int&j) const
  {
    return ((*__A)(i,j) != 0);
  }

  /** 
   * Writes the name of the operator
   * 
   * @return "SecondOrderOperator"
   */
  std::string typeName() const
  {
    return "SecondOrderOperator";
  }

  /** 
   * Read-only access to the coefficient @f$ A_{ij} @f$
   * 
   * @param i line number
   * @param j column number
   * 
   * @return @f$ A_{ij} @f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  A(const int&i, const int&j) const
  {
    return (*__A)(i,j);
  }

  /** 
   * Read-only access to the matrix @f$ A @f$
   * 
   * @return __A
   */
  ConstReferenceCounting<SecondOrderOperator::Matrix>
  A() const
  {
    return __A;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c coefficient
   * 
   * @return @f$ \nabla \cdot c A \nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    ReferenceCounting<SecondOrderOperator::Matrix> A2
      = new SecondOrderOperator::Matrix;

    for (size_t i=0; i<3; ++i)
      for (size_t j=0; j<3; ++j) {
	if ((*__A)(i,j) != 0) {
	  ScalarFunctionBuilder functionBuilder;
	  functionBuilder.setFunction((*__A)(i,j));
	  functionBuilder.setBinaryOperation(BinaryOperation::product,c);
	  (*A2)(i,j) = functionBuilder.getBuiltFunction();
	}
      }

    return (new SecondOrderOperator(A2));
  }

  //! Returns a pointer on the opposed SecondOrderOperator operator.
  /** 
   * gets the opposite operator
   * 
   * @return @f$ -\nabla\cdot A\nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator-() const
 {
   ReferenceCounting<SecondOrderOperator::Matrix> A2
     = new SecondOrderOperator::Matrix;

    for (size_t i=0; i<3; ++i)
      for (size_t j=0; j<3; ++j) {
	if ((*__A)(i,j) != 0) {
	  ScalarFunctionBuilder functionBuilder;
	  functionBuilder.setFunction((*__A)(i,j));
	  functionBuilder.setUnaryMinus();
	  (*A2)(i,j) = functionBuilder.getBuiltFunction();
	}
      }

    return (new SecondOrderOperator(A2));
  }

  /** 
   * Constructor
   * 
   * @param A given matrix
   */
  SecondOrderOperator(ReferenceCounting<SecondOrderOperator::Matrix> A)
    : PDEOperator(PDEOperator::secondorderop,
		  9),
      __A(A)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param O given operator
   */
  SecondOrderOperator(const SecondOrderOperator& O)
    : PDEOperator(O),
      __A(O.__A)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~SecondOrderOperator()
  {
    ;
  }
};

#endif // SECOND_ORDER_OPERATOR_HPP
