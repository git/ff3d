//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef OCTREE_MESH_BUILDER_HPP
#define OCTREE_MESH_BUILDER_HPP

#include <Domain.hpp>
#include <Mesh.hpp>
#include <OctreeMesh.hpp>

/**
 * @file   OctreeMeshBuilder.hpp
 * @author Stephane Del Pino
 * @date   Sat Aug 18 17:44:39 2007
 * 
 * @brief Builds an octree mesh according to a background mesh and a
 * domain
 * 
 */
class OctreeMeshBuilder
{
private:
  ConstReferenceCounting<Domain>
  __domain;			/**< the domain */

  ConstReferenceCounting<Mesh>
  __mesh;			/**< the background mesh */

  const size_t __maximumLevel;	/**< the level of each octree */

  ConstReferenceCounting<OctreeMesh>
  __octreeMesh;			/**< the result octree mesh */

  /** 
   * Copy constructor is forbidden
   * 
   */
  OctreeMeshBuilder(const OctreeMeshBuilder&);

public:
  /** 
   * Construct the mesh
   * 
   */
  void buildMesh();

  /** 
   * Read only access to the built octree mesh
   * 
   * @return __octreeMesh
   */
  ConstReferenceCounting<OctreeMesh> getOctreeMesh() const
  {
    return __octreeMesh;
  }

  /** 
   * Read only access to the octree mesh as a base mesh
   * 
   * @return __octreeMesh
   */
  ConstReferenceCounting<Mesh> getBaseMesh() const
  {
    return static_cast<const OctreeMesh*>(__octreeMesh);
  }

  /** 
   * Constructor
   * 
   * @param domain the domain
   * @param mesh the background mesh
   * @param realLevel the level
   */
  OctreeMeshBuilder(ConstReferenceCounting<Domain> domain,
		    ConstReferenceCounting<Mesh> mesh,
		    const real_t& realLevel)
    : __domain(domain),
      __mesh(mesh),
      __maximumLevel(static_cast<size_t>(realLevel))
  {
    if (realLevel<0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot use a negative level to build an octree mesh",
			 ErrorHandler::normal);
    }
  }

  /** 
   * Destructor
   * 
   */
  ~OctreeMeshBuilder()
  {
    ;
  }
};

#endif // OCTREE_MESH_BUILDER_HPP
