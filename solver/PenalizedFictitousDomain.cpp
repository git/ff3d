//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <PenalizedFictitousDomain.hpp>
#include <BoundaryConditionDiscretizationPenalty.hpp>

template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization,
	  typename MeshType>
ReferenceCounting<BoundaryConditionDiscretization>
PenalizedFictitousDomain::__discretizeBoundaryConditionsOnMesh()
{
  BoundaryConditionDiscretizationPenalty<MeshType,
                                         TypeOfDiscretization>* bcDiscretization
    = new BoundaryConditionDiscretizationPenalty<MeshType,
                                                 TypeOfDiscretization>(problem(),
								       dynamic_cast<const MeshType&>(mesh()),
								       __degreeOfFreedomSet,
								       __epsilon);
  (*bcDiscretization).associatesMeshesToBoundaryConditions();
  return bcDiscretization;
}


template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization>
ReferenceCounting<BoundaryConditionDiscretization>
PenalizedFictitousDomain::__discretizeBoundaryConditions()
{
  switch (mesh().type()) {
  case Mesh::cartesianHexahedraMesh: {
    return this->__discretizeBoundaryConditionsOnMesh<TypeOfDiscretization,
                                                      Structured3DMesh>();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected mesh type",
		       ErrorHandler::unexpected);
    return 0;
  }
  }
}


ReferenceCounting<BoundaryConditionDiscretization>
PenalizedFictitousDomain::discretizeBoundaryConditions()
{
  switch(__discretizationType[0].type()) {
  case ScalarDiscretizationTypeBase::lagrangianFEM1: {
    return this->__discretizeBoundaryConditions<ScalarDiscretizationTypeBase::lagrangianFEM1>();
  }
  case ScalarDiscretizationTypeBase::lagrangianFEM2: {
    return this->__discretizeBoundaryConditions<ScalarDiscretizationTypeBase::lagrangianFEM2>();
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Discretization type not implemented",
		       ErrorHandler::normal);
    return 0;
  }
  }
}
