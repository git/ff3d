//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef ELIMINATED_FICTITIOUS_DOMAIN_HPP
#define ELIMINATED_FICTITIOUS_DOMAIN_HPP

#include <FictitiousDomainMethod.hpp>

#include <EliminatedFictitiousDomainOptions.hpp>
#include <GetParameter.hpp> 

/**
 * @file   EliminatedFictitiousDomain.hpp
 * @author Stephane Del Pino
 * @date   Mon Nov  3 18:45:13 2003
 * 
 * @brief Fictitious domain like method using elimination at coarse
 * grid vertices to take into account Dirichlet conditions.
 * 
 */
class EliminatedFictitiousDomain
  : public FictitiousDomainMethod
{
private:
  /** 
   * Discretize the boundary conditions using the specific MeshType.
   * 
   * @return boundary conditions discretization
   */
  template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization>
  ReferenceCounting<BoundaryConditionDiscretization> __discretizeBoundaryConditions();

  /** 
   * Discretize the boundary conditions using the specific MeshType.
   * 
   * @return boundary conditions discretization
   */
  template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization,
	    typename MeshType>
  ReferenceCounting<BoundaryConditionDiscretization> __discretizeBoundaryConditionsOnMesh();

  /** 
   * Discretize the boundary conditions using the specific MeshType.
   * 
   * @return boundary conditions discretization
   */
  ReferenceCounting<BoundaryConditionDiscretization> discretizeBoundaryConditions();

public:
  /** 
   * Constructs a EliminatedFictitiousDomain discretization using a
   * given mesh and a given degree of freedom set
   * 
   * @param discretization a discretization type
   * @param mesh a given mesh
   * @param dof a given degree of freedom set
   * 
   */
  EliminatedFictitiousDomain(const DiscretizationType& discretization,
			     ConstReferenceCounting<Mesh> mesh,
			     const DegreeOfFreedomSet& dof)
    : FictitiousDomainMethod(discretization, mesh, dof)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~EliminatedFictitiousDomain()
  {
    ;
  }
};

#endif // ELIMINATED_FICTITIOUS_DOMAIN_HPP

