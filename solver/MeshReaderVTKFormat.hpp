//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008- St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_READER_VTK_FORMAT_HPP
#define MESH_READER_VTK_FORMAT_HPP

#include <MeshReader.hpp>
#include <string>

class MeshReaderVTKFormat
  : public MeshReader
{
public:
  enum CellType {
    VTK_VERTEX               =  1,
    VTK_POLY_VERTEX          =  2,
    VTK_LINE                 =  3,
    VTK_POLY_LINE            =  4,
    VTK_TRIANGLE             =  5,
    VTK_TRIANGLE_STRIP       =  6,
    VTK_POLYGONE             =  7,
    VTK_PIXEL                =  8,
    VTK_QUAD                 =  9,
    VTK_TETRA                = 10,
    VTK_VOXEL                = 11,
    VTK_HEXAHEDRON           = 12,
    VTK_WEDGE                = 13,
    VTK_PYRAMID              = 14,
    VTK_QUADRATIC_EDGE       = 21,
    VTK_QUADRATIC_TRIANGLE   = 22,
    VTK_QUADRATIC_QUAD       = 23,
    VTK_QUADRATIC_TETRA      = 24,
    VTK_QUADRATIC_HEXAHEDRON = 25
  };

private:
  /** 
   * Copy constructor is forbidden
   * 
   * @param M a given MeshReaderVTKFormat
   */
  MeshReaderVTKFormat(const MeshReaderVTKFormat& M);

public:

  /** 
   * Constructor
   * 
   * @param s the filename
   */
  MeshReaderVTKFormat(const std::string& s);

  /** 
   * Destructor
   * 
   */
  ~MeshReaderVTKFormat()
  {
    ;
  }
};

#endif // MESH_READER_VTK_FORMAT_HPP
