//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef LAGRANGIAN_FINITE_ELEMENT_HPP
#define LAGRANGIAN_FINITE_ELEMENT_HPP

/**
 * @file   LagrangianFiniteElement.hpp
 * @author Stephane Del Pino
 * @date   Fri Dec 26 17:16:51 2003
 * 
 * @brief  Lagrangian finite element base class
 * 
 * Implements all needed functionalities required for Lagrangian
 * finite elements in 3d
 * 
 * @note Uses static polymorphism
 * 
 * @note numberOfDegreesOfFreedom is redundent since FiniteElementType
 * contains it. Would be nice to find a C++ way to remove it...
 */

template <size_t numberOfDegreesOfFreedom,
	  typename FiniteElementType,
	  typename GivenQuadratureType>
class LagrangianFiniteElement
{
public:
  typedef GivenQuadratureType
  QuadratureType;		/**< default quadrature type */

  enum {
    numberOfQuadraturePoints = QuadratureType::numberOfQuadraturePoints
  };

  typedef
  TinyVector<numberOfDegreesOfFreedom>
  ElementaryVector;		/**< type of elementary vector */

  typedef
  TinyMatrix<numberOfDegreesOfFreedom,
	     numberOfDegreesOfFreedom>
  ElementaryMatrix;		/**< type of elementary matrix */

private:  
  /** 
   * The static polymorphism core function
   * 
   * @return the casted oject
   */
  FiniteElementType& self()
  {
    return static_cast<FiniteElementType&>(*this);
  }

protected:
  /** 
   * Computes hat function value at quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function value
   */
  real_t __W  (const size_t& i, const size_t& j)
  {
    return self().W(i,self().integrationVertices()[j]);
  }

  /** 
   * Computes hat function derivative by x at quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's derivative value
   */
  real_t __dxW(const size_t& i, const size_t& j)
  {
    return self().dxW(i,self().integrationVertices()[j]);
  }

  /** 
   * Computes hat function derivative by y at quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's derivative value
   */
  real_t __dyW(const size_t& i, const size_t& j)
  {
    return self().dyW(i,self().integrationVertices()[j]);
  }

  /** 
   * Computes hat function derivative by z at quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's derivative value
   */
  real_t __dzW(const size_t& i, const size_t& j)
  {
    return self().dzW(i,self().integrationVertices()[j]);
  }

  
  TinyMatrix<numberOfDegreesOfFreedom,numberOfQuadraturePoints>
  __w;				/**< hat function values at quadrature points */

  TinyMatrix<numberOfDegreesOfFreedom,numberOfQuadraturePoints>
  __dxw;			/**< hat function dx values at quadrature points */

  TinyMatrix<numberOfDegreesOfFreedom,numberOfQuadraturePoints>
  __dyw;			/**< hat function dy values at quadrature points */

  TinyMatrix<numberOfDegreesOfFreedom,numberOfQuadraturePoints>
  __dzw;			/**< hat function dz values at quadrature points */

public:
  /** 
   * Read-only access to hat function value at a quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's value
   */
  inline const real_t& W(const size_t& i, const size_t& j) const
  {
    return __w(i,j);
  }

  /** 
   * Read-only access to hat function's derivative by x value at a
   * quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's value
   */
  inline const real_t& dxW(const size_t& i, const size_t& j) const
  {
    return __dxw(i,j);
  }

  /** 
   * Read-only access to hat function's derivative by y value at a
   * quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's value
   */
  inline const real_t& dyW(const size_t& i, const size_t& j) const
  {
    return __dyw(i,j);
  }

  /** 
   * Read-only access to hat function's derivative by z value at a
   * quadrature point
   * 
   * @param i the hat function number
   * @param j the number of quadrature point
   * 
   * @return the function's value
   */
  inline const real_t& dzW(const size_t& i, const size_t& j) const
  {
    return __dzw(i,j);
  }

public:
  /** 
   * Computes elementary matrix associated to \f$ \int w_j w_i \f$ on
   * a given element using the associated conform transformation
   * 
   * @param matElem the elementary matrix
   * @param T the given transformation
   */
  template <typename ConformTransformation>
  inline void integrateWjWi(ElementaryMatrix& matElem,
			    const ConformTransformation& T) const
  {
    ElementaryMatrix tmp = 0;

    for (size_t k=0; k<numberOfQuadraturePoints; ++k) { // Loop on integration vertices
      for (size_t j=0; j<numberOfDegreesOfFreedom; ++j) {
	for (size_t i=0; i<=j; ++i) {
	  tmp(i,j)
	    += W(i,k) * W(j,k) * QuadratureType::instance().weight(k);
	}
      }
    }

    // for this operator, matElem is symetric.
    for (size_t j=0; j<numberOfDegreesOfFreedom; ++j)
      for (size_t i=j+1; i<numberOfDegreesOfFreedom; ++i)
	tmp(i,j) = tmp(j,i);

    matElem += tmp;
  }

  /** 
   * Computes elementary matrix associated to \f$ \int \partial_{x_n} w_j
   * w_i \f$ on a given element using the associated conform
   * transformation
   * 
   * @param matElem the elementary matrix
   * @param n the \f$ n \f$ in \f$ \partial_{x_n} \f$
   * @param T the given transformation
   */
  template <typename ConformTransformation>
  inline void integrateDWjWi(ElementaryMatrix& matElem,
			     const size_t& n,
			     const ConformTransformation& T) const
  {
    ElementaryMatrix tmp = 0;

    for (size_t k=0; k<numberOfQuadraturePoints; ++k) { // Loop on integration vertices
      for (size_t j=0; j<numberOfDegreesOfFreedom; ++j) {
	const real_t fj
	  = dxW(j,k)*T.invJacobian(0,n)
	  + dyW(j,k)*T.invJacobian(1,n)
	  + dzW(j,k)*T.invJacobian(2,n);
	for (size_t i=0; i<numberOfDegreesOfFreedom; ++i) {
	  tmp(i,j)
	    += fj * W(i,k)  * QuadratureType::instance().weight(k);
	}
      }
    }
    matElem += tmp;
  }

  /** 
   * Computes elementary matrix associated to \f$ \int w_j
   * \partial_{x_n} w_i \f$ on a given element using the associated
   * conform transformation
   * 
   * @param matElem the elementary matrix
   * @param n the \f$ n \f$ in \f$ \partial_{x_n} \f$
   * @param T the given transformation
   */
  template <typename ConformTransformation>
  inline void integrateWjDWi(ElementaryMatrix& matElem,
			     const size_t& n,
			     const ConformTransformation& T) const
  {
    ElementaryMatrix tmp = 0;

    for (size_t k=0; k<numberOfQuadraturePoints; ++k) { // Loop on integration vertices
      for (size_t i=0; i<numberOfDegreesOfFreedom; ++i) {
	const real_t fi
	  = dxW(i,k)*T.invJacobian(0,n)
	  + dyW(i,k)*T.invJacobian(1,n)
	  + dzW(i,k)*T.invJacobian(2,n);
	for (size_t j=0; j<numberOfDegreesOfFreedom; ++j) {
	  tmp(i,j)
	    += fi * W(j,k) * QuadratureType::instance().weight(k);
	}
      }
    }
    matElem += tmp;
  }

  /** 
   * Computes elementary matrix associated to \f$ \int \partial_{x_n} w_j
   * \partial_{x_m} w_i \f$ on a given element using the associated
   * conform transformation
   * 
   * @param matElem the elementary matrix
   * @param n the \f$ n \f$ in \f$ \partial_{x_n} \f$
   * @param m the \f$ m \f$ in \f$ \partial_{x_m} \f$
   * @param T the given transformation
   */
  template <typename ConformTransformation>
  inline void integrateDWjDWi(ElementaryMatrix& matElem,
			      const size_t& n,
			      const size_t& m,
			      const ConformTransformation& T) const
  {
    ElementaryMatrix tmp = 0;

    for (size_t k=0; k<numberOfQuadraturePoints; ++k) { // Loop on integration vertices
      for (size_t j=0; j<numberOfDegreesOfFreedom; ++j) {
	const real_t fj
	  = dxW(j,k)*T.invJacobian(0,n)
	  + dyW(j,k)*T.invJacobian(1,n)
	  + dzW(j,k)*T.invJacobian(2,n);
	for (size_t i=0; i<numberOfDegreesOfFreedom; ++i) {
	  tmp(i,j)
	    += fj
	    * (  dxW(i,k)*T.invJacobian(0,m)
	       + dyW(i,k)*T.invJacobian(1,m)
	       + dzW(i,k)*T.invJacobian(2,m) )
	    * QuadratureType::instance().weight(k);
	}
      }
    }

    matElem += tmp;
  }

  /** 
   * Computes elementary vector associated to \f$ \int f w_i \f$ on a
   * given element using the associated conform transformation
   * 
   * @param vectElem the elementary vector
   * @param T the given transformation
   * @param f \f$ f \f$ values at quadrature points
   */
  template <typename ConformTransformation>
  inline void integrateWj(ElementaryVector& vectElem,
			  const ConformTransformation& T,
			  const TinyVector<numberOfQuadraturePoints, real_t>& f) const
  {
    vectElem = 0;

    for (size_t k=0; k<numberOfQuadraturePoints; ++k)
      for (size_t j=0; j<numberOfDegreesOfFreedom; ++j) {
	vectElem[j]
	  += W(j,k)
	  *  f[k]
	  *  QuadratureType::instance().weight(k);
      }
  }

  /** 
   * Constructor
   * 
   */
  LagrangianFiniteElement()
  {
    try {
      for (size_t i=0; i<numberOfDegreesOfFreedom; ++i) {
	for(size_t j=0; j<numberOfQuadraturePoints; ++j) {
	  __w(i,j)   =   __W(i,j);
	  __dxw(i,j) = __dxW(i,j);
	  __dyw(i,j) = __dyW(i,j);
	  __dzw(i,j) = __dzW(i,j);
	}
      }
    }
    catch(ErrorHandler e) {
      e.writeErrorMessage();    
    }
    catch(...) {
      fferr(0) << "error: Unknown exception caught!\n";
      fferr(0) << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
    }
  }

  /** 
   * Destructor
   * 
   */
  virtual ~LagrangianFiniteElement()
  {
    ;
  }
};

#endif // LAGRANGIAN_FINITE_ELEMENT_HPP
