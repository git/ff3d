//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_OPERATOR_ALPHA_DX_U_DX_V_HPP
#define VARIATIONAL_OPERATOR_ALPHA_DX_U_DX_V_HPP

#include <VariationalBilinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorAlphaDxUDxV.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 23 18:02:04 2002
 * 
 * @brief  @f$ a(u,v) = \int\alpha\partial_{x_j} u \partial_{x_i} v @f$
 * 
 * 
 */
class VariationalAlphaDxUDxVOperator
  : public VariationalBilinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __Alpha;			/**< @f$\alpha@f$ */

  const size_t __i;		/**< @f$ i @f$ in @f$\partial_{x_i}@f$ */
  const size_t __j;		/**< @f$ j @f$ in @f$\partial_{x_j}@f$ */
public:
  /** 
   * Access to @f$\alpha@f$
   * 
   * @return __Alpha
   */
  ConstReferenceCounting<ScalarFunctionBase> alpha() const
  {
    return __Alpha;
  }

  /** 
   * Access to the first derivative component number
   * 
   * @return __i
   */
  const size_t& i() const
  {
    return __i;
  }

  /** 
   * Access to the second derivative component number
   * 
   * @return __j
   */
  const size_t& j() const
  {
    return __j;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int c\alpha\partial_{x_i}u\partial_{x_j}v @f$
   */
  ReferenceCounting<VariationalBilinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalAlphaDxUDxVOperator* newOperator
      = new VariationalAlphaDxUDxVOperator(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__Alpha);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    (*newOperator).__Alpha = functionBuilder.getBuiltFunction();
    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param unknownNumber unknown number
   * @param unknownProperty unknown property
   * @param testFunctionNumber  test function number
   * @param testFunctionProperty  test function property
   * @param alpha the diffusion coefficient
   * @param i first derivative component number (in @f$\partial_{x_i}@f$)
   * @param j second derivative component number (in @f$\partial_{x_j}@f$)
   */
  VariationalAlphaDxUDxVOperator(const size_t& unknownNumber,
				 const VariationalOperator::Property& unknownProperty,
				 const size_t& testFunctionNumber,
				 const VariationalOperator::Property& testFunctionProperty,
				 ConstReferenceCounting<ScalarFunctionBase> alpha,
				 const size_t& i,
				 const size_t& j)
    : VariationalBilinearOperator(VariationalBilinearOperator::alphaDxUDxV,
				  unknownNumber, unknownProperty,
				  testFunctionNumber, testFunctionProperty),
      __Alpha(alpha),
      __i(i),
      __j(j)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param V original operator
   */
  VariationalAlphaDxUDxVOperator(const VariationalAlphaDxUDxVOperator& V)
    : VariationalBilinearOperator(V),
      __Alpha(V.__Alpha),
      __i(V.__i),
      __j(V.__j)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalAlphaDxUDxVOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_ALPHA_DX_U_DX_V_HPP
