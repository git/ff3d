//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef SOLVER_DRIVER_HPP
#define SOLVER_DRIVER_HPP

#include <SolverDriverOptions.hpp>
#include <GetParameter.hpp>

#include <DiscretizationType.hpp>

#include <Solution.hpp>
#include <DegreeOfFreedomSet.hpp>

#include <Problem.hpp>

#include <Mesh.hpp>

/*!
  \class SolverDriver

  This class manages the procedure to solve a problem:
  discretization method, ...

  \author St�phane Del Pino.
*/

class SolverDriver
{
public:
  enum MethodClass {
    fem,
    fictitiousFEM,
    fictitiousSpectralLagrange,
    fictitiousSpectralLegendre,
    spectralLagrange,
    spectralLegendre
  };

private:

  ConstReferenceCounting<Problem> __p;

  Solution& __u;

  DiscretizationType __discretizationType;

  ConstReferenceCounting<Mesh> __mesh;

  const DegreeOfFreedomSet& __degreeOfFreedomSet;
  const MethodClass __methodClass;

  SolverDriverOptions::MethodType __methodType;

  GetParameter<SolverDriverOptions> __options;
public:

  SolverDriver(ConstReferenceCounting<Problem> aProblem,
	       Solution& anUnknown,
	       const DiscretizationType& discretizationType,
	       ConstReferenceCounting<Mesh> aMesh,
	       const DegreeOfFreedomSet& dof,
	       const MethodClass methodClass)
    : __p(aProblem),
      __u(anUnknown),
      __discretizationType(discretizationType),
      __mesh(aMesh),
      __degreeOfFreedomSet(dof),
      __methodClass(methodClass)
  {
    __methodType = __options.value().type();
  }

  void run();

  ~SolverDriver()
  {
    ;
  }
};

#endif // SOLVER_DRIVER_HPP
