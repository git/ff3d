//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ScalarFunctionMaxComputer.hpp>

#include <Mesh.hpp>
#include <ScalarFunctionBase.hpp>

#include <limits>

ScalarFunctionMaxComputer::
ScalarFunctionMaxComputer(ConstReferenceCounting<Mesh> mesh,
			  ConstReferenceCounting<ScalarFunctionBase> function)
  : __mesh(mesh),
    __function(function)
{
  __maxValue = std::numeric_limits<real_t>::min();

  const Mesh& m = *__mesh;
  const ScalarFunctionBase& f = *__function;

  for (size_t i=0; i<m.numberOfVertices(); ++i) {
    const Vertex& X = m.vertex(i);
    const real_t value = f(X);
    __maxValue = std::max(__maxValue,value);
  }
}

ScalarFunctionMaxComputer::
ScalarFunctionMaxComputer(const ScalarFunctionMaxComputer& c)
  : __maxValue(c.__maxValue),
    __mesh(c.__mesh),
    __function(c.__function)
{
  ;
}

ScalarFunctionMaxComputer::
~ScalarFunctionMaxComputer()
{
  ;
}
