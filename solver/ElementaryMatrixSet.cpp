//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ElementaryMatrixSet.hpp>

#include <Problem.hpp>
#include <PDESystem.hpp>

#include <SecondOrderOperator.hpp>

#include <VariationalOperatorAlphaDxUDxV.hpp>
#include <VariationalOperatorNuUdxV.hpp>
#include <VariationalOperatorNuDxUV.hpp>

#include <VariationalProblem.hpp>

/*!
  Determines the number of needed elementary matrices for a given
  PDE System at construction
*/

#define EMS(a,b)							  \
template <>								  \
ElementaryMatrixSet<TinyMatrix<a,b> >::					  \
ElementaryMatrixSet(const Problem& problem)				  \
  : __divMuGrad(false),							  \
    __massOperator(false),						  \
    __secondOrderOperator(false),					  \
    __firstOrderOperator(false),					  \
    __secondOrderOperatorList(false),					  \
    __firstOrderUdxVList(false),					  \
    __firstOrderDxUVList(false)						  \
{									  \
									  \
  switch (problem.type()) {						  \
  case Problem::pde: {						  \
    const PDESystem& pdeSystem = dynamic_cast<const PDESystem&>(problem); \
									  \
    for (size_t i=0; i<pdeSystem.numberOfEquations(); ++i) {		  \
      const PDE& pde = pdeSystem[i].pde();				  \
      for (size_t j=0; j<pdeSystem.numberOfEquations(); ++j) {		  \
	const PDEOperatorSum& pdeOpSum = *(pde[j]);			  \
	for (size_t k=0; k<pdeOpSum.numberOfOperators(); ++k) {		  \
	  switch ((*pdeOpSum[k]).type()) {				  \
	  case PDEOperator::firstorderop: {				  \
	    __firstOrderOperator = true;				  \
	    for (size_t m=0; m<3; ++m)					  \
	      __firstOrderDxUVList[m] = true;				  \
	    break;							  \
	  }								  \
	  case PDEOperator::divmugrad: {				  \
	    __divMuGrad = true;						  \
	    break;							  \
	  }								  \
	  case PDEOperator::secondorderop: {				  \
	    __secondOrderOperator = true;				  \
									  \
	    const SecondOrderOperator& so				  \
	      = dynamic_cast<const SecondOrderOperator&>(*pdeOpSum[k]);	  \
									  \
	    for (size_t m=0; m<3; ++m)					  \
	      for (size_t n=0; n<3; ++n) {				  \
		if (so.isSet(m,n))					  \
		  __secondOrderOperatorList(m,n) = true;		  \
	      }								  \
	    break;							  \
	  }								  \
	  case PDEOperator::massop: {					  \
	    __massOperator = true;					  \
	    break;							  \
	  }								  \
	  default: {							  \
            throw ErrorHandler(__FILE__,__LINE__,			  \
			       "unexpected operator type",		  \
			       ErrorHandler::unexpected);		  \
	  }								  \
	  }								  \
	}								  \
      }									  \
    }									  \
    break;								  \
  }									  \
  case Problem::variational: {					  \
    const VariationalProblem& P						  \
      = dynamic_cast<const VariationalProblem&>(problem);		  \
    for (VariationalProblem::bilinearOperatorConst_iterator		  \
	   i = P.beginBilinearOperator();				  \
	 i != P.endBilinearOperator(); ++i) {				  \
      switch ((*(*i)).type()) {						  \
      case VariationalBilinearOperator::muGradUGradV: {			  \
	__divMuGrad = true;						  \
	break;								  \
      }									  \
      case VariationalBilinearOperator::alphaDxUDxV: {			  \
	__secondOrderOperator = true;					  \
	const VariationalAlphaDxUDxVOperator& I				  \
	  = static_cast<const VariationalAlphaDxUDxVOperator&>(*(*i));	  \
	__secondOrderOperatorList(I.i(), I.j()) = true;			  \
	break;								  \
      }									  \
      case VariationalBilinearOperator::nuUdxV: {			  \
	__firstOrderOperator = true;					  \
	const VariationalNuUdxVOperator& I				  \
	  = static_cast<const VariationalNuUdxVOperator&>(*(*i));	  \
	__firstOrderUdxVList[I.i()] = true;				  \
	break;								  \
      }									  \
      case VariationalBilinearOperator::nuDxUV: {			  \
	__firstOrderOperator = true;					  \
	const VariationalNuDxUVOperator& I				  \
	  = static_cast<const VariationalNuDxUVOperator&>(*(*i));	  \
	__firstOrderDxUVList[I.i()] = true;				  \
	break;								  \
      }									  \
      case VariationalBilinearOperator::alphaUV: {			  \
	__massOperator = true;						  \
	break;								  \
      }									  \
      default: {							  \
        throw ErrorHandler(__FILE__,__LINE__,				  \
			   "unexpected operator type",			  \
			   ErrorHandler::unexpected);			  \
      }									  \
      }									  \
    }									  \
    break;								  \
  }									  \
  default: {								  \
    throw ErrorHandler(__FILE__,__LINE__,				  \
		       "unexpected problem type",			  \
		       ErrorHandler::unexpected);			  \
  }									  \
  }									  \
}

EMS(27,27); 			/**< Q2-Q2 */
EMS(10,10);			/**< P2-P2 */
EMS(8,8);			/**< Q1-Q1 */
EMS(4,4);			/**< P1-P1 */
EMS(1,1);			/**< Q0-Q0 P0-P0 */
