//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef Q2_QUADRANGLE3D_FINITE_ELEMENT_HPP
#define Q2_QUADRANGLE3D_FINITE_ELEMENT_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <QuadratureFormula.hpp>
#include <ConformTransformation.hpp>

#include <LagrangianFiniteElement.hpp>
#include <ThreadStaticBase.hpp>

class Q2Quadrangle3DFiniteElement
  : public ThreadStaticBase<Q2Quadrangle3DFiniteElement>,
    public LagrangianFiniteElement<9, Q2Quadrangle3DFiniteElement,
				   QuadratureFormulaQ2Quadrangle3D>
{
private:
  static TinyVector<3, real_t> __massCenter; /**< mass center of the reference element */

  inline real_t __w1(const real_t& x) const
  {
    return (x-1)*(2*x-1);
  }

  inline real_t __w2(const real_t& x) const
  {
    return 4*x*(1-x);
  }

  inline real_t __w3(const real_t& x) const
  {
    return x*(2*x-1);
  }

  inline real_t __dw1(const real_t& x) const
  {
    return 4*x-3;
  }

  inline real_t __dw2(const real_t& x) const
  {
    return 4-8*x;
  }

  inline real_t __dw3(const real_t& x) const
  {
    return 4*x-1;
  }

public:
  enum {
    numberOfDegreesOfFreedom = 9, /**< DOF means Degrees of Freedom */
    numberOfVertexDegreesOfFreedom = 1,
    numberOfEdgeDegreesOfFreedom = 1,
    numberOfFaceDegreesOfFreedom = 1,
    numberOfVolumeDegreesOfFreedom = 0
  };

  /** 
   * returns the mass center of the reference element
   * 
   * @return __massCenter
   */
  static const TinyVector<3, real_t>& massCenter()
  {
    return __massCenter;
  }

  /** 
   * Computes a hat function at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ w_i(\mathbf{x}) \f$
   */
  real_t W  (const size_t& i, const TinyVector<3, real_t>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_x w_i(\mathbf{x}) \f$
   */
  real_t dxW(const size_t& i, const TinyVector<3, real_t>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_y w_i(\mathbf{x}) \f$
   */
  real_t dyW(const size_t& i, const TinyVector<3, real_t>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_z w_i(\mathbf{x}) \f$
   */
  real_t dzW(const size_t& i, const TinyVector<3, real_t>& x) const;

  inline const
  TinyVector<QuadratureType::numberOfQuadraturePoints,
	     TinyVector<3, real_t> >&
  integrationVertices() const
  {
    return QuadratureType::instance().vertices();
  }

  Q2Quadrangle3DFiniteElement()
  {
    ;
  }

  ~Q2Quadrangle3DFiniteElement()
  {
    ;
  }
};

#endif // Q2_QUADRANGLE3D_FINITE_ELEMENT_HPP

