//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef MESH_DOMAIN_TETRAHEDRIZOR_HPP
#define MESH_DOMAIN_TETRAHEDRIZOR_HPP

#include <MeshTetrahedrizor.hpp>
#include <Domain.hpp>

/**
 * @file   MeshDomainTetrahedrizor.hpp
 * @author Stephane Del Pino
 * @date   Sat Sep  4 20:29:55 2004
 * 
 * @brief Constructs a tetrahedral mesh of a given domain using a
 * background domain.
 *
 * @note The given mesh is not intended to be used for computations,
 * only for visualization.
 * 
 * @todo run() function could be optimized a lot.
 */

class MeshDomainTetrahedrizor
  : public MeshTetrahedrizor
{
private:
  ConstReferenceCounting<Domain> __domain; /**< The meshed domain */

  /** 
   * Copy constructor is forbiden
   * 
   * @param m a give MeshTetrahedrizor
   * 
   */
  MeshDomainTetrahedrizor(const MeshDomainTetrahedrizor& m);

public:
  /** 
   * Runs the tetrahedral mesh generation
   * 
   * @param builtLocalizationTools tells to build or not the localization tools
   * 
   */
  virtual void run(const bool& builtLocalizationTools = true);

  /** 
   * Constructs a MeshDomainTetrahedrizor with a given \a inputMesh
   * and \a domain
   * 
   * @param inputMesh the orignal mesh
   * @param domain the domain to mesh
   * 
   */
  MeshDomainTetrahedrizor(ConstReferenceCounting<Mesh> inputMesh,
			  ConstReferenceCounting<Domain> domain)
    : MeshTetrahedrizor(inputMesh),
      __domain(domain)
  {
    ;
  }

  /** 
   * 
   * Destructor
   * 
   */
  ~MeshDomainTetrahedrizor()
  {
    ;
  }
};

#endif // MESH_DOMAIN_TETRAHEDRIZOR_HPP
