//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef WRITER_BASE_HPP
#define WRITER_BASE_HPP

#include <ReferenceCounting.hpp>
#include <FileDescriptor.hpp>

#include <string>
#include <vector>
#include <string>

class ScalarFunctionBase;
class FieldOfScalarFunction;
class Mesh;

/**
 * @file   WriterBase.hpp
 * @author St�phane Del Pino
 * @date   Fri Feb 23 12:48:57 2007
 * 
 * @brief  Writer base class
 */
class WriterBase
{
public:
  typedef std::vector<ConstReferenceCounting<FieldOfScalarFunction> > FieldList;
  typedef std::vector<ConstReferenceCounting<ScalarFunctionBase> > ScalarFunctionList;

protected:
  ConstReferenceCounting<Mesh>
  __mesh;			/**< the saving mesh */
  const std::string __filename;	/**< name of the file */

  const FileDescriptor&
  __fileDescriptor;		/**< describes file type */
  const std::string __CR;	/**< type of carriage return for text files */

  FieldList __fieldList;	/**< the field list */

  ScalarFunctionList __scalarFunctionList;
				/**< the function list */

private:
  /** 
   * Forbidden copy constructor
   * 
   */
  WriterBase(const WriterBase&);

public:
  /** 
   * Adds a function to the list of functions
   * 
   * @param function given function
   */
  void add(ConstReferenceCounting<ScalarFunctionBase> function);

  /** 
   * Adds a field to the list of fields
   * 
   * @param field given field
   */
  void add(ConstReferenceCounting<FieldOfScalarFunction> field);

  /** 
   * writes the function
   * 
   */
  virtual void proceed() const=0;

  /** 
   * Constructor
   * 
   * @param mesh the mesh used for storage 
   * @param filename the filename of the storage file
   * @param fileDescriptor describes file type
   */
  WriterBase(ConstReferenceCounting<Mesh> mesh,
	     const std::string& filename,
	     const FileDescriptor& fileDescriptor);

  /** 
   * Destructor
   * 
   */
  virtual ~WriterBase();
};

#endif // WRITER_BASE_HPP
