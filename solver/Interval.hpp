//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef INTERVAL_HPP
#define INTERVAL_HPP

#include <Types.hpp>

/**
 * @file   Interval.hpp
 * @author Dris Yakoubi
 * @date   Sun Apr 29 20:45:29 2007
 * 
 * @brief  describes intervals @f$ (a,b) @f$
 * 
 */
class Interval
{
private:
  const real_t __a;		/**< @f$ a @f$ */
  const real_t __b;		/**< @f$ b @f$ */

public:
  /** 
   * Read-only access to @f$ a @f$
   * 
   * @return __a
   */
  const real_t& a() const;

  /** 
   * Read-only access to @f$ b @f$
   * 
   * @return __b
   */
  const real_t& b() const;

  /** 
   * Copy constructor
   * 
   * @param i given interval
   */
  Interval(const Interval& i);

  /** 
   * Constructs an interval
   * 
   * @param a  @f$ a @f$
   * @param b  @f$ b @f$
   */
  Interval(const real_t& a,
	   const real_t& b);

  /** 
   * Destructor
   * 
   */
  ~Interval();
};

#endif // INTERVAL_HPP
