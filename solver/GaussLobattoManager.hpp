//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008- St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef GAUSS_LOBATTO_MANAGER_HPP
#define GAUSS_LOBATTO_MANAGER_HPP

#include <ThreadStaticBase.hpp>
#include <ReferenceCounting.hpp>

#include <GaussLobatto.hpp>

#include <vector>

/**
 * @file   GaussLobattoManager.hpp
 * @author St�phane Del Pino
 * @date   Mon Mar 10 22:51:49 2008
 * 
 * @brief  Manager for Gauss-Lobatto vertices
 */
class GaussLobattoManager
  : public ThreadStaticBase<GaussLobattoManager>
{
private:
  mutable
  std::vector<ConstReferenceCounting<GaussLobatto> >
  __gaussLobatto;		/**< Stored Gauss-Lobatto formulae */

  /** 
   * Builds missing Gauss-Lobatto formulae up to @a n
   * 
   * @param n the value for completing the set of Gauss-Lobatto
   * vertices
   */
  void __buildGaussLobatto(const size_t& n) const;

public:
  /** 
   * Read-only access to the @a n th formula
   * 
   * @param n number of the set of vertices
   * 
   * @return the @n th formula
   */
  const GaussLobatto& get(const size_t& n) const;

  /** 
   * Read-only access to a reference on the @a n th formula
   * 
   * @param n number of the set of vertices
   * 
   * @return the @n th formula
   */
  ConstReferenceCounting<GaussLobatto> getReference(const size_t& n) const;

  /** 
   * Default constructor
   * 
   */
  GaussLobattoManager()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~GaussLobattoManager()
  {
    ;
  }
};

#endif // GAUSS_LOBATTO_MANAGER_HPP
