//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// Implement the sum of scalar PDE Operators

#ifndef PDE_OPERATOR_SUM_HPP
#define PDE_OPERATOR_SUM_HPP

#include <vector>

#include <StreamCenter.hpp>

#include <PDEOperator.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   PDEOperatorSum.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 23:47:35 2006
 * 
 * @brief  descibe the sum of PDEOperators
 * 
 */
class PDEOperatorSum
{
private:
  typedef
  std::vector<ConstReferenceCounting<PDEOperator> >
  OperatorList;			/**< @typedef std::vector<ConstReferenceCounting<PDEOperator> > OperatorList
				   list of PDEOperator sum. */

  OperatorList __operators;	/**< list of operators in the sum */

public:
  /** 
   * Read-only access to the numberof operators in the sum
   * 
   * @return 
   */
  size_t numberOfOperators() const
  {
    return __operators.size();
  }

  /** 
   * "multiplies" a PDEOperatorSum by a coefficient
   * 
   * @param c the coefficient
   * 
   * @return the new PDEOperatorSum
   */
  ReferenceCounting<PDEOperatorSum>
  operator* (const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    PDEOperatorSum* newPDEOperatorSum = new PDEOperatorSum();
    newPDEOperatorSum->__operators.reserve(__operators.size());

    for (OperatorList::const_iterator i = __operators.begin();
	 i != __operators.end(); ++i) {
      newPDEOperatorSum->add((*(*i))*c);
    }

    return newPDEOperatorSum;
  }

  /** 
   * Read-only access to the ith PDEOperator
   * 
   * @param i the number of the PDEOperator
   * 
   * @return __operators[i]
   */
  ConstReferenceCounting<PDEOperator>
  operator[](const size_t& i) const
  {
    ASSERT (i<__operators.size());
    return (__operators[i]);
  }

  /** 
   * Affects a PDEOperatorSum
   * 
   * @param pdeOperatorSum 
   * 
   * @return the new PDEOperatorSum
   */
  const PDEOperatorSum&
  operator=(const PDEOperatorSum& pdeOperatorSum)
  {
    __operators.resize(pdeOperatorSum.__operators.size());
    std::copy(pdeOperatorSum.__operators.begin(),
	      pdeOperatorSum.__operators.end(),
	      __operators.begin());

    return *this; 
  }

  /** 
   * Writes the PDEOperatorSum
   * 
   * @param os output stream
   * @param pdeOperatorSum the PDE operator sum
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const PDEOperatorSum& pdeOperatorSum)
  {
    if (pdeOperatorSum.__operators.size() > 0) {
      os << *(pdeOperatorSum.__operators[0]);
      for (size_t i=1; i<pdeOperatorSum.__operators.size(); ++i)
	os << '+' << *(pdeOperatorSum.__operators[i]);
    } else {
      os << 0;
    }

    return os;
  }

  /** 
   * Add a PDE operator to the list
   * 
   * @param pdeOperator the operator to add
   */
  void add(ConstReferenceCounting<PDEOperator> pdeOperator)
  {
    __operators.push_back(pdeOperator);
  }

  /** 
   * Constructor
   * 
   */
  PDEOperatorSum()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~PDEOperatorSum()
  {
    ;
  }
};

#endif // PDE_OPERATOR_SUM_HPP
