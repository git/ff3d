//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_INTEGRATE_HPP
#define SCALAR_FUNCTION_INTEGRATE_HPP

#include <ScalarFunctionBase.hpp>
#include <FEMFunctionBase.hpp>

/**
 * @file   ScalarFunctionIntegrate.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:35:04 2006
 * 
 * @brief  integrates a function in a direction @f$ \int_a^b f dx_i @f$
 *  
 * @bug only implemented for FEM functions
 */
class ScalarFunctionIntegrate
  : public ScalarFunctionBase
{
public:
  enum Direction {
    x=0,
    y=1,
    z=2
  };

private:
  ConstReferenceCounting<ScalarFunctionBase>
  __lowerBound;			/**< lower bound of integration @f$ a @f$ */
  ConstReferenceCounting<ScalarFunctionBase>
  __upperBound;			/**< upper bound of integration @f$ b @f$ */
  ConstReferenceCounting<FEMFunctionBase>
  __functionToIntegrate;	/**< function to integrate @f$ f @f$ */
  const Direction __direction;	/**< direction of integration */

  /** 
   * Evaluate the function using the mesh specialization
   * 
   * @param X the position evaluation
   */
  template <typename MeshType>
  real_t __evaluate(const TinyVector<3,real_t>& X) const;

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

public:
  /** 
   * Evaluates the function to the position @f$ X @f$
   * 
   * @param X position
   * 
   * @return @f$ \int_d f @f$
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param lowerBound lower bound of integration @f$ a @f$
   * @param upperBound upper bound of integration @f$ b @f$
   * @param functionToIntegrated function to integrate
   * @param direction direction of integration
   */
  ScalarFunctionIntegrate(ConstReferenceCounting<ScalarFunctionBase> lowerBound,
			  ConstReferenceCounting<ScalarFunctionBase> upperBound,
			  ConstReferenceCounting<ScalarFunctionBase> functionToIntegrated,
			  const ScalarFunctionIntegrate::Direction& direction);

  /** 
   * Copy contructor
   * 
   * @param f original function
   */
  ScalarFunctionIntegrate(const ScalarFunctionIntegrate& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionIntegrate();
};

#endif // SCALAR_FUNCTION_INTEGRATE_HPP
