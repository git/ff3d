//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_DISCRETIZATION_TYPE_LAGRANGE_HPP
#define SCALAR_DISCRETIZATION_TYPE_LAGRANGE_HPP

#include <ScalarDiscretizationTypeBase.hpp>
#include <TinyVector.hpp>

/**
 * @file   ScalarDiscretizationTypeLagrange.hpp
 * @author Stephane Del Pino
 * @date   Mon May 30 23:28:37 2005
 * 
 * @brief This class describes types of discretization for scalar
 * lagrange quantities
 * 
 */
class ScalarDiscretizationTypeLagrange
  : public ScalarDiscretizationTypeBase
{
private:
  TinyVector<3,size_t> __degrees;
  TinyVector<3,real_t> __a;
  TinyVector<3,real_t> __b;

  void __instanciable() const {}
public:

  const TinyVector<3,size_t>& degrees() const
  {
    return __degrees;
  }

  const TinyVector<3,real_t>& a() const
  {
    return __a;
  }

  const TinyVector<3,real_t>& b() const
  {
    return __b;
  }

  ScalarDiscretizationTypeLagrange(const TinyVector<3,size_t>& degrees,
				   const TinyVector<3,real_t>& a,
				   const TinyVector<3,real_t>& b)
    : ScalarDiscretizationTypeBase(ScalarDiscretizationTypeBase::spectralLagrange),
      __degrees(degrees),
      __a(a),
      __b(b)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d originale lagrange discretization type
   */
  ScalarDiscretizationTypeLagrange(const ScalarDiscretizationTypeLagrange& d)
    : ScalarDiscretizationTypeBase(d),
      __degrees(d.__degrees),
      __a(d.__a),
      __b(d.__b)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarDiscretizationTypeLagrange()
  {
    ;
  }
};

#endif // SCALAR_DISCRETIZATION_TYPE_LAGRANGE_HPP
