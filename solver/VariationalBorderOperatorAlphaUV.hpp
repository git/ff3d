//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_BORDER_OPERATOR_ALPHA_U_V_HPP
#define VARIATIONAL_BORDER_OPERATOR_ALPHA_U_V_HPP

#include <VariationalBilinearOperator.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalBorderOperatorAlphaUV.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 12:01:07 2006
 * 
 * @brief  represents a term @f$ \int_\Gamma \alpha uv @f$
 * 
 */
class VariationalBorderOperatorAlphaUV
  : public VariationalBilinearBorderOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __Alpha;			/**< Mass term @f$\alpha@f$ */

public:
  /** 
   * Access to @f$\alpha@f$
   * 
   * @return *__Alpha
   */
  const ScalarFunctionBase&
  alpha() const
  {
    return *__Alpha;
  }

  /** 
   * Constructor of the @f$ \int \alpha u v @f$ term
   * 
   * @param unknownNumber the number of the unknown
   * @param testFunctionNumber the number of the test function
   * @param alpha @f$ \alpha @f$
   * @param border the border where to compute the integral
   */
  VariationalBorderOperatorAlphaUV(const size_t& unknownNumber,
				   const size_t& testFunctionNumber,
				   ConstReferenceCounting<ScalarFunctionBase> alpha,
				   ConstReferenceCounting<Boundary> border)
    : VariationalBilinearBorderOperator(VariationalBilinearBorderOperator::alphaUV,
					unknownNumber, testFunctionNumber, border),
      __Alpha(alpha)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V 
   */
  VariationalBorderOperatorAlphaUV(const VariationalBorderOperatorAlphaUV& V)
    : VariationalBilinearBorderOperator(V),
      __Alpha(V.__Alpha)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalBorderOperatorAlphaUV()
  {
    ;
  }
};

#endif // VARIATIONAL_BORDER_OPERATOR_ALPHA_U_V_HPP
