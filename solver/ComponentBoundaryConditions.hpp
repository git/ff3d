//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef COMPONENTBOUNDARYCONDITIONS_HPP
#define COMPONENTBOUNDARYCONDITIONS_HPP

#include <BoundaryCondition.hpp>
#include <vector>

/*!
  \class ComponentBoundaryConditions

  This class is used to describe sets of boundary conditions applied to a
  scalar unknown.

  \author St�phane Del Pino
*/

class ComponentBoundaryConditions
{
private:
  //! The list of Boundary Conditions.
  std::vector<const BoundaryCondition*> bclist;

public:

  //! Add a boundary condition to the list.
  void AddBoundaryCondition(const BoundaryCondition& BC)
  {
    bclist.push_back(&BC);
  }


  //! read only access to the number of boundary condition.
  const size_t NbBoundaryCondition() const
  {
    return bclist.size();
  }

  //! returns the ith boundary condition.
  const BoundaryCondition& operator[] (const size_t i) const
  {
    ASSERT(i<bclist.size());
    return *(bclist[i]);
  }

  //! writes the ComponentBoundaryConditions.
  friend std::ostream& operator << (std::ostream& os,
				    const ComponentBoundaryConditions& CBC)
  {
    for(size_t i=0; i<CBC.bclist.size(); ++i)
      os << *(CBC.bclist[i]) << '\n';
    return os;
  }

  //! Copy contructor.
  ComponentBoundaryConditions(const ComponentBoundaryConditions& CBC)
    : bclist(CBC.bclist)
  {
    ;
  }

  //! Default constructor is void.
  ComponentBoundaryConditions()
  {
    ;
  }
};

#endif // COMPONENTBOUNDARYCONDITIONS_HPP

