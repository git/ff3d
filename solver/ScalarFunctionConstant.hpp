//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_CONSTANT_HPP
#define SCALAR_FUNCTION_CONSTANT_HPP

#include <Types.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionConstant.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:54:54 2006
 * 
 * @brief  constant function
 * 
 */
class ScalarFunctionConstant
  : public ScalarFunctionBase
{
private:
  const real_t __value;		/**< function value */

protected:
  /** 
   * Writes the function to a stream
   * 
   * @param os the output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << __value;
    return os;
  }

public:
  /** 
   * Evaluates the function at position @f$ X @f$
   * 
   * @return __value
   */
  real_t operator()(const TinyVector<3, real_t>& X) const
  {
    return __value;
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return true
   */
  bool canBeSimplified() const
  {
    return true;
  }

  /** 
   * Constructor
   * 
   * @param value constant value
   */
  ScalarFunctionConstant(const real_t& value)
    : ScalarFunctionBase(ScalarFunctionBase::constant),
      __value(value)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionConstant()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_CONSTANT_HPP
