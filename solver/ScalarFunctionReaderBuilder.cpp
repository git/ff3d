//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ScalarFunctionReaderBuilder.hpp>

#include <ScalarFunctionReaderBase.hpp>

#include <ScalarFunctionReaderMedit.hpp>
#include <ScalarFunctionReaderRaw.hpp>
#include <ScalarFunctionReaderVTK.hpp>

#include <Mesh.hpp>

void
ScalarFunctionReaderBuilder::
setFunctionName(const std::string& functionName)
{
  __functionName = functionName;
}

void
ScalarFunctionReaderBuilder::
setComponent(const size_t& component)
{
  __componentNumber = component;
}


ReferenceCounting<ScalarFunctionReaderBase>
ScalarFunctionReaderBuilder::
getReader() const
{
  if (__componentNumber<0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot read negative component number: "+stringify(__componentNumber),
		       ErrorHandler::normal);
  }

  switch (__fileDescriptor.format()) {
  case FileDescriptor::medit: {
    return new ScalarFunctionReaderMedit(__filename, __mesh, __functionName, __componentNumber);
  }
  case FileDescriptor::raw: {
    return new ScalarFunctionReaderRaw(__filename, __mesh, __functionName, __componentNumber);
  }
  case FileDescriptor::vtk: {
    return new ScalarFunctionReaderVTK(__filename, __mesh, __functionName, __componentNumber);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Cannot read function in format "+__fileDescriptor.formatName(),
		       ErrorHandler::normal);
    return 0;
  }
  }
}

ScalarFunctionReaderBuilder::
ScalarFunctionReaderBuilder(const std::string& filename,
			    ConstReferenceCounting<Mesh> mesh,
			    const FileDescriptor& fileDescriptor)
  : __filename(filename),
    __mesh(mesh),
    __fileDescriptor(fileDescriptor),
    __componentNumber(0)
{
  ;
}

ScalarFunctionReaderBuilder::
ScalarFunctionReaderBuilder(const ScalarFunctionReaderBuilder& builder)
  : __filename(builder.__filename),
    __mesh(builder.__mesh),
    __fileDescriptor(builder.__fileDescriptor),
    __functionName(builder.__functionName),
    __componentNumber(builder.__componentNumber)
{
  ;
}

ScalarFunctionReaderBuilder::
~ScalarFunctionReaderBuilder()
{
  ;
}
