//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef KRYLOV_SOLVER_OPTIONS_HPP
#define KRYLOV_SOLVER_OPTIONS_HPP

#include <ParametrizableObject.hpp>

class KrylovSolverOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  enum Type {
    conjugateGradient,
    biConjugateGradient,
    biConjugateGradientStabilized,
    fgmres,
    gmres
  };

  enum PreconditionerType {
    diagonal,
    incompleteCholeski,
    multiGrid,
    spectralFEM,
    none
  };

  Type type()
  {
    KrylovSolverOptions::Type t = conjugateGradient;
    get("type", t);
    return t;
  }

  PreconditionerType precond()
  {
    KrylovSolverOptions::PreconditionerType t = none;
    get("precond", t);
    return t;
  }

  static const char * identifier()
  {
    // autodoc: "used to modify krylov solver"
    return "krylov";
  }

  explicit KrylovSolverOptions()
  {
    // autodoc:"is used to select the type of solver"
    EnumParameter<Type>* E1
      = new EnumParameter<Type>(KrylovSolverOptions::
				conjugateGradient,"type");

    // autodoc: "selects the conjugate gradient"
    (*E1).addSwitch("cg",
		   KrylovSolverOptions::conjugateGradient);
    // autodoc: "selects the bi-conjugate gradient (for non symetric problems)"
    (*E1).addSwitch("bicg",
		   KrylovSolverOptions::biConjugateGradient);
    // autodoc: "selects the bi-conjugate gradient stabilized (for non symetric problems)"
    (*E1).addSwitch("bicgstab",
		   KrylovSolverOptions::biConjugateGradientStabilized);
    // autodoc: "selects the flexible general minimum residual method (for non symetric problems)"
    (*E1).addSwitch("fgmres",
		   KrylovSolverOptions::fgmres);
    // autodoc: "selects the general minimum residual method (for non symetric problems)"
    (*E1).addSwitch("gmres",
		   KrylovSolverOptions::gmres);
    add(E1);


    // autodoc: "is used to select the preconditioner"
    EnumParameter<PreconditionerType>* E2
      = new EnumParameter<PreconditionerType>(KrylovSolverOptions::none,
					      "precond");

    // autodoc: "preconditions with the diagonal of the operator"
    (*E2).addSwitch("diagonal",
		   KrylovSolverOptions::diagonal);
    // autodoc: "incomplete choleski factorization"
    (*E2).addSwitch("ichol",
		   KrylovSolverOptions::incompleteCholeski);
    // autodoc: "multigrid finite difference solver. By now, the grid must be $(2^{n_x}+1)\times(2^{n_y}+1)\times(2^{n_z}+1)$."
    (*E2).addSwitch("multigrid",
		   KrylovSolverOptions::multiGrid);
    // autodoc: "preconditions a spectral method by a finite element method"
    (*E2).addSwitch("sfem",
		   KrylovSolverOptions::spectralFEM);
    // autodoc: "no preconditioning"
    (*E2).addSwitch("none",
		   KrylovSolverOptions::none);
    add(E2);
  }

  ~KrylovSolverOptions()
  {
    ;
  }
};

#endif // KRYLOV_SOLVER_OPTIONS_HPP
