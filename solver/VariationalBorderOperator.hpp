//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_BORDER_OPERATOR_HPP
#define VARIATIONAL_BORDER_OPERATOR_HPP

#include <Boundary.hpp>

/**
 * @file   VariationalBorderOperator.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 18:19:22 2002
 * 
 * @brief  Variational Operator living on a border
 * 
 * Variational border operators
 */
class VariationalBorderOperator
{
private:
  const size_t __testFunctionNumber; /**< test function number */

  ConstReferenceCounting<Boundary>
  __boundary;			/**< The boundary where the integral is computed */

public:

  /** 
   * Read-only access to the boundary
   * 
   * @return __boundary
   */
  ConstReferenceCounting<Boundary>
  boundary() const
  {
    return __boundary;
  }

  /** 
   * Returns the test function number
   * 
   * @return __testFunctionNumber
   */
  const size_t& testFunctionNumber() const
  {
    return __testFunctionNumber;
  }

  /** 
   * Copy constructor
   * 
   * @param number number of the test function
   * @param boundary the boundary where the variational border operator is defined
   */
  VariationalBorderOperator(const size_t& number,
			    ConstReferenceCounting<Boundary> boundary)
    : __testFunctionNumber(number),
      __boundary(boundary)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param vo VariationalBorderOperator
   */
  VariationalBorderOperator(const VariationalBorderOperator& vo)
    : __testFunctionNumber(vo.__testFunctionNumber),
      __boundary(vo.__boundary)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalBorderOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_BORDER_OPERATOR_HPP

