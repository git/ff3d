//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Q1HexahedronFiniteElement.hpp>

TinyVector<3, real_t> Q1HexahedronFiniteElement::__massCenter(0.5, 0.5, 0.5);

const size_t
Q1HexahedronFiniteElement::
facesDOF[Hexahedron::NumberOfFaces][Q1HexahedronFiniteElement::numberOfFaceLivingDegreesOfFreedom]
= {{ 0, 1, 2, 3},
   { 0, 1, 4, 5},
   { 1, 2, 5, 6},
   { 2, 3, 6, 7},
   { 0, 3, 4, 7},
   { 4, 5, 6, 7}};

real_t
Q1HexahedronFiniteElement::W(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
  case 0: {
    return (1-x)*(1-y)*(1-z);
  }
  case 1: {
    return x*(1-y)*(1-z);
  }
  case 2: {
    return x*y*(1-z);
  }
  case 3: {
    return (1-x)*y*(1-z);
  }
  case 4: {
    return (1-x)*(1-y)*z;
  }
  case 5: {
    return x*(1-y)*z;
  }
  case 6: {
    return x*y*z;
  }
  case 7: {
    return (1-x)*y*z;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q1HexahedronFiniteElement::dxW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
  case 0: {
    return -(1-y)*(1-z);
  }
  case 1: {
    return (1-y)*(1-z);
  }
  case 2: {
    return y*(1-z);
  }
  case 3: {
    return -y*(1-z);
  }
  case 4: {
    return -(1-y)*z;
  }
  case 5: {
    return (1-y)*z;
  }
  case 6: {
    return y*z;
  }
  case 7: {
    return -y*z;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q1HexahedronFiniteElement::dyW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& z = X[2];

  switch (i) {
  case 0: {
    return (1-x)*-(1-z);
  }
  case 1: {
    return x*-(1-z);
  }
  case 2: {
    return x*(1-z);
  }
  case 3: {
    return (1-x)*(1-z);
  }
  case 4: {
    return (1-x)*-z;
  }
  case 5: {
    return x*-z;
  }
  case 6: {
    return x*z;
  }
  case 7: {
    return (1-x)*z;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t 
Q1HexahedronFiniteElement::dzW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
  case 0: {
    return -(1-x)*(1-y);
  }
  case 1: {
    return -x*(1-y);
  }
  case 2: {
    return -x*y;
  }
  case 3: {
    return -(1-x)*y;
  }
  case 4: {
    return (1-x)*(1-y);
  }
  case 5: {
    return x*(1-y);
  }
  case 6: {
    return x*y;
  }
  case 7: {
    return (1-x)*y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}
