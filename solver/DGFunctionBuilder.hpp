//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DG_FUNCTION_BUILDER_HPP
#define DG_FUNCTION_BUILDER_HPP

#include <ReferenceCounting.hpp>
#include <Vector.hpp>

class ScalarFunctionBase;
class DGFunctionBase;
class Mesh;
class ScalarDiscretizationTypeDG;

/**
 * @file   DGFunctionBuilder.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 18:11:58 2006
 * 
 * @brief Factory for finite element functions
 * 
 */
class DGFunctionBuilder
{
private:
  ReferenceCounting<DGFunctionBase>
  __builtFunction;		/**< the built function */

  /** 
   * Template function that builds the function according to a mesh
   * type and a kind of discretization
   * 
   * @param d discretization type
   * @param mesh specialized type mesh
   */
  template <typename MeshType>
  void __build(const ScalarDiscretizationTypeDG& d,
	       const MeshType* mesh);

public:
  /** 
   * General function to build a function according to a mesh and a
   * discretization type
   * 
   * @param d discretization type
   * @param mesh a mesh
   */
  void build(const ScalarDiscretizationTypeDG& d,
	     const Mesh* mesh);

  /** 
   * Build a function whose values are initialized to a given @f$ f @f$
   * 
   * @param d discretization type
   * @param mesh a mesh
   * @param f given function
   */
  void build(const ScalarDiscretizationTypeDG& d,
	     const Mesh* mesh,
	     const ScalarFunctionBase& f);

  /** 
   * Build a function whose values are initialized to given values
   * 
   * @param d discretization type
   * @param mesh a mesh
   * @param values given values
   * @param outsideValue returned value outside the mesh (necessary
   * for function simplification)
   */
  void build(const ScalarDiscretizationTypeDG& d,
	     const Mesh* mesh,
	     const Vector<real_t>& values,
	     const real_t& outsideValue = 0);

  /** 
   * Access to the built function
   * 
   * @return __builtFunction
   */
  ReferenceCounting<DGFunctionBase> getBuiltDGFunction();

  /** 
   * Read-only access to the built function viewed as a standard
   * scalar function
   * 
   * @return __builtFunction
   */
  ConstReferenceCounting<ScalarFunctionBase> getBuiltScalarFunction() const;

  /** 
   * Constructor
   * 
   */
  DGFunctionBuilder();

  /** 
   * Detructor
   * 
   */
  ~DGFunctionBuilder();
};

#endif // DG_FUNCTION_BUILDER_HPP
