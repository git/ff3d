//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef PENALIZED_FICTITOUS_DOMAIN_HPP
#define PENALIZED_FICTITOUS_DOMAIN_HPP

#include <FictitiousDomainMethod.hpp>

#include <PenalizedFictitousDomainOptions.hpp>
#include <GetParameter.hpp>

/**
 * @file   PenalizedFictitousDomain.hpp
 * @author Stephane Del Pino
 * @date   Mon Nov  3 18:41:41 2003
 * 
 * @brief Fictitious domain like method using penalty for Dirichlet
 * discretization
 * 
 */
class PenalizedFictitousDomain
  : public FictitiousDomainMethod
{
private:
  GetParameter<PenalizedFictitousDomainOptions> __options; /**< options of the method */

  real_t __epsilon;		/**< Penalty coefficient */

  /** 
   * Discretize the boundary conditions using the specific MeshType.
   * 
   * @return boundary conditions discretization
   */
  template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization,
	    typename MeshType>
  ReferenceCounting<BoundaryConditionDiscretization> __discretizeBoundaryConditionsOnMesh();

  /** 
   * Discretize the boundary conditions using the specific MeshType.
   * 
   * @return boundary conditions discretization
   */
  template <ScalarDiscretizationTypeBase::Type TypeOfDiscretization>
  ReferenceCounting<BoundaryConditionDiscretization> __discretizeBoundaryConditions();

  /** 
   * Discretize the boundary conditions
   * 
   * @return boundary conditions discretization
   */
  ReferenceCounting<BoundaryConditionDiscretization> discretizeBoundaryConditions();

public:
  /** 
   * Constructs a PenalizedFictitousDomain discretization using a
   * given mesh and a given degree of freedom set
   * 
   * @param discretizationType the type of discretization
   * @param mesh a given mesh
   * @param dof a given degree of freedom set
   * 
   */
  PenalizedFictitousDomain(const DiscretizationType&  discretizationType,
			   ConstReferenceCounting<Mesh> mesh,
			   const DegreeOfFreedomSet& dof)
    : FictitiousDomainMethod(discretizationType, mesh, dof)
  {
    __epsilon = __options.value().epsilon();
  }

  /**
   * Destructor
   * 
   */
  ~PenalizedFictitousDomain()
  {
    ;
  }
};

#endif // PENALIZED_FICTITOUS_DOMAIN_HPP

