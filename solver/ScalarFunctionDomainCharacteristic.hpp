//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_DOMAIN_CHARACTERISTIC_HPP
#define SCALAR_FUNCTION_DOMAIN_CHARACTERISTIC_HPP

#include <ScalarFunctionBase.hpp>
class Domain;

/**
 * @file   ScalarFunctionDomainCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:09:47 2006
 * 
 * @brief characteristic function of a domain @f$ \mathbf{1}_\Omega @f$
 * 
 */
class ScalarFunctionDomainCharacteristic
  : public ScalarFunctionBase
{
private:
  ConstReferenceCounting<Domain>
  __domain;			/**< @f$ \Omega @f$ */

  /** 
   * Writes a function to a stream
   * 
   * @param os an output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "one({domain})";
    return os;
  }

public:
  /** 
   * Evaluates the function
   * 
   * @param X position of evaluation
   * 
   * @return 1 if @f$ x\in\Omega @f$, 0 elsewhere
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param domain @f$ \Omega @f$
   */
  ScalarFunctionDomainCharacteristic(ConstReferenceCounting<Domain> domain);

  /** 
   * Copy constructor
   * 
   * @param f original function
   */
  ScalarFunctionDomainCharacteristic(const ScalarFunctionDomainCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionDomainCharacteristic();
};

#endif // SCALAR_FUNCTION_DOMAIN_CHARACTERISTIC_HPP
