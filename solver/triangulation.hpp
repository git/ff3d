//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 Pascal Have

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

//  $Id$

#ifndef TRIANGULATION_HPP
#define TRIANGULATION_HPP

#include <cstdlib>
#include <ostream>
#include <vector>
#include <list>
//#include "point.hpp"
#include "Triangle.hpp"
#include "connected_triangle.hpp"

// il faut juste que sizeof(int)>=2
const int HSide[] = {0x100,0x200,0x400}; // pourquoi on peut le mettre dans la class
const int HMask  = 0x00FF; // Mask de lecture d'une partie du tag (dans le cas de stockage 
const int HUMask  = 0xFF00;// condens�e de bool�ens et d'un entier sur le m�me tag : Economie)

class Triangulation {
public:
  typedef real_t PointNumType;
  typedef std::vector<Vertex *> CurveVertex;
 private:
  Vertex __corners[4];
protected:
  typedef std::vector<Vertex> Points;
  typedef std::list<ConnectedTriangle> Triangles;
  Triangles __triangles; //! tableau de triangles.  

  const Points * __points;
public:
  Triangulation() { }
  void setBox(const PointNumType xmin, const PointNumType ymin,
	      const PointNumType xmax, const PointNumType ymax);

  //! Construit la triangulation d'un ensemble de points suivants des contraintes de type bord, trous ou segments
  bool triangulize(Points & points,
		   const CurveVertex & envVertex,
		   const std::vector<CurveVertex> & holes,
		   const std::vector<CurveVertex> & curves);

  void export_mesh(std::ostream &o) const;

  const Triangles & getTriangles() { return __triangles; }

private:
  bool __scanNeigh(const TriangleIndex & start,
		 const unsigned in,
		 Vertex * newPoint,
		 Vertex * &last);
  void __insertPoint(Vertex * newPoint, const TriangleIndex & curTriangle); 

  //! Ins�re un  point dans la triangulation precedente � partir du triangle T
  inline void __findInsert(Vertex & newPoint) {
      __insertPoint(&newPoint,__find_P_in_elt(newPoint));
  }

  //! Cherche le triangle associe a P
  TriangleIndex __find_P_in_elt(const Vertex &P); 
   
  //! Test si un point est dans le cercle circonscrit au triangle
  bool __isInCircle(const Vertex &point, const Triangle &tri) const;

  //! Retourne l'arrete opp au sommet k du triangle num_tri1.
  void __permutation(TriangleIndex num_tri1,const unsigned k);

  //! Elimination compl�te du triangle num_tri.
  void __elimination_tri(TriangleIndex num_tri);

  void __resetTags();

  bool __checkAll() ;

  void __deleteTriangle(const TriangleIndex & tri) {
    for(Triangles::iterator i = __triangles.begin(); i != __triangles.end(); ++i) {
      if (&*i == tri) {
	__triangles.erase(i);
	return;
      }
    }
    ASSERT(false);
    exit(1);
  }

  void __newTriangle(const ConnectedTriangle & tri) {
    __triangles.push_back(tri);
  }

  bool __colorize(const Points & points);

  TriangleIndex __findVertex(const Vertex * const p);

  /*! Peut �tre mettre le bool en template => code optimis� car pas de cas particulier : trait� � la compil */
  bool __checkLine(const CurveVertex &L, const bool closed);
};

#endif /* TRIANGULATION_HPP */
