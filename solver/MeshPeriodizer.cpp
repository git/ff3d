//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <MeshPeriodizer.hpp>
#include <Structured3DMesh.hpp>

template <typename MeshType>
void MeshPeriodizer::__build(const bool& builtLocalizationTools)
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "not implemented",
		     ErrorHandler::unexpected);
}

template <>
void MeshPeriodizer::
__build<Structured3DMesh>(const bool& builtLocalizationTools)
{
  const Structured3DMesh& inputMesh
    = static_cast<const Structured3DMesh&>(*__input);

  bool xperiodicity = false;
  bool yperiodicity = false;
  bool zperiodicity = false;

  for (ReferencesMapping::const_iterator i = __referencesMapping.begin();
       i != __referencesMapping.end(); ++i) {
    const size_t periodicityDescriptor = (*i).first+(*i).second;
    switch(periodicityDescriptor) {
    case 1: { // 0+1
      xperiodicity = true;
      break;
    }
    case 5: { // 2+3
      yperiodicity = true;
      break;
    }
    case 9: { // 4+5
      zperiodicity = true;
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot built periodic mesh using borders "
			 +stringify((*i).first)+" and "
			 +stringify((*i).second),
			 ErrorHandler::normal);
      
    }
    }
  }

  Vector<size_t> correspondances(inputMesh.numberOfVertices());
  for (size_t i=0; i<inputMesh.numberOfVertices(); ++i) {
    correspondances[i] = i;
  }

  const Structured3DMeshShape& shape = inputMesh.shape();

  if (xperiodicity) {
    const size_t nx_1 = shape.nx()-1;
    for (size_t j=0; j<shape.ny(); ++j) {
      for (size_t k=0; k<shape.nz(); ++k) {
	const size_t n0 = shape(0,j,k);
	const size_t n1 = shape(nx_1,j,k);
	correspondances[n1] = correspondances[n0];
      }
    }
  }

  if (yperiodicity) {
    const size_t ny_1 = shape.ny()-1;
    for (size_t i=0; i<shape.nx(); ++i) {
      for (size_t k=0; k<shape.nz(); ++k) {
	const size_t n2 = shape(i,0,k);
	const size_t n3 = shape(i,ny_1,k);
	correspondances[n3] = correspondances[n2];
      }
    }
  }

  if (zperiodicity) {
    const size_t nz_1 = shape.nz()-1;
    for (size_t i=0; i<shape.nx(); ++i) {
      for (size_t j=0; j<shape.ny(); ++j) {
	const size_t n4 = shape(i,j,0);
	const size_t n5 = shape(i,j,nz_1);
	correspondances[n5] = correspondances[n4];
      }
    }
  }

  std::map<size_t, size_t> temp;
  for (size_t i=0; i<inputMesh.numberOfVertices(); ++i) {
    temp[correspondances[i]] = 0;
  }
  size_t nbCorrespondances = 0;
  for (std::map<size_t, size_t>::iterator i = temp.begin();
       i != temp.end(); ++i) {
    i->second = nbCorrespondances++;
  }


  Vector<size_t>* pNewCorrespondances
    = new Vector<size_t>(inputMesh.numberOfVertices());
  Vector<size_t>& newCorrespondances = *pNewCorrespondances;

  for (size_t i=0; i<inputMesh.numberOfVertices(); ++i) {
    newCorrespondances[i] = temp[correspondances[i]];
  }

  VerticesCorrespondance* vc
    = new VerticesCorrespondance(nbCorrespondances,
				 pNewCorrespondances);

  __mesh = new Structured3DMesh(shape,
				vc);
}

void MeshPeriodizer::run(const bool& builtLocalizationTools)
{
  switch ((*__input).type()) {
  case Mesh::cartesianHexahedraMesh: {
    this->__build<Structured3DMesh>(builtLocalizationTools);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented: unexpected mesh type",
		       ErrorHandler::unexpected);
  }
  }
}
