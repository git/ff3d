//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_OPERATOR_F_DX_G_V_HPP
#define VARIATIONAL_OPERATOR_F_DX_G_V_HPP

#include <VariationalLinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorFdxGV.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 12:35:07 2006
 * 
 * @brief  describes the term @f$ \int f\partial_{x_i}g\,v @f$
 * 
 */
class VariationalOperatorFdxGV
  : public VariationalLinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< the @f$ f@f$ function */
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< the @f$ g@f$ function */
  const size_t __i;		/**< The @f$ i @f$ in @f$\partial_{x_i}@f$  */
public:
  /** 
   * Access to @f$ f@f$
   * 
   * @return *__f
   */
  const ScalarFunctionBase& f() const
  {
    return *__f;
  }

  /** 
   * Access to @f$ g@f$
   * 
   * @return *__g
   */
  const ScalarFunctionBase& g() const
  {
    return *__g;
  }

  /** 
   * gets the derivation direction of @f$ g @f$
   * 
   * @return __i
   */
  const size_t& number() const
  {
    return __i;
  }

  /** 
   * "multiplies" the operator by a coefficient @f$ c @f$
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int c\,f\partial_{x_i}g\,v @f$
   */
  ReferenceCounting<VariationalLinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalOperatorFdxGV* newOperator
      = new VariationalOperatorFdxGV(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__f);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    (*newOperator).__f = functionBuilder.getBuiltFunction();
    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param testFunctionNumber the test function number
   * @param testFunctionProperty  test function property
   * @param f the given @f$ f @f$ function
   * @param g the given @f$ g @f$ function
   * @param i the index in @f$ \int f \partial_{x_i} g @f$
   */
  VariationalOperatorFdxGV(const size_t& testFunctionNumber,
			   const VariationalOperator::Property& testFunctionProperty,
			   ConstReferenceCounting<ScalarFunctionBase> f,
			   ConstReferenceCounting<ScalarFunctionBase> g,
			   const size_t& i)
    : VariationalLinearOperator(VariationalLinearOperator::FdxGV,
				testFunctionNumber, testFunctionProperty),
      __f(f),
      __g(g),
      __i(i)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V another VariationalOperatorFdxGV
   */
  VariationalOperatorFdxGV(const VariationalOperatorFdxGV& V)
    : VariationalLinearOperator(V),
      __f(V.__f),
      __g(V.__g),
      __i(V.__i)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalOperatorFdxGV()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_F_DX_G_V_HPP
