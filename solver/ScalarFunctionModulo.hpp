//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_MODULO_HPP
#define SCALAR_FUNCTION_MODULO_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionModulo.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 11:33:47 2006
 * 
 * @brief returns the modulo of two operands @f$ f\% g @f$
 * 
 */
class ScalarFunctionModulo
  : public ScalarFunctionBase
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< first operand @f$ f @f$ */
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< second operand @f$ g @f$ */

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << '(' << *__f << '%' << *__g << ')';
    return os;
  }

public:
  /** 
   * Evaluates the function at position @f$ X @f$
   * 
   * @param X position of evaluation
   * 
   * @return @f$ E(f(X)) \bmod E(g(X)) @f$
   */
  real_t operator()(const TinyVector<3,real_t>& X) const
  {
    return static_cast<int>((*__f)(X))%static_cast<int>((*__g)(X));
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param f first operand @f$ f @f$
   * @param g second operand @f$ g @f$
   */
  ScalarFunctionModulo(ConstReferenceCounting<ScalarFunctionBase> f,
		       ConstReferenceCounting<ScalarFunctionBase> g)
    : ScalarFunctionBase(ScalarFunctionBase::modulo),
      __f(f),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f given function
   */
  ScalarFunctionModulo(const ScalarFunctionModulo& f)
    : ScalarFunctionBase(f),
      __f(f.__f),
      __g(f.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionModulo()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_MODULO_HPP
