//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <BoundaryConditionDiscretizationSpectralLegendreConform.hpp>
#include <ErrorHandler.hpp>

#include <VariationalProblem.hpp>
#include <DegreeOfFreedomSet.hpp>
#include <VariationalBorderOperatorAlphaUV.hpp>
#include <VariationalBorderOperatorFV.hpp>
#include <VariationalLinearOperator.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <Discretization.hpp>
#include <ScalarDiscretizationTypeLegendre.hpp>

#include <BoundaryReferences.hpp>

#include <UnAssembledMatrix.hpp>

#include <DoubleHashedMatrix.hpp>


void BoundaryConditionDiscretizationSpectralLegendreConform::
getDiagonal(BaseVector& X) const
{
  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(X);
  
  switch(this->problem().type()) {
  case Problem::variational: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::bilinearBorderOperatorConst_iterator
	   iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	 iBorderOperator != variationalProblem.endBilinearBorderOperator();
	 ++iBorderOperator) {
      
      const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
      switch (borderOperator.type()) {
      case VariationalBilinearBorderOperator::alphaUV: {
	const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	  = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();
	
	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = 
	    dynamic_cast<const BoundaryReferences&>(*boundary);
	  
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	    const size_t boundaryNumber = *iBoundary;  
	 	    
	    const size_t& numberOfUnknown = this->problem().numberOfUnknown();
	    
	    Vector<TinyVector<3,Vector<Vector<real_t> > > >  quadratureBaseValuesU(numberOfUnknown);
	    TinyVector<3,size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }
	    
	    for (size_t unknownNumber =0; unknownNumber < numberOfUnknown;++ unknownNumber){
	      
	      const	TinyVector<3, ConstReferenceCounting<LegendreBasis> >& unknownBasis
		= __basisU[unknownNumber];
	      
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValuesU[unknownNumber][i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  
		  real_t xj=(*__transformU[unknownNumber][direction[i]]).inverse((*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j)));
		  
		  quadratureBaseValuesU[unknownNumber][i][j].resize(unknownBasis[direction[i]]->dimension());
		  
		  unknownBasis[direction[i]]->getValues(xj,quadratureBaseValuesU[unknownNumber][i][j]);
		}
	      }
	      
	      quadratureBaseValuesU[unknownNumber][2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1. + 2.*j;
		quadratureBaseValuesU[unknownNumber][2][j].resize(unknownBasis[direction[2]]->dimension());
		unknownBasis[direction[2]]->getValues(xj,quadratureBaseValuesU[unknownNumber][2][j]);
	      }
	    }
	    
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
	    const real_t jacobianDet = __transform[direction[0]]->determinant()*
	      __transform[direction[1]]->determinant();
	       
	    Vector<Vector<real_t> > alpha_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      alpha_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s< __gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		alpha_rs[r][s]=0;
	      }
	    }
	    
	    for (size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      const real_t& x = nodes[0][r];
	      for (size_t s=0; s <__gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
		const real_t& y = nodes[1][s];
				
		switch(boundaryNumber) {
		case 0: {
		  alpha_rs[r][s] = alpha(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  alpha_rs[r][s] = alpha(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().b(2));
		  break;
		}
		}
	      }
	    }
	    
	   
	    const TinyVector<3,size_t>& dofShape = __dofShape[operatorAlphaUV.unknownNumber()];
	    
	    TinyVector<3,size_t> I;
	    
	    for(size_t unknownNumber=0;unknownNumber< numberOfUnknown; unknownNumber++) {
	    
	      const  TinyVector<3, ConstReferenceCounting<LegendreBasis> >& unknownBasis
		= __basisU[unknownNumber];
	      
	      for(size_t m=0; m< unknownBasis[direction[2]]->dimension(); ++m){
		I[direction[2]]=m;
		for(size_t n=0; n< unknownBasis[direction[0]]->dimension(); ++n){
		  I[direction[0]]=n;
		  for(size_t p=0; p<unknownBasis[direction[1]]->dimension(); ++p){
		    I[direction[1]]=p;
		    size_t dofNumber  = I[0]*dofShape[1]*dofShape[2] + I[1]*dofShape[2] + I[2];
		    
		    for(size_t r=0; r< __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
		      for(size_t s=0; s < __gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
			v[__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),dofNumber)]
			  += quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][2][boundaryNumber%2][m]
			  *quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][2][boundaryNumber%2][m]
			  *quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][0][n]
			  *quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][0][n]
			  *quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][1][p]
			  *quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][1][p]
			  *__gaussLobatto[direction[0]]->weight(r)
			  *__gaussLobatto[direction[1]]->weight(s)
			  *alpha_rs[r][s]*jacobianDet;
		      }
		    }
		  }
		}
	      }
	    }
	  }

	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);	
      }
      }
    }
    break;
  }
  case Problem::pde: {
    break; 
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}


void BoundaryConditionDiscretizationSpectralLegendreConform::
setMatrix(ReferenceCounting<BaseMatrix> givenA,
	  ReferenceCounting<BaseVector> b) const
{
  switch((*givenA).type()) {
  case BaseMatrix::doubleHashedMatrix: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);	
    break;
  }
  case BaseMatrix::unAssembled: {
    UnAssembledMatrix& A = dynamic_cast<UnAssembledMatrix&>(*givenA);
    A.setBoundaryConditions(this);
    break;
  }

  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexected matrix type",
		       ErrorHandler::unexpected);
  }
  }
}


void BoundaryConditionDiscretizationSpectralLegendreConform::
setSecondMember(ReferenceCounting<BaseMatrix> givenA,
		ReferenceCounting<BaseVector> givenb) const
{
  ffout(2)<<"- assembling second membre-boundary\n";
  Vector<real_t>& b= (static_cast<Vector<real_t>&>(*givenb));
  
  switch(this->problem().type()) {
  case Problem::variational: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::linearBorderOperatorConst_iterator
	   iLinearBorderOperator = variationalProblem.beginLinearBorderOperator();
	 iLinearBorderOperator != variationalProblem.endLinearBorderOperator();
	 ++iLinearBorderOperator) {
      
      const VariationalLinearBorderOperator& linearBorderOperator = **iLinearBorderOperator;
      switch (linearBorderOperator.type()) {
      case VariationalLinearBorderOperator::FV: {
	const VariationalBorderOperatorFV& operatorFV
	  = dynamic_cast<const VariationalBorderOperatorFV&>(linearBorderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorFV.boundary();
	
	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = 
	    dynamic_cast<const BoundaryReferences&>(*boundary);
	  
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& g = operatorFV.f();
	    const size_t boundaryNumber = *iBoundary;  
	    
	    const size_t& numberOfUnknown = this->problem().numberOfUnknown();
	    
	    TinyVector<3, size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }
	    
	    Vector<TinyVector<3,Vector<Vector<real_t> > > >  quadratureBaseValues(numberOfUnknown);
	    for (size_t testFunctionNumber =0; testFunctionNumber < numberOfUnknown; ++ testFunctionNumber){
	      const  TinyVector<3, ConstReferenceCounting<LegendreBasis> >& testBasis
		= __basisU[testFunctionNumber];		
	      
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValues[testFunctionNumber][i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		
		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  real_t xj= (*__transformU[testFunctionNumber][direction[i]]).inverse((*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j)));
		  
		  quadratureBaseValues[testFunctionNumber][i][j].resize(testBasis[direction[i]]->dimension());
		  testBasis[direction[i]]->getValues(xj,quadratureBaseValues[testFunctionNumber][i][j]);
		}
	      }
	      
	      quadratureBaseValues[testFunctionNumber][2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1.+2.*j;
		quadratureBaseValues[testFunctionNumber][2][j].resize(testBasis[direction[2]]->dimension());
		testBasis[direction[2]]->getValues(xj,quadratureBaseValues[testFunctionNumber][2][j]);
	      }
	    }

	    // nodes construction	    
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j< (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
	    
	    const real_t jacobianDet = (*__transform[direction[0]]).determinant()*
	       (*__transform[direction[1]]).determinant();
	    
	    for (size_t i=0; i < (*__gaussLobatto[direction[0]]).numberOfPoints(); ++i) {
	      const real_t& x = nodes[0][i];
	      const real_t& wi = (*__gaussLobatto[direction[0]]).weight(i)
		;
	      
	      for (size_t j=0; j < (*__gaussLobatto[direction[1]]).numberOfPoints(); ++j) {
		const real_t& y = nodes[1][j];
		const real_t& wij = (*__gaussLobatto[direction[1]]).weight(j)*wi
		  ;
		
		real_t gValue = wij* jacobianDet;
		switch(boundaryNumber) {
		case 0: {
		  gValue *= g(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  gValue *= g(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  gValue *= g(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  gValue *= g(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  gValue *= g(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  gValue *= g(x, y, __mesh.shape().b(2));
		  break;
		}
		default: {
		  throw ErrorHandler(__FILE__,__LINE__,
				     "not implemented",
				     ErrorHandler::unexpected);
		}
		}
		
		const	TinyVector<3, ConstReferenceCounting<LegendreBasis> >& testBasis
		  = __basisU[operatorFV.testFunctionNumber()];
		
		const TinyVector<3,size_t>& dofShape = 
		  __dofShape[operatorFV.testFunctionNumber()];
		
		TinyVector<3,size_t> J;
		for (size_t ml=0; ml < testBasis[direction[0]]->dimension(); ++ml){
		  J[direction[0]] = ml;
		  for (size_t pl=0; pl < testBasis[direction[1]]->dimension(); ++pl){
		    J[direction[1]] = pl;
		    for (size_t ql=0; ql < testBasis[direction[2]]->dimension(); ++ql){
		      J[direction[2]] = ql;
		      const  size_t l =
			J[0]*dofShape[1]*dofShape[2] + J[1]*dofShape[2] + J[2];
		      b[__degreeOfFreedomSet(operatorFV.testFunctionNumber(),l)] 
			
			+=quadratureBaseValues[operatorFV.testFunctionNumber()][0][i][ml]
			* quadratureBaseValues[operatorFV.testFunctionNumber()][1][j][pl]
			* quadratureBaseValues[operatorFV.testFunctionNumber()][2][boundaryNumber%2][ql]
			*gValue
			;		
		    }
		  }
		}
	      }
	    }
	  }
	 
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);
	}
	  
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);
      }
      }
    }
    
    break;
  }
    
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}


void BoundaryConditionDiscretizationSpectralLegendreConform::
timesX(const BaseVector& X, BaseVector& Z) const
{
  const Vector<real_t>& u = dynamic_cast<const Vector<real_t>&>(X);
  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(Z);
  
  switch(this->problem().type()) {
  case Problem::variational: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::bilinearBorderOperatorConst_iterator
	   iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	 iBorderOperator != variationalProblem.endBilinearBorderOperator();
	 ++iBorderOperator) {
      
      const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
      switch (borderOperator.type()) {
      case VariationalBilinearBorderOperator::alphaUV: {
	
	const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	  = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();
	
	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = dynamic_cast<const BoundaryReferences&>(*boundary);
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	    const size_t boundaryNumber = *iBoundary;  
	    
	    const size_t& numberOfUnknown = this->problem().numberOfUnknown();
	    
	    Vector<TinyVector<3,Vector<Vector<real_t> > > >  quadratureBaseValuesU(numberOfUnknown);
	    TinyVector<3,size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }
	    
	    for (size_t unknownNumber =0; unknownNumber < numberOfUnknown;++ unknownNumber){
	      
	      const	TinyVector<3, ConstReferenceCounting<LegendreBasis> >& unknownBasis
		= __basisU[unknownNumber];
	      
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValuesU[unknownNumber][i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  
		  real_t xj=(*__transformU[unknownNumber][direction[i]]).inverse((*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j)));
		  
		  quadratureBaseValuesU[unknownNumber][i][j].resize(unknownBasis[direction[i]]->dimension());
		  
		  unknownBasis[direction[i]]->getValues(xj,quadratureBaseValuesU[unknownNumber][i][j]);
		}
	      }
	      
	      quadratureBaseValuesU[unknownNumber][2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1. + 2.*j;
		quadratureBaseValuesU[unknownNumber][2][j].resize(unknownBasis[direction[2]]->dimension());
		unknownBasis[direction[2]]->getValues(xj,quadratureBaseValuesU[unknownNumber][2][j]);
	      }
	    }
	    
	    // nodes construction
	    
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
	    const real_t jacobianDet = __transform[direction[0]]->determinant()*
	      __transform[direction[1]]->determinant();
	    
	    const size_t unknownNumber= operatorAlphaUV.unknownNumber();
	    const	TinyVector<3, ConstReferenceCounting<LegendreBasis> >&unknownBasis
	      = __basisU[unknownNumber];
	    
	    Vector<Vector<real_t> >  u_jk(unknownBasis[direction[0]]->dimension());
	    for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
	      u_jk[j].resize(unknownBasis[direction[1]]->dimension());
	      for(size_t k=0; k< unknownBasis[direction[1]]->dimension(); ++k){
		u_jk[j][k]=0;
	      }
	    }
	    
	    {
	      const TinyVector<3,size_t>& dofShapeU = __dofShape[unknownNumber];	      
	      TinyVector<3,size_t> J;
	      for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
		J[direction[0]]=j;
		for(size_t k=0; k< unknownBasis[direction[1]]->dimension(); ++k){
		  J[direction[1]]=k;
		  real_t& u_jk_value = u_jk[j][k];
		  for(size_t i=0; i< unknownBasis[direction[2]]->dimension(); ++i){
		    J[direction[2]]=i;
		    const size_t dofNumberU  =
		      J[0]*dofShapeU[1]*dofShapeU[2] + J[1] *dofShapeU[2] +J[2];
		    u_jk_value
		      += quadratureBaseValuesU[unknownNumber][2][boundaryNumber%2][i]
		      *u[__degreeOfFreedomSet(operatorAlphaUV.unknownNumber(),dofNumberU)];
		  }
		}
	      }
	    }
	    
	    Vector<Vector<real_t> >  u_js(unknownBasis[direction[0]]->dimension());
	    for(size_t j=0; j<unknownBasis[direction[0]]->dimension(); ++j){
	      u_js[j].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		u_js[j][s]=0;
	      }
	    }
	    
	    for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
	      for(size_t s=0; s< __gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_js_value = u_js[j][s];
		for(size_t k=0; k< unknownBasis[direction[1]]->dimension(); ++k){
		  u_js_value
		    += quadratureBaseValuesU[unknownNumber][1][s][k]* u_jk[j][k];
		}
	      }
	    }
	    
	    Vector<Vector<real_t> >  u_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      u_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		u_rs[r][s]=0;
	      }
	    }
	    
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_rs_value = u_rs[r][s];
		for(size_t j=0; j<unknownBasis[direction[0]]->dimension(); ++j){
		  u_rs_value
		    += quadratureBaseValuesU[unknownNumber][0][r][j]*u_js[j][s];
		}
	      }
	    }
	    
	    
	    Vector<Vector<real_t> > alpha_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      alpha_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s< __gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		alpha_rs[r][s]=0;
	      }
	    }
	    
	    for (size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      const real_t& x = nodes[0][r];
	      for (size_t s=0; s <__gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
		const real_t& y = nodes[1][s];
		

		switch(boundaryNumber) {
		case 0: {
		  alpha_rs[r][s] = alpha(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  alpha_rs[r][s] = alpha(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().b(2));
		  break;
		}
		}
	      }
	    }
	    
	    const size_t testFunctionNumber= operatorAlphaUV.testFunctionNumber();
	    const TinyVector<3, ConstReferenceCounting<LegendreBasis> >&testBasis
	      = __basisU[testFunctionNumber];
	    Vector<Vector<real_t> >  v_rp(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      v_rp[r].resize(testBasis[direction[1]]->dimension());
	      for(size_t p=0; p<testBasis[direction[1]]->dimension(); ++p){
		v_rp[r][p]=0;
	      }
	    }
	    
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t p=0; p<testBasis[direction[1]]->dimension(); ++p){
		real_t& v_rp_value = v_rp[r][p];
		for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		  v_rp_value
		    += quadratureBaseValuesU[testFunctionNumber][1][s][p]
		    *u_rs[r][s]
		    *__gaussLobatto[direction[1]]->weight(s)
		    * alpha_rs[r][s]*jacobianDet;
		}
	      }
	    }
	    
	    Vector<Vector<real_t> >  v_np(testBasis[direction[0]]->dimension());
	    for(size_t n=0; n<testBasis[direction[0]]->dimension(); ++n){
	      v_np[n].resize(testBasis[direction[1]]->dimension());
	      for(size_t p=0; p<testBasis[direction[1]]->dimension(); ++p){
		v_np[n][p]=0;
	      }
	    }
	    
	    for(size_t n=0; n<testBasis[direction[0]]->dimension(); ++n){
	      for(size_t p=0; p<testBasis[direction[1]]->dimension(); ++p){
		real_t& v_np_value = v_np[n][p];
		for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
		  v_np_value
		    += quadratureBaseValuesU[testFunctionNumber][0][r][n]
		    *v_rp[r][p]*
		    __gaussLobatto[direction[0]]->weight(r);
		}
	      }
	    }
	    
	    const TinyVector<3,size_t>& dofShapeV = __dofShape[testFunctionNumber];
	    TinyVector<3,size_t> I;
	    
	    for(size_t n=0; n<testBasis[direction[0]]->dimension(); ++n){
	      I[direction[0]]=n;
	      for(size_t p=0; p<testBasis[direction[1]]->dimension(); ++p){
		I[direction[1]]=p;
		for(size_t m=0; m<testBasis[direction[2]]->dimension(); ++m){
		  I[direction[2]]=m;		
		  size_t dofNumberV =
		    I[0]*dofShapeV[1]*dofShapeV[2] + I[1]*dofShapeV[2] +I[2];
		  
		  v[__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),dofNumberV)]
		    += quadratureBaseValuesU[testFunctionNumber][2][boundaryNumber%2][m]
		    *v_np[n][p];
		}
	      }
	    }
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);	
      }
      }
    }
    break;
  }
  case Problem::pde: {
    break; 
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}

void BoundaryConditionDiscretizationSpectralLegendreConform::
transposedTimesX(const BaseVector& X, BaseVector& Z) const
{
  const Vector<real_t>& u = dynamic_cast<const Vector<real_t>&>(X);
  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(Z);
  
  switch(this->problem().type()) {
  case Problem::variational: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::bilinearBorderOperatorConst_iterator
	   iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	 iBorderOperator != variationalProblem.endBilinearBorderOperator();
	 ++iBorderOperator) {
      
      const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
      switch (borderOperator.type()) {
      case VariationalBilinearBorderOperator::alphaUV: {
	
	const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	  = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();
	
	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = dynamic_cast<const BoundaryReferences&>(*boundary);
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	    const size_t boundaryNumber = *iBoundary;  
	    
	    const size_t& numberOfUnknown = this->problem().numberOfUnknown();
	   
	    Vector<Vector<Vector<real_t> > > u_jk(numberOfUnknown);
	    Vector<Vector<Vector<real_t> > > u_js(numberOfUnknown);
	    Vector<Vector<Vector<real_t> > > u_rs(numberOfUnknown);
	    Vector<Vector<Vector<real_t> > > v_rp(numberOfUnknown);
	    Vector<Vector<Vector<real_t> > > v_np(numberOfUnknown);	  
	  
	    TinyVector<3, size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }  
	    
	    Vector<TinyVector<3,Vector<Vector<real_t> > > >  quadratureBaseValuesU(numberOfUnknown);
	    for (size_t unknownNumber =0; unknownNumber < numberOfUnknown; ++unknownNumber){
	      
	      const	TinyVector<3, ConstReferenceCounting<LegendreBasis> >&unknownBasis
		= __basisU[unknownNumber];
	      	      
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValuesU[unknownNumber][i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());

		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  
		  real_t xj=(*__transformU[unknownNumber][direction[i]]).inverse((*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j)));
		  
		  quadratureBaseValuesU[unknownNumber][i][j].resize(unknownBasis[direction[i]]->dimension());
		  unknownBasis[direction[i]]->getValues(xj,quadratureBaseValuesU[unknownNumber][i][j]);
		}
	      }
	      
	      quadratureBaseValuesU[unknownNumber][2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1. + 2.*j;
		quadratureBaseValuesU[unknownNumber][2][j].resize(unknownBasis[direction[2]]->dimension());
		unknownBasis[direction[2]]->getValues(xj,quadratureBaseValuesU[unknownNumber][2][j]);
	      }
	    }
	    
	    //construction des nodes
	    
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
	    const real_t jacobianDet = __transform[direction[0]]->determinant()*
	      __transform[direction[1]]->determinant();
	    
	    //const size_t unknownNumber= operatorAlphaUV.unknownNumber();
	    const TinyVector<3, ConstReferenceCounting<LegendreBasis> >&unknownBasis
	      = __basisU[operatorAlphaUV.unknownNumber()];
	    
	    u_jk[operatorAlphaUV.unknownNumber()].resize(unknownBasis[direction[0]]->dimension());
	    for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
	      u_jk[operatorAlphaUV.unknownNumber()][j].resize( unknownBasis[direction[1]]->dimension());
	      for(size_t k=0; k < unknownBasis[direction[1]]->dimension(); ++k){
		u_jk[operatorAlphaUV.unknownNumber()][j][k]=0;
	      }
	    }
	    
	    {
	      const TinyVector<3,size_t>& dofShape = __dofShape[operatorAlphaUV.unknownNumber()];
	      TinyVector<3,size_t> J;
	      for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
		J[direction[0]]=j;
		for(size_t k=0;  k < unknownBasis[direction[1]]->dimension(); ++k){
		  J[direction[1]]=k;
		  real_t& u_jk_value = u_jk[operatorAlphaUV.unknownNumber()][j][k];
		  for(size_t i=0; i < unknownBasis[direction[2]]->dimension(); ++i){
		    J[direction[2]]=i;
		    const size_t dofNumberU = 
		      J[0] *dofShape[1]*dofShape[2] + J[1] *dofShape[2] +J[2] ;
		    u_jk_value
		      += quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][2][boundaryNumber%2][i]
		      *u[__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),dofNumberU)];
		  }
		}
	      }
	    }
	    
	    u_js[operatorAlphaUV.unknownNumber()].resize(unknownBasis[direction[0]]->dimension());
	    for(size_t j=0; j< unknownBasis[direction[0]]->dimension(); ++j){
	      u_js[operatorAlphaUV.unknownNumber()][j].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		u_js[operatorAlphaUV.unknownNumber()][j][s]=0;
	      }
	    }
	    
	    for(size_t j=0; j < unknownBasis[direction[0]]->dimension(); ++j){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_js_value = u_js[operatorAlphaUV.unknownNumber()][j][s];
		for(size_t k=0; k < unknownBasis[direction[1]]->dimension(); ++k){
		  u_js_value
		    += quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][1][s][k]
		    * u_jk[operatorAlphaUV.unknownNumber()][j][k];
		}
	      }
	    }
	    u_rs[operatorAlphaUV.unknownNumber()].resize(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      u_rs[operatorAlphaUV.unknownNumber()][r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		u_rs[operatorAlphaUV.unknownNumber()][r][s]=0;
	      }
	    }
	    
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_rs_value = u_rs[operatorAlphaUV.unknownNumber()][r][s];
		for(size_t j=0; j < unknownBasis[direction[0]]->dimension(); ++j){
		  u_rs_value
		    += quadratureBaseValuesU[operatorAlphaUV.unknownNumber()][0][r][j]
		    *u_js[operatorAlphaUV.unknownNumber()][j][s];
		}
	      }
	    }
	    
	    Vector<Vector<real_t> > alpha_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      alpha_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		alpha_rs[r][s]=0;
	      }
	    }
	    
	    for (size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      const real_t& x = nodes[0][r];
	      for (size_t s=0; s <__gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
		const real_t& y = nodes[1][s];
		
		switch(boundaryNumber) {
		case 0: {
		  alpha_rs[r][s] = alpha(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  alpha_rs[r][s] = alpha(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().b(2));
		  break;
		}
		}
	      }
	    }
	    
	    const TinyVector<3, ConstReferenceCounting<LegendreBasis> >&testBasis
	      = __basisU[operatorAlphaUV.testFunctionNumber()];
	    
	    v_rp[operatorAlphaUV.testFunctionNumber()].resize(__gaussLobatto[direction[0]]->numberOfPoints());
	    
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      v_rp[operatorAlphaUV.testFunctionNumber()][r].resize(testBasis[direction[1]]->dimension());
	      for(size_t p=0; p < testBasis[direction[1]]->dimension(); ++p){
		v_rp[operatorAlphaUV.testFunctionNumber()][r][p]=0;
	      }
	    }
	    
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t p=0; p < testBasis[direction[1]]->dimension(); ++p){
		real_t& v_rp_value = v_rp[operatorAlphaUV.testFunctionNumber()][r][p];
		for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		  v_rp_value
		    += quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][1][s][p]
		    *u_rs[operatorAlphaUV.unknownNumber()][r][s]
		    *__gaussLobatto[direction[1]]->weight(s)
		    * alpha_rs[r][s]*jacobianDet;
		}
	      }
	    }
	    
	    v_np[operatorAlphaUV.testFunctionNumber()].resize(testBasis[direction[0]]->dimension());	
	    for(size_t n=0; n < testBasis[direction[0]]->dimension(); ++n){
	      v_np[operatorAlphaUV.testFunctionNumber()][n].resize(testBasis[direction[1]]->dimension());
	      for(size_t p=0; p < testBasis[direction[1]]->dimension(); ++p){
		v_np[operatorAlphaUV.testFunctionNumber()][n][p]=0;
	      }
	    }
	    
	    for(size_t n=0; n < testBasis[direction[0]]->dimension(); ++n){
	      for(size_t p=0; p < testBasis[direction[1]]->dimension(); ++p){
		real_t& v_np_value = v_np[operatorAlphaUV.testFunctionNumber()][n][p];
		for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r){
		  v_np_value
		    += quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][0][r][n]
		    *v_rp[operatorAlphaUV.testFunctionNumber()][r][p]*
		    __gaussLobatto[direction[0]]->weight(r);
		}
	      }
	    }
	    
	    const TinyVector<3,size_t>& dofShapeV = __dofShape[operatorAlphaUV.testFunctionNumber()];
	    TinyVector<3,size_t> I;	    
	   
	    for(size_t n=0; n < testBasis[direction[0]]->dimension(); ++n){
	      I[direction[0]]=n;
	      for(size_t p=0; p < testBasis[direction[1]]->dimension(); ++p){
		I[direction[1]]=p;
		for(size_t m=0; m < testBasis[direction[2]]->dimension(); ++m){
		  I[direction[2]]=m; 
		  const size_t dofNumberV = 
		    I[0]* dofShapeV[1]*dofShapeV[2] +  I[1]*dofShapeV[2] + I[2]; 
		  
		  v[__degreeOfFreedomSet(operatorAlphaUV.unknownNumber(),dofNumberV)]
		    += quadratureBaseValuesU[operatorAlphaUV.testFunctionNumber()][2][boundaryNumber%2][m]
		    *v_np[operatorAlphaUV.testFunctionNumber()][n][p];
		}
	      }
	    }
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);	
      }
      }
    }
    break;
  }
  case Problem::pde: {
    break; 
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}






