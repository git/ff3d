//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SPECTRAL_CONFORM_TRANSFORMATION_HPP
#define SPECTRAL_CONFORM_TRANSFORMATION_HPP

#include <Types.hpp>
#include <Interval.hpp>

/**
 * @file   SpectralConformTransformation.hpp
 * @author Dris Yakoubi
 * @date   Sun Apr 29 20:56:03 2007
 * 
 * @brief  1D affine transformation @f$ T : (-1,1) \mapsto (a,b)@f$
 * 
 */
class SpectralConformTransformation
{
private:
  const Interval __interval;	/**< interval @f$(a,b)@f$ */
 
public:
  /** 
   * Computes @f$ T(x) @f$
   * 
   * @return @f$ T(x) @f$
   */
  real_t operator()(const real_t& x) const;

  /** 
   * Computes @f$ T^{-1}(x) @f$
   * 
   * @return @f$ T^{-1}(x) @f$
   */
  real_t inverse(const real_t& x) const;

  /** 
   * Computes the determinent of the Jacobian of the inverse
   * transformation
   * 
   * @return @f$ \frac{2}{b-a} @f$
   */
  real_t inverseDeterminant() const;
  
  /** 
   * Computes the determinent of the Jacobian 
   * transformation
   * 
   * @return @f$ \frac{b-1}{2} @f$
   */
  real_t determinant() const;
  /** 
   * Constructor
   * 
   * @param interval @f$ (a,b) @f$
   */
  SpectralConformTransformation(const Interval& interval);

  /** 
   * copy constructor
   * 
   * @param s a given spectral conform transformation
   */
  SpectralConformTransformation (const SpectralConformTransformation& s);

  /** 
   * Destructor
   * 
   */
  ~SpectralConformTransformation();
};

#endif // SPECTRAL_CONFORM_TRANSFORMATION_HPP
