//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_DERIVATIVE_HPP
#define SCALAR_FUNCTION_DERIVATIVE_HPP

#include <ScalarFunctionBase.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   ScalarFunctionDerivative.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:57:12 2006
 * 
 * @brief  scalar function derivative
 * 
 * @bug only implemented for discret functions
 */
class ScalarFunctionDerivative
  : public ScalarFunctionBase
{
public:
  enum Direction {
    x,
    y,
    z
  };

private:
  const Direction __direction;	/**< direction of derivation */
  ConstReferenceCounting<ScalarFunctionBase>
  __functionToDerive;	/**< function to derive */

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

public:
  /** 
   * Evaluates the derivative at point @f$ X @f$
   * 
   * @return @f$ \partial_i f(X) @f$
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param direction direction of derivation
   * @param functionToDerive the function to derive
   */
  ScalarFunctionDerivative(const ScalarFunctionDerivative::Direction& direction,
			   ConstReferenceCounting<ScalarFunctionBase> functionToDerive);

  /** 
   * Copy constructor
   * 
   * @param f the function to copy
   */
  ScalarFunctionDerivative(const ScalarFunctionDerivative& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionDerivative();
};

#endif // SCALAR_FUNCTION_DERIVATIVE_HPP
