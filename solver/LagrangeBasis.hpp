//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef LAGRANGE_BASIS_HPP
#define LAGRANGE_BASIS_HPP

#include <Types.hpp>
#include <Vector.hpp>

class LagrangeBasis
{
private:
  const size_t __degree;	/**< degree @f$ d @f$ of the basis */

  Vector<real_t> __nodes;
  Vector<real_t> __denominatorValues;
  
public:
  size_t degree() const
  {
    return __nodes.size()-1;
  }
  
  size_t dimension() const
  {
    return __nodes.size();
  }
  
  
  void getValues(const real_t& x,
		 Vector<real_t>& values) const;
  
  
  real_t getValue(const real_t& x,
		  const size_t& i) const;
  
  void getDerivativeValues(const real_t& x,
			   Vector<real_t>& values) const;
  
  
  real_t getDerivativeValue(const real_t& x,
			    const size_t& i) const;
  
  
  LagrangeBasis(const size_t& degree);

  LagrangeBasis(const Vector<real_t>& nodes);

  LagrangeBasis(const LagrangeBasis& lagrangeBasis);
  
  ~LagrangeBasis();
};
#endif // LAGRANGE_BASIS_HPP




