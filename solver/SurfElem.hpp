//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// This is the base class used to deal with Surfacic Elements

#ifndef SURF_ELEM_HPP
#define SURF_ELEM_HPP

#include <Cell.hpp>

#include <limits>

/**
 * @file   SurfElem.hpp
 * @author Stephane Del Pino
 * @date   Sat Oct 29 20:01:07 2005
 * 
 * @brief  This class is the base class for Surfacic Elements.
 * 
 * @note This is a very old class that definilty needs a lifting ;-)
 */

class SurfElem
  : public Cell
{
protected:
  /**
   * The mother cell (ie: a 3d element which interacts with the triangle)
   * 
   */
  const Cell* __motherCell;
  size_t __motherCellFaceNumber;

public:
  /**
   * Access to the mother cell
   * 
   */
  const Cell& mother() const
  {
    ASSERT(__motherCell!=0);
    return *__motherCell;
  }

  /**
   * Access to the mother cell face number
   * 
   */
  size_t motherCellFaceNumber() const
  {
    ASSERT(__motherCell!=0);
    return __motherCellFaceNumber;
  }

  /**
   * Sets the mother cell
   * 
   */
  void setMother(const Cell* cell,
		 const size_t faceNumber)
  {
    __motherCell = cell;
    __motherCellFaceNumber = faceNumber;
  }

  /** 
   * Operator equal
   * 
   * @param S 
   * 
   * @return *this
   */
  inline const SurfElem& operator = (const SurfElem& S)
  {
    Cell::operator=(S);
    __motherCell = S.__motherCell;
    __motherCellFaceNumber = S.__motherCellFaceNumber;

    return *this;
  }

  /** 
   *  Returns the normal to the face.
   * 
   * 
   * @return normal
   */
  virtual const TinyVector<3,real_t> normal() const = 0;

  SurfElem(const size_t& numberOfVertices,
	   const size_t& reference = 0)
    : Cell(numberOfVertices, reference),
      __motherCell(0),
      __motherCellFaceNumber(std::numeric_limits<size_t>::max())
  {
    ;
  }

  SurfElem(const SurfElem& s)
    : Cell(s),
      __motherCell(s.__motherCell),
      __motherCellFaceNumber(s.__motherCellFaceNumber)
  {
    ;
  }

  //! Virtual destructor
  virtual ~SurfElem()
  {
    ;
  }
};


#endif // SURF_ELEM_HPP
