//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FACES_SET_HPP
#define FACES_SET_HPP

#include <Vector.hpp>

template <typename FaceType>
class FacesSet
{
private:
  Vector<FaceType> __faces;
public:

  /**
   * Read-only access to the number of faces.
   * 
   * @return the number of faces of the seto
   */
  inline const size_t& numberOfFaces() const
  {
    return __faces.size();
  }

  /** 
   * Change the size of the face container.
   * 
   * @param size the new size of the face set
   *
   * @note all data are lost!
   */
  inline void setNumberOfFaces(const size_t& size)
  {
    __faces.resize(size);
  }

  /** 
   * Access to an face according to its number
   * 
   * @param i the face number
   * 
   * @return the ith faces
   */
  inline const FaceType& operator[](const size_t& i) const
  {
    return __faces[i];/// bounds are checked by the Vector class
  }

  /** 
   * Access to an face according to its number
   * 
   * @param i the face number
   * 
   * @return the ith faces
   */
  inline FaceType& operator[](const size_t& i)
  {
    return __faces[i];/// bounds are checked by the Vector class
  }

  /** 
   * Returns the number of a given face \a f
   * 
   * @param f the given face
   * 
   * @return its number in the faces set
   */
  size_t number(const FaceType& f) const
  {
    return __faces.number(f);
  }

  /** 
   * Constructs a FacesSet to a given size \a s
   * 
   * @param s the given size
   */
  FacesSet(const size_t& s)
    : __faces(s)
  {
    ;
  }

  /** 
   * Copies an EdgesSet
   * 
   * @param F the given FacesSet
   */
  FacesSet(const FacesSet& F)
    : __faces(F.__faces)
  {
    ;
  }
  /** 
   * Destructs the FacesSet
   * 
   */
  ~FacesSet()
  {
    ;
  }
};

#endif // FACES_SET_HPP
